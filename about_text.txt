ProViz is an interactive protein exploration tool, which searches several databases
for information about a given query protein. Data relevant to the protein like an
alignment of homologues, linear motifs, post translational modifications, domains,
secondary structure, sequence variations and others are graphically represented
relative to their position in the protein.<br><br>
