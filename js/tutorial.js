// Copyright (C) 2016 Norman E. Davey <normandavey@gmail.com>, Jean Manguy <jean.manguy@ucdconnect.ie> and Peter Jehl <peter.jehl@ucdconnect.ie>
// This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
// Public License as published by the Free Software Foundation; either version 2.1 of the License, or (at your option)
// any later version.
// 
// This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
// 
// You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
// the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Author contact: Norman E. Davey <normandavey@gmail.com> / Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
// Author contact: Jean Manguy <jean.manguy@ucdconnect.ie> / Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
// Author contact: Peter Jehl <peter.jehl@ucdconnect.ie> / Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.

var templateTour = "<div class='popover tour'><div class='arrow'></div><h3 class='popover-title'></h3><div class='popover-content'></div><div class='popover-navigation'><div class='btn-group'><button class='btn btn-sm btn-default' data-role='prev'>« Prev</button><button class='btn btn-sm btn-default' data-role='next'>Next »</button></div><button class='btn btn-sm btn-default' data-role='end'>End</button></div></div>";

var templateTourHomeToIndex = "<div class='popover tour'><div class='arrow'></div><h3 class='popover-title'></h3><div class='popover-content'></div><div class='popover-navigation'><div class='btn-group'><button class='btn btn-sm btn-default' data-role='prev'>« Prev</button><button class='btn btn-sm btn-default' data-role=''>Continue »</button></div><button class='btn btn-sm btn-default' data-role='end'>End</button></div></div>";


var tourHomeSteps = [
    {
        element: '.logo.fleft',
        title: "Welcome to ProViz",
        content: "During this quick guide we will present how to find a protein of interest and load the visualisation."
    },
    {
        element: '#searchTerm',
        title: "Search engine",
        content: '<p>The Search field is linked to the UniProt protein search engine. Use <a href="./index.php?page=help#protein_search">any term to search</a> for the protein in the UniParc database (e.g. Cyclin-dependent kinase inhibitor 1B)</p>',
        onShown: function () {
            "use strict";
            $('#searchTerm').val('Cyclin-dependent kinase inhibitor 1B');
        },
        placement: 'bottom'
    },
    {
        element: '#launch-search',
        title: "Start search",
        content: '<p>Here, We only present the use of the search engine but one could also upload a <a target="_blank" title="Help page - custom alignment" href="./index.php?page=help#custom_input">custom alignment</a> as input.</p>',
        placement: 'right',
        onShown: function () {
            "use strict";
            $('#form-search').submit();
        }
    },
    {
        element: '#searchResults',
        title: "Search results",
        content: '<p>After a small loading time ProViz will propose a list of protein matching your query.</p><p>A protein marked by <i style="color: rgb(0, 128, 0);" title="Reviewed UniProt entry" class="fa fa-check"></i> is a <a target="_blank" href="http://www.uniprot.org/help/entry_status" title="UniProt help page - entry status">reviewed protein</a>.</p>',
        placement: 'left'
    },
    {
        element: '#searchResults #P46527 .showProteinSearch',
        title: "Choose a protein",
        content: '<p>Once that you find your protein of interest you can access to the visualisation</p>',
        placement: 'right',
        reflex: true,
        onShow: function () {
            "use strict";
            $('#searchResults #P46527 .showProteinSearch').css('pointer-events', 'none');
        },
        onHidden: function () {
            "use strict";
            $('#searchResults #P46527 .showProteinSearch').css('pointer-events', '');
        }
    },
    {
        path: './proviz.php?uniprot_acc=P46527&tutorial=1'
    }
];


var tourProVizSteps = [
    /*{
     orphan: true,
     title: 'ProViz tutorial',
     content: '<p>During this guided tutorial we will present the main functionalities of ProViz.</p><p>A guide about the search engine is also available <a target="_blank" href="./index.php?page=home&tutorial=1" title="Load the search engine guide">here</a>.</p>'
     },
     {
     element: '#protein_name_container',
     title: 'name',
     placement: 'right',
     content: 'link to Uniprot...'
     },
     {
     element: '#architectureLine',
     title: 'Overview',
     placement: 'bottom',
     content: 'In this view the whole protein is displayed.'

     },*/
    /*{
     element: '#trackInfo',
     title: 'Tracks labels',
     placement: 'right',
     content: '...'

     },
     {
     element: '#trackData',
     title: 'Tracks',
     placement: 'top',
     content: '...'

     },*/

    /*{
     element: '#Options',
     title: 'options tab',
     placement: 'right',
     content: '...',
     onShow: function () {
     "use strict";
     $('#Options').click();
     }
     },
     {
     element: '#Proteins',
     title: 'proteins tab',
     placement: 'right',
     content: '...',
     onShow: function () {
     "use strict";
     $('#Proteins').click();
     }
     },
     {
     element: '#Features',
     title: 'features tab',
     placement: 'right',
     content: '...',
     onShow: function () {
     "use strict";
     $('#Features').click();
     }
     },
     {
     element: '#Help',
     title: 'help tab',
     placement: 'right',
     content: '...',
     onShow: function () {
     "use strict";
     $('#Help').click();
     }
     },
     {
     element: '#About',
     title: 'abouts tab',
     placement: 'right',
     content: '...',
     onShow: function () {
     "use strict";
     $('#About').click();
     }
     }*/
    /*{
     element: '#alignment_options_select',
     title: 'choose alignment source',
     placement: 'bottom',
        content: '...'
    },
     {
     element: '#clades_options_select',
     title: 'select clade',
     placement: 'bottom',
     content: 'Only with GeneTree'
     },
     {
     element: '.topOptionsContainer_button.show_gaps',
     title: 'Show gaps',
     placement: 'bottom',
     content: '...'
     },
     {
     element: '.topOptionsContainer_button#search',
     title: 'Search',
     placement: 'bottom',
     content: '...'
     },
     {
     element: '#search-regexp',
     title: 'Search',
     placement: 'bottom',
     content: '...',
     onShow: function () {
     "use strict";
     $("#searchContainer").show();
     $('.topOptionsContainer_button#search').addClass('topOptionsContainer_button_selected');
     },
     onHidden: function () {
     "use strict";
     $("#searchContainer").hide();
     $('.topOptionsContainer_button#search').removeClass('topOptionsContainer_button_selected');
     }
     },
     {
     element: '.topOptionsContainer_button.compactView',
     title: 'Compact view',
     placement: 'bottom',
     content: '...'
     },*/
    /*{
        element: '.topOptionsContainer_button#focus',
        title: 'Highlight',
        placement: 'bottom',
        content: '...'
    },
    {
        element: '#focusContainer .trigger_focus',
        title: 'Highlight',
        placement: 'bottom',
        content: '...',
        onShow: function () {
            "use strict";
            $("#focusContainer").show();
            $('#trackInfo #i_selector, #trackInfo #selector, #trackData #d_selector, #trackData #selector').show();
            PROVIZ.options.selector = true;
            $('.topOptionsContainer_button#focus').addClass('topOptionsContainer_button_selected');
        }
    },
    {
        element: '#focusContainer .trigger_resize',
        title: 'Resize',
        placement: 'bottom',
        content: '...'
    },
    {
        element: '#focusContainer .trigger_export',
        title: 'Export fasta',
        placement: 'bottom',
        content: '...',
        onHidden: function () {
            "use strict";
            $("#focusContainer").hide();
            $('#trackInfo #i_selector, #trackInfo #selector, #trackData #d_selector, #trackData #selector').hide();
            PROVIZ.options.selector = false;
            $('.topOptionsContainer_button#focus').removeClass('topOptionsContainer_button_selected');
        }
    },
    {
        element: '.topOptionsContainer_button#add_track',
        title: 'Custom tracks [advanced use]',
        placement: 'bottom',
        content: '...'
    },
    {
        element: '.topOptionsContainer_button.recolour',
        title: 'Recolour alignment',
        placement: 'bottom',
        content: '...'
    },
    {
        element: '.topOptionsContainer_button.reset_view',
        title: 'Reset',
        placement: 'bottom',
        content: '...'
    },
    {
        element: '.topOptionsContainer_button.download_pdf',
        title: 'Download PDF',
        placement: 'bottom',
        content: '...'
    },
    {
        element: '#archi-nav-left',
        title: 'Move',
        placement: 'right',
        content: 'Keyboard shortcut: &#8592;'
    },
    {
        element: '#archi-nav-right',
        title: 'Move',
        placement: 'left',
        content: 'Keyboard shortcut: &#8594;'
    },
    {
        element: '#architectureLine .ui-slider-handle',
        title: 'Move',
        placement: 'bottom',
        content: '...',
        onShow: function () {
            "use strict";
            PROVIZ.slider.panSmplSlider.slider('option', 'value', 20);
        }
     },*/
];

function loadTour(page) {
    "use strict";
    var steps = [];
    if (page == 'home') {
        steps = tourHomeSteps;
    } else if (page == 'proviz') {
        steps = tourProVizSteps;
    }

    return new Tour({
        steps: steps,
        storage: false,
        template: templateTour
    });
}

