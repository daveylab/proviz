/*global $:false, jQuery:false */

// Copyright (C) 2016 Norman E. Davey <normandavey@gmail.com>, Jean Manguy <jean.manguy@ucdconnect.ie> and Peter Jehl <peter.jehl@ucdconnect.ie>
// This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
// Public License as published by the Free Software Foundation; either version 2.1 of the License, or (at your option)
// any later version.
// 
// This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
// 
// You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
// the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Author contact: Norman E. Davey <normandavey@gmail.com> / Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
// Author contact: Jean Manguy <jean.manguy@ucdconnect.ie> / Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
// Author contact: Peter Jehl <peter.jehl@ucdconnect.ie> / Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.


var proviz_location = "./proviz.php";
// namespace
;
INDEX = {
    // main
    index: function () {
        $(".input_help").hide();


        $("#protein_load").click(function () {
            $("#searchTerm").val("Cyclin-dependent kinase inhibitor 1B");
        });

        $("#protein_clear").click(function () {
            $("#searchTerm").val("");
            $("#searchResults").text("");
        });


        $("#alignment_load").click(function () {
            $("#alignment_input").val(">CDN1B_HUMAN__P46527 Cyclin-dependent kinase inhibitor 1B; OS=Homo sapiens (Human). GN=CDKN1B PE=1 SV=1\n------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------MSNVRVSNGSPSLERMDA-------RQAE-HPKPSACRNLFG--PV---DHEELTRDLEKHCRDMEEASQRKWNFDFQNHKPLEG----KYEWQEVEKGSL-PEFYYRPPRP-PKGA--CKVP-A-QESQDVSGSRPAAPLIGAP--ANSEDTHLVDPKTDP-SDSQTGLA-EQCAGIRKRPATDD-----------------SSTQNKR-----ANRTEENVSDGSPN--AGSVEQ-TPKKPGLRRR------------------QT------------------------------------------------------------------------------------------------------------------------------\n>CDN1B_MOUSE__P46414 Cyclin-dependent kinase inhibitor 1B OS=Mus musculus GN=Cdkn1b PE=1 SV=2\n------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------MSNVRVSNGSPSLERMDA-------RQAE-HPKPSACRNLFG--PV---NHEELTRDLEKHCRDMEEASQRKWNFDFQNHKPLEG----RYEWQEVERGSL-PEFYYRPPRP-PKSA--CKVL-A-QESQDVSGSRQAVPLIGSQ--ANSEDRHLVDQMPDS-SDNPAGLA-EQCPGMRKRPAAED-----------------SSSQNKR-----ANRTEENVSDGSPN--AGTVEQ-TPKKPGLRRQ------------------T-------------------------------------------------------------------------------------------------------------------------------\n>tr_CHICK__Q8JIV2 Cyclin-dependent kinase inhibitor p27 Kip1 OS=Gallus gallus GN=CDKN1B PE=2 SV=1\n------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------MSNVRISNGSPTLERMEA-------RQSE-YPKPSACRNLFG--PV---NHEELNRDLKKHRKEMEEACQRKWNFDFQNHKPLEG----RYEWQAVEKGSS-PDFYFRQPRL-SKAV--CKSA-G-RQSLDVNGNCQTAICAASQ--GISEDTHCVGRKTDV-AGSQTDFA-EQCAGQRKRPAADD-----------------SSPQNKR-----ANTTEEEVSEDSPS--ASSVEQ-TPKKSSPRRH------------------QT------------------------------------------------------------------------------------------------------------------------------\n>tr_ORNAN__F6U4K2 Uncharacterized protein (Fragment) OS=Ornithorhynchus anatinus GN=CDKN1B PE=4 SV=1\n------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------IDNHE--KQRGSVERAWA-------WESE-WIQPSACRNLFG--PV---DHADVGRDLRRHCRDLAD-RRRRWNFDFRNHRPLEG----RYEWRPVDRDSL-PDFYRRPPPP-RRGP--RRGP-GE-EPGEEDRPRDRPQREEGD--GDEEGAHLAGRKTDA-SERQTAFP-DRCPAPRKRPAPDD-----------------SSPPNKR-----AHPTEEGVPGGAPG--TGPVEQ-TPEKPSLRRR------------------QT------------------------------------------------------------------------------------------------------------------------------\n>tr_DANRE__Q90YX4 Cyclin-dependent kinase inhibitor 1b (P27, kip1) OS=Danio rerio GN=cdkn1bb PE=2 SV=1\n------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------MSNVRLSNGSPTLERMEA-------RLSD-QPKPSACRNLFG--PV---DHEELKKDFQRQLRTMEDASAEAWNFDFSTHTPRAD---GRYQWEALDIRSV-PGFYSRSERGKGSDIHISSSGNNNDNNVDVNGNHDCRVTEQSA-----------------------ETP-ETLREPRKRSSCLD-----------------SSCQSKR-----SHICVDEVTRTPRK--PKKPR--KPLSPTPT----------------------------------------------------------------------------------------------------------------------------------------------------\n>tr_NEMVE__A7RYF3 Predicted protein OS=Nematostella vectensis GN=v1g203984 PE=4 SV=1\nMSLYHSQEPRVAEAGSISSLSSFSSDVVSAEESTRIHDGSSYEEEPTSSISEQEQDTSDDISSVSSHSITTVPRLSPDELKLLSRVCHLLRKRNEPLPIGHIGNDPELQELICKIHVTTFDSAEPFKWKTFLGKCNDTLDLSQRTLKGTQEWCVSLKECHKKRCPSTTSRLTDNSLDAHTSKDSNITEHAPVKLSKNQLRKLDSSLRNRLLSYLQTNSGEHALRDIVNQKCILKG--VQNWEVSVILE---SFVGVNFFEQLSENL---KVIMERGNSRNGVQRNLFGTAPA---DHSKTQDDLKREMEQLSKEKAKKWNFDFENLKPLPP---GRYKWERVGKRLQT---RESPPLDKTTT------------------ETSCSRT-------------------------------SEIVPSRKRPYNLRTRDVSARGTCLRPIEPPSSPRSKRW--------VEPFQTYKPD----------PKTG------------DKL------SLQKNLLDPS------------------------------------------------------------------------------------------------------------------------");
        });

        $("#alignment_clear").click(function () {
            $("#alignment_input").val("");
        });


        // fill the search input when user clicks on one example
        $('#examples_search li').click(function () {
            "use strict";
            $('#searchTerm').val($(this).text());
        });

        // tabs
        $('.tabs nav a').click(function (e) {
            "use strict";
            e.preventDefault();
            var that = $(this);
            var target = that.attr('href');
            that.parent().find('a').not(that).removeClass('tab-menu-active');
            that.addClass('tab-menu-active');
            that.parent().parent().find('.tab').removeClass('tab-active');
            that.parent().parent().find(target).addClass('tab-active');
        });


        $("#protein_example_load").click(function () {
            $("#searchResults").text("");
            $("#search_input_help").toggle();
        });

        $("#alignment_example_load").click(function () {
            $("#alignment_input_help").toggle();
            //$("#search_input_help").hide();
            //$("#file_input_help").hide();
        });

        $("#file_example_load").click(function () {
            $("#file_input_help").toggle();
            //$("#alignment_input_help").hide();
            //$("#search_input_help").hide();
        });

        /*
         $("#alignment_input").keypress(function(e){
         if(e.which === 13){
         var alignment = $("#alignment_input").val();

         if (alignment.length < 1)
         {
         $("#searchResults").html("Alignment/protein is too short. Please try another alignment/protein");
         }
         else
         {
         INDEX.createJobId(alignment.replace(/\n/g, "\\n"));
         }
         }
         });*/

        $("#alignment_file_input_button").change(function () {
            $(".input_error").hide();
            $(".input_help").hide();

            var file = $(":file")[0].files[0];
            if (file) {
                var reader = new FileReader();
                reader.readAsText(file);
                reader.onload = function (e) {
                    alignment = e.target.result;
                    if(INDEX.checkValidFasta(alignment)){
                        INDEX.createJobId(alignment);
                    } else {
                        $("#alignment_input_error").html("Alignment/protein is not in fasta format. Please try another alignment/protein");
                        $(".input_error").show();
                    }
                };
            }
        });

        INDEX.createVisitedList();
        //loadJoyrideHome();
    },
    searchTerm: function () {
        "use strict";
        var term = escapeHtml($("#searchTerm").val());
        $(".input_error, .input_help").hide();
        INDEX.getProteinSearchResults(term);
    },
    submitAlignmentTextarea: function () {
        $(".input_error").hide();
        $(".input_help").hide();

        var alignment = $("#alignment_input").val();
        if(INDEX.checkValidFasta(alignment)){
            INDEX.createJobId(alignment);
        } else {
            $("#alignment_input_error").html("Alignment/protein is not in fasta format. Please try another alignment/protein");
            $(".input_error").show();
        }
    },

    // COOKIE STUFF
    createVisitedList: function () {
        var containersVisited = $("#visited").empty();
        var cookies = Cookies.getJSON();
        console.log(cookies);
        var proteins = {};

        // only proteins' cookies
        $.each(cookies, function (id, cookie) {
            "use strict";
            if (stringStartsWith(id, 'proviz_p_')) {
                proteins[id] = cookie;
            }
        });

        var example = (Object.keys(proteins).length == 0 );

        // if new visitor: add an example protein
        if (example) {
            proteins['P46527'] = {
                time: null,
                url: "./proviz.php?uniprot_acc=P46527",
                protein_name: "Cyclin-dependent kinase inhibitor 1B",
                gene_name: "CDKN1B",
                species: "Homo sapiens"
            }
        }

        // transform object to array
        var arrProteins = [];
        $.each(proteins, function (id, protein) {
            "use strict";
            protein.id = id;
            arrProteins.push(protein);
        });


        // sort proteins by last visit
        function SortByTime(b, a){
            var aTime = a.time;
            var bTime = b.time;
            return ((aTime < bTime) ? -1 : ((aTime > bTime) ? 1 : 0));
        }

        arrProteins.sort(SortByTime);

        // build the list
        $.each(arrProteins, function (i, protein) {
            "use strict";
            if (!protein.hasOwnProperty('gene_name')) {
                return false;
            }
            var dateViewed = new Date(protein.time);
            var lastVisit = (example) ? "" : "<br>Last visit: " + dateViewed.toLocaleString();
            $('<div>')
                .appendTo(containersVisited)
                .addClass('visitedList')
                .attr('id', protein.id)
                .append(
                    $('<div>')
                        .addClass("visitedListDivs")
                        .append(
                            $('<a>')
                                .attr({
                                    id: protein.id,
                                    target: "_blank",
                                    href: protein.url,
                                    title: "Load \"" + protein.protein_name + "\"." + lastVisit
                                })
                                .addClass("showProteinCookie")
                                .append(
                                    $('<b>')
                                                .html(protein.protein_name + " (" + protein.gene_name + ")<br>")
                                        )
                                        .append(
                                            $('<small>')
                                                .text(protein.species)
                                        )
                        )
                )
                .append(
                    $('<div>')
                        .addClass("deleteCookieDiv")
                        .append(
                            $('<a>')
                                .addClass("deleteCookie")
                                .attr({
                                    id: protein.id
                                })
                                .append(
                                    $('<i>')
                                        .addClass("fa fa-times")
                                )
                        )
                )
            ;
        });

        // title
        containersVisited.prepend(
            $('<h4>')
                .html((example) ? "<i class='fa fa-compass'></i> Example" : '<i class="fa fa-floppy-o"></i> Recent')
        );

        if (!example) {
            containersVisited.append(
                $('<button>')
                    .text("reset")
                    .addClass("deleteAllCookie button")
                    .css({
                        "font-size": "0.95em"
                    })

            );
        }

        //listeners
        $(".deleteCookie").click(function () {
            var protein_acc = $(this).attr("id");
            INDEX.removeProteinCookie(protein_acc);
        });

        $(".deleteAllCookie").click(function () {
            INDEX.removeAllProteinCookies();
            $("#visited").hide();
        });


    },

    addProteinCookie: function (protein_acc, protein_name, protein_species) {
        Cookies.set(protein_acc, {"protein_name": protein_name, "protein_species": protein_species});
    },

    removeProteinCookie: function (protein_acc) {
        Cookies.remove(protein_acc);
        $("#" + protein_acc + ".visitedList").remove();
    },


    removeAllProteinCookies: function () {
        var cookies = Cookies.getJSON();

        $.each(cookies, function (id, cookie) {
            "use strict";
            if (stringStartsWith(id, 'proviz_p_')) {
                Cookies.remove(id);
                $("#" + id + ".visitedList").remove();
            }
        });
    },

    // SEARCH STUFF
    showSearchResults: function (searchTermResults) {
        var proteins = searchTermResults.split(/\r?\n/); // split every lines
        var colnames = proteins.shift().split(/\t/); // extract column names
        proteins.pop(); // remove the last line (empty)

        var resultsContainer = $("#searchResults");

        // treat each protein
        $.each(proteins, function (i, protein) {
            "use strict";

            // transform the tab delimited to an object
            protein = protein.split(/\t/).reduce(function (o, v, i) {
                o[colnames[i]] = v;
                return o;
            }, {});


            // adjustments
            protein.reviewed = (protein.Status == "reviewed");
            protein.name = protein['Protein names'].split('(')[0];


            // building of the list
            $('<div>')
                .addClass('searchList')
                .attr('id', protein.Entry)
                .appendTo(resultsContainer)

                .append(
                    $('<a>')
                        .text('view')
                        .attr({
                            href: proviz_location + "?uniprot_acc=" + protein.Entry,
                            target: '_blank',
                            title: 'Load "' + protein.name + '" in ProViz'
                        })
                        .addClass('showProteinSearch')
                )
                .append(
                    $('<i>')
                        .addClass((protein.reviewed) ? 'fa fa-check' : 'fa fa-times')
                        .attr('title', (protein.reviewed) ? 'Reviewed UniProt entry' : 'Unreviewed UniProt entry')
                        .css({
                            cursor: 'pointer',
                            color: (protein.reviewed) ? 'green' : '',
                            marginRight: '5px'
                        })
                )
                .append(
                    $('<a>')
                        .text(protein.name + " (" + protein['Gene names  (primary )'] + ")")
                        .attr({
                            href: proviz_location + "?uniprot_acc=" + protein.Entry,
                            target: '_blank',
                            title: 'Load "' + protein.name + '" in ProViz'
                        })
                )
                .append(
                    $('<small>')
                        .text(protein.Organism)
                )
                .append(
                    $("<a>")
                        .html('UniProt ' + protein.Entry + ' <i class="fa fa-external-link"></i>')
                        .attr({
                            href: "http://www.uniprot.org/uniprot/" + protein.Entry,
                            target: '_blank',
                            title: 'Go to UniProt'
                        })
                        .addClass('link_search_uniprot')
                )
            ;
        });

    },

    getProteinSearchResults: function (searchTerm) {
        "use strict";
        searchTerm = escapeHtml(searchTerm);
        if (searchTerm.length < 2) {
            $("#search_input_error").text("Search term \"" + searchTerm + "\" is too short. Please try another search term.").show();
            $("#searchResults").html("");
        }
        else {
            $("span#spinner").show().delay(10).queue(function (next) {
                var resultsContainer = $("#searchResults");
                resultsContainer.html("");

                searchTerm = encodeURIComponent(searchTerm);
                var uniprot_url = 'http://www.uniprot.org/uniprot/?query=(' + searchTerm + ')&format=tab&columns=id,protein%20names,organism,organism-id,reviewed,genes(PREFERRED)&sort=score&limit=25';
                if (navigator.sayswho === "MSIE 9" && window.XDomainRequest) {
                    // Use Microsoft XDR
                    var xdr = new XDomainRequest();
                    xdr.open("get", uniprot_url);
                    xdr.onload = function () {
                        INDEX.showSearchResults(xdr.responseText);
                    };
                    xdr.send();
                } else {
                    $.ajax({
                            type: "GET",
                            url: uniprot_url,
                            cache: true,
                            dataType: "text",
                            success: function (uniprot_data) {
                                "use strict";
                                if (uniprot_data.length > 0) {
                                    $("#searchResults").html("");
                                    INDEX.showSearchResults(uniprot_data);
                                } else {
                                    $("#search_input_error").text("No result for \"" + searchTerm + "\". Please try another search term.").show();
                                    $("#searchResults").html("");
                                }
                            },
                            error: function (error) {
                                $("#search_input_error")
                                    .html("Error downloading protein data:<br>")
                                    .append(JSON.stringify(error, null, 2) + "<br>")
                                    .append("Probably an issue with the UniProt search rest server. You can skip this step by getting the UniProt accession and using http://slim.ucd.ie/proviz/proviz.php?uniprot_acc=XXXXXX where XXXXXX is a UniProt accession e.g. P04637")
                                    .show();
                            },
                            complete: function () {
                                $("#spinner").hide();
                            }
                        }
                    );
                }
                next();
            });
        }
    },

    createJobId: function (alignment) {
        if (navigator.sayswho === "MSIE 9" && window.XDomainRequest) {
            // Use Microsoft XDR
            var xdr = new XDomainRequest();
            xdr.open("get", uniprot_url);
            xdr.onload = function () {
                INDEX.showSearchResults(xdr.responseText);
            };
            xdr.send();
        } else {
            $.ajax({
                type: "POST",
                url: "./createJob.php",
                dataType: "json",
                data: {"user_alignment": alignment},
                success: function (jobId) {
                    window.location = proviz_location + "?jobId=" + jobId;
                },
                error: function (error) {
                    $("#jobError")
                        .html("Error downloading job id:<br>")
                        .append(JSON.stringify(error, null, 2) + "<br>");
                }
            });
        }
    },

    checkValidFasta: function(alignment){
        var valid = true;
        if(!alignment.startsWith(">")){
            valid = false;
        } else {
            var fasta = alignment.trim().split("\n");
            var seqs = [];
            for(i = 0 ; i < fasta.length; i++){
                if(fasta[i].startsWith(">")){
                    seqs.push(fasta[i].trim());
                    seqs.push("");
                } else {
                    seqs[seqs.length-1] = seqs[seqs.length-1] + fasta[i].trim();
                }
            }
            var aliLength = seqs[1].length;
            for(i = 0 ; i < seqs.length; i = i + 2){
                if(!seqs[i].startsWith(">")){
                    valid = false;
                    break;
                } else if((seqs[i+1].length !== aliLength) || (!seqs[i+1].match(/^[A-Z\-]+$/))){
                    valid = false;
                    break;
                }
            }
        }
        return valid;
    }
};


navigator.sayswho = (function () {
    var ua = navigator.userAgent, tem,
        M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE ' + (tem[1] || '');
    }
    if (M[1] === 'Chrome') {
        tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
        if (tem != null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);
    return M.join(' ');
})();


var entityMap = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    '"': '&quot;',
    "'": '&#39;',
    "/": '&#x2F;'
};

function escapeHtml(string) {
    return String(string).replace(/[&<>"'\/]/g, function (s) {
        return entityMap[s];
    });
}

function stringStartsWith (string, prefix) {
    return string.slice(0, prefix.length) == prefix;
}
