/*global $:false, jQuery:false */

// namespace

// Copyright (C) 2016 Norman E. Davey <normandavey@gmail.com>, Jean Manguy <jean.manguy@ucdconnect.ie> and Peter Jehl <peter.jehl@ucdconnect.ie>
// This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
// Public License as published by the Free Software Foundation; either version 2.1 of the License, or (at your option)
// any later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
// the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Author contact: Norman E. Davey <normandavey@gmail.com> / Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
// Author contact: Jean Manguy <jean.manguy@ucdconnect.ie> / Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
// Author contact: Peter Jehl <peter.jehl@ucdconnect.ie> / Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.

;
PROVIZ = {
    // main
    protein_acc: "",
    options: {
        gapped: false,
        compact: false,
        search: false,
        selector: false,
        archi: true,
        focus: false,
        clades: false,
        hide_alignment_protein: false,
        max_visualisation_length: 5000,
        max_visible_alignment_size: 25000,
    },
    colours: ["#A9F5D0", "#F2F5A9", "#F7BE81", "#FFA1A1", "#A9E2F3", "#81F79F", "#CECEF6", "#ABDBEB", "#BDBDBD", "#58FAD0", "#A9F5D0", "#F2F5A9", "#F7BE81", "#FFA1A1", "#A9E2F3", "#81F79F", "#CECEF6", "#ABDBEB", "#BDBDBD", "#58FAD0", "#A9F5D0", "#F2F5A9", "#F7BE81", "#FFA1A1", "#A9E2F3", "#81F79F", "#CECEF6", "#ABDBEB", "#BDBDBD", "#58FAD0", "#A9F5D0", "#F2F5A9", "#F7BE81", "#FFA1A1", "#A9E2F3", "#81F79F", "#CECEF6", "#ABDBEB", "#BDBDBD", "#58FAD0", "#A9F5D0", "#F2F5A9", "#F7BE81", "#FFA1A1", "#A9E2F3", "#81F79F", "#CECEF6", "#ABDBEB", "#BDBDBD", "#58FAD0"],
    containers: {
        "conservations": ['conservation'],
        //"switch": ['modification switches'],
        "modification": ['modified residue', 'phospho.elm'],
        "motifs": ['elm', 'short sequence motif'],
        "structure": ['pfam', 'secondary structure', 'PDB', 'homology model'],
        "topology": ["transmembrane region", "topological domain"],
        "mutation": ['mutagenesis site'],
        "features": ['switches', 'peptide', 'chain', 'DNA-binding region', 'region of interest', 'metal ion-binding site', 'site', 'cross-link', "nucleotide phosphate-binding", "active site", "binding site"],
        "snps": ['sequence variant'],
        "isoform": ['splice variant'],
        "predictions": ['mobiDB', 'iupred', 'anchor', 'psiPred', 'elm_regex']
    },
    containers_basic: {
        //"conservations": ['conservation'],
        //"switch": ['modification switches'],
        "modification": ['modified residue', 'phospho.elm'],
        "motifs": ['elm', 'short sequence motif'],
        "structure": ['pfam', 'secondary structure'],
        "topology": ["transmembrane region", "topological domain"],
        "mutation": ['mutagenesis site'],
        //"features": ['switches', 'peptide', 'chain', 'DNA-binding region', 'region of interest', 'metal ion-binding site', 'site', 'cross-link', "nucleotide phosphate-binding", "active site", "binding site"],
        //"snps": ['sequence variant'],
        //"isoform": ['splice variant'],
        "predictions": ['mobiDB', 'iupred']//, 'anchor', 'psiPred', 'elm_regex']
    },
    loadData: function (parameters) {
        if (typeof parameters == 'undefined') {
            console.error('variable parameters needed');
            return false;
        }
        PROVIZ.parameters = parameters;
        PROVIZ.protein_acc = parameters.uniprot_acc;
        var jobid = parameters.jobId;

        if (PROVIZ.protein_acc !== null || jobid !== null) {
            $("#loadingInfo").append($('<span>').attr('id', 'loadAlign').html("<br>Retrieving data from web services.<br>Processing data for ProViz visualisation.<br><br><i>Please be patient. <br>Depending on the size of the protein and the amount of data available this can take a little while...</i>"));
            parameters.output = 'json';
            $.ajax({
                method: "GET",
                url: "./rest/proviz.php?" + $.param(parameters),
                dataType: "json",
                cache: true
            }).done(function (data) {
                try {
                    PROVIZ.data = data;
                    if (!PROVIZ.parameters.print) {
                        tmp = $('<p/>').html(data.alignmentData);
                        var homologue_count = tmp.find('.aliLine').length;
                        var visualisation_length = data.ali_end - data.ali_start;
                        var visible_alignment_size = homologue_count * visualisation_length;
                    }

                    //
                    //

                    if (!PROVIZ.parameters.visualisation_length_redirect) {
                        if (!PROVIZ.parameters.print) {
                            PROVIZ.createPageData(data);
                            if (cookie_compliance) {
                                PROVIZ.createCookie('proviz_p_' + PROVIZ.protein_acc, data);//Setup cookies
                            }
                            PROVIZ.createAlignmentOptions(data);//Setup the alignment links
                            PROVIZ.populateGeneTreeOptions(data);
                        }

                        PROVIZ.populateFeatures(data);// Add features
                        PROVIZ.populateAlignment(data);
                        PROVIZ.populateQuerySequence(data); // Add query sequence
                        PROVIZ.addCladeNumber(data);
                        PROVIZ.dictSequences = extractSequences();
                        PROVIZ.aas = PROVIZ.getAas();
                        PROVIZ.offsetGapped = PROVIZ.createOffset('gapped');
                        PROVIZ.offsetUngapped = PROVIZ.createOffset('ungapped');
                        PROVIZ.populateOffetBar(data);

                        var warning_text, warning_div;
                        var delay = 20;
                        if (visualisation_length > PROVIZ.options.max_visualisation_length) {
                            PROVIZ.parameters.visualisation_length_redirect = true;
                            warning_text = '<h3>Protein is longer than the maximum length for the ProViz tool</h3> \
							Due to issues with browser performance we need to limit the length of a ProViz visualisation to ' + PROVIZ.options.max_visualisation_length + ' residues. <br>\
							The currently selected visualisation is longer than this cut-off.<br>Please choose a region of the protein to visualise.<br><br>\
							\
							<b>Choose a region</b>\
							<label for="min">start</label> <input type="number" value="1" name="ali_start" id="min_resize" class="btn-proviz" required>\
							<label for="max">end</label> <input type="number" value="' + PROVIZ.options.max_visualisation_length + '" name="ali_end" id="max_resize" class="btn-proviz" required>';

                            warning_div = $('<div>')
                                .attr('id', 'visible_alignment_size')
                                .addClass('warning_header')
                                .html(warning_text);


                            $('<button>')
                                .addClass('btn-proviz big-btn trigger_resize')
                                .html('<i class="fa fa-arrows-h"></i> resize')
                                .on('click', function () {
                                    PROVIZ.resizeRedirect();
                                })
                                .appendTo(warning_div);

                            $('<button>')
                                .addClass('btn-proviz big-btn')
                                .html('<a href="./index.php">home</a>')
                                .appendTo(warning_div);

                            warning_div.prependTo("section");


                            $(".headerDataContainer .sidebarContainer, .headerNameContainer").hide();

                        } else if (visible_alignment_size > PROVIZ.options.max_visible_alignment_size) {
                            PROVIZ.options.hide_alignment_protein = true;


                            warning_text = '<h3><i class="fa fa-exclamation-triangle"></i> Alignment is larger than the maximum alignment size for the ProViz tool</h3> \
							Due to issues with browser performance we need to limit the number of amino acids in a ProViz alignment to ' + PROVIZ.options.max_visible_alignment_size + '. <br>\
							The current ProViz alignment is larger than this cut-off.\
							Consequently, we have removed the aligned proteins from the visualisation to improve the performance of the ProViz tools<br>\
							Aligned proteins can be added back to the visualisation using the <b>Protein</b> tab in the toolbar at the left of the visualisation<br>';

                            warning_div = $('<div>')
                                .attr('id', 'visible_alignment_size')
                                .addClass('warning_header')
                                .html(warning_text);

                            $('<button>')
                                .text('OK')
                                .addClass('btn-proviz')
                                .css("margin", "5px")
                                .on('click', function () {
                                    $(".warning_header").slideUp();
                                })
                                .appendTo(warning_div);

                            var models = $('#trackInfo #alignment .aliLine[data-model=1]');
                            if (models.length > 0) {
                                $('<button>')
                                    .html('Display sequences from model organisms (' + models.length + ')')
                                    .css("margin", "5px")
                                    .addClass('btn-proviz')
                                    .attr('data-tooltip', 'if sequences from model organisms are present in the alignment')
                                    .on('click', function () {
                                        "use strict";
                                        models.each(function (k, element) {
                                            var $element = $(element);
                                            var id = $element.attr('id').split('_')[1];
                                            setTimeout(function () {
                                                $element.show();
                                                $('#trackData #alignment #d_' + id).show();
                                                PROVIZ.updateCountHiddenProteins();
                                            }, delay * k)
                                        });
                                        setTimeout(function () {
                                            $(".warning_header").slideUp();
                                            PROVIZ.closeSidebar();
                                        }, (models.length) * delay);
                                    })
                                    .appendTo(warning_div);
                            }
                            prots = $('#trackInfo #alignment .aliLine').filter(':hidden');
                            $('<button>')
                                .html('<i class="fa fa-exclamation-triangle"></i> Display the whole alignment (' + prots.length + ')')
                                .css("margin", "5px")
                                .addClass('btn-proviz')
                                .on('click', function () {
                                    "use strict";
                                    prots.each(function (k, element) {
                                        var $element = $(element);
                                        var id = $element.attr('id').split('_')[1];
                                        setTimeout(function () {
                                            $element.show();
                                            $('#trackData #alignment #d_' + id).show();
                                            PROVIZ.updateCountHiddenProteins();
                                        }, delay * k);
                                    });
                                    setTimeout(function () {
                                        $(".warning_header").slideUp();
                                        PROVIZ.closeSidebar();
                                    }, (prots.length + 1) * delay);
                                })
                                .appendTo(warning_div);

                            warning_div.prependTo("body");

                            $('section').click(function () {
                                "use strict";
                                $(".warning_header").slideUp();
                            })
                        } else {
                            // display all sequences of the alignment
                            var prots = $('#trackInfo #alignment .aliLine').filter(':hidden');
                            prots.each(function (k, element) {
                                var $element = $(element);
                                var id = $element.attr('id').split('_')[1];
                                setTimeout(function () {
                                    $element.show();
                                    $('#trackData #alignment #d_' + id).show();
                                    PROVIZ.updateCountHiddenProteins();
                                }, delay * k);
                            });
                        }


                        if (!PROVIZ.parameters.print) {
                            PROVIZ.populateArchitecture(data);
                            PROVIZ.populateMeta(data.meta);
                            PROVIZ.populateFunction(data.function);
                        }

                        PROVIZ.slider = new Slider();
                        PROVIZ.resizeDataContainers(data);

                        if (PROVIZ.urlParam('rest_url') !== null) {
                            PROVIZ.loadRest(PROVIZ.urlParam('rest_url'));
                        }
                        if (!PROVIZ.parameters.print) {
                            if (PROVIZ.urlParam('tools') !== null) {
                                if (PROVIZ.urlParam('tools') === "degradation") {
                                    $(".tableContainer").html('<b>APC/C degron motif predictions</b><br><span class="loadingIcon"><i class="fa fa-cog fa-3 fa-spin"></i></span><br><span id="loadingInfo">Searching for APC/C degrons<br><br></span>');
                                    $.getScript("./js_tools/degradation.js").done(function (script, textStatus) {
                                        try {
                                            degradation.annotate(data);
                                        } catch (e) {
                                            $(".tableContainer").html("Failed to run degradation widget");
                                            console.error(e);
                                        }
                                    }).fail(function (jqxhr, settings, exception) {
                                        $(".tableContainer").html("Failed to run degradation widget");
                                        console.error(jqxhr, settings, exception);
                                    });

                                    }
                                    else if (PROVIZ.urlParam('tools') === "pp2a") {
                                        $(".tableContainer").html('<b>PP2A B56 motif predictions</b><br><span class="loadingIcon"><i class="fa fa-cog fa-3 fa-spin"></i></span><br><span id="loadingInfo">Searching for PP2A docking motifs<br><br></span>');
                                        $.getScript("./js_tools/pp2a.js").done(function (script, textStatus) {
                                            try {
                                                pp2a.annotate(data);
                                            } catch (e) {
                                                $(".tableContainer").html("Failed to run degradation widget");
                                                console.error(e);
                                            }
                                        }).fail(function (jqxhr, settings, exception) {
                                            $(".tableContainer").html("Failed to run degradation widget");
                                            console.error(jqxhr, settings, exception);
                                        });
                                    }
                                    else if (PROVIZ.urlParam('tools') === "lc8") {
                                        $(".tableContainer").html('<b>LC8 TQT motif predictions</b><br><span class="loadingIcon"><i class="fa fa-cog fa-3 fa-spin"></i></span><br><span id="loadingInfo">Searching for LC8 TQT motifs<br><br></span>');
                                        $.getScript("./js_tools/lc8.js").done(function (script, textStatus) {
                                            try {
                                                LC8.annotate(data);
                                            } catch (e) {
                                                $(".tableContainer").html("Failed to run degradation widget");
                                                console.error(e);
                                            }
                                        }).fail(function (jqxhr, settings, exception) {
                                            $(".tableContainer").html("Failed to run degradation widget");
                                            console.error(jqxhr, settings, exception);
                                        });
                                    }
                                    else if (PROVIZ.urlParam('tools') === "rbdmap") {
                                        $(".tableContainer").html('<b>RNA binding sites</b><br><span class="loadingIcon"><i class="fa fa-cog fa-3 fa-spin"></i></span><br><span id="loadingInfo">Searching for APC/C degrons<br><br></span>');

                                        $.getScript("./js_tools/rbdmap.js").done(function (script, textStatus) {
                                            try {
                                                rbdmap.annotate(data);
                                            } catch (e) {
                                                $(".tableContainer").html("Failed to run degradation widget");
                                                console.error(e);
                                            }
                                        }).fail(function (jqxhr, settings, exception) {
                                            $(".tableContainer").html("Failed to run degradation widget");
                                            console.error(jqxhr, settings, exception);
                                        });
                                    }
                                    else {
                                        $(".tableContainer").html('<b>' + PROVIZ.urlParam('tools') + '</b><br><span class="loadingIcon"><i class="fa fa-cog fa-3 fa-spin"></i></span><br><span id="loadingInfo">Searching for hits<br><br></span>');

                                        $.getScript("./js_tools/" + PROVIZ.urlParam('tools') + ".js").done(function (script, textStatus) {
                                            try {
                                                TOOL.annotate(data);
                                            } catch (e) {
                                                $(".tableContainer").html("Failed to run degradation widget");
                                                console.error(e);
                                            }
                                        }).fail(function (jqxhr, settings, exception) {
                                            $(".tableContainer").html("Failed to run degradation widget");
                                            console.error(jqxhr, settings, exception);
                                        });
                                    }


                            }
                        }
                    }

                } catch (e) {
                    console.error(e);
                    if (!PROVIZ.parameters.print) {
                        $("#loadingInfo").append("Failed<br>");
                    } else {
                        $('body').text('Failed to load ProViz');
                    }
                }


            }).fail(function () {
                if (!PROVIZ.parameters.print) {
                    $("#loadingInfo").html('ProViz failed to load your protein.<br><br><a class="topOptionsContainer_button" data-tooltip="back Home" href="./index.php">Back</a>').hide();
                } else {
                    $('body').text('Failed to load ProViz');
                }
            }).always(function () {

                if (!(typeof PROVIZ.data == "undefined")) {
                    PROVIZ.resizeVisualisation();
                    if (!PROVIZ.parameters.print && !PROVIZ.parameters.visualisation_length_redirect) {
                        PROVIZ.setupVisualisation();
                        PROVIZ.setupFastaDiv();
                        PROVIZ.createListeners();

                        setTimeout(function () {
                            "use strict";
                            PROVIZ.aas = PROVIZ.getAas();
                        }, 100);


                        // load tooltips
                        $(document).ready(function () {
                            PROVIZ.updateCountHiddenProteins();
                            $('#cookies').show();
                            "use strict";
                            // general
                            $(document).tooltip({
                                items: 'button, [data-tooltip], [data-pos], [data-indel]',
                                content: function () {
                                    "use strict";
                                    var element = $(this);
                                    var dataTooltip = element.data('tooltip');
                                    var dataPos = element.data('pos');
                                    if (typeof element.data('pos') == 'string') {
                                        var pos = element.data('pos').split(":");
                                        if (pos[0] == pos[1]) {
                                            dataPos = pos[0];
                                        } else {
                                            dataPos = element.data('pos');
                                        }
                                    }

                                    var dataIndel = element.data('indel');
                                    var dataHomologytype = element.parent().data('homologytype');
                                    var dataGenetreetaxon = element.parent().data('genetreetaxon');
                                    var text = [];
                                    text.push(dataPos);
                                    text.push(dataTooltip);
                                    text.push(dataIndel);
                                    if (dataHomologytype) {
                                        text.push('<br>homologytype: ' + dataHomologytype);
                                    }
                                    if (dataGenetreetaxon) {
                                        text.push('<br>Genetree taxon: ' + dataGenetreetaxon);
                                    }
                                    return $.trim(text.join(' '));
                                },
                                track: true,
                                html: true,
                                tooltipClass: "proviz-tooltip-jquery",
                                hide: false
                            });
                        });

                        PROVIZ.tutorial = loadTour('proviz');
                        PROVIZ.tutorial.init(true);
                        if (parameters.tutorial == 1) {
                            PROVIZ.tutorial.start(true);
                        }
                        $('footer').parent().show();
                    } else {
                        PROVIZ.loadInputs();
                        $('#trackInfo #selector, #trackData #selector').remove();
                    }
                    if (PROVIZ.parameters.visualisation_length_redirect) {
                        $(".loadingContainer").hide();
                        $(".visualisationContainer").hide();
                    }
                    document.readyState = 'proviz-complete';
                }
                //---
            });
        }
    },
    slider: {}
    ,
    dictSequences: []
    ,
    aas: {}
    ,
    offsetGapped: {}
    ,
    offsetUngapped: {}
    ,
    //-----
    waiting: function (handle) {
        if (typeof handle == "undefined") {
            return false;
        }
        var loadingC = $('.loadingContainer');
        loadingC.show().addClass("overlay").delay(10).queue(function (next) {
            try {
                handle.call();
            } catch (e) {
                console.error(e);
            }
            next();
            loadingC.hide().removeClass("overlay");
        });
    },
    loadInputs: function () {
        if (parameters.regexp) {
            colorRegex(parameters.regexp, PROVIZ.dictSequences);
        }
        if (parameters.condensed == 1) {
            PROVIZ.doCompact();
        }
        if (parameters.gapped == 1) {
            PROVIZ.doToggleGaps();
        }
        if (parameters.focus_start != null && parameters.focus_end) {
            PROVIZ.slider.focus(parameters.focus_start, parameters.focus_end);
        }
    },
    //-----
    createListeners: function () {
        if (!PROVIZ.parameters.print) {
            $(document).on('dragenter', function (e) {
                e.stopPropagation();
                e.preventDefault();
            });
            $(document).on('dragover', function (e) {
                e.stopPropagation();
                e.preventDefault();
            });
            $(document).on('drop', function (e) {
                e.stopPropagation();
                e.preventDefault();
            });

            $(document).keydown(function (event) {
                if (event.which === 38) {
                    event.preventDefault();
                    PROVIZ.keyboard_up();
                }

                if (event.which === 40) {
                    event.preventDefault();
                    PROVIZ.keyboard_down();
                }
            });
            $('.input').change(function () {
                $('#submit').attr('href', "proviz.php?uniprot_acc=" + $("#uniprot_acc").text() + "&ali_start=" + $("#start").val() + "&ali_end=" + $("#end").val());
            });
            $(".compactView").click(PROVIZ.compactView);
            $(".recolour").click(PROVIZ.recolourProteins);
            $(".show_architecture").click(PROVIZ.handleShowHideArchitecture);
            $(".download_pdf").click(PROVIZ.handleClickDownload);
            $('.reset_view').click(PROVIZ.reset);
            $("#alignment_options_select").on('change', function () {
                PROVIZ.changeAlignment($(this).val());
            });
            $("#clades_options_select").on('change', function () {
                PROVIZ.changeClades($(this).val());
            });
            $(".show_gaps").click(PROVIZ.toggleGaps);
            $(".show_clades").click(PROVIZ.toggleClades);
            $("#alignments").click(function () {
                $("#aliContainer").toggle();
            });
            $("#options").click(function () {
                $("#optContainer").toggle();
            });
            $("#tracks").click(function () {
                $("#trkContainer").toggle();
            });
            $("#duplications").click(function () {
                $("#duplicationContainer").toggle();
            });
            $("#showAllButton").click(function () {
                $(".container").show();
            });
            $("#hideAllButton").click(function () {
                $(".container").hide();
            });
            $('.trigger_resize').click(PROVIZ.resizeView);

            $(".topOptionsContainer_button").click(function (event) {
                PROVIZ.handleTopButtonsClick(this);
            });

            $(".select_protein").click(PROVIZ.handleSelectProtein);
            $(".select_feature").click(PROVIZ.handleSelectFeature);
            $(".hide_feature").click(PROVIZ.handleHideFeature);
            $(".hide_protein").click(function (event) {
                PROVIZ.hideProtein(this);
            });
            var $sidebarContainer = $('.sidebarContainer');

            $sidebarContainer.on('click', ".sidebar_feature_button", function () {
                PROVIZ.toggleSidebarFeature(this);
            }).on('click', ".sidebar_features_button", function () {
                PROVIZ.toggleSidebarFeatures(this);
            }).on('click', ".featureGroup_all", function () {
                PROVIZ.toggleSidebarFeaturesAll(this);
            });

            $sidebarContainer.on('click', ".show_all_protein", function () {
                PROVIZ.populateProteinSidebar(this);
            });
            $sidebarContainer.on('click', ".show_hidden_protein", function () {
                PROVIZ.showProtein(this);
            });

            $(".sidebarClose").click(function () {
                PROVIZ.closeSidebar();
            });
            $(".sidebar_tab").click(function () {
                PROVIZ.handleSidebarClick(this);
            });
            // add tracks via drop zone
            $('.drop_zone, .drop_zone input').on({
                dragenter: function (e) {
                    $(this).css('background-color', 'lightBlue');
                },
                dragleave: function (e) {
                    $(this).css('background-color', 'white');
                },
                drop: PROVIZ.handleDropFile,
                change: PROVIZ.handleDropFile
            });
            var handleResetSlider = function () {
                PROVIZ.slider.reset();
            };

            var handleFocusSlider = function () {
                PROVIZ.slider.focus($('#min').val(), $('#max').val());
            };

            $('.trigger_reset_slider').click(handleResetSlider);
            $('.trigger_focus').click(handleFocusSlider);
            $('body').click(PROVIZ.slider.handleCtrlClick);

            //Regexp
            $('#searchregexpbtn').click(handleSearchRegex);
            $('#reset-searchregexp').click(handleResetRegexp);
        }


        $(window).resize(PROVIZ.resizeVisualisation);
    },
    handleDropFile: function (e) {
        "use strict";
        var help = $('.drop_help_text');
        e.preventDefault();
        var f;
        if (typeof e.originalEvent.dataTransfer === "object") {
            f = e.originalEvent.dataTransfer.files[0];
        } else {
            if (typeof this.files === "object") {
                f = this.files[0];
            }
        }
        if (typeof f !== "undefined" && (f.type === "text/csv" || f.type === "text/xml" || f.type === "application/json")) {
            PROVIZ.readFile(f, function (e) {
                var tracks = [];
                var contents = e.target.result;
                if (f.name.endsWith("xml")) {
                    var xmlDoc = "";
                    if (window.DOMParser) {
                        var parser = new DOMParser();
                        xmlDoc = parser.parseFromString(contents, "text/xml");
                    } else { // Internet Explorer
                        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                        xmlDoc.async = false;
                        xmlDoc.loadXML(contents);
                    }
                    tracks = PROVIZ.parseXMLTrack(xmlDoc);
                } else if (f.name.endsWith("csv")) {
                    tracks = PROVIZ.parseCSVTrack(contents);
                } else if (f.name.endsWith("json")) {
                    tracks = JSON.parse(contents);
                    // compute len if missing
                    $.each(tracks, function (itrack, track) {
                        "use strict";
                        $.each(track.data, function (iel, el) {
                            if (typeof el.len == 'undefined') {
                                el.len = el.end - el.start + 1 || 1;
                            }
                        });
                    });
                }
                PROVIZ.displayTracks(tracks);
                help.text('file loaded!');
            });
        } else {
            if (typeof f !== "undefined") {
                help.text('Failed to load file!');
            }
        }
    },
    //-----
    //-----
    keyboard_delete: function () {
        var $selected_button = $(document).find('.fa-check-circle-o');
        var $current, current_id;
        if ($selected_button.hasClass("select_protein")) {
            $current = $($selected_button).closest('.aliLine');
            current_id = $current.attr("id").split("_")[1];
            $("#i_" + current_id).find(".aliLine").hide();
            $("#d_" + current_id).find(".aliLine").hide();
            var sidebarProteinContent = $(".sidebarProteinsContent");
            $('<span>')
                .addClass('show_hidden_protein')
                .attr('id', 'i_' + current_id)
                .html('Show ' + $current.closest(".aliLine").find(".ali_name").text() + '<br>')
                .appendTo(sidebarProteinContent);

            PROVIZ.updateCountHiddenProteins();
        }
        if ($selected_button.hasClass("select_feature")) {
            $current = $($selected_button).closest('.featureLine');
            current_id = $current.attr("id").split("_")[1];
            $("#i_" + current_id).find(".featureLine").hide().removeClass("featureLineVisible");
            $("#d_" + current_id).find(".featureLine").hide().removeClass("featureLineVisible");
            $("#b_" + current_id).find(".sidebar_feature_button").append($('<i>').addClass('fa fa-toggle-off'));
            PROVIZ.checkContainerVisibility($("#" + $current.attr("id")).find('.featureLine').closest(".container"));
        }
    },
    moveLine: function (up) {
        var movement = (up) ? 'insertBefore' : 'insertAfter';
        var prevNext = (up) ? 'prevAll' : 'nextAll';
        var $selected_button = $('.fa-check-circle-o');
        var $trackInfo = $("#trackInfo");
        var $trackData = $("#trackData");
        var $current, $previous, current_id, previous_id;
        var selectAliLine = '.aliLine';
        var selectContainer = '.container';
        var selectFeatLine = '.featureLine';
        var selectAlign = '#alignment, .container.clade';
        if ($selected_button.hasClass("select_protein")) {
            if ($selected_button.length === 1) {
                $current = $($selected_button).closest('.aliLine');
                $previous = $current[prevNext]('.aliLine:visible');
                current_id = $current.attr("id").split("_")[1];
                previous_id = $previous.attr("id").split("_")[1];

                if ($previous.length !== 0) {
                    var trackInfoAlignment = $trackInfo.find(selectAlign);
                    var trackDataAlignment = $trackData.find(selectAlign);
                    var infoCurrent = '#i_' + current_id + selectAliLine;
                    var infoPrevious = '#i_' + previous_id + selectAliLine;
                    var dataCurrent = '#d_' + current_id + selectAliLine;
                    var dataPrevious = '#d_' + previous_id + selectAliLine;
                    trackInfoAlignment.find(infoCurrent)[movement](trackInfoAlignment.find(infoPrevious));
                    trackInfoAlignment.find(dataCurrent)[movement](trackInfoAlignment.find(dataPrevious));
                    trackDataAlignment.find(infoCurrent)[movement](trackDataAlignment.find(infoPrevious));
                    trackDataAlignment.find(dataCurrent)[movement](trackDataAlignment.find(dataPrevious));
                }
            }
        }
        else {
            if ($selected_button.length === 1) {
                $current = $selected_button.closest(selectFeatLine);
                $previous = $current.prevAll('.featureLine:visible');
                current_id = $current.attr("id").split("_")[1];

                if (typeof $previous.attr("id") === 'undefined') {
                    $current = $selected_button.closest(selectContainer);
                    $previous = $current[prevNext]('.container:visible');

                    if ($previous.length !== 0) {
                        var idContainerCurrent = "#" + $current.attr("id") + selectContainer;
                        var idContainerPrevious = "#" + $previous.attr("id") + selectContainer;
                        $trackInfo.find(idContainerCurrent)[movement]($trackInfo.find(idContainerPrevious));
                        $trackData.find(idContainerCurrent)[movement]($trackData.find(idContainerPrevious));
                    }
                } else {
                    previous_id = $previous.attr("id").split("_")[1];
                    var infoLineCurrent = "#i_" + current_id + selectFeatLine;
                    var infoLinePrevious = "#i_" + previous_id + selectFeatLine;
                    var dataLineCurrent = "#d_" + current_id + selectFeatLine;
                    var dataLinePrevious = "#d_" + previous_id + selectFeatLine;
                    $trackInfo.find(infoLineCurrent)[movement]($trackInfo.find(infoLinePrevious));
                    $trackInfo.find(dataLineCurrent)[movement]($trackInfo.find(dataLinePrevious));
                    $trackData.find(infoLineCurrent)[movement]($trackData.find(infoLinePrevious));
                    $trackData.find(dataLineCurrent)[movement]($trackData.find(dataLinePrevious));
                }
            }
        }
    },
    moveLineUp: function () {
        PROVIZ.moveLine(true);
    },
    moveLineDown: function () {
        PROVIZ.moveLine(false);
    },
    keyboard_up: function () {
        var lineSelected = ($('.fa-check-circle-o').length > 0);

        if (lineSelected) {
            PROVIZ.moveLineUp();
        } else {
            PROVIZ.slider.moveUp();
        }

    },
    keyboard_down: function () {
        var lineSelected = ($('.fa-check-circle-o').length > 0);
        if (lineSelected) {
            PROVIZ.moveLineDown();
        } else {
            PROVIZ.slider.moveDown();
        }

    },
    //-----
    //-----
    createCookie: function (protein_acc, data) {
        var d = new Date();
        var time = d.getTime();

        Cookies.set(protein_acc, {
            "protein_name": data.protein_name,
            "gene_name": data.gene_name,
            "species": data.species,
            "url": window.location.href,
            "time": time
        }, {expires: 365});
        PROVIZ.tidyCookies();
    },
    tidyCookies: function () {
        var proteins = Cookies.getJSON();
        //for (var proteinAcc in proteins) {
        for (var j = Object.keys(proteins).length - 1; j >= 0; j--) {
            if (Object.keys(proteins).length - j > 6) {
                Cookies.remove(Object.keys(proteins)[j]);
            }
        }
    },
    //-----
    //-----
    setupVisualisation: function () {
        $(".sidebarContainer, .headerNameContainer").show();
        $('#loadAlign').remove();
        $(".loadingContainer").hide();

        if (PROVIZ.options.archi) {
            $('#architectureContainer').show();
            $('.show_architecture').addClass('topOptionsContainer_button_selected')
        } else {
            $('#architectureContainer').hide();
            $('.show_architecture').removeClass('topOptionsContainer_button_selected')
        }
        if (PROVIZ.options.search) {
            $('#searchContainer').show();
            $('#search').addClass('topOptionsContainer_button_selected')
        } else {
            $('#searchContainer').hide();
            $('#search').removeClass('topOptionsContainer_button_selected')
        }
        if (PROVIZ.options.focus) {
            $('#focusContainer').show();
            $('#focus').addClass('topOptionsContainer_button_selected')
        } else {
            $('#focusContainer').hide();
            $('#focus').removeClass('topOptionsContainer_button_selected')
        }
    },
    fastamodal: {
        fasta: null,
        linkDownload: null,
        linkPSSM: null,
        fastaBck: null,
        fastaBlock: null
    },
    handleCreateFastaModal: function () {
        // modal
        PROVIZ.fastamodal.fastaBlock = $('<div>')
            .appendTo($(document.body))
            .addClass('modal-content')
            .attr('id', 'fastaBlock');

        PROVIZ.fastamodal.fastaBck = $('<div>')
            .appendTo($(document.body))
            .addClass('modal-background')
            .attr('id', 'fasta-Bck')
            .on('click', function () {
                PROVIZ.fastamodal.fastaBlock.removeClass('active');
                PROVIZ.fastamodal.fastaBck.removeClass('active');
            });


        var fastaHeader = $('<div>')
            .appendTo(PROVIZ.fastamodal.fastaBlock)
            .css('width', '100%')
            .css('display', 'inline-block')
            .css('float', 'left');

        var fastaContent = $('<div>')
            .appendTo(PROVIZ.fastamodal.fastaBlock)
            .css('width', '100%')
            .css('overflow', 'hidden')
            .css('float', 'left');

        var fastaFooter = $('<div>')
            .appendTo(PROVIZ.fastamodal.fastaBlock)
            .css('vertical-align', 'bottom')
            .css('width', '100%')
            .css('float', 'left');

        $('<span>')
            .appendTo(fastaHeader)
            .css('display', 'inline')
            .html('<strong>Fasta<strong>').append($('<span>').attr('id', 'pos-fasta'));

        $('<span>')
            .appendTo(fastaHeader)
            .css('display', 'inline')
            .css('float', 'right')
            .addClass('modal-close')
            .html('<i class="fa fa-times"></i>')
            .on('click', function () {
                PROVIZ.fastamodal.fastaBlock.removeClass('active');
                PROVIZ.fastamodal.fastaBck.removeClass('active');
            });


        PROVIZ.fastamodal.linkDownload = $('<a>')
            .appendTo(fastaFooter)
            .prop('href', '#')
            .attr('id', 'link-download-fasta')
            .addClass('btn-proviz')
            .prop('download', 'align.fasta')
            .html('download');


        //PROVIZ.fastamodal.linkPSSM = $('<a>')
        //    .appendTo(fastaFooter)
        //    .prop('href', '#')
        //    .addClass('btn-proviz')
        //    .html('HMMER');


        PROVIZ.fastamodal.fasta = $('<textarea>')
            .appendTo(fastaContent)
            .prop('readonly', true)
            .attr('id', 'content-fasta')
            .addClass('fasta-textarea ')
            .on('click', function () {
                this.focus();
                this.select();
            });

        $('.trigger_export_all').click(PROVIZ.handleChangeFastaAll);
        $('.trigger_export').click(PROVIZ.handleChangeFasta);
    },
    handleChangeFasta: function () {
        "use strict";
        var start = parseInt($('#min').val()) - PROVIZ.data.ali_start - 1;
        var start_name = parseInt($('#min').val());
        var end = parseInt($('#max').val()) - PROVIZ.data.ali_start - 1;
        var end_name = parseInt($('#max').val());
        var name = PROVIZ.data.protein_acc;
        var visibles = getVisibleSequenceLines();
        var isGapped = PROVIZ.options.gapped;
        var output = createFasta(PROVIZ.dictSequences, isGapped, start, end, visibles);
        PROVIZ.fastamodal.fasta.val(output);
        $('#pos-fasta').html(' from: ' + start_name + ' to: ' + end_name);
        PROVIZ.fastamodal.linkDownload
            .prop('download', 'alignment_' + name + '_' + start_name + '_' + end_name + '.fasta')
            .prop('href', "data:text/plain;charset=UTF-8," + encodeURIComponent(output));
        PROVIZ.fastamodal.fastaBlock.addClass('active');
        PROVIZ.fastamodal.fastaBck.addClass('active');
        location.href = '#fastaBlock';
    },
    handleChangeFastaAll: function () {
        "use strict";
        var start = 0;
        var start_name = PROVIZ.data.ali_end;
        var end = PROVIZ.data.ali_end - PROVIZ.data.ali_start - 1;
        var end_name = PROVIZ.data.ali_end;
        var name = PROVIZ.data.protein_acc;
        var visibles = getVisibleSequenceLines();
        var isGapped = PROVIZ.options.gapped;
        var output = createFasta(PROVIZ.dictSequences, isGapped, start, end, visibles);
        PROVIZ.fastamodal.fasta.val(output);
        $('#pos-fasta').html(' from: ' + start_name + ' to: ' + end_name);
        PROVIZ.fastamodal.linkDownload
            .prop('download', 'alignment_' + name + '_' + start_name + '_' + end_name + '.fasta')
            .prop('href', "data:text/plain;charset=UTF-8," + encodeURIComponent(output));
        PROVIZ.fastamodal.fastaBlock.addClass('active');
        PROVIZ.fastamodal.fastaBck.addClass('active');
        location.href = '#fastaBlock';
    }
    ,
    setupFastaDiv: function () {
        setTimeout(PROVIZ.handleCreateFastaModal, 200);
    },
    /*createTitle: function () {
        return 'ProViz' + ' - ' + PROVIZ.data.protein_name + ' - ' + PROVIZ.data.gene_name + ' - ' + PROVIZ.data.protein_acc;
    },*/
    createPageData: function () {
        //Change document title
        var protein_acc = PROVIZ.data.protein_acc;
        if (protein_acc != null) {
            /*if (typeof PROVIZ.data.gene_name !== 'undefined' && PROVIZ.data.gene_name !== '') {
                var title = PROVIZ.createTitle();
                document.title = title;
            }*/

            //Setup some variables for the javascript
            $("#uniprot_acc")
                .attr("data-length", PROVIZ.data.protein_length)
                .html(protein_acc);

            //Add name and species - Add link to uniprot
            $('#protein-name')
                .text(PROVIZ.data.protein_name + ' (' + PROVIZ.data.gene_name + ')');
            $('#link-to-uniprot')
                .attr("href", "http://www.uniprot.org/uniprot/" + protein_acc);
            $('#protein-species')
                .text(PROVIZ.data.species);
        } else if (typeof parameters.jobId != 'undefined' && parameters.jobId != '') {
            $('#protein-name').text('job: ' + parameters.jobId);
            $('#link-to-uniprot').remove();
            $('#protein-species').remove();
        }

    }
    ,
    createAlignmentOptions: function (data) {
        if (data.available_alignments.length > 0) {
            $.each(data.available_alignments, function (i, align) {
                $("#alignment_options_select").append(
                    $('<option>')
                        .val(data.available_alignments_id[i])
                        .text(align)
                );
            });
            if (data.available_alignments.length > 0) {
                $(".alignment_options_picker_count").html(data.available_alignments.length);
            }
            else {
                $(".alignment_options_picker, .show_gaps, .recolour").hide();
            }
        } else {
            $("#alignment_options_select").remove();
            $(".alignment_options_picker").remove();
        }
    }
    ,
    populateAlignment: function (data) {
        if (data.alignmentInfo != undefined) {
            var container_html_info = $('<div>')
                .addClass('container')
                .attr('id', 'alignment')
                .css('width', '204px')
                .html(data.alignmentInfo);

            container_html_info.find('.aliLine').hide();

            if (PROVIZ.options.hide_alignment_protein) {
                container_html_info.find('.aliLine').hide();
            }

            $("#trackInfo").prepend(container_html_info);

            var container_html_data = $('<div>')
                .addClass('container')
                .attr('id', 'alignment')
                .html(data.alignmentData);

            container_html_data.find('.aliLine').hide();


            $("#trackData").prepend(container_html_data);

            if (PROVIZ.options.hide_alignment_protein) {
                PROVIZ.populateProteinSidebar();
            }
        }
    },
    addCladeNumber: function (data) {
        "use strict";
        if (data.available_alignments != null) {
            var genetree_mode = PROVIZ.urlParam('genetree_mode');
            if (data.available_alignments[0] == "GeneTree") {
                if (genetree_mode == "paralog" || genetree_mode == "all") {
                    var $aliTypes = $("#trackInfo").find("#alignment, #sequence").find(".aliLine");
                    var count = 0;
                    $($aliTypes).each(function () {
                        count++;
                        var clade = $(this).find(".aliType").data("cladenr");
                        var paralogClade = $('<div>')
                            .addClass('cladeNumber')
                            .attr('data-tooltip', 'clade #' + clade)
                            .css({
                                background: PROVIZ.colours[clade]
                            })
                            .text(clade);
                        $(this).find(".featureHelp").append(paralogClade);
                    });
                }
            }
        }
    },
    splitAlignment: function () {
        PROVIZ.waiting(function () {
            var containers = {};
            var clades = [];
            $("#trackData #alignment").hide();
            var aliTypes = $("#trackInfo").find("#alignment").find(".aliLine");
            $(aliTypes).each(function (i, line) {
                var protein = $(line).find(".aliType");
                var id = $(line).attr("id");
                var id_number = id.split("_")[1];
                var clade = protein.data('cladenr');
                if (containers[clade] == undefined) {
                    containers[clade] = [];
                    clades.push(clade);
                }
                containers[clade].push(id_number);
            });
            $.each(clades.reverse(), function (i, clade) {
                "use strict";
                PROVIZ.containeriseClade(clade, containers[clade])
            });
            //$("#alignment").show();
        });
    }
    ,
    unsplitAlignment: function () {
        PROVIZ.waiting(function () {
            var $trackInfo = $("#trackInfo");
            var $trackData = $("#trackData");
            var $aliTypes = $trackInfo.find(".clade").find(".aliLine");
            var count = 0;
            $(".clade").hide();
            var containers = {};
            containers["alignment"] = [];

            var cladeInfo = [];
            var cladeData = [];
            $($aliTypes).each(function () {
                var id = $(this).attr("id");
                var id_number = id.split("_")[1];
                var infoLine = $("#i_" + id_number + ".aliLine");
                var dataLine = $("#d_" + id_number + ".aliLine");
                cladeInfo.push(infoLine);
                cladeData.push(dataLine);
            });
            $trackInfo.find("#alignment").append(cladeInfo);
            $trackData.find("#alignment").append(cladeData);


            $(".clade").remove();

            $aliTypes = $trackInfo.find("#alignment").find(".container").find(".aliLine");
            count = 0;
            $($aliTypes).each(function () {
                count++;
                $(this).find(".aliType").data("clade", Math.floor(count / 15));
            });
            $trackData.find("#alignment").show();
        });
    },
    containeriseClade: function (cladeName, ids) {
        var cladeInfo = [];
        var cladeData = [];

        $.each(ids, function (i, id) {
            "use strict";
            var infoLine = $("#i_" + id + ".aliLine");
            var dataLine = $("#d_" + id + ".aliLine");
            cladeInfo.push(infoLine);
            cladeData.push(dataLine);
        });

        var container_html_info = $('<div>')
            .addClass('container clade')
            .attr('id', 'clade' + cladeName)
            .css('width', '204px')
            .append(cladeInfo);

        var container_html_data = $('<div>')
            .addClass('container clade')
            .attr('id', 'clade' + cladeName)
            .append(cladeData);


        var ali_start = parseInt(PROVIZ.urlParam('ali_start')) || 1;
        var ali_end = parseInt(PROVIZ.urlParam('ali_end')) || $("#uniprot_acc").data().length;
        var container_length = (ali_end - ali_start + 1) * 10;

        container_html_data.width(container_length + 'px');

        $("#trackInfo").find("#sequence").after(container_html_info);
        var $trackData = $("#trackData");
        $trackData.find("#sequence").after(container_html_data);
    },
    populateQuerySequence: function (data) {
        var sequence_info = $('<div>')
            .addClass('container')
            .attr('id', "sequence")
            .css('width', '204px')
            .html(data.alignmentInfoQuery);
        var sequence_data = PROVIZ.newRow("sequence", "", $(data.alignmentDataQuery));
        $("#trackInfo").prepend(sequence_info.show());
        $("#trackData").prepend(sequence_data.data.show());
    }
    ,
    populateFeatures: function (data) {
        $.each(PROVIZ.containers, function (i, container) {
            "use strict";
            var container_info_html = '';
            var container_data_html = '';


            $.each(container, function (j, trackname) {
                var track = data.tracks[trackname];
                if (typeof track != 'undefined' && track[0] != "") {
                    container_info_html += track[0];
                    container_data_html += track[1];
                }
            });

            if (container_info_html.length > 0) {
                var container_info = $('<div>')
                    .addClass('container')
                    .attr('id', i)
                    .css('width', '204px')
                    .html(container_info_html);

                var container_data = $('<div>')
                    .addClass('container')
                    .attr('id', i)
                    .html(container_data_html);

                $("#trackInfo").append(container_info);
                $("#trackData").append(container_data);
            }
        });
    }
    ,
    getAas: function () {
        "use strict";
        var sequence = $('#trackData').find('#sequence');
        var aas = {};
        aas.gapped = sequence.find('.aC');
        aas.ungapped = aas.gapped.filter(':not(.gap)');
        return aas;
    },
    populateOffetBar: function (data) {
        // Add query sequence
        var offset_info_html = $('<div>')
            .addClass('container')
            .attr('id', 'offset')
            .css('width', '204px')
            .html(data.offsetInfo);

        var offset_data_html = PROVIZ.newRow("offset", "", PROVIZ.offsetUngapped, 14);
        $("#trackInfo").prepend(offset_info_html);
        $("#trackData").prepend(offset_data_html.data.show());
        // add div for the focus
        $('<div>').addClass('slider-selection-bg slider-align').insertAfter($('.offsetData')).hide();
    },
    createOffset: function (type, nstep) {
        "use strict";
        type = type || 'ungapped';
        nstep = nstep || 25;
        var offset = $('<div>').addClass('offsetData').css('height', '14px');
        var nAA = PROVIZ.dictSequences[0]['length' + type];
        var start = PROVIZ.data.ali_start;
        var end = PROVIZ.data.ali_end;
        var firstAAnogap = 0;
        try {

            $.each(PROVIZ.dictSequences[0].positions, function (i, position) {
                if (position.ungapped == 1) {
                    firstAAnogap = i;
                    return false;
                }
            });

            var firstDivisiblebynstep = start - (start % nstep) + nstep;
            var lastDivisiblebynstep = end - (end % nstep);

            var posFirstStep = 0;
            var posLastStep = end - start;
            $.each(PROVIZ.dictSequences[0].positions, function (i, position) {
                if (position.ungapped == firstDivisiblebynstep - start) {
                    posFirstStep = i;
                    return false;
                }
            });

            $.each(PROVIZ.dictSequences[0].positions, function (i, position) {
                if (position.ungapped == lastDivisiblebynstep - start) {
                    posLastStep = i;
                    return false;
                }
            });

            var insideSteps = [];
            for (var i = posFirstStep; i <= posLastStep; i = i + nstep) {
                insideSteps.push(i + start);
            }

            // start extremity
            if (insideSteps[0] - start >= 10) {
                insideSteps.unshift(start + 1);
            }

            // first and intermediates
            $.each(insideSteps, function (n, value) {
                var positionAA = 0;
                $.each(PROVIZ.dictSequences[0].positions, function (i, position) {
                    if (position.ungapped == value - start) {
                        positionAA = parseInt(PROVIZ.dictSequences[0].positions[i][type]) - 1;
                        return false;
                    }
                });
                if (n < 1 || end - value >= 10) {
                    var percentMargin = positionAA / nAA * 100;
                    var step = $('<div>')
                        .addClass('oC offsetRight')
                        .css('margin-left', percentMargin + '%')
                        .attr('data-offset', value)
                        .append($('<i>').addClass('fa fa-long-arrow-down'));
                    step.appendTo(offset);
                }
            });

            // end extremity
            var step = $('<div>')
                .addClass('oC offsetLeft')
                .css('right', '0')
                .attr('data-offset', end)
                .append($('<i>').addClass('fa fa-long-arrow-down'));
            step.appendTo(offset);

        } catch (e) {
            console.error(e);
        }
        return offset;
    },
    populateArchitecture: function (data) {
        //Add architecture
        var container_html = $('<div>')
            .attr('id', 'architectureLine')
            .addClass('slider-pan-container')
            .html(data.architecture);
        var previous_button = $('<button>')
            .addClass('btn-proviz')
            .attr('id', 'archi-nav-left')
            .attr('data-tooltip', 'move left (shortcut: &#8592;)')
            .addClass('archi-nav')
            .html('<i class="fa fa-arrow-left"></i>');
        var next_button = $('<button>')
            .addClass('btn-proviz')
            .attr('id', 'archi-nav-right')
            .attr('data-tooltip', 'move right (shortcut: &#8594;)')
            .addClass('archi-nav')
            .html('<i class="fa fa-arrow-right"></i>');

        $("#architectureContainer").append(previous_button).append(next_button).append(container_html);
    }
    ,
    resizeDataContainers: function (data, widthUnit) {
        widthUnit = widthUnit || 10;

        // var ali_start = parseInt(PROVIZ.urlParam('ali_start')) || 1;
        // var ali_end = parseInt(PROVIZ.urlParam('ali_end')) || data.protein_length;

        //if (ali_start != null) {
        var container_length = PROVIZ.data.protein_length * widthUnit;
        /*}
         else {
         container_length = data.protein_length * 10;
         }*/
        $("#trackData").find(".container").width(container_length + "px");
    }
    ,
    //-----
    //-----
    handleSidebarClick: function (clicked_element) {
        $(".sidebar_text").text("");
        $(".sidebarOptionsContent").hide();
        $(".sidebarProteinsContent").hide();

        var $id = $(clicked_element).attr("id");

        if ($(clicked_element).hasClass("open")) {
            PROVIZ.closeSidebar();
            return false;
        } else {
            $(".sidebar_tab_selected")
                .addClass("sidebar_tab")
                .removeClass("sidebar_tab_selected");

            $(".sidebar_tab").removeClass("open");

            $(clicked_element)
                .addClass("open")
                .removeClass("sidebar_tab")
                .addClass("sidebar_tab_selected");

            $(".sidebar_title").text($id);
            $(".sidebarContainer").animate({left: "0px"});

            switch ($id) {
                case "Options":
                    PROVIZ.populateOptionsSidebar();
                    break;
                case "Proteins":
                    PROVIZ.populateProteinSidebar();
                    break;
                case "Features":
                    PROVIZ.populateFeatureSidebar();
                    break;
                case "About":
                    PROVIZ.populateAboutSidebar();
                    break;
                case "Help":
                    PROVIZ.populateHelpSidebar();
                    break;
            }
        }


    },
    handleTopButtonsClick: function (clicked_element) {
        PROVIZ.closeSidebar();
        clicked_element = $(clicked_element);
        var $id = clicked_element.attr("id");
        var classSelect = "topOptionsContainer_button_selected";
        var toOpen = !(clicked_element.hasClass(classSelect));
        var addOrRemoveClass = (toOpen) ? 'addClass' : 'removeClass';
        var showOrHide = (toOpen) ? 'show' : 'hide';
        // topOptionsContainer_button_selected is handled differently for the toggle functions
        try {
            switch ($id) {
                case "search":
                    $("#searchContainer")[showOrHide]();
                    clicked_element[addOrRemoveClass](classSelect);
                    break;
                case "focus":
                    $("#focusContainer")[showOrHide]();
                    $('#trackInfo #i_selector, #trackInfo #selector, #trackData #d_selector, #trackData #selector')[showOrHide]();
                    PROVIZ.options.selector = toOpen;
                    clicked_element[addOrRemoveClass](classSelect);
                    break;
                case "add_track":
                    $(".drop_zone")[showOrHide]();
                    clicked_element[addOrRemoveClass](classSelect);
                    break;
            }
        } catch (e) {
        }
    },
    handleShowHideArchitecture: function () {
        "use strict";
        var toOpen = !PROVIZ.options.archi;
        var addOrRemoveClass = (toOpen) ? 'addClass' : 'removeClass';
        var showOrHide = (toOpen) ? 'show' : 'hide';
        $("#architectureContainer")[showOrHide]();
        $(".show_architecture")[addOrRemoveClass]('topOptionsContainer_button_selected');
        PROVIZ.options.archi = !PROVIZ.options.archi;
    },
    gatherParams: function () {
        var params = {};
        if (parameters.uniprot_acc) {
            params.uniprot_acc = parameters.uniprot_acc;
        }
        if (parameters.jobId) {
            params.jobId = parameters.jobId;
        }
        var toDisable = [];

        var visibleAlign = $('#alignment .ali_name').filter(':visible');
        if (visibleAlign.length == 0) {
            params.alignment = 1;
            toDisable.push('alignment');
        } else {
            var alignToShow = [];
            $.each(visibleAlign, function (i, align) {
                var proteinId = $(align).data('proteinId');
                if (proteinId && proteinId != '') {
                    alignToShow.push($(align).attr('data-proteinId'))
                }
            });
            if (alignToShow.length > 0) {
                params.showAln = alignToShow.join(',');
            }
            params.alignment = PROVIZ.data.settings.alignment;
            if (params.alignment == "GeneTree") {
                params.genetree_mode = PROVIZ.data.settings.geneTreeMode;
            }
        }


        if (PROVIZ.options.gapped) {
            params.gapped = 1;
            params.disable_features = 1;
        } else {
            params.gapped = 0;
            var featuresToDisable = $('#trackInfo .container .featureLine').filter(':hidden').not('#i_selector');
            if (featuresToDisable.length == 0) {
                params.disable_features = 0;
            } else {
                $.each(featuresToDisable, function (i, feature) {
                    toDisable.push($(feature).attr('data-disable'));
                });
                var disabled = PROVIZ.data.settings.disable;
                $.each(disabled, function (feature, value) {
                    if (value == 1) {
                        toDisable.push(feature);
                    }
                });
                if (toDisable.length > 0) {
                    params.disable = toDisable.join(',');
                }
            }
        }



        var searchRegexp = $('#search-regexp').val();
        if (searchRegexp.length > 0 && PROVIZ.options.search) {
            params.regexp = encodeURI(searchRegexp);
        }

        params.ali_start = Math.max(PROVIZ.data.ali_start + 1, 1);
        params.ali_end = PROVIZ.data.ali_end;
        params.condensed = (PROVIZ.options.compact) ? 1 : 0;

        if (PROVIZ.options.focus) {
            params.focus_start = $(PROVIZ.slider.inputMin).val();
            params.focus_end = $(PROVIZ.slider.inputMax).val();
        }
        return params;
    },
    handleClickDownload: function () {
        "use strict";
        var params = PROVIZ.gatherParams();
        var size_data = $('#trackData #offset').width();
        params.w = parseInt(size_data) + 208;
        params.h = parseInt($('.featureContainer').height()) + 35;
        var url = './php/create_pdf.php?' + $.param(params);
        $(this).attr('href', url).attr('download', PROVIZ.protein_acc + '.pdf');
    },
    handleShowHideLegend: function () {
        "use strict";
        var toOpen = !PROVIZ.options.legend;
        var showOrHide = (toOpen) ? 'show' : 'hide';
        $("#trackInfo")[showOrHide]();
        PROVIZ.options.legend = !PROVIZ.options.legend;
    },
    populateFeatureSidebar: function () {
        var $containers = $("#trackData").find(".container");
        var div_showhideall = $('<div>')
            .addClass('show_features_all_container');

        $('<button>')
            .addClass('featureGroup_all btn-proviz')
            .attr('id', 'show')
            .text('show all')
            .appendTo(div_showhideall);

        $('<button>')
            .addClass('featureGroup_all btn-proviz')
            .attr('id', 'hide')
            .text('hide all')
            .css('float', 'right')
            .appendTo(div_showhideall);

        var featuresContainer = $('<div>')
            .addClass('sidebarFeaturesContent');

        // title groups / features
        var featuresTitle = $('<div>')
            .addClass('featureBlock')
            .appendTo(featuresContainer);

        $('<div>')
            .addClass('featureColumn featureBlockTitle fcLeft')
            .text('Groups')
            .appendTo(featuresTitle);

        $('<div>')
            .addClass('featureColumn featureBlockTitle fcRight')
            .text('Features')
            .appendTo(featuresTitle);

        // features groups
        $containers.each(function () {
            "use strict";
            var that = $(this);
            var name = (that.attr('data-name') != undefined) ? that.attr('data-name') : that.attr('id');
            var groupVisible = that.is(":visible");
            var groupToggleOnOff = (groupVisible) ? 'fa-toggle-on' : 'fa-toggle-off';
            var featuresTypes = that.find('.featureLine');

            // feature block
            var block = $('<div>')
                .addClass('featureBlock')
                .appendTo(featuresContainer);

            var groupToggle = $('<a>')
                .addClass('sidebar_features_button')
                .attr('id', name)
                .append($('<i>').addClass('fa ' + groupToggleOnOff));

            $('<div>')
                .addClass('featureColumn fcLeft')
                .text(name + ' ')
                .append(groupToggle)
                .appendTo(block);

            // features
            if (featuresTypes.length > 0 && name != 'selector' && name != 'sequence') {
                var features = $('<div>')
                    .addClass('featureColumn fcRight sidebar_features')
                    .appendTo(block);

                featuresTypes.each(function () {
                    var thatFeature = $(this);
                    var featureVisible = thatFeature.is(":visible");
                    var featureToggleOnOff = (featureVisible) ? 'fa-toggle-on' : 'fa-toggle-off';
                    var nameFeature = (thatFeature.attr('data-name') != undefined) ? thatFeature.attr('data-name') : thatFeature.attr('id');
                    var feature_id = nameFeature.split("_")[1];

                    var featureToggle = $('<a>')
                        .addClass('sidebar_feature_button')
                        .attr('id', 'b_' + feature_id)
                        .append($('<i>').addClass('fa ' + featureToggleOnOff));

                    var name = $('#trackInfo').find('#' + that.attr('id')).find('#i_' + thatFeature.attr('id').split('_')[1]).text();
                    $('<div>')
                        .addClass('')
                        .text(' ' + name)
                        .prepend(featureToggle)
                        .appendTo(features);
                });
            }

        });
        var $sidebarText = $(".sidebar_text");
        $sidebarText.append(div_showhideall);
        $sidebarText.append(featuresContainer);
    }
    ,
    populateProteinSidebar: function (clicked_element) {
        clicked_element = $(clicked_element);
        var button_choice = clicked_element.attr("id");
        var prots, delay = 20;
        if (button_choice == "hide" || button_choice == "show") {
            if (button_choice === "hide") {
                prots = $('#trackInfo #alignment .aliLine').filter(':visible');
            } else {
                prots = $('#trackInfo #alignment .aliLine').filter(':hidden');
            }
            prots.each(function (k, element) {
                var $element = $(element);
                var id = $element.attr('id').split('_')[1];
                setTimeout(function () {
                    $element[button_choice]();
                    $('#trackData #alignment #d_' + id)[button_choice]();
                    PROVIZ.updateCountHiddenProteins();
                }, delay * k);
            });
            setTimeout(PROVIZ.populateProteinSidebar, 5 + prots.length * delay);
            return false;
        }
        var $sidebarText = $(".sidebar_text").empty();


        var $trackInfo = $('#trackInfo');
        var proteins = $trackInfo.find('.aliLine');
        var proteinsVisible = $trackInfo.find('.aliLine:visible');
        var proteinsHidden = $trackInfo.find('.aliLine:hidden');


        var div_showhideall = $('<div>').addClass("show_features_all_container");

        $sidebarText.append(div_showhideall);

        $('<button>')
            .addClass('show_all_protein btn-proviz')
            .attr('id', 'show_models')
            .text('show models only')
            .appendTo(div_showhideall)
            .click(function () {
                var prots = $('#trackInfo #alignment .aliLine');
                var model = prots.filter('[data-model=1]').filter(':hidden');
                var non_model = prots.filter('[data-model=0]').filter(':visible');
                var delay = 20;
                model.each(function (k, element) {
                    var $element = $(element);
                    var id = $element.attr('id').split('_')[1];
                    setTimeout(function () {
                        $element.show();
                        $('#trackData #alignment #d_' + id).show();
                        PROVIZ.updateCountHiddenProteins();
                    }, delay * k);
                });
                non_model.each(function (k, element) {
                    var $element = $(element);
                    var id = $element.attr('id').split('_')[1];
                    setTimeout(function () {
                        $element.hide();
                        $('#trackData #alignment #d_' + id).hide();
                        PROVIZ.updateCountHiddenProteins();
                    }, delay * k);
                });
                setTimeout(PROVIZ.populateProteinSidebar, 10 + Math.max(model.length, non_model.length) * delay);
            });

        $('<button>')
            .addClass('show_all_protein btn-proviz')
            .attr('id', 'show')
            .text('show all')
            .appendTo(div_showhideall);

        $('<button>')
            .addClass('show_all_protein btn-proviz')
            .attr('id', 'hide')
            .text('hide all')
            .appendTo(div_showhideall);

        var proteinsContainer = $('<div>');

        //var title = $('<h4>').text('Hidden proteins from the alignment').appendTo(proteinsContainer);

        var modelOrganisms = $('<div>').appendTo(proteinsContainer);
        var titleModels = $('<h6>')
            .appendTo(modelOrganisms)
            .addClass('show_all_category')
            .html('Model organisms')
            .append(
                $('<i>').addClass('fa fa-arrow-right').css('margin-left', '10px')
            );
        var modelsProteins = $('<div>').addClass('container-hidden-proteins').appendTo(modelOrganisms);
        var no_model = $('<div>').appendTo(proteinsContainer);
        var titleNoModels = $('<h6>')
            .appendTo(no_model)
            .addClass('show_all_category')
            .html('<br>Non-model organisms')
            .append(
                $('<i>').addClass('fa fa-arrow-right').css('margin-left', '10px')
            );

        var no_modelsProteins = $('<div>').addClass('container-hidden-proteins').appendTo(no_model);

        proteinsHidden.each(function () {
            "use strict";
            var thatprotein = $(this);
            var prot = $('<div>')
                .addClass('show_hidden_protein')
                .attr('id', thatprotein.attr('id'))
                .attr('data-tooltip', 'show ' + thatprotein.text())
                .text(thatprotein.text())
                .append($('<i>').addClass('fa fa-arrow-right').css('margin-left', '10px'));

            if (thatprotein.data('model') == '1') {
                prot.appendTo(modelsProteins);
            } else {
                prot.appendTo(no_modelsProteins);
            }
        });

        titleModels.click(function () {
            "use strict";
            var prots = $('#trackInfo #alignment .aliLine[data-model=1]').filter(':hidden');
            var delay = 20;
            prots.each(function (k, element) {
                var $element = $(element);
                var id = $element.attr('id').split('_')[1];
                setTimeout(function () {
                    $element.show();
                    $('#trackData #alignment #d_' + id).show();
                    PROVIZ.updateCountHiddenProteins();
                }, delay * k);
            });
            setTimeout(PROVIZ.populateProteinSidebar, 5 + prots.length * delay);
        });

        titleNoModels.click(function () {
            "use strict";
            var prots = $('#trackInfo #alignment .aliLine[data-model=0]').filter(':hidden');
            var delay = 20;
            prots.each(function (k, element) {
                var $element = $(element);
                var id = $element.attr('id').split('_')[1];
                setTimeout(function () {
                    $element.show();
                    $('#trackData #alignment #d_' + id).show();
                    PROVIZ.updateCountHiddenProteins();
                }, delay * k);
            });
            setTimeout(PROVIZ.populateProteinSidebar, 5 + prots.length * delay);
        });

        $sidebarText.append(proteinsContainer);
    },
    populateOptionsSidebar: function () {
        var content_options = $('<div>')
            .css({
                width: '100%'
            });

        var miscOptions = $('<div>')
            .append($('<h3>').text('Session').addClass('options_title'))
            .append($('<a>').attr('href', './index.php').css('text-decoration', 'none').append(PROVIZ.createSidebarOption('New session', 'home_button', 'fa-times home_button')))
            .append(PROVIZ.createSidebarOption('Reset', 'reset_view', 'fa-refresh'))
            .append($('<div>')
                .text('Download PDF')
                .addClass('optionSidebar')
                .append(
                    $('<a>')
                        .addClass('topOptionsContainer_button options_toggle download_pdf')
                        .attr('href', '#')
                        .append(
                            $('<i>').addClass('fa fa-file-pdf-o')
                        )
                )
            );
        content_options.append(miscOptions);

        var alignOptions = $('<div>')
            .append(PROVIZ.createSidebarOption('Compact the view', 'compactView', 'fa-star', PROVIZ.options.compact))
            ;
        var alignTitle = "";
        if ($('#alignment').length > 0) {
            alignTitle = "Alignment";
            alignOptions
                .append(PROVIZ.createSidebarOption('Show/hide gaps', 'show_gaps', 'fa-indent', PROVIZ.options.gapped))
                .append(PROVIZ.createSidebarOption('Recolour alignment', 'recolour', 'fa-paint-brush'));
        } else {
            alignTitle = "Sequence";
        }
        alignOptions
            .prepend($('<h3>').text(alignTitle).addClass('options_title'));
        content_options.append(alignOptions);

        var archiOptions = $('<div>')
            .append($('<h3>').text('Architecture').addClass('options_title'))
            .append(PROVIZ.createSidebarOption('Show/hide architecture', 'show_architecture', 'fa-info-circle', PROVIZ.options.archi))
        /*.append($('<div>').css({height: '50px', border: '1px solid #8e8e8e'}).text('Legend... TODO'))*/;
        content_options.append(archiOptions);

        var selectOptions = $('<div>')
            .append($('<h3>').text('Selection').addClass('options_title'))
            .append(PROVIZ.createSidebarOption('Slider', 'show_slider', 'fa-expand', PROVIZ.options.selector))
            .append($('#min').clone().attr('id', 'minSidebar'))
            .append($('#max').clone().attr('id', 'maxSidebar'))
            .append(PROVIZ.createSidebarOption('Focus (highlight a region of interest)', 'trigger_focus', 'fa-eye', PROVIZ.options.focus))
            .append(PROVIZ.createSidebarOption('Resize', 'trigger_resize', 'fa-arrows-h'))
            .append(PROVIZ.createSidebarOption('Reset', 'reset_focus', 'fa-eye-slash'))
            ;
        if (PROVIZ.urlParam('genetree_mode') == 'paralog' || PROVIZ.urlParam('genetree_mode') == 'all') {
            selectOptions.append(PROVIZ.createSidebarOption('Split clades', 'trigger_clades', 'fa-chain-broken'))
        }
        content_options.append(selectOptions);

        var newTrackOptions = $('<div>')
            .append(
                $('<h3>')
                    .text('Add new tracks ')
                    .addClass('options_title')
                    .append(
                        $('<i>').addClass('fa fa-question-circle').attr('data-tooltip', 'see Help > use of custom data')
                    )
            )
            .append(
                $('<div>')
                    .addClass('drop_zone')
                    .text('Drop or choose a file here!')
                    .append(
                        $('<input>').attr('type', 'file')
                    )
                    .append(
                        $('<div>').addClass('drop_help_text')
                    )
                    .show()
            );
        content_options.append(newTrackOptions);

        var downloads = $('<div>')
            .append($('<h3>').text('Download').addClass('options_title'))
            .append(PROVIZ.createSidebarOption('Export selection to fasta', 'trigger_export', 'fa-file-text'))
            .append(PROVIZ.createSidebarOption('Export all to fasta', 'trigger_export_all', 'fa-file-text'))
            ;
        content_options.append(downloads);


        // fill the panel
        $(".sidebar_text").empty().append(content_options);


        // listeners
        $(".show_gaps").unbind('click', PROVIZ.toggleGaps).click(PROVIZ.toggleGaps);
        $(".compactView").unbind('click', PROVIZ.compactView).click(PROVIZ.compactView);
        $(".recolour").unbind('click', PROVIZ.recolourProteins).click(PROVIZ.recolourProteins);
        $(".show_architecture").unbind('click', PROVIZ.handleShowHideArchitecture).click(PROVIZ.handleShowHideArchitecture);
        $(".download_pdf").unbind('click', PROVIZ.handleClickDownload).click(PROVIZ.handleClickDownload);

        $('.drop_zone, .drop_zone input', ".sidebar_text").on({
            dragenter: function (e) {
                $(this).css('background-color', 'lightBlue');
            },
            dragleave: function (e) {
                $(this).css('background-color', 'white');
            },
            drop: PROVIZ.handleDropFile,
            change: PROVIZ.handleDropFile
        });

        $('.reset_view').click(PROVIZ.reset);
        $('.show_slider').click(function () {
            "use strict";
            showHideElProviz('selector', !PROVIZ.options.selector);
            showHideElProviz('d_selector', !PROVIZ.options.selector);
            showHideElProviz('i_selector', !PROVIZ.options.selector);
            $('.show_slider')[(!PROVIZ.options.selector) ? 'addClass' : 'removeClass']('topOptionsContainer_button_selected');
            PROVIZ.options.selector = !PROVIZ.options.selector;
        });
        $('.sidebar_text .trigger_focus').click(function () {
            "use strict";
            var classSelected = 'topOptionsContainer_button_selected';
            var start = $('#minSidebar').val();
            var end = $('#maxSidebar').val();
            if (start == PROVIZ.slider.checkMin && end == PROVIZ.slider.checkMax) {
                PROVIZ.slider.reset();
            } else {
                PROVIZ.slider.checkMin = start;
                PROVIZ.slider.checkMax = end;
                PROVIZ.slider.inputMin.val(start);
                PROVIZ.slider.inputMax.val(end);
                PROVIZ.slider.inputs.change();
                PROVIZ.slider.focus(start, end);
                $('.trigger_focus').addClass(classSelected)
            }
        });
        $('.reset_focus').click(function () {
            "use strict";
            PROVIZ.slider.reset();
        });
        $('.trigger_export').unbind('click', PROVIZ.handleChangeFasta).click(PROVIZ.handleChangeFasta);
        $('.trigger_export_all').unbind(PROVIZ.handleChangeFastaAll).click(PROVIZ.handleChangeFastaAll);
        $('.trigger_resize').unbind('click', PROVIZ.resizeView).click(PROVIZ.resizeView);
        $('.trigger_clades').unbind('click', PROVIZ.toggleClades).click(PROVIZ.toggleClades);
        $('.trigger_reset_slider').unbind().click(function () {
            "use strict";
            PROVIZ.slider.reset();
        })


    },
    createSidebarOption: function (text, classname, iconname, state) {
        "use strict";
        var selected = (state) ? 'topOptionsContainer_button_selected' : '';
        return $('<div>')
            .text(text)
            .addClass('optionSidebar')
            .append(
                $('<div>')
                    .addClass('topOptionsContainer_button options_toggle ' + classname + ' ' + selected)
                    .append(
                        $('<i>').addClass('fa ' + iconname)
                    )
            );
    },
    populateHelpSidebar: function () {
        var $sidebarText = $(".sidebar_text");
        $sidebarText
            .append(
                $('<span>').html('More help is available in the <a style="text-decoration: underline" href="./index.php?page=help">"Help"</a> page.<br><br>')
            )
        //.append($('<span>').html('<a class="btn-proviz" href="#">Start tutorial</a>'))
        ;
        $.get("./help_text.txt", function (data) {
            $sidebarText
                .append(data);
            $.each($(".headline"), function () {
                var type = $(this).attr("id").split("_")[0];
                $("#" + type + "_title").click(function () {
                    $("#" + type + "_text").toggle();
                    $("#" + type + "_title").children("i").toggle();
                });
            });
        });

    }
    ,
    populateAboutSidebar: function () {
        var sidebar_text = $(".sidebar_text");
        sidebar_text.append($('<h4>').text('ProViz'));
        $.get("./about_text.txt", function (data) {
            sidebar_text.append(data);
        }).then(function () {
            sidebar_text.append($('<h4>').text('Reference'));
            sidebar_text.append($('<p>').html('<a target="_blank" href="https://dx.doi.org/10.1093/nar/gkw265" title="ProViz\'s article in Nucleic Acid Research" style="font-family: Helvetica; color: #4179B5; text-decoration:none; font:Helvetica Neue;">"ProViz&nbsp;—&nbsp;a web-based visualization tool to investigate the functional and evolutionary features of protein sequences."</a><br>Peter Jehl, Jean Manguy, Denis C. Shields, Desmond G. Higgins, and Norman E. Davey<br><small>Nucl. Acids Res. <i>April 16, 2016</i> doi:10.1093/nar/gkw265</small>'));

            sidebar_text.append($('<h4>').text('Licence'));
            sidebar_text.append($('<p>').html('ProViz is distributed under the <a href="index.php?page=about#licence">GPLv3 licence</a>'));

            sidebar_text.append($('<h4>').text('EULA'));
            sidebar_text.append($('<p>').html('The ProViz tool is available for use according to the <a href="index.php?page=eula">ProViz end-user license agreement (EULA).</a>'));

        });
    }
    ,
    populateGeneTreeOptions: function (data) {
        var $showClades = $(".show_clades");
        if (data.available_alignments !== null) {
            if (data.available_alignments[0] === "GeneTree") {
                var genetree_mode = PROVIZ.urlParam('genetree_mode');

                var optortholog = $('<option>').val('ortholog').text('Orthologues');
                var optparalog = $('<option>').val('paralog').text('Paralogues');
                var optall = $('<option>').val('all').text('All (slow)');

                var $cladesOptionsSelect = $("#clades_options_select");

                switch (genetree_mode) {
                    case 'ortholog':
                        $cladesOptionsSelect
                            .append(optortholog.prop('selected', true))
                            .append(optparalog)
                            .append(optall);
                        $showClades.remove();
                        break;
                    case 'paralog':
                        $cladesOptionsSelect
                            .append(optortholog)
                            .append(optparalog.prop('selected', true))
                            .append(optall);
                        break;
                    case 'all':
                        $cladesOptionsSelect
                            .append(optortholog)
                            .append(optparalog)
                            .append(optall.prop('selected', true));
                        break;
                    default:
                        $cladesOptionsSelect
                            .append(optortholog.prop('selected', true))
                            .append(optparalog)
                            .append(optall);
                        $showClades.remove();
                        break;
                }
            } else {
                $(".clades_options_picker").remove();
                $showClades.remove();
            }
        } else {
            $(".clades_options_picker").remove();
            $showClades.remove();
        }
    },
    populateMeta: function (meta) {
        "use strict";
        if (meta != null) {
            $("#sequence").find('.ali_name')
                .attr({
                    "data-tooltip": "<b>UniProt info</b><br>sequence version: " + meta.version + "<br>downloaded: " + meta.modified + "<br>dataset: " + meta.dataset
                })
                .append(' <i class="fa fa-clock-o"></i>');
        }
    },
    populateFunction: function (func) {
        "use strict";
        $('#info_protein_function').text(func);
    }
    ,
    showHideInformation: function (lineclass, buttonid) {
        var target = $(lineclass);
        var button = $(buttonid);
        if (target.css("visibility") === "visible") {
            target.css("visibility", "hidden");
            target.css("height", "0px");
            button.css("background-color", "#FEFEFE");
        } else {
            target.css("height", "");
            target.css("visibility", "visible");
            button.css("background-color", "#DCDCDC");
        }
    }
    ,
    displayTracks: function (tracks) {
        var ali_start = parseInt(PROVIZ.urlParam('ali_start')) || 1;
        var ali_end = parseInt(PROVIZ.urlParam('ali_end')) || $("#uniprot_acc").data().length;
        var diff = 0;


        // Restraining ali_start and ali_end
        $.each(tracks, function (itrack, track) {
            if (track.type == "feature" || track.type == "peptides") {
                track.data = $.grep(track.data, function (el) {
                    return el.start < ali_end && el.end > ali_start;
                });
                $.each(track.data, function (iel, el) {
                    if (track.type == "peptides") {
                        if (el.start < ali_start) {
                            diff = ali_start - el.start;
                            el.len = el.len - diff;
                        }
                        if (el.end > ali_end) {
                            diff = el.end - ali_end;
                            el.len = el.len - diff;
                            el.text = el.text.substring(0, el.text.length - diff);
                        }
                    }
                    el.start = Math.max(el.start, ali_start);
                    el.end = Math.min(el.end, ali_end);
                });
            } else {
                track.data = $.grep(track.data, function (el) {
                    return el.position <= ali_end && el.position >= ali_start;
                });
            }
        });

        /*for (var i = 0; i < tracks.length; i++) {
         for (var j = 0; j < tracks[i].data.length; j++) {
         if (tracks[i].type === "feature" || tracks[i].type === "peptides") {
         if ((parseInt(tracks[i].data[j].end) < ali_start) || (parseInt(tracks[i].data[j].start) > ali_end)) {
         tracks[i].data.splice(j, 1);
         j--;
         } else {
         if (parseInt(tracks[i].data[j].start) < ali_start) {
         tracks[i].data[j].start = ali_start;
         tracks[i].data[j].len = tracks[i].data[j].end - tracks[i].data[j].start + 1;
         }
         if (parseInt(tracks[i].data[j].end) > ali_end) {
         tracks[i].data[j].end = ali_end;
         tracks[i].data[j].len = tracks[i].data[j].end - tracks[i].data[j].start + 1;
         }
         }
         } else {
         if ((parseInt(tracks[i].data[j].position) < ali_start) || (parseInt(tracks[i].data[j].position) > ali_end)) {
         tracks[i].data.splice(j, 1);
         j--;
         }
         }
         }
         }
         */

        // Layering
        for (var i = 0; i < tracks.length; i++) {
            if (tracks[i].type === "feature" || tracks[i].type === "peptides") {
                tracks[i].data = PROVIZ.layering(tracks[i].data);
            }
        }

        // Printing
        PROVIZ.printTracks(tracks);
    },
    handleSelectProtein: function (event) {
        $(".featureType").removeClass('ali_name_selected');
        $(".select_feature").removeClass('fa-check-circle-o').addClass('fa-circle-o');

        var $addChecked = $(this).hasClass("fa-circle-o");

        $(".select_protein").removeClass('fa-check-circle-o').addClass('fa-circle-o');

        //var $current = $(this).closest(".aliLine").find(".ali_name");
        var $current_id = $(this).closest(".aliLine").attr("id").split("_")[1];

        $(".aliLine").removeClass('ali_name_selected').removeClass('ali_selected');
        $(".featureLine").removeClass('ali_name_selected').removeClass('ali_selected');

        if ($addChecked) {
            $(this).removeClass('fa-circle-o').addClass("fa-check-circle-o");
            $("#i_" + $current_id + ".aliLine").addClass('ali_name_selected');
            $("#d_" + $current_id + ".aliLine").addClass('ali_selected');
        }
    }
    ,
    handleSelectFeature: function (event) {
        $(".ali_name").removeClass('ali_name_selected');
        var $selectProtein = $(".select_protein");
        $selectProtein.removeClass('fa-check-circle-o').addClass('fa-circle-o');

        var $addChecked = $(this).hasClass("fa-circle-o");

        var select_feature = ".select_feature";
        $(select_feature).removeClass('fa-check-circle-o').addClass('fa-circle-o');

        var featureLine = ".featureLine";
        //var $current = $(this).closest(featureLine).find(".featureType");
        var $current_id = $(this).closest(featureLine).attr('id').split("_")[1];

        $(featureLine).removeClass('ali_name_selected').removeClass('ali_selected');
        var aliLine = ".aliLine";
        $(aliLine).removeClass('ali_name_selected').removeClass('ali_selected');

        $(".featureType").removeClass('ali_name_selected');
        if ($addChecked) {
            $(this).removeClass('fa-circle-o').addClass("fa-check-circle-o");
            $("#i_" + $current_id + featureLine).addClass('ali_name_selected');
            $("#d_" + $current_id + featureLine).addClass('ali_selected');
        }
    }
    ,
    handleHideFeature: function (event) {
        var feature_choice = $(this).closest(".featureLine").attr("id");
        var feature_id = feature_choice.split("_")[1];
        $("#i_" + feature_id + ".featureLine" + ', ' + "#d_" + feature_id + ".featureLine").hide().removeClass("featureLineVisible");
        $("#b_" + feature_id + ".sidebar_feature_button").html('<i class="fa fa-toggle-off"></i>');
        PROVIZ.checkContainerVisibility($(this).closest(".container"));
    }
    ,
    resizeVisualisation: function () {
        var resizeOffset = (PROVIZ.parameters.print > 0) ? 0 : 40;
        var visualisationContainerWidth = $('body').width() - resizeOffset;
        var $visualisationContainer = $('.visualisationContainer');
        $visualisationContainer.width(visualisationContainerWidth);
        $('.headerNameContainer').width(visualisationContainerWidth);
        $('#trackData').width(visualisationContainerWidth - 208);
        $visualisationContainer.show();

        var $trackData = $('#trackData');
        var trackDataWidth = $trackData.width();
        var sequenceWidth = $trackData.find('#sequence').innerWidth();
        var percentOverflow = Math.round((trackDataWidth < sequenceWidth) ? ((trackDataWidth / sequenceWidth) * 100) : 100);
        $(".overflow").width(percentOverflow + "%");

    }
    ,
    toggleClades: function () {
        var toToggle = PROVIZ.options.clades;
        if (toToggle) {
            PROVIZ.unsplitAlignment();
        }
        else {
            PROVIZ.splitAlignment();
        }
        $('.show_clades, .trigger_clades').toggleClass("topOptionsContainer_button_selected");
        PROVIZ.options.clades = !PROVIZ.options.clades;
    }
    ,
    toggleGaps: function () {
        try {
            // uncompact the view
            if (PROVIZ.options.compact) {
                PROVIZ.compactView();
                //$('.compactView').removeClass('topOptionsContainer_button_selected');
            }
            PROVIZ.waiting(PROVIZ.doToggleGaps);
            // reset focus
            PROVIZ.slider.reset();

        } catch (e) {
            console.error(e);
        }
    },
    doToggleGaps: function () {
        "use strict";
        var $archiBtn = $('.show_architecture');
        var $trackData = $("#trackData");
        var $sequence = $trackData.find("#sequence.container");
        var $sequenceInternal = $sequence.find('#d_sequence');
        var $sequenceInternal2 = $sequence.find('#d_sequence').find('#d_0');
        var $alignment = $trackData.find("#alignment.container, .container.clade");
        var visualisationContainerWidth = $(document).width() - 60;
        var $visualisationContainer = $('.visualisationContainer');
        var $container = $(".container").not('#sequence, #alignment, #offset, .container.clade');
        var $gap = $(".gap");
        var $buttons = $('.show_gaps');
        var $offsetContainer = $trackData.find('#d_offset');

        var showgaps = !PROVIZ.options.gapped;

        // compute new length
        var new_offset = (showgaps) ? PROVIZ.offsetGapped : PROVIZ.offsetUngapped;
        var new_length = ((showgaps) ? PROVIZ.dictSequences[0].lengthgapped : PROVIZ.dictSequences[0].lengthungapped) * 10 + 'px';

        //if (showgaps) {
        // hide architecture container if displayed to avoid issues with the scrolling
        if (PROVIZ.options.archi) {
            $archiBtn.click();
        }
        //}

        // modify values length with the new one
        $container[(!showgaps) ? 'show' : 'hide']();
        $sequence.width(new_length).show();
        $sequenceInternal.width(new_length).show();
        $sequenceInternal2.width(new_length).show();
        $alignment.width(new_length).show();
        $gap[(showgaps) ? 'show' : 'hide']();
        $visualisationContainer.width(visualisationContainerWidth);

        // change offset
        $offsetContainer.css('width', new_length).empty().append(new_offset.css('width', new_length));
        // add div for the focus
        $('<div>').addClass('slider-selection-bg slider-align').insertAfter($('.offsetData')).hide();
        $offsetContainer.parent().css('width', new_length).show();

        // change value options
        PROVIZ.options.gapped = !PROVIZ.options.gapped;
        $buttons[(showgaps) ? 'addClass' : 'removeClass']('topOptionsContainer_button_selected');

        // resize visualisation
        PROVIZ.resizeVisualisation()
    }
    ,
    compactView: function () {
        // remove gaps
        if (PROVIZ.options.gapped) {
            PROVIZ.toggleGaps();
        }
        PROVIZ.waiting(PROVIZ.doCompact);
        PROVIZ.slider.reset();
    },
    doCompact: function () {
        "use strict";
        var toCompact = !PROVIZ.options.compact;
        var new_length_container = 0;
        var $trackData = $('#trackData');
        var $buttons = $('.compactView');

        try {
            if (toCompact) {
                // style for compact div
                $("<style id='compactStyle' type='text/css'>\
				#trackData .aliLine div.aC,  #trackData .peptide .aC { \
				width: 5px; \
				font-size: 0;\
				height: 14px;\
				float: left; \
				}\
				\
				#trackData .container div.hC, #trackData .container div.hCL { \
				float: left; \
				position: absolute; \
				cursor: pointer; \
				width: 5px; \
				}\
				\
				#trackData .featureLine div.fC { \
				font-size: 0;\
				}\
				\
			</style>\
			").appendTo("head");
                $trackData.find(".fC, .peptide").each(function () {
                    "use strict";
                    var that = $(this);
                    that.width(that.width() / 2);
                    that.css({'left': (that.position().left / 2)});
                });
                $trackData.find('.hC, .hBC, .hCL').each(function () {
                    "use strict";
                    var that = $(this);
                    that.css({'left': (that.position().left / 2)});
                });
                // change length containers
                new_length_container = PROVIZ.dictSequences[0].lengthungapped * 10 / 2;
                $trackData.find('.container, .featureData, .offsetData, .featureLine, .aliLine, .dblslider').css('width', new_length_container + 'px');
            } else {
                // remove compact style
                $('#compactStyle').remove();
                $trackData.find(".fC, .peptide").each(function () {
                    "use strict";
                    var that = $(this);
                    that.width(that.width() * 2);
                    that.css({'left': (that.position().left * 2)});
                });
                $trackData.find('.hC, .hBC, .hCL').each(function () {
                    "use strict";
                    var that = $(this);
                    that.css({'left': (that.position().left * 2)});
                });
                // change length containers
                new_length_container = PROVIZ.dictSequences[0].lengthungapped * 10;
                $trackData.find('.container, .featureData, .offsetData, .featureLine, .dblslider').css('width', new_length_container + 'px');
                $trackData.find('.aliLine').css('width', ''); // no specific length -> 100%
            }
            // change value options
            PROVIZ.options.compact = toCompact;
            $buttons[(toCompact) ? 'addClass' : 'removeClass']('topOptionsContainer_button_selected');
        } catch (e) {
            console.error(e);
        }
    }
    ,
    closeSidebar: function () {
        $(".sidebar_tab_selected")
            .removeClass("open")
            .addClass("sidebar_tab")
            .removeClass("sidebar_tab_selected");
        $(".sidebar_text").empty();
        $(".sidebarContainer").animate({left: "-280px"}, 250);
        $('.visualisationContainer').animate({left: "20px"});
    }
    ,
    changeClades: function (clade) {
        var sPageURL = PROVIZ.urlParams();
        var sURLVariables = sPageURL.split('&');
        var redirectUrl = [];
        var protein_acc = $('#uniprot_acc').text();
        redirectUrl.push("uniprot_acc=" + protein_acc);
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] !== "alignment" && sParameterName[0] !== "genetree_mode" && sParameterName[0] !== "showAln" && sParameterName[0] !== "jobId" && sParameterName[0] !== "uniprot_acc") {
                redirectUrl.push(sParameterName[0] + "=" + sParameterName[1]);
            }
        }
        redirectUrl.push("alignment=GeneTree");
        redirectUrl.push("genetree_mode=" + clade);
        var redirectUrlStr = redirectUrl.join("&");

        window.location = "./proviz.php?" + redirectUrlStr;

    }
    ,
    resizeRedirect: function () {
        var sPageURL = PROVIZ.urlParams();
        var sURLVariables = sPageURL.split('&');
        var redirectUrl = [];

        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] !== "ali_start" && sParameterName[0] !== "ali_end") {
                redirectUrl.push(sParameterName[0] + "=" + sParameterName[1]);
            }
        }

        var min_resize = parseInt($('#min_resize').val())
        var max_resize = parseInt($('#max_resize').val())

        if (max_resize - min_resize > PROVIZ.options.max_visualisation_length) {
            max_resize = min_resize + PROVIZ.options.max_visualisation_length - 1;
        }

        redirectUrl.push("ali_start=" + min_resize);
        redirectUrl.push("ali_end=" + max_resize);
        var redirectUrlStr = redirectUrl.join("&");

        window.location = "./proviz.php?" + redirectUrlStr;

    }
    ,
    resizeView: function () {
        var sPageURL = PROVIZ.urlParams();
        var sURLVariables = sPageURL.split('&');
        var redirectUrl = [];

        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] !== "ali_start" && sParameterName[0] !== "ali_end") {
                redirectUrl.push(sParameterName[0] + "=" + sParameterName[1]);
            }
        }

        redirectUrl.push("ali_start=" + parseInt($('#min').val()));
        redirectUrl.push("ali_end=" + parseInt($('#max').val()));
        var redirectUrlStr = redirectUrl.join("&");

        window.location = "./proviz.php?" + redirectUrlStr;
    }
    ,
    changeAlignment: function (searchDB) {
        var params = {};
        if (searchDB == "Custom") {
            params = {
                jobId: PROVIZ.data.settings.jobId
            }
        } else {
            params = {
                uniprot_acc: PROVIZ.data.settings.uniprot_acc
            }
        }
        if (PROVIZ.data.settings.ali_start != -1) {
            params['ali_start'] = PROVIZ.data.settings.ali_start + 1;
        }
        if (PROVIZ.data.settings.ali_end != PROVIZ.data.settings.lengthSeq) {
            params['ali_end'] = PROVIZ.data.settings.ali_end;
        }


        params['alignment'] = searchDB;

        if (PROVIZ.data.settings.tools != "") {
            params['tools'] = PROVIZ.data.settings.tools;
        }

        /*for (var i = 0; i < sURLVariables.length; i++) {
         var sParameterName = sURLVariables[i].split('=');

         if (sParameterName[0] !== "alignment" && sParameterName[0] !== "showAln" && sParameterName[0] !== "jobId" && sParameterName[0] !== "uniprot_acc") {
         redirectUrl.push(sParameterName[0] + "=" + sParameterName[1]);
         }
         }*/
        var redirectUrlStr;
        redirectUrlStr = $.param(params);

        window.location = "./proviz.php?" + redirectUrlStr;
    }
    ,
    hideProtein: function (clicked_element) {
        var protein_choice = $(clicked_element).closest(".aliLine").attr("id");
        var protein_id = protein_choice.split("_")[1];
        var protein_name = $(clicked_element).closest(".aliLine").find(".ali_name").text();

        $("#i_" + protein_id + ".aliLine").hide();
        $("#d_" + protein_id + ".aliLine").hide();
        var $trackInfo = $("#trackInfo");
        var proteins = $trackInfo.find('.aliLine');
        var proteinsVisible = $trackInfo.find('.aliLine:visible');

        PROVIZ.updateCountHiddenProteins();
    }
    ,
    showProtein: function (clicked_element) {
        var protein_choice = $(clicked_element).attr("id");
        var protein_id = protein_choice.split("_")[1];
        $(clicked_element).remove();
        $("#i_" + protein_id + ".aliLine").show();
        $("#d_" + protein_id + ".aliLine").show();
        PROVIZ.updateCountHiddenProteins();
    },
    updateCountHiddenProteins: function () {
        "use strict";
        var $trackInfo = $('#trackInfo');
        var proteins = $trackInfo.find('.aliLine');
        var proteinsVisible = $trackInfo.find('.aliLine:visible');
        $(".protein_count").text(proteins.length - proteinsVisible.length);
    }
    ,
    toggleSidebarFeature: function (clicked_element) {
        if (!PROVIZ.options.gapped) { // avoid to show features while gapped are shown
            clicked_element = $(clicked_element);
            var feature_choice = clicked_element.attr("id").replace(/\s/g, '-');
            var feature_id = feature_choice.split("_")[1];

            var toggleElement = clicked_element.find('i');

            if (toggleElement.hasClass('fa-toggle-on')) {
                toggleElement.removeClass('fa-toggle-on').addClass('fa-toggle-off');
            } else {
                toggleElement.removeClass('fa-toggle-off').addClass('fa-toggle-on');
            }

            $("#i_" + feature_id + ".featureLine").toggle().toggleClass("featureLineVisible");
            $("#d_" + feature_id + ".featureLine").toggle().toggleClass("featureLineVisible");
            //var visible = PROVIZ.checkContainerVisibility($("#d_" + feature_id + ".featureLine").closest(".container"));
        }
    }
    ,
    toggleSidebarFeatures: function (clicked_element) {
        if (!PROVIZ.options.gapped) { // avoid to show features while gapped are shown
            clicked_element = $(clicked_element);
            var feature_group_choice = clicked_element.attr("id").replace(/\s/g, '-');
            var $featureGrpChoiceContainer = $("#" + feature_group_choice + ".container");
            var features = $featureGrpChoiceContainer.find(".featureLine");
            $featureGrpChoiceContainer.toggle();

            var $featureGrpBtnToggle = clicked_element.find("i");
            var show = $featureGrpBtnToggle.hasClass("fa-toggle-off");
            var showHide = (show) ? 'show' : 'hide';
            var addremoveClass = (show) ? 'addClass' : 'removeClass';
            var classToggle = (show) ? 'fa-toggle-on' : 'fa-toggle-off';

            $featureGrpBtnToggle.removeClass().addClass('fa ' + classToggle);
            features.each(function () {
                var thatFeature = $(this);
                var current_id = thatFeature.attr("id").split("_")[1];
                $("#i_" + current_id + ".featureLine")[showHide]()[addremoveClass]("featureLineVisible");
                $("#d_" + current_id + ".featureLine")[showHide]()[addremoveClass]("featureLineVisible");
                $("#b_" + current_id + ".sidebar_feature_button").find('i').removeClass().addClass('fa ' + classToggle);
            });
            PROVIZ.checkContainerVisibility(features);
        }
    }
    ,
    toggleSidebarFeaturesAll: function (clicked_element) {
        if (!PROVIZ.options.gapped) {
            clicked_element = $(clicked_element);
            var button_choice = clicked_element.attr("id");
            var $container = $(".container");
            var $features = $container.find(".featureLine");
            var show = (button_choice === 'show');
            var classToggle = (show) ? 'fa-toggle-on' : 'fa-toggle-off';
            var addremoveClass = (show) ? 'addClass' : 'removeClass';
            $container[button_choice]();

            $(".sidebar_feature_button").find('i').removeClass().addClass('fa ' + classToggle);
            $(".sidebar_features_button").find('i').removeClass().addClass('fa ' + classToggle);
            $features[button_choice]()[addremoveClass]("featureLineVisible");
        }
    }
    ,
    recolourProteins: function () {
        var idList = [];
        $("#trackInfo").find("#alignment .aliLine").filter(':visible').find(".ali_name").each(function () {
            idList.push($(this).data("proteinid")); // .split(":")[1]
        });
        window.location = "./proviz.php?" + PROVIZ.urlParams() + "&showAln=" + idList.join(",");
    }
    ,
    reset: function () {
        window.location = "./proviz.php?uniprot_acc=" + PROVIZ.urlParam("uniprot_acc");
    }
    ,
    //-----
    //-----
    layering: function (data) {
        var container = [];
        container[0] = [];

        data.sort(function (a, b) {
            return a.start - b.start;
        });
        for (var i = 0; i < data.length; i++) {
            if (typeof data[i].html === "undefined") {
                for (var j = 0; j < container.length; j++) {
                    if (container[j].length === 0) {
                        container[j].push(data[i]);
                        break;
                    } else if ((parseInt(container[j][container[j].length - 1].end, 10) + 1) < parseInt(data[i].start, 10)) {
                        container[j].push(data[i]);
                        break;
                    } else if (j === (container.length - 1)) {
                        tmp = [];
                        tmp.push(data[i]);
                        container.push(tmp);
                        break;
                    }
                }
            } else {
                container[0].push(data[i]);
            }
        }
        return container;
    }
    ,
    readFile: function (f, callback) {
        var r = new FileReader();
        r.onload = callback;
        r.readAsText(f);
    }
    ,
    parseCSVTrack: function (contents) {
        var tmpTracks = [];
        var conArr = contents.split(/\r\n?|\n/);
        var header = conArr[0].split(",");
        for (var i = 1; i < conArr.length; i++) {
            if (conArr[i] != '' && conArr[i].match(/^,*$/) == null) {
                var lineArr = conArr[i].split(",");

                // transform the line to an associative array
                var dictLine = {};
                $.each(lineArr, function (i, value) {
                    "use strict";
                    dictLine[header[i]] = value;
                });

                // new track
                if (typeof tmpTracks[dictLine.track - 1] === "undefined") {
                    var tmpTrackArr = {};
                    if (dictLine.track === "") {
                        alert("No track number given.");
                        return 0;
                    }
                    tmpTrackArr.position = dictLine.t_position;
                    tmpTrackArr.name = dictLine.t_name;
                    tmpTrackArr.type = dictLine.t_type;
                    tmpTrackArr.colour = dictLine.t_colour;
                    tmpTrackArr.text_colour = dictLine.t_text_colour;
                    tmpTrackArr.opacity = dictLine.t_opacity;
                    tmpTrackArr.help = dictLine.t_help;
                    tmpTrackArr.data = [];
                    tmpTracks[dictLine.track - 1] = tmpTrackArr;
                }
                // new entry
                var tmpEntryArr = {};
                tmpEntryArr.start = dictLine.start;
                tmpEntryArr.end = dictLine.end;
                tmpEntryArr.len = dictLine.end - dictLine.start + 1 || 1;
                tmpEntryArr.position = dictLine.position;
                tmpEntryArr.text = dictLine.text;
                tmpEntryArr.value = dictLine.value;
                tmpEntryArr.sequence = dictLine.sequence;
                tmpEntryArr.hover = dictLine.hover;
                tmpEntryArr.link = dictLine.link;
                tmpEntryArr.colour = dictLine.colour;
                tmpEntryArr.opacity = dictLine.opacity;
                tmpEntryArr.text_colour = dictLine.text_colour;

                tmpTracks[dictLine.track - 1].data.push(tmpEntryArr);
            }
        }
        return tmpTracks;
    }
    ,
    parseXMLTrack: function (xmlDoc) {
        var tmpTracks = [];
        if (xmlDoc.getElementsByTagName("tracks").length === 1) {
            var tracks = xmlDoc.getElementsByTagName("track");
            if (tracks.length > 0) {
                $.each(tracks, function (indexTrack, track) {
                    if (!track.getAttribute("position")) {
                        alert("no position specified");
                        return;
                    }
                    if (!track.getAttribute("type")) {
                        alert("no type specified");
                        return;
                    }
                    var tmpTrackArr = {
                        type: track.getAttribute("type"),
                        position: track.getAttribute("position"),
                        name: track.getAttribute("name"),
                        colour: track.getAttribute("colour"),
                        opacity: track.getAttribute("opacity"),
                        id: track.getAttribute("id"),
                        class: track.getAttribute("class"),
                        height: track.getAttribute("height"),
                        help: track.getAttribute("help"),
                        text_colour: track.getAttribute("text_colour"),
                        data: []
                    };
                    var entries = track.getElementsByTagName("entry");
                    if (entries.length > 0) {
                        $.each(entries, function (indexEntry, entry) {
                            if (!entry.getAttribute("position") && (!entry.getAttribute("start") && !entry.getAttribute("end"))) {
                                alert("start and stop or position has to be given");
                                return;
                            }
                            if (entry.getAttribute("text") && entry.getAttribute("value")) {
                                alert("text and value are set. only one allowed");
                                return;
                            }
                            var tmpEntryArr = {
                                start: entry.getAttribute("start"),
                                end: entry.getAttribute("end"),
                                position: entry.getAttribute("position"),
                                len: entry.getAttribute("end") - entry.getAttribute("start") + 1 || 1,
                                text: entry.getAttribute("text"),
                                sequence: entry.getAttribute("sequence"),
                                value: entry.getAttribute("value"),
                                link: entry.getAttribute("link"),
                                colour: entry.getAttribute("colour"),
                                opacity: entry.getAttribute("opacity"),
                                id: entry.getAttribute("id"),
                                class: entry.getAttribute("class"),
                                hover: entry.getAttribute("hover"),
                                text_colour: entry.getAttribute("text_colour")
                            };
                            tmpTrackArr.data.push(tmpEntryArr);
                        });
                    } else {
                        alert("no entries given");
                        return;
                    }
                    tmpTracks.push(tmpTrackArr);
                });
                return tmpTracks;
            } else {
                alert("no tracks given");
                return;
            }
        } else {
            alert("multiple <tracks> tags given, only one allowed");
            return;
        }
    }
    ,
    newRow: function (name, description, content, height) {
        height = height || 14;
        var nameid = name.replace(/\s/g, '-');
        var row = {};
        var infowidth = '204px';

        var datawidth = (PROVIZ.data.protein_length * 10) + 'px';

        row.info = $('<div>')
            .addClass('container')
            .attr('id', nameid)
            .attr('data-name', name)
            .css('width', infowidth)
            .hide();

        var info = $('<div>').appendTo(row.info)
            .addClass('featureLine featureLineVisible')
            .attr('id', 'i_' + nameid)
            .css('width', infowidth)
            .css('height', height + 'px')
            .append($('<div>').addClass('featureType').html(name).css('height', height + 'px'))
            ;

        var help = $('<div>').addClass('featureHelp')
            .attr('data-tooltip', description)
            .append($('<i>').addClass('fa fa-circle-o select_feature').click(PROVIZ.handleSelectFeature))
            .append($('<i>').addClass('fa fa-times hide_feature').click(PROVIZ.handleHideFeature));
        // add calls
        if (description != '') {
            help.append($('<i>').addClass('fa fa-question-circle'));
        }

        info.append(help);

        row.data = $('<div>')
            .addClass('container data')
            .attr('id', nameid)
            .attr('data-name', name)
            .css('width', datawidth)
            //.css('min-height', height + 2 + 'px')
            .hide();

        $('<div>').appendTo(row.data)
            .addClass('featureLine featureLineVisible')
            .attr('id', 'd_' + nameid)
            .attr('data-name', name)
            .css('height', height + 'px')
            .css('width', datawidth)
            .append(content.css('width', datawidth));
        return row;
    },
    addTrack: function (i, track) {
        var row;
        if (track.type === 'peptides') {
            row = PROVIZ.newPeptides(i, track);
        } else if (track.type === 'histogram') {
            row = PROVIZ.newHistogram(i, track);
        } else if (track.type === 'feature') {
            row = PROVIZ.newFeature(i, track);
        }
        var $alignment = $('#alignment');
        var anchor = ($alignment.length > 0) ? '#alignment' : '#sequence';
        var beforeAfter = (parseInt(track.position) > 0) ? 'insertAfter' : 'insertBefore';
        if ($alignment.length > 0 && parseInt(track.position) < 0) {
            anchor = '#sequence';
        }
        row.info.show()[beforeAfter]($('#trackInfo ' + anchor));
        row.data.show()[beforeAfter]($('#trackData ' + anchor));
    }
    ,
    newFeature: function (index, track) {
        var ali_start = parseInt(PROVIZ.urlParam('ali_start')) || 1;
        var ali_end = parseInt(PROVIZ.urlParam('ali_end')) || $("#uniprot_acc").data().length;
        var trackHeight = track.height || 14 * track.data.length;
        var trackName = track.name || 'custom track #' + index;
        var description = track.help || '';
        var trackId = track.id || 'custom_track_' + index;
        var trackClass = track.class || '';

        var features = $('<div>') // data
            .attr('id', trackId)
            .addClass('featureData ' + trackClass)
            .css('height', trackHeight + 'px')
            ;

        $.each(track.data, function (isubtrack, subtrack) {
            $.each(subtrack, function (ifeat, feature) {
                var featureItem = $((feature.link && feature.link != "") ? '<a>' : '<span>')
                    .text(feature.text)
                    .css('color', feature.text_colour || track.text_colour || '')
                    .css('opacity', feature.opacity || track.opacity || '');
                if (feature.link) {
                    featureItem.prop('href', feature.link || '').attr('target', '_blank');
                }
                feature.class = feature.class || '';
                var featureDiv = $('<div>').appendTo(features)
                    .attr('id', feature.id)
                    .attr('data-tooltip', feature.hover)
                    .attr('data-pos', feature.start + ':' + feature.end)
                    .addClass('fC ' + feature.class || '')
                    .css('width', feature.len * 10)
                    .css('left', (feature.start - ali_start) * 10)
                    .css('top', isubtrack * 13)
                    .css('background-color', feature.colour || track.colour || '')

                    .append(featureItem)
                //.click(PROVIZ.slider.handleCtrlClick)
                    ;
            });
        });
        return PROVIZ.newRow(trackName, description, features, trackHeight);
    }
    ,
    newPeptides: function (index, track) {
        var ali_start = parseInt(PROVIZ.urlParam('ali_start')) || 1;
        var ali_end = parseInt(PROVIZ.urlParam('ali_end')) || $("#uniprot_acc").data().length;
        var trackHeight = track.height || 14 * track.data.length;
        var trackName = track.name || 'custom track #' + index;
        var description = track.help || '';
        var trackId = track.id || 'custom_track_' + index;
        var trackClass = track.class || '';

        var features = $('<div>') // data
            .attr('id', trackId)
            .addClass('featureData ' + trackClass)
            .css('height', trackHeight + 'px')
            ;

        $.each(track.data, function (isubtrack, subtrack) {
            $.each(subtrack, function (ifeat, feature) {

                var aas = feature.sequence.split("");
                var splittedPeptide = $('<a>');
                if (feature.link) {
                    splittedPeptide.prop('href', feature.link || '').attr('target', '_blank');
                }

                feature.scores = feature.score || Array.apply(null, Array(aas.length)).map(function () {
                        return 1
                    });

                $.each(aas, function (i, aa) {
                    splittedPeptide.append($('<div>')
                        .addClass('aC')
                        .css('float', "left")
                        .css('font-weight', "700")
                        .css('height', 11)
                        .css('opacity', feature.scores[i])
                        .css('color', feature.text_colour || track.text_colour || '')
                        .text(aa));
                });

                feature.class = feature.class || '';
                var featureDiv = $('<div>')
                    .appendTo(features)
                    .attr('id', feature.id)
                    .attr('data-tooltip', feature.hover)
                    .attr('data-pos', feature.start + ':' + feature.end)
                    .addClass('fC ' + feature.class)
                    .css('height', 11)
                    .css('width', feature.len * 10)
                    .css('left', (feature.start - ali_start) * 10)
                    .css('top', isubtrack * 14)
                    .css('background-color', feature.colour || track.colour || '')
                    .css('opacity', feature.opacity || track.opacity || '')
                    //.click(PROVIZ.slider.handleCtrlClick)
                    .append(splittedPeptide)
                    ;
            });
        });
        return PROVIZ.newRow(trackName, description, features, trackHeight);
    }
    ,
    newHistogram: function (index, track) {
        var max = Math.max.apply(Math, track.data.map(function (o) {
            return o.value;
        }));
        var min = Math.min.apply(Math, track.data.map(function (o) {
            return o.value;
        }));
        var ali_start = PROVIZ.data.ali_start;
        var ali_end = PROVIZ.data.ali_end;


        var trackHeight = (min < 0) ? 41 : 21;
        var trackName = track.name || 'custom track #' + index;
        var description = track.help || '';
        var trackId = track.id || 'custom_track_' + index;
        var trackClass = track.class || '';

        var features = $('<div>') // data
            .attr('id', trackId)
            .addClass('featureData ' + trackClass)
            .css('height', trackHeight + 'px')
            ;

        // baseline
        $('<div>').appendTo(features)
            .css('position', 'relative')
            .css('height', '1px')
            .css('top', '20px')
            .css('width', $('#trackData #alignment').width() + 'px')
            .css('background-color', '#AAAAAA');

        //style="position:relative;height:1px;top:20px;width:' + ($("#alignment").width()) + 'px;background-color:#AAAAAA"

        $.each(track.data, function (ihisto, histo) {
            histo.class = histo.class || '';
            var featureDiv = $('<div>')
                .appendTo(features)
                .addClass('hC ' + histo.class)
                .attr('data-tooltip', histo.hover)
                .attr('data-pos', histo.position)
                .css('left', (histo.position - ali_start) * 10)
                .css('height', (histo.value > 0) ? histo.value * 20 : histo.value * -20)
                .css('background-color', histo.colour || histo.colour || '')
                .css('color', histo.text_colour || histo.text_colour || '')
                .css('opacity', histo.opacity || histo.opacity || '')
                ;
            if (histo.value < 0) {
                featureDiv.css('top', '21px');
            } else {
                featureDiv.css('bottom', (min > 0) ? '1px' : '21px');
            }
        });
        return PROVIZ.newRow(trackName, description, features, trackHeight);
    }
    ,
    printTracks: function (tracks) {
        $.each(tracks, PROVIZ.addTrack);
        return false;
    }
    ,
    //-----
    //-----
    checkContainerVisibility: function (container_check) {
        var show = !(container_check.find(".featureLineVisible").length === 0);
        var showHide = (show) ? 'show' : 'hide';
        var toggleClass = (show) ? 'fa-toggle-on' : 'fa-toggle-off';
        var id = container_check.attr("id");
        var divId = $("#" + id);
        divId.find(".container")[showHide]();
        divId.find(".sidebar_features_button").find('i').removeClass().addClass('fa ' + toggleClass);
        return show;
    }
    ,
    loadRest: function (rest_url) {
        "use strict";
        var slider = PROVIZ.slider;
        if (navigator.sayswho === "MSIE 9" && window.XDomainRequest) {
            // Use Microsoft XDR
            var xdr = new XDomainRequest();
            xdr.open("get", rest_url);
            xdr.onload = function () {
                var tracks = [];
                var contents = xdr.responseText;
                if (rest_url.endsWith("xml")) {
                    var xmlDoc = "";
                    if (window.DOMParser) {
                        var parser = new DOMParser();
                        xmlDoc = parser.parseFromString(contents, "text/xml");
                    } else { // Internet Explorer
                        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                        xmlDoc.async = false;
                        xmlDoc.loadXML(contents);
                    }
                    tracks = PROVIZ.parseXMLTrack(xmlDoc);
                } else if (rest_url.endsWith("csv")) {
                    tracks = PROVIZ.parseCSVTrack(contents);
                } else if (rest_url.endsWith("json")) {
                    tracks = JSON.parse(contents);
                    // compute len if missing
                    $.each(tracks, function (itrack, track) {
                        "use strict";
                        $.each(track.data, function (iel, el) {
                            if (typeof el.len == 'undefined') {
                                el.len = el.end - el.start + 1 || 1;
                            }
                        });
                    });
                }
                PROVIZ.displayTracks(tracks);
            };
            xdr.send();
        } else {
            $.ajax({
                type: "GET",
                url: rest_url,
                dataType: "text",
                success: function (data) {
                    var tracks = [];
                    var contents = data;
                    if (rest_url.endsWith("xml")) {
                        var xmlDoc = "";
                        if (window.DOMParser) {
                            var parser = new DOMParser();
                            xmlDoc = parser.parseFromString(contents, "text/xml");
                        } else { // Internet Explorer
                            xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                            xmlDoc.async = false;
                            xmlDoc.loadXML(contents);
                        }
                        tracks = PROVIZ.parseXMLTrack(xmlDoc);
                    } else if (rest_url.endsWith("csv")) {
                        tracks = PROVIZ.parseCSVTrack(contents);
                    } else if (rest_url.endsWith("json")) {
                        tracks = JSON.parse(contents);
                        // compute len if missing
                        $.each(tracks, function (itrack, track) {
                            "use strict";
                            $.each(track.data, function (iel, el) {
                                if (typeof el.len == 'undefined') {
                                    el.len = el.end - el.start + 1 || 1;
                                }
                            });
                        });
                    }
                    PROVIZ.displayTracks(tracks);
                },
                error: function (error) {
                    console.error(JSON.stringify(error, null, 2));
                }
            });
        }
    },
    urlParam: function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results == null) {
            return null;
        }
        else {
            return results[1] || 0;
        }
    },

    urlParams: function () {
        return window.location.search.substring(1);
    },

    print_r: function (array, return_val, sep) {
        //  discuss at: http://phpjs.org/functions/print_r/
        //        http: //kevin.vanzonneveld.net
        // original by: Michael White (http://getsprink.com)
        // improved by: Ben Bryan
        // improved by: Brett Zamir (http://brett-zamir.me)
        // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        //    input by: Brett Zamir (http://brett-zamir.me)
        //  depends on: echo
        //   example 1: print_r(1, true);
        //   returns 1: 1
        sep = "\n";
        var output = '',
            pad_char = ' ',
            pad_val = 4,
            d = this.window.document,
            getFuncName = function (fn) {
                var name = (/\W*function\s+([\w\$]+)\s*\(/).exec(fn);
                if (!name) {
                    return '(Anonymous)';
                }
                return name[1];
            };
        repeat_char = function (len, pad_char) {
            var str = '';
            for (var i = 0; i < len; i++) {
                str += pad_char;
            }
            return str;
        };
        formatArray = function (obj, cur_depth, pad_val, pad_char) {
            if (cur_depth > 0) {
                cur_depth++;
            }

            var base_pad = repeat_char(pad_val * cur_depth, pad_char);
            var thick_pad = repeat_char(pad_val * (cur_depth + 1), pad_char);
            var str = '';

            if (typeof obj === 'object' && obj !== null && obj.constructor && getFuncName(obj.constructor) !== 'PHPJS_Resource') {
                str += 'Array' + sep + base_pad + '(' + sep;
                for (var key in obj) {
                    str += thick_pad + '[' + key + '] => ' + formatArray(obj[key], cur_depth + 1, pad_val, pad_char) + sep;
                }
                str += base_pad + ')' + sep;
            } else if (obj === null || typeof obj === "undefined") {
                str = '';
            } else { // for our "resource" class
                str = obj.toString();
            }

            return str;
        };
        output = formatArray(array, 0, pad_val, pad_char);
        return output;
    }

};

String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};


navigator.sayswho = (function () {
    var ua = navigator.userAgent, tem,
        M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE ' + (tem[1] || '');
    }
    if (M[1] === 'Chrome') {
        tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
        if (tem != null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);
    return M.join(' ');
})();
