"use strict";

var page = require('webpage').create(),
    system = require('system'),
    address, output, size, pageWidth, pageHeight;

function checkReadyState() {
    setTimeout(function () {
        var readyState = page.evaluate(function () {
            return document.readyStateCustom;
        });
        readyState = 'proviz-complete';
        if ("proviz-complete" === readyState) {
            page.render(output);
            console.log('page rendered ->' + output);
            phantom.exit();
        } else {
            console.log('new try: ' + readyState);
            checkReadyState();
        }
    }, 100);
}


if (system.args.length < 3 || system.args.length > 5) {
    console.error('not enough args');
    phantom.exit(1);
} else {
    address = system.args[1];
    output = system.args[2];
    if (system.args.length > 3 && system.args[2].substr(-4) === ".pdf") {
        size = system.args[3].split('*');
        pageWidth = parseInt(size[0], 10);
        pageHeight = parseInt(size[1], 10);
        page.viewportSize = {width: pageWidth*3, height: pageHeight};
        page.customHeaders = {
            "DNT": "1"
        };
        page.open(address, function (status) {
            if (status !== 'success') {
                console.log('Unable to load the address!');
                phantom.exit(1);
            } else {
                page.paperSize = {margin: '0px', width: pageWidth*2, height: pageHeight*2};

                page.clipRect = { top: 0, left: 0, width: pageWidth, height: pageHeight};
                page.evaluate(function(w, h) {
                    document.body.style.width = w + "px";
                    document.body.style.height = h + "px";
                }, pageWidth, pageHeight);

                console.log('begin render');
                setTimeout(checkReadyState, 100);
            }
        });
    } else {
        console.error('not PDF');
        phantom.exit(1);
    }
}

