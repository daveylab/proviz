"use strict";
//var slider;

// Copyright (C) 2016 Norman E. Davey <normandavey@gmail.com>, Jean Manguy <jean.manguy@ucdconnect.ie> and Peter Jehl <peter.jehl@ucdconnect.ie>
// This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
// Public License as published by the Free Software Foundation; either version 2.1 of the License, or (at your option)
// any later version.
// 
// This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
// 
// You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
// the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Author contact: Norman E. Davey <normandavey@gmail.com> / Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
// Author contact: Jean Manguy <jean.manguy@ucdconnect.ie> / Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
// Author contact: Peter Jehl <peter.jehl@ucdconnect.ie> / Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.


var getUrlVars = function () {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
};


var handleSearchRegex = function (event) {
    event.preventDefault();
    var parent = $(event.target).parent();
    var help = $('#help-text-search-regexp');
    var dictSequences = PROVIZ.dictSequences;
    PROVIZ.waiting(function () {
        var input = $('#search-regexp').val();
        var countFound = 0;
        var nchars = input.length;
        if (nchars > 1) {
            colorAA(true);
            countFound = colorRegex(input, dictSequences);
            PROVIZ.options.search = true;
        }
        help.html('(found ' + countFound + ')').show();
        if (countFound > 0) {
            parent.removeClass('has-error').addClass('has-success');
        } else {
            parent.addClass('has-error').removeClass('has-success');
        }
    });
};


var handleResetRegexp = function (event) {
    event.preventDefault();
    PROVIZ.waiting(function () {
        colorAA(true);
        $('.aaMatched').removeClass('aaMatched');
        $('#help-text-search-regexp').html('');
        $('#search-regexp').val('');
        $(event.target).parent().removeClass('has-error').removeClass('has-success');
        PROVIZ.options.search = false;
    });
    hidePositionSearch('alignment');
};


var hidePositionSearch = function (typesearch) {
    $('.architectureTrack.resultSearch_' + typesearch).remove();
};


var colorAA = function (yes) {
    if (yes) {
        $('#colorlessStyle').remove();
    } else {
        $("<style id='colorlessStyle' type=\"text/css\">#trackData .aliLine div.aC, #trackData .aliLine div.aCC { opacity: 0.5 }</style>")
            .appendTo("head");
    }
};


function showHideElProviz(id, active) {
    var showHide = (active) ? 'show' : 'hide';
    var targets = $('#trackInfo, #trackData').find('#' + id);
    targets[showHide]();
}


// extract sequences
// with gap and without
function extractSequences() {
    var dictSequences = {};
    $('#trackData').find('.aliLine').each(function (indexseqdiv, sequencediv) {
        var sequence_gapped = '';
        var sequence_ungapped = '';
        var sequence_true = '';
        var positions = [];
        var pos_ungapped = 0;
        $(sequencediv).find('.aC').each(function (indexaa, aa) {
            aa = $(aa);
            sequence_gapped += aa.html();
            if (!aa.hasClass('gap') && !(aa.html().localeCompare('-') == 0)) {
                sequence_ungapped += aa.html();
                sequence_true += aa.html();
                positions.push({gapped: indexaa, ungapped: pos_ungapped++})
            } else if (!aa.hasClass('gap') && (aa.html().localeCompare('-') == 0)) {
                sequence_ungapped += aa.html();
            } else if (!(aa.html().localeCompare('-') == 0) && aa.hasClass('gap')) {
                sequence_true += aa.html();
                positions.push({gapped: indexaa, ungapped: pos_ungapped})
            }
        });
        dictSequences[indexseqdiv] = {
            positions: positions,
            gapped: sequence_gapped,
            ungapped: sequence_ungapped,
            trueseq: sequence_true,
            lengthtrue: sequence_true.length,
            lengthgapped: sequence_gapped.length,
            lengthungapped: sequence_ungapped.length
        };
    });
    return dictSequences;
}

function getSubSequence(dictsequences, index, start, end, gapped) {
    // default: gapped version
    if (typeof gapped == 'undefined') {
        gapped = true;
    }
    if (start === undefined) {
        start = 1;
    }
    var startTrueSeq = dictsequences[0].positions[start] || dictsequences[0].positions[0];
    var endTrueSeq = dictsequences[0].positions[end - 1] || dictsequences[0].positions[dictsequences[0].positions.length - 1];

    start = startTrueSeq[(gapped) ? 'gapped' : 'ungapped'];
    end = endTrueSeq[(gapped) ? 'gapped' : 'ungapped'];
    var sequence = dictsequences[index][(gapped) ? 'gapped' : 'ungapped'];
    return sequence.substring(start, end + 1)
}

function formatFasta(id, sequence) {
    var fasta = ">" + id + "\r\n";
    var i = 0;
    while (i < sequence.length) {
        fasta = fasta + sequence.substring(i, i + 60) + "\r\n";
        i = parseInt(i) + 60;
    }
    return fasta;
}

function getIdSequences() {
    var list = [];
    $('.ali_name').each(function (index, name) {
        list.push($(name).attr('data-proteinId'));
    });
    return list;
}

function valuesToArray(obj) {
    return Object.keys(obj).map(function (key) {
        if (obj.hasOwnProperty(key)) {
            return obj[key];
        }
    });
}

function createFasta(dictsequences, gapped, start, end, visibles) {
    end = parseInt(end) + 1;
    if (dictsequences === undefined) {
        console.error('dictsequences is undefined');
        return false;
    }
    // default: gapped version
    if (typeof gapped == 'undefined') {
        gapped = true;
    }
    var fasta = '';
    var listIdsequences = getIdSequences();

    $.each(listIdsequences, function (index, id) {
        if (visibles.indexOf(index) > -1) {
            fasta += formatFasta(id, getSubSequence(dictsequences, index, start, end, gapped));
        }
    });
    return fasta;
}

// Constructor
var Slider = function () {
    //init vars
    this.trackData = $('#trackData').scrollLeft(0);
    this.trackInfo = $('#trackInfo');
    this.sequence = this.trackData.find('#sequence');
    this.eltAlign = this.sequence;
    this.aas = this.sequence.find('.aC').filter(':not(.gap)');
    this.min = parseInt(this.aas.first().attr('data-pos'));
    this.max = parseInt(this.aas.last().attr('data-pos'));
    var originalMin = this.min;
    var originalMax = this.max;
    this.inputMin = $('#min');
    this.inputMax = $('#max');
    this.checkMin = -1;
    this.checkMax = -1;
    this.lengthProtein = PROVIZ.data.settings.lengthSeq;

    // getters
    this.getOriginalMin = function () {
        return originalMin;
    };
    this.getOriginalMax = function () {
        return originalMax;
    };


    // creation of the divs
    this.offsetData = this.trackData.find('#offset');
    this.offsetInfo = this.trackInfo.find('#offset');

    this.panDblslider = $('<div>').addClass('dblslider');
    this.row = PROVIZ.newRow('selector', 'selector', this.panDblslider);
    this.row.data.insertBefore(this.offsetData);
    this.row.info.insertBefore(this.offsetInfo);

    var that = this;

    this.panDblslider.slider({
        animate: 'fast',
        range: true,
        min: this.min,
        max: this.max,
        values: [this.min, this.max],
        change: function (event, ui) {
            var start = parseInt(ui.values[0]);
            var end = parseInt(ui.values[1]);

            if (parseInt(that.inputMin.val()) != start || parseInt(that.inputMax.val()) != end) {
                // prevent to do that if the two values changed
                that.inputMin.val(start);
                that.inputMax.val(end);
            }
            that.center(start, end);
        }
    });

    // add the button to highlight on the slider
    // TODO
    var range = this.panDblslider.find('.ui-slider-range');
    range.attr('data-tooltip-btnslider', 'true');

    // hide
    this.row.info.find('#i_selector').hide();
    this.row.data.find('#d_selector').hide();


    // slider architecture
    this.panSmplContainer = $('.slider-pan-container');
    that = this;
    this.panSmplSlider = $('<div>')
        .appendTo(that.panSmplContainer)
        .css('margin-bottom', '-0px');

    this.panSmplSlider.slider({
        animate: 'fast',
        min: 1,
        max: parseInt(that.lengthProtein),
        value: that.min,
        slide: function (event, ui) {
            if (ui.value < that.min || ui.value > that.max) {
                return false;
            }
        },
        change: function (event, ui) {
            if (ui.value < that.min || ui.value > that.max) {
                return false;
            }
            if (event.originalEvent != undefined) {
                // modified by handle
                // move dbl slider
                that.panDblslider.slider('option', "values", [Math.max(ui.value - 5, that.min), Math.min(ui.value + 5, that.max)]);
            }
        }
    });

    // init of the inputs
    this.inputMin.attr('min', this.min).attr('max', this.max).val(this.min);
    this.inputMax.attr('min', this.min).attr('max', this.max).val(this.max);
    that = this;
    this.inputs = that.inputMin.add(that.inputMax);


    this.inputs.on({
        change: function () {
            // update sidebar
            $('#minSidebar').val(that.inputMin.val());
            $('#maxSidebar').val(that.inputMax.val());
            that.panDblslider.slider("option", "values", [that.inputMin.val(), that.inputMax.val()]);
        }
    });

    var $offsetData = $('.offsetData');
    this.focusBlockCurtainRight = $('<div>').addClass('slider-bg-curtain').css('right', 0).insertAfter($offsetData).hide();
    this.focusBlock = $('<div>').addClass('slider-selection-bg slider-align').insertAfter($offsetData).hide();
    this.focusBlockCurtainLeft = $('<div>').addClass('slider-bg-curtain').css('left', 0).insertAfter($offsetData).hide();

    this.handleCtrlClick = function (e) {
        if ((e.metaKey || e.ctrlKey)) {
            document.getSelection().removeAllRanges(); // fix a firefox issue where text is selected, it's ugly
            e.preventDefault();
            var target = $(e.target).closest('.fC, .archiCell, .peptide, #d_sequence .aC');
            if (target.length > 0) {
                var pos = target.attr('data-pos').split(':');
                var start = parseInt(pos[0]);
                var end;
                if (pos.length > 1) {
                    end = parseInt(pos[1]);
                } else {
                    end = start
                }
                if (start == that.checkMin && end == that.checkMax) {
                    that.reset();
                } else {
                    that.checkMin = start;
                    that.checkMax = end;
                    that.inputMin.val(start);
                    that.inputMax.val(end);
                    that.inputs.change();
                    that.focus(start, end);
                    PROVIZ.options.focus = true;
                    $('.trigger_focus').addClass('topOptionsContainer_button_selected')
                }
            } else {
                that.reset();
            }


        }
    };

    if (PROVIZ.data.settings.ali_start == 0) {
        PROVIZ.data.settings.ali_start = -1;
    }
    var starterOverflowBox = (PROVIZ.data.settings.ali_start + 1) / that.lengthProtein * 100;
    var enderOverflowBox = (that.lengthProtein - PROVIZ.data.settings.ali_end - 1) / that.lengthProtein * 100;


    // display of the overflow in the container element
    this.percentOverflow = 0;


    // follow the scroll
    that = this;

    try {
        // shadow over the architecture to show the part visible
        var leftCurtain = $('<div>')
            .addClass('curtain curtain-right')
            .css({
                left: 0,
                'border-right': '1px solid black',
                width: starterOverflowBox + '%'
            });

        var rightCurtain = $('<div>')
            .addClass('curtain curtain-right')
            .css({
                width: enderOverflowBox + '%'
            });

        var overflowContainer = $('<div>')
            .css({
                top: 0,
                left: starterOverflowBox + '%',
                width: (100 - starterOverflowBox - enderOverflowBox) + '%',
                position: 'absolute',
                'pointer-events': 'none',
                'box-sizing': 'border-box',
                height: '100%'
            });

        var overflow = $("<div>").addClass('overflow').appendTo(overflowContainer);
        overflow
            .css('top', '0')
            .css('min-height', '100%')
            .css('left', '0%')
            .css('width', this.percentOverflow + '%');

        this.trackData.scroll(function () {
            var move = Math.floor(that.trackData.scrollLeft() / $(that.eltAlign).innerWidth() * 100 * 100) / 100;
            overflow.css('left', move + '%');
        });

        this.panSmplContainer.append(leftCurtain);
        this.panSmplContainer.append(overflowContainer);
        this.panSmplContainer.append(rightCurtain);

    } catch (e) {
        console.error(e);
    }

    this.moveUp = function () {
        var body = $("body, html");
        body.scrollTop(body.scrollTop() - 100);
    };

    this.moveDown = function () {
        var body = $("body, html");
        body.scrollTop(body.scrollTop() + 100);
    };

    // move with arrow keys
    that = this;
    var doc = $(document);
    doc.keydown(function (event) {
        if (event.key === "ArrowLeft") {
            that.move('left', 100);
        } else if (event.key === "ArrowRight") {
            that.move('right', 100);
        }
    });
    // move with arrow buttons
    $('#archi-nav-left').click(function () {
        that.move('left', 100);
    });
    $('#archi-nav-right').click(function () {
        that.move('right', 100);
    });

    // integration with the focus button
    that = this;
    $('.headerNameContainer #focus').on('click', function () {
        var open = $(this).hasClass("topOptionsContainer_button_selected");
        var showOrHide = (open) ? 'show' : 'hide';
        that.row.info[showOrHide]();
        that.row.data[showOrHide]();
    });
};

Slider.prototype.move = function (side, amount) {
    var that = this;
    var position = that.trackData.scrollLeft();
    amount = (side == 'right') ? amount : -amount;
    that.trackData.scrollLeft(position + amount);
};


// functions
// reset the value of the slider to the originals values
Slider.prototype.reset = function () {
    $('.slider-selection-bg').hide();
    $('.slider-bg-curtain').hide();
    $('.trigger_focus').removeClass('topOptionsContainer_button_selected');
    this.checkMin = -1;
    this.checkMax = -1;
};

Slider.prototype.findAAByPosition = function (position) {
    var aas = this.aas;
    for (var aa in aas) {
        if (aas.hasOwnProperty(aa)) {
            var aaSelected = $(aas[aa]);
            if ((aaSelected.position().left) == position) {
                return aaSelected;
            }
        }
    }
    return null;
};

Slider.prototype.findAAByNum = function (num) {
    var aas = this.aas;
    for (var aa in aas) {
        if (aas.hasOwnProperty(aa) && parseInt($(aas[aa]).attr('data-pos')) == num) {
            return $(aas[aa]);
        }
    }
    return null;
};


Slider.prototype.focus = function (start, end) {
    var that = this;
    var widthAA = that.aas.first().width();
    start = parseInt(start);
    end = parseInt(end);
    var aas = that.aas;
    // swap values if not in the right order
    if (start > end) {
        var tmp = start;
        start = end;
        end = tmp;
    }

    // limit the focus to min and max of the visble part
    var min = parseInt(aas.first().attr('data-pos'));
    var max = parseInt($(aas[aas.length - 1]).attr('data-pos'));
    start = Math.max(start, min);
    end = Math.min(end, max);

    // check if the focus id is coherent
    if (jQuery.type(start) === 'number' && jQuery.type(end) === 'number' && start >= min && end <= max) {
        var left = that.findAAByNum(start).position().left + that.trackData.scrollLeft();
        var right = that.findAAByNum(end).position().left + widthAA + that.trackData.scrollLeft();

        if (parseInt(that.inputMin.val()) != start || parseInt(that.inputMax.val()) != end) {
            that.inputMin.val(start);
            that.inputMax.val(end);
        }

        that.focusBlockCurtainLeft
            .css({
                left: 0,
                width: left
            }).show();

        that.focusBlock
            .css('left', left)
            .css('width', right - left)
            .show();

        var width = (PROVIZ.data.settings.ali_end - PROVIZ.data.settings.ali_start) * 10 - right;
        that.focusBlockCurtainRight
            .css({
                left: right,
                width: width
            }).show();


        // animate
        that.center(start, end);

        PROVIZ.options.focus = true;
    } else {
        console.error('wrong values');
    }
};

// center the viz around the selection
Slider.prototype.center = function (min, max) {
    var that = this;
    var aaMiddle = Math.floor((max - min) / 2) + min;
    var middle = that.findAAByNum(aaMiddle).position().left + that.trackData.scrollLeft();
    var overflowWidth = that.trackData.width();
    var positionleft = Math.max(middle - overflowWidth / 2, 0);
    that.trackData.animate({scrollLeft: positionleft}, 'fast');
};


function findRegex(input, dictsequences) {
    var result;
    var listMatches = {};
    var count = 0;
    try {
        var re = new RegExp(input, 'ig');
        $.each(dictsequences, function (index, seq) {
            listMatches[index] = [];
            while ((result = re.exec(seq.trueseq)) !== null) {
                count++;
                listMatches[index].push({start: seq.positions[result.index], end: seq.positions[re.lastIndex - 1]});
            }
        });
    } catch (err) {
        return {matches: listMatches, count: count, error: err.message};
    }
    return {matches: listMatches, count: count};
}

function colorRegex(input, dictsequences) {
    var result = findRegex(input, dictsequences);
    var searchRegexpDiv = $('#searchRegexp');
    if (typeof result.error != "undefined") {
        searchRegexpDiv.addClass('has-error');
        searchRegexpDiv.find('label').attr('data-error', 'Error: ' + result.error);
        return 0;
    } else {
        searchRegexpDiv.removeClass('has-error');
        searchRegexpDiv.find('label').attr('data-error', '');
    }
    var listMatches = result.matches;
    if (typeof result.count != "undefined" && result.count > 0) {
        $('.aaMatched').removeClass('aaMatched');
        colorAA(false);
        $.each(listMatches, function (indexseq, matches) {
            $.each(matches, function (index, match) {
                var start = match.start['gapped'];
                var end = match.end['gapped'] + 1;
                $('#trackData').find('.aliLine:eq(' + indexseq + ')').find('.aC, .aCC').slice(start, end).addClass('aaMatched')
            })
        });
        hidePositionSearch('alignment');
    }
    return result.count;
}

function getVisibleSequenceLines() {
    var visibles = [];
    $('#trackData').find('.aliLine:visible').each(function (i, seq) {
        visibles.push(parseInt($(seq).attr('id').split('_')[1]));
    });
    return visibles;
}

/*
 +SHARE
 + */

// format strings, from https://stackoverflow.com/questions/610406/javascript-equivalent-to-printf-string-format
if (!String.format) {
    String.format = function(format) {
        var args = Array.prototype.slice.call(arguments, 1);
        return format.replace(/{(\d+)}/g, function(match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}

function shareSocial(url) {
    var params = PROVIZ.gatherParams();
    var title = document.title;
    var urlProviz = encodeURIComponent(infos.url_project + 'proviz.php?' + $.param(params));
    var urlShare = String.format(url, urlProviz, title);
    window.open(urlShare, '_blank');
}

function shareTwitter() {
    var url = "https://twitter.com/intent/tweet?url={0}&text={1}";
    shareSocial(url);
}

function shareFacebook() {
    var url = "https://www.facebook.com/sharer.php?u={0}&t={1}";
    shareSocial(url);
}

function shareReddit() {
    var url = "http://reddit.com/submit?url={0}&title={1}";
    shareSocial(url);
}

function shareGooglePlus() {
    var url = "https://plus.google.com/share?url={0}";
    shareSocial(url);
}

function shareLinkedIn() {
    var url = "http://www.linkedin.com/shareArticle?url={0}&title={1}";
    shareSocial(url);
}

function shareEmail() {
    var url = "mailto:?subject={1}&body={0}";
    var params = PROVIZ.gatherParams();
    var title = document.title;
    var urlProviz = encodeURIComponent(infos.url_project + 'proviz.php?' + $.param(params));
    var urlShare = String.format(url, urlProviz, title);
    window.location = urlShare;
}


