<?php

//
//     ProViz - protein visualisation tool
//     Copyright (C) 2016  Norman E. Davey, Peter Jehl, Jean Manguy
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//     Author contact: Norman E. Davey <normandavey@gmail.com>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Peter Jehl <peter.jehl@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Jean Manguy <jean.manguy@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.


header("Cache-Control: max-age=2592000"); // cache 30days
$cachePath = dirname(__FILE__)."/rest/data/";

include_once 'config.php';
$parameters = array(
    'uniprot_acc' => filter_input(INPUT_GET, 'uniprot_acc', FILTER_SANITIZE_STRING),
    'jobId' => filter_input(INPUT_GET, 'jobId', FILTER_SANITIZE_STRING),
    'print' => (filter_input(INPUT_GET, 'print', FILTER_SANITIZE_NUMBER_INT) == '1'),
    'ali_start' => filter_input(INPUT_GET, 'ali_start', FILTER_SANITIZE_NUMBER_INT),
    'ali_end' => filter_input(INPUT_GET, 'ali_end', FILTER_SANITIZE_NUMBER_INT),
    'hideAln' => filter_input(INPUT_GET, 'hideAln', FILTER_SANITIZE_STRING),
    'showAln' => filter_input(INPUT_GET, 'showAln', FILTER_SANITIZE_STRING),
    'alignment' => filter_input(INPUT_GET, 'alignment', FILTER_SANITIZE_STRING),
    'enable' => filter_input(INPUT_GET, 'enable', FILTER_SANITIZE_STRING),
    'disable' => filter_input(INPUT_GET, 'disable', FILTER_SANITIZE_STRING),
    'disable_features' => filter_input(INPUT_GET, 'disable_features', FILTER_SANITIZE_NUMBER_INT),
    'collapse' => filter_input(INPUT_GET, 'collapse', FILTER_SANITIZE_STRING),
    'output' => filter_input(INPUT_GET, 'output', FILTER_SANITIZE_STRING),
    'genetree_mode' => filter_input(INPUT_GET, 'genetree_mode', FILTER_SANITIZE_STRING),
    'gapped' => filter_input(INPUT_GET, 'gapped', FILTER_SANITIZE_NUMBER_INT),
    'condensed' => filter_input(INPUT_GET, 'condensed', FILTER_SANITIZE_NUMBER_INT),
    'regexp' => filter_input(INPUT_GET, 'regexp', FILTER_SANITIZE_STRING),
    'focus_start' => filter_input(INPUT_GET, 'focus_start', FILTER_SANITIZE_NUMBER_INT),
    'focus_end' => filter_input(INPUT_GET, 'focus_end', FILTER_SANITIZE_NUMBER_INT),
    'tutorial' => filter_input(INPUT_GET, 'tutorial', FILTER_SANITIZE_NUMBER_INT),
    'tools' => filter_input(INPUT_GET, 'tools', FILTER_SANITIZE_STRING),
);

if (isset($parameters['uniprot_acc'])) {
    // check if the id is valid
    $uniprot_acc = $parameters['uniprot_acc'];
    $disableArr = $parameters['disable_features'];
    
    include dirname(__FILE__)."/rest/options.php";
    include dirname(__FILE__)."/rest/loader/uniprot_loader.php";
    include dirname(__FILE__)."/rest/util.php";
    if (!file_exists($cachePath . "uniprot")) {
        mkdir($cachePath . "uniprot");
    }
    $uniprotPath = $cachePath . "uniprot/" . $uniprot_acc . ".xml";
    if (!file_exists($uniprotPath) || (filesize($uniprotPath) == 0)) {
        $url = "http://www.uniprot.org/uniprot/" . $uniprot_acc . ".xml";
        if (get_headers($url)[0] !== "HTTP/1.1 404 Not Found") {
            $contents = fileReader($url);
            if($contents == ""){
                header("HTTP/1.0 404 Not Found");
                echo "Uniprot id not found";
                die();
            }
            fileSaver($uniprotPath, $contents);
        } else {
            header("HTTP/1.0 404 Not Found");
            echo "Uniprot id not found";
            die();
        }
    } else {
        $contents = fileReader($uniprotPath);
    }
    $uniLoad = new uniprot_loader($contents, $uniprot_acc, $disableArr);
    $nameProtein = $uniLoad->getUniprotName();
    $nameGene = $uniLoad->getGeneName();
    $functionProtein = $uniLoad->getFunctionProtein();
    $title = $config['name'] . ' - ' . $nameProtein . ' - ' . $nameGene . ' - ' . $uniprot_acc;

} else if (isset($parameters['jobId'])) {
    $title = $config['name'] . ' - ' . $parameters['jobId'];
} else {
    $title = $config['name'];
    header('HTTP/1.0 404 Not Found');
    echo "At least one valid parameter is required: 'uniprot_acc' or 'jobId'";
    exit();
}
$page = 'proviz';
$infos = array(
    'url_project' => $config['url project'],
    'handle_twitter' => $config['handle twitter'],
);

$canon_url = $config['url project'] . 'proviz.php?uniprot_acc=' . $parameters['uniprot_acc'];

?>

<!DOCTYPE html>
<html lang="en-IE">
<head>
    <?php if (!$parameters['print']): include 'php/meta.php'; endif; ?>
    <?php include 'php/styles.php' ?>
</head>
<body>
<?php if (!$parameters['print']): include "php/cookie_banner.php"; endif; ?>
<div id="uniprot_acc" style="display: none" data-length=""></div>
<section>
    <?php
    if (!$parameters['print']): include "php/loading_spinner.php"; endif;
    if (!$parameters['print']): include "php/sidebar.php"; endif;
    if (!$parameters['print']): include "php/header_visualisation.php"; endif;
    ?>
    <div class="visualisationContainer">
        <?php if (!$parameters['print']): include "php/header_data_container.php"; endif; ?>
        <div class="featureContainer">
            <div id="trackInfo"></div>
            <div id="trackData"></div>
            <?php if (!$parameters['print']): include "php/meta_uniprot.php"; endif; ?>
        </div>
    </div>

</section>
<?php include "php/footer.php"; ?>
<?php //include "php/tutorial_proviz.php"; ?>
<?php include "php/javascripts.php"; ?>
<script>
    var cookie = $('.cookie-not-accept');
    if (cookie.length > 0) {
        cookie.bind('click', nocomply);
        $('html, body').click(comply).scroll(comply);
    }
    var cookie_compliance = cookie_compliance || false;
    var parameters = <?php echo json_encode($parameters); ?>;
    var infos = <?php echo json_encode($infos); ?>;
    $(function () {
        PROVIZ.loadData(parameters);
    });
</script>
<?php if (!$parameters['print']): include "php/tracking.php"; endif; ?>
</body>
</html>

