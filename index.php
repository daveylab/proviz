<?php
header("Cache-Control: max-age=2592000"); // cache 30days
include_once 'config.php';
$title = $config['name'];
$page = 'home';
$inputPageToLoad = filter_input(INPUT_GET, 'page', FILTER_SANITIZE_STRING);

// sanitize page

$accepttedValues = ["help", "about", "home","contact","feedback","cookies","funding","eula"];
if (isset($inputPageToLoad) && in_array($inputPageToLoad, $accepttedValues)) {
    $page = $inputPageToLoad;
    $title = "ProViz - " . $page;
} else {
    $page = "home";
    $title = $config['name'] . " - protein interactive exploration tool";
}
$canon_url = $config['url project'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once 'php/meta.php' ?>
    <?php include 'php/styles.php' ?>
</head>
<body>
<?php include "php/cookie_banner.php" ?>

<div class="container">
    <?php
    include("./php/header.php");

    include("./php/{$page}.php");

    include("./php/footer.php");
    ?>
</div>

<?php include "php/javascripts.php"?>
<script>
    if ($('.cookie-not-accept').length > 0) {
        $('.cookie-not-accept').bind('click', nocomply);
        $('html, body').click(comply).scroll(comply);
    }
    var cookie_compliance = cookie_compliance || false;
    $(document).ready(function () {
        $('#cookies').show();
        jQuery.support.cors = true;

        $(document).tooltip(
            {
                track: true,
                tooltipClass: 'customTooltip',
                hide: false,
                content: function () {
                    return $(this).prop('title');
                }
            }
        );
        <?php if ($page == "home"): ?>
        INDEX.index();
        <?php endif; ?>
    });
    var tour = loadTour('home');
    tour.init(true);


</script>
<?php if ($page == "home" && isset($_GET['tutorial']) && $_GET['tutorial'] == 1): ?>
    <script>
        $(document).ready(function () {
            tour.start(true);
        });
    </script>
<?php endif; ?>
<?php include_once("php/tracking.php") ?>
</body>
</html>

