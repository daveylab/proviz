/*global $:false, jQuery:false */


// Copyright (C) 2016 Norman E. Davey <normandavey@gmail.com>, Jean Manguy <jean.manguy@ucdconnect.ie> and Peter Jehl <peter.jehl@ucdconnect.ie>
// This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
// Public License as published by the Free Software Foundation; either version 2.1 of the License, or (at your option)
// any later version.
// 
// This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
// 
// You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
// the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Author contact: Norman E. Davey <normandavey@gmail.com> / Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
// Author contact: Jean Manguy <jean.manguy@ucdconnect.ie> / Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
// Author contact: Peter Jehl <peter.jehl@ucdconnect.ie> / Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.


var proviz_location = "./proviz.php";
// namespace
;
INDEX = {
	// main
	index: function () {
		
		$("#search").click(function () {
			var searchTerm = $("#searchTerm").val();
			INDEX.getProteinSearchResults(searchTerm);
		});
		
		
		$('#searchTerm').keypress(function (e) {
		  if (e.which === 13) {
			var searchTerm = $("#searchTerm").val();
			INDEX.getProteinSearchResults(searchTerm);
		  }
		});
                
		$("#alignment_input_button").click(function(){
				var alignment = $("#alignment_input").val();
				console.log(alignment);
				INDEX.createJobId(alignment.replace(/\n/g, "\\n"));
		});
		
		$("#alignment_input").keypress(function(e){
		  if(e.which === 13){
				var alignment = $("#alignment_input").val();
				INDEX.createJobId(alignment.replace(/\n/g, "\\n"));
		  }
		});
		
		$("#alignment_file_input_button").change(function(){
				var file = $(":file")[0].files[0];
				if(file){
					var reader = new FileReader();
					reader.readAsText(file);
					reader.onload = function(e) {
						INDEX.createJobId(e.target.result.replace(/\n/g, "\\n"));
					};
				}
		});

		INDEX.createVisitedList();
		
	},
        
	// COOKIE STUFF
	createVisitedList: function () {
		proteins = Cookies.getJSON();
		$("#visited").html("");
		
		var outStr = "";
		var protein_count = 0;
		
		for (var proteinAcc in proteins) {
			if (proteins[proteinAcc].hasOwnProperty('protein_name')) 
				{
				var tmpStr = '<div class="visitedList" id="' + proteinAcc + '">';
				tmpStr = tmpStr + '<div class="visitedListDivs">';
				tmpStr = tmpStr + '<a href="' +  proviz_location + "?uniprot_acc=" + proteinAcc + '&tools=degradation"  class="button showProteinCookie" id="' + proteinAcc + '">view</a>';
				tmpStr = tmpStr + '</div>';
				tmpStr = tmpStr + '<div class="visitedListDivs proteinName">';
				tmpStr = tmpStr + '<a target="_blank" href="http://www.uniprot.org/uniprot/' + proteinAcc + '">' + proteins[proteinAcc].protein_name + "</a> ";
				tmpStr = tmpStr + '<br><small>' + proteins[proteinAcc].species + '</small>' + " ";
				tmpStr = tmpStr + '</div>';
				tmpStr = tmpStr + '<div class="deleteCookieDiv">';
				tmpStr = tmpStr + '<a class="deleteCookie" id="' + proteinAcc + '"><i class="fa fa-times"></i></a> ';
				tmpStr = tmpStr + '</div>';
				tmpStr = tmpStr + "</div>";
		
				outStr = tmpStr + outStr;
				
				protein_count++;
				}
		}

		if (protein_count > 0)
			{
			outStr = '<label>Recent</label>' + outStr;
			outStr = outStr + '<div><small><a class="deleteAllCookie">reset</a></small></div>';
			$("#visited").append(outStr);
			}
		else
			{
			outStr = '<label>Example</label>';
			outStr = outStr + '<div class="visitedList" id="Q08981"><div class="visitedListDivs"><a href="./proviz.php?uniprot_acc=Q08981&tools=degradation&alignment=4892" class="button showProteinCookie" id="Q08981">view</a></div><div class="visitedListDivs proteinName"><a target="_blank" href="http://www.uniprot.org/uniprot/Q08981">APC/C-CDH1 modulator 1</a> <br><small>Saccharomyces cerevisiae (strain ATCC 204508 / S288c)</small> </div><div class="deleteCookieDiv"><a class="deleteCookie" id="Q08981"><i class="fa fa-times"></i></a> </div></div>';
			
			$("#visited").append(outStr);
			}
			
		//listeners
		$(".deleteCookie").click(function () {
			var protein_acc = $(this).attr("id");
			INDEX.removeProteinCookie(protein_acc);
		});
		
			$(".deleteAllCookie").click(function () {
			INDEX.removeAllProteinCookies();
			$("#visited").hide();
		});
		
		
	},
	
	addProteinCookie: function (protein_acc, protein_name, protein_species) {
		Cookies.set(protein_acc, {"protein_name":protein_name, "protein_species":protein_species });
	},
	removeProteinCookie: function (protein_acc) {
		Cookies.remove(protein_acc);
		$("#" + protein_acc + ".visitedList").remove();
	},
	removeAllProteinCookies: function () {
		proteins = Cookies.getJSON();
	
		for (var proteinAcc in proteins) {
			Cookies.remove(proteinAcc);
			$("#" + proteinAcc + ".visitedList").remove();
			}
	},
	
	// SEARCH STUFF
	showSearchResults: function (searchTermResults) {
		var proteins = searchTermResults.split("\n");
		
		outStr = "";
	
		for(i = 1; i < proteins.length -1; i++)
		{
			var proteinBits = proteins[i].split("\t");
			var proteinAcc = proteinBits[0];
			var proteinName = proteinBits[1].split("(");
			var geneBits = proteinBits[2].split(" ");
			
			proteinName = proteinName[0];
			
			geneName = geneBits[0]
			
			var species = proteinBits[3];
			var taxon_id  = proteinBits[4];
			
			var tmpStr = '<div class="searchList" id="' + proteinAcc + '">';
			tmpStr = tmpStr + '<div class="searchListDivs">';
			tmpStr = tmpStr + '<a href="' +  proviz_location +  "?uniprot_acc=" + proteinAcc + '&tools=degradation" class="showProteinSearch">view</a>';
			tmpStr = tmpStr + '</div>';
			tmpStr = tmpStr + '<div class="searchListDivs">';
			tmpStr = tmpStr + '<a target="_blank" href="http://www.uniprot.org/uniprot/' + proteinAcc + '">' + proteinName  + " (" + geneName  + ')' + "</a> ";
			tmpStr = tmpStr + '</div>';
			tmpStr = tmpStr + '<div class="searchListDivs">';
			tmpStr = tmpStr + '<small>' + species + '</small>' + " ";
			tmpStr = tmpStr + '</div>';
			tmpStr = tmpStr + "</div>";
		
			outStr = outStr + tmpStr;
			
		} 
		
		outStr = outStr + '<small>If the protein/species of interest is not present in the list above please search directly on UniProt and use the UniProt accession in the form above</small>'
		
		$("#searchResults").html(outStr);
	},
        
	getProteinSearchResults: function (searchTerm) {
	
		$("#searchResults").html('Searching <i class="fa fa-cog fa-3 fa-spin"></i>').delay(10).queue(function(next){
			searchTerm = encodeURIComponent(searchTerm);
			var uniprot_url = 'http://www.uniprot.org/uniprot/?query=(name%3A' + searchTerm + '+OR+gene%3A' + searchTerm + '+OR+mnemonic%3A' + searchTerm + ')+AND+keyword%3A"Reference+proteome+%5BKW-1185%5D"&format=tab&columns=id,protein%20names,genes,organism,organism-id&sort=score&limit=25';
		
			$.ajax({
			type: "GET",
			url: uniprot_url,
			dataType: "text",
			success: function(uniprot_data){ 
				INDEX.showSearchResults(uniprot_data);
			},
			error: function(error){
				$("#searchResults").html("Error downloading protein data:<br>");
				$("#searchResults").append(JSON.stringify(error, null, 2) + "<br>");
				$("#searchResults").append("Probably an issue with the UniProt search rest server. You can skip this step by getting the UniProt accession and using http://slim.ucd.ie/proviz/proviz.html?uniprot_acc=XXXXXX where XXXXXX is a UniProt accession e.g. P04637");
				
			}
			});
		next();
		
		});
		},
                
	createJobId: function (alignment) {
	
                $.ajax({
                type: "POST",
                url: "./createJob.php",
                dataType: "json",
                data: {"user_alignment" : alignment},
                success: function(jobId){ 
                		console.log(proviz_location + "?jobId=" + jobId);
                        window.location = proviz_location + "?jobId=" + jobId;
                },
                error: function(error){
                        $("#jobError").html("Error downloading job id:<br>");
                        $("#jobError").append(JSON.stringify(error, null, 2) + "<br>");
                        console.log(JSON.stringify(error, null, 2) + "<br>");
                }
                });
            }
};

$(document).ready ( function(){
	INDEX.index();
});