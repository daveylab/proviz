// Copyright (C) 2016 Norman E. Davey <normandavey@gmail.com>, Jean Manguy <jean.manguy@ucdconnect.ie> and Peter Jehl <peter.jehl@ucdconnect.ie>
// This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
// Public License as published by the Free Software Foundation; either version 2.1 of the License, or (at your option)
// any later version.
// 
// This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
// 
// You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
// the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Author contact: Norman E. Davey <normandavey@gmail.com> / Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
// Author contact: Jean Manguy <jean.manguy@ucdconnect.ie> / Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
// Author contact: Peter Jehl <peter.jehl@ucdconnect.ie> / Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.


var validated = {"A1Z6I7":[{"type":"KEN box","start":"7","end":"9","pmid":"17369399"}],"B6K3K8":[{"type":"D box","start":"106","end":"112","pmid":"23786411"},{"type":"D box","start":"72","end":"78","pmid":"23786411"}],"O00429":[{"type":"D box","start":"473","end":"479","pmid":"21325626"}],"O13329":[{"type":"D box-like","start":"417","end":"423","pmid":"24361936"}],"O16810":[{"type":"D box","start":"292","end":"298","pmid":"16195415,22801552,23707760"}],"O43521":[{"type":"D box","start":"76","end":"82","pmid":"24871945"},{"type":"D box","start":"85","end":"91","pmid":"24871945"}],"O43683":[{"type":"ABBA motif","start":"527","end":"532","pmid":"25669885"},{"type":"KEN box","start":"535","end":"537","pmid":"17158872"},{"type":"KEN box","start":"625","end":"627","pmid":"17158872"}],"O48533":[{"type":"D box","start":"119","end":"126","pmid":"22167059"},{"type":"IR tail","start":"258","end":"259","pmid":"22167059"},{"type":"KEN-like","start":"92","end":"95","pmid":"22167059"}],"O59767":[{"type":"KEN box","start":"20","end":"22","pmid":"17406666,17369399"}],"O60566":[{"type":"KEN box","start":"26","end":"28","pmid":"20016069,23091007"},{"type":"KEN box","start":"304","end":"306","pmid":"22193957"},{"type":"ABBA motif","start":"528","end":"533","pmid":"25669885"}],"O75330":[{"type":"D box","start":"659","end":"665","pmid":"20471943"},{"type":"KEN box","start":"696","end":"698","pmid":"20471943"},{"type":"D box-like","start":"702","end":"711","pmid":"20471943"}],"O75496":[{"type":"D box","start":"23","end":"29","pmid":"9635433"}],"O89019":[{"type":"D box","start":"490","end":"496","pmid":"12471060"},{"type":"D box","start":"907","end":"913","pmid":"12471060"}],"O94925":[{"type":"KEN box","start":"656","end":"658","pmid":"23195958"}],"O95997":[{"type":"D box","start":"61","end":"67","pmid":"10411507,11179223,23091007"},{"type":"KEN box","start":"9","end":"11","pmid":"11179223,23091007"}],"P03116":[{"type":"KEN box","start":"28","end":"30","pmid":"11073992"},{"type":"D box","start":"346","end":"352","pmid":"11073992"}],"P04183":[{"type":"KEN box","start":"203","end":"205","pmid":"14701726"}],"P0C2X8":[{"type":"KEN box","start":"353","end":"355","pmid":"12679038"}],"P10815":[{"type":"D box","start":"59","end":"65","pmid":"1846030,8885231"},{"type":"D box","start":"59","end":"65","pmid":"8895572"}],"P11157":[{"type":"KEN box","start":"30","end":"32","pmid":"12655059"}],"P12757":[{"type":"D box","start":"164","end":"170","pmid":"11741538"}],"P13350":[{"type":"D box","start":"36","end":"42","pmid":"1846030,8885231,10733526"}],"P13864":[{"type":"KEN box","start":"647","end":"649","pmid":"15899874"}],"P14635":[{"type":"D box","start":"42","end":"48","pmid":"10559878,11179223,23708605,10733526"}],"P14785":[{"type":"KEN box","start":"13","end":"15","pmid":"17314514"},{"type":"D box","start":"46","end":"52","pmid":"17314514"}],"P15036":[{"type":"D box","start":"20","end":"26","pmid":"19160489"}],"P18606":[{"type":"D box","start":"41","end":"47","pmid":"1846030,8885231"}],"P20248":[{"type":"D box","start":"47","end":"53","pmid":"11285279"},{"type":"ABBA motif","start":"99","end":"104","pmid":"25669885"}],"P20439":[{"type":"D box","start":"37","end":"43","pmid":"7588612"}],"P21135":[{"type":"KEN box","start":"11","end":"13","pmid":"9312055"},{"type":"D box","start":"33","end":"39","pmid":"9312055"},{"type":"D box","start":"52","end":"58","pmid":"9312055"}],"P23497":[{"type":"D box","start":"165","end":"172","pmid":"22086178"}],"P23624":[{"type":"D box","start":"23","end":"29","pmid":"17493939,23707760"}],"P24699":[{"type":"D box","start":"93","end":"99","pmid":"11073992"}],"P24869":[{"type":"KEN box","start":"100","end":"102","pmid":"8020094"},{"type":"D box","start":"25","end":"31","pmid":"11719221"}],"P26309":[{"type":"D box","start":"17","end":"23","pmid":"21118994"},{"type":"KEN-like","start":"545","end":"548","pmid":"21118994"}],"P27895":[{"type":"KEN box","start":"932","end":"934","pmid":"11694576"},{"type":"D box-like","start":"954","end":"962","pmid":"11694576,23707760"}],"P30283":[{"type":"D box","start":"56","end":"62","pmid":"17620341"},{"type":"ABBA motif","start":"99","end":"105","pmid":"25287299"}],"P30304":[{"type":"KEN box","start":"141","end":"143","pmid":"12234927"}],"P30305":[{"type":"KEN box","start":"192","end":"194","pmid":"17599046"}],"P30307":[{"type":"KEN box","start":"151","end":"153","pmid":"11842186"}],"P32325":[{"type":"D box","start":"10","end":"16","pmid":"25287299"},{"type":"D box","start":"62","end":"68","pmid":"10594027,25287299"}],"P32562":[{"type":"D box","start":"17","end":"23","pmid":"9482731,9560342"},{"type":"D box","start":"61","end":"67","pmid":"9482731,9560342"}],"P32789":[{"type":"KEN box","start":"116","end":"118","pmid":"16855400"},{"type":"D box","start":"150","end":"156","pmid":"16855400"}],"P33981":[{"type":"D box","start":"256","end":"262","pmid":"20729194,22430208"}],"P34244":[{"type":"KEN box","start":"775","end":"777","pmid":"11562348"},{"type":"D box","start":"828","end":"834","pmid":"11562348,23708605"}],"P35209":[{"type":"KEN box","start":"461","end":"463","pmid":"22416277"}],"P36630":[{"type":"D box","start":"51","end":"57","pmid":"11163211"}],"P38936":[{"type":"D box","start":"86","end":"92","pmid":"17679094,22045811"}],"P39001":[{"type":"D box","start":"40","end":"46","pmid":"17889668"}],"P40316":[{"type":"D box","start":"85","end":"91","pmid":"11553328,23707760"}],"P41005":[{"type":"D box","start":"34","end":"40","pmid":"18331722"},{"type":"KEN box","start":"7","end":"9","pmid":"18331722"}],"P41134":[{"type":"D box","start":"126","end":"132","pmid":"16810178"}],"P41410":[{"type":"KEN box","start":"26","end":"28","pmid":"18426916"}],"P42261":[{"type":"D box","start":"126","end":"132","pmid":"21186356"},{"type":"D box","start":"43","end":"49","pmid":"21186356"}],"P43633":[{"type":"KEN box","start":"200","end":"202","pmid":"16855400"},{"type":"D box","start":"224","end":"230","pmid":"16855400"}],"P47074":[{"type":"KEN box","start":"30","end":"32","pmid":"17406666"},{"type":"D box","start":"429","end":"435","pmid":"17369399"}],"P49454":[{"type":"KEN box","start":"3125","end":"3127","pmid":"20053638"}],"P50082":[{"type":"D box","start":"296","end":"302","pmid":"23816140"},{"type":"KEN-like","start":"345","end":"348","pmid":"23816140"}],"P50275":[{"type":"D box","start":"760","end":"766","pmid":"9036857"}],"P51955":[{"type":"D box","start":"361","end":"369","pmid":"11742988"},{"type":"KEN box","start":"391","end":"393","pmid":"11742988,10733526"},{"type":"Region","start":"420","end":"445","pmid":"11742988"},{"type":"IR tail","start":"444","end":"445","pmid":"11742988"}],"P52732":[{"type":"KEN box","start":"1022","end":"1024","pmid":"24751481"},{"type":"D box","start":"1047","end":"1053","pmid":"24751481"}],"P53350":[{"type":"D box","start":"337","end":"343","pmid":"14734534"}],"P53718":[{"type":"D box","start":"7","end":"13","pmid":"21562221"}],"P54792":[{"type":"D box","start":"409","end":"415","pmid":"19805045"}],"Q01094":[{"type":"Region","start":"358","end":"437","pmid":"22580462"}],"Q02363":[{"type":"D box","start":"100","end":"106","pmid":"16810178"}],"Q03898":[{"type":"D box","start":"7","end":"13","pmid":"17173039"}],"Q04116":[{"type":"KEN box","start":"329","end":"331","pmid":"20526282"},{"type":"D box","start":"332","end":"338","pmid":"21562221"},{"type":"D box","start":"340","end":"346","pmid":"21562221"}],"Q06266":[{"type":"KEN box","start":"365","end":"367","pmid":"23049888"},{"type":"D box","start":"414","end":"420","pmid":"23049888"}],"Q07820":[{"type":"D box","start":"207","end":"213","pmid":"18758239"}],"Q08050":[{"type":"D box","start":"13","end":"19","pmid":"18758239"},{"type":"D box","start":"15","end":"21","pmid":"18758239"},{"type":"KEN box","start":"207","end":"209","pmid":"18498748"},{"type":"KEN box","start":"207","end":"209","pmid":"18758239"},{"type":"D box","start":"6","end":"12","pmid":"18758239"}],"Q08490":[{"type":"D box","start":"486","end":"492","pmid":"25236599"}],"Q08981":[{"type":"D box","start":"119","end":"125","pmid":"18498748,18519589,18596038,23707760"},{"type":"ABBA motif","start":"61","end":"66","pmid":"23707760"},{"type":"D box","start":"8","end":"14","pmid":"18498748,18519589,18596038,23707760"},{"type":"KEN-like","start":"90","end":"92","pmid":"23707760"},{"type":"KEN box","start":"98","end":"100","pmid":"18498748,18519589,18596038,23707760"}],"Q12280":[{"type":"Region","start":"1","end":"42","pmid":"17942599"}],"Q12834":[{"type":"KEN box","start":"97","end":"99","pmid":"10733526"}],"Q13309":[{"type":"D box","start":"3","end":"9","pmid":"15014502,15014503"}],"Q14807":[{"type":"KEN box","start":"504","end":"506","pmid":"17726374"}],"Q15004":[{"type":"KEN box","start":"78","end":"80","pmid":"21628590"}],"Q15303":[{"type":"D box","start":"992","end":"998","pmid":"17638867"}],"Q15398":[{"type":"D box","start":"?","end":"?","pmid":"20471943"},{"type":"D box","start":"26","end":"32","pmid":"20471943"},{"type":"KEN box","start":"32","end":"34","pmid":"20471943"}],"Q15468":[{"type":"KEN box","start":"1243","end":"1245","pmid":"24485834"}],"Q15784":[{"type":"D box","start":"213","end":"219","pmid":"19900895"}],"Q16875":[{"type":"KEN box","start":"142","end":"144","pmid":"19448625,24169697"}],"Q20402":[{"type":"D box","start":"130","end":"136","pmid":"17869113"},{"type":"D box","start":"96","end":"102","pmid":"17869113"}],"Q23973":[{"type":"D box-like","start":"21","end":"27","pmid":"24019759"}],"Q24454":[{"type":"D box","start":"30","end":"36","pmid":"12724352"},{"type":"KEN box","start":"7","end":"9","pmid":"12724352"}],"Q38819":[{"type":"D box","start":"55","end":"61","pmid":"16415207"}],"Q4KLP7":[{"type":"KEN box","start":"7","end":"9","pmid":"21059905"}],"Q563C3":[{"type":"KEN box","start":"85","end":"87","pmid":"15837422"}],"Q5FBB7":[{"type":"KEN box","start":"310","end":"312","pmid":"19015261"},{"type":"D box","start":"438","end":"444","pmid":"19015261"}],"Q6K1Z8":[{"type":"D box","start":"63","end":"69","pmid":"21505434"}],"Q6UVJ0":[{"type":"KEN box","start":"589","end":"591","pmid":"17681132"}],"Q7JUM5":[{"type":"D box","start":"36","end":"42","pmid":"24972868"}],"Q7TS74":[{"type":"KEN box","start":"183","end":"185","pmid":"24260314"}],"Q84MM9":[{"type":"D box","start":"51","end":"57","pmid":"22434193"}],"Q86T82":[{"type":"KEN box","start":"782","end":"784","pmid":"21596315"}],"Q86WB0":[{"type":"D box","start":"314","end":"320","pmid":"22205987"},{"type":"D box","start":"402","end":"408","pmid":"22205987"}],"Q86XJ1":[{"type":"D box","start":"610","end":"616","pmid":"23469016"}],"Q8IWB6":[{"type":"D box","start":"533","end":"539","pmid":"22405274"}],"Q8IWF2":[{"type":"D box","start":"246","end":"252","pmid":"20972601"},{"type":"KEN box","start":"460","end":"462","pmid":"20972601"},{"type":"D box","start":"582","end":"588","pmid":"20972601"}],"Q8IWQ3":[{"type":"KEN box","start":"603","end":"605","pmid":"23029325"},{"type":"KEN box","start":"603","end":"605","pmid":"23029325"}],"Q8NI77":[{"type":"IR tail","start":"877","end":"878","pmid":"23288039"}],"Q8R080":[{"type":"KEN box","start":"731","end":"733","pmid":"10733526"}],"Q8WWK9":[{"type":"KEN box","start":"81","end":"83","pmid":"17376772"}],"Q8WWL7":[{"type":"D box","start":"60","end":"66","pmid":"12185076"}],"Q91820":[{"type":"KEN box","start":"6","end":"8","pmid":"12208850,11964384"}],"Q969U6":[{"type":"D box","start":"303","end":"309","pmid":"21725316"}],"Q96GD4":[{"type":"D box","start":"315","end":"321","pmid":"16204042"},{"type":"KEN box","start":"4","end":"6","pmid":"15923616"}],"Q96KQ7":[{"type":"Region","start":"772","end":"1296","pmid":"22178396"}],"Q96RL1":[{"type":"D box","start":"255","end":"261","pmid":"22426463"}],"Q96RU7":[{"type":"D box","start":"195","end":"201","pmid":"20064487"}],"Q99618":[{"type":"D box","start":"198","end":"204","pmid":"12679038"},{"type":"KEN box","start":"258","end":"260","pmid":"12679038"}],"Q99728":[{"type":"D box","start":"112","end":"118","pmid":"20471943"},{"type":"D box","start":"38","end":"44","pmid":"20471943"}],"Q99741":[{"type":"D box","start":"56","end":"62","pmid":"16153703"},{"type":"KEN box","start":"81","end":"83","pmid":"16153703"}],"Q9BSJ6":[{"type":"D box","start":"53","end":"61","pmid":"18757745"}],"Q9BW19":[{"type":"D box","start":"5","end":"11","pmid":"24510915"}],"Q9BXS6":[{"type":"KEN box","start":"384","end":"386","pmid":"20471943,17618083"},{"type":"D box","start":"433","end":"439","pmid":"20471943"}],"Q9CXH7":[{"type":"D box","start":"428","end":"434","pmid":"19015261"}],"Q9D1C1":[{"type":"D box","start":"129","end":"135","pmid":"10930472"}],"Q9FMH5":[{"type":"D box","start":"35","end":"41","pmid":"9836745"}],"Q9H467":[{"type":"KEN box","start":"93","end":"95","pmid":"23776205"}],"Q9H9B1":[{"type":"Region","start":"426","end":"922","pmid":"22178396"}],"Q9I7I0":[{"type":"D box","start":"75","end":"81","pmid":"7588612"}],"Q9I869":[{"type":"KEN-like","start":"561","end":"564","pmid":"12773557,23707760"}],"Q9JMB7":[{"type":"D box","start":"218","end":"224","pmid":"23328397"}],"Q9LD02":[{"type":"D box","start":"31","end":"37","pmid":"15004270,9836745"}],"Q9LGE3":[{"type":"D box","start":"73","end":"81","pmid":"18701670"}],"Q9LJG6":[{"type":"KEN-like","start":"14","end":"16","pmid":"24206843"},{"type":"D box","start":"46","end":"52","pmid":"24206843"}],"Q9M2R1":[{"type":"D box","start":"104","end":"110","pmid":"22844260"},{"type":"IR tail","start":"242","end":"243","pmid":"22844260"},{"type":"KEN-like","start":"80","end":"83","pmid":"22844260"}],"Q9NQW6":[{"type":"D box","start":"16","end":"22","pmid":"16040610"},{"type":"D box","start":"41","end":"47","pmid":"16040610"}],"Q9NYD6":[{"type":"D box","start":"177","end":"183","pmid":"12853486"},{"type":"D box","start":"320","end":"326","pmid":"12853486"}],"Q9P2P5":[{"type":"D box","start":"740","end":"746","pmid":"24163370"}],"Q9UKT4":[{"type":"D box","start":"322","end":"328","pmid":"16921029"},{"type":"D box","start":"322","end":"328","pmid":"23708605"},{"type":"LRRL tail","start":"441","end":"447","pmid":"23708001,23708605"}],"Q9ULW0":[{"type":"KEN box","start":"87","end":"89","pmid":"16287863"}],"Q9URT4":[{"type":"KEN box","start":"542","end":"544","pmid":"20921411"},{"type":"KEN box","start":"575","end":"577","pmid":"20921411"}],"Q9Y2I6":[{"type":"KEN box","start":"495","end":"497","pmid":"17403670"},{"type":"D box","start":"633","end":"639","pmid":"17403670"}],"Q9Y2M0":[{"type":"D box","start":"14","end":"20","pmid":"22854063"},{"type":"KEN box","start":"212","end":"214","pmid":"22854063"}]};

var dbox = {"0":{"-":0,"S":0.19,"T":-0.48,"G":-0.25,"A":0,"P":0.27,"C":-3.35,"I":-0.22,"L":-0.2,"V":-0.09,"M":0.41,"F":-1.05,"H":-0.97,"Y":0.44,"W":1.03,"K":1.79,"R":1.26,"D":-0.64,"E":0.08,"N":-0.19,"Q":0.05},
"1":{"-":0,"S":0.38,"T":-0.1,"G":-0.33,"A":-1.59,"P":2.23,"C":-3.85,"I":-1.71,"L":0.86,"V":-0.97,"M":-0.58,"F":1.13,"H":0.03,"Y":0.59,"W":-3.9,"K":1.23,"R":0.31,"D":-1.18,"E":0.07,"N":-0.29,"Q":0.41},
"2":{"-":0,"S":-4.79,"T":-8.01,"G":-9.25,"A":-4.44,"P":-9.08,"C":-10.4,"I":-10.1,"L":-9.23,"V":-9.53,"M":-8.34,"F":-9.87,"H":-1.43,"Y":-8.62,"W":-9.78,"K":-2.47,"R":8.05,"D":-8.55,"E":-6.94,"N":-7.31,"Q":-3.01},
"3":{"-":0,"S":0.79,"T":1.39,"G":-2.22,"A":0.43,"P":-2.82,"C":2.47,"I":0.17,"L":1.5,"V":-1.54,"M":1.04,"F":-1.21,"H":0.21,"Y":-0.75,"W":-4.98,"K":1.94,"R":1.41,"D":-1.21,"E":-1.88,"N":-0.56,"Q":-1.03},
"4":{"-":0,"S":-0.02,"T":-1.15,"G":-1.54,"A":5.28,"P":5.28,"C":3.22,"I":-1.15,"L":-0.09,"V":-0.67,"M":-3.89,"F":1.72,"H":-1.18,"Y":-4.32,"W":2,"K":0.22,"R":0.14,"D":-4.14,"E":-1.07,"N":-1.42,"Q":-1.6},
"5":{"-":0,"S":-9.9,"T":-8.59,"G":-11.2,"A":-8.88,"P":-10.4,"C":-8.69,"I":-5.66,"L":8.66,"V":-6.45,"M":-5.15,"F":3.77,"H":-10.2,"Y":-7.92,"W":-8.79,"K":-9.94,"R":-9.62,"D":-11.2,"E":-10.4,"N":-10.9,"Q":-9.62},
"6":{"-":0,"S":0.54,"T":-0.01,"G":0.8,"A":-0.17,"P":0.9,"C":-0.6,"I":-0.89,"L":-1.01,"V":-0.15,"M":-0.94,"F":-0.96,"H":0.82,"Y":-0.77,"W":-3.37,"K":1.14,"R":0.61,"D":-0.6,"E":0,"N":0.1,"Q":0.92},
"7":{"-":0,"S":-0.14,"T":-0.33,"G":-1.74,"A":-0.82,"P":-0.11,"C":-4.56,"I":1.75,"L":-0.29,"V":-0.25,"M":-1.55,"F":-4.43,"H":-1.01,"Y":-4.04,"W":3.21,"K":-0.96,"R":-0.3,"D":4.37,"E":4.37,"N":0.49,"Q":0.5},
"8":{"-":0,"S":-0.84,"T":0.2,"G":-3.94,"A":-0.12,"P":-1.71,"C":1.25,"I":9.299999999999999,"L":9.299999999999999,"V":9.299999999999999,"M":0.47,"F":-0.02,"H":-4.98,"Y":0.01,"W":-5.71,"K":1.5,"R":-1.67,"D":-2.39,"E":-2.57,"N":-2.81,"Q":0.3},
"9":{"-":0,"S":1.28,"T":1.69,"G":-2.09,"A":-1.47,"P":-0.18,"C":2.82,"I":-0.7,"L":0.58,"V":-0.64,"M":0.64,"F":-4.76,"H":-3.47,"Y":-4.49,"W":-5.51,"K":0.05,"R":-1.27,"D":-0.25,"E":-0.24,"N":3.22,"Q":0.17},
"10":{"-":0,"S":-0.49,"T":-1.44,"G":-1.7,"A":0.36,"P":-1.02,"C":-5.34,"I":0.51,"L":0.33,"V":-2.13,"M":-1.32,"F":-5.42,"H":0.47,"Y":-4.91,"W":-6.2,"K":-0.47,"R":0.39,"D":0.73,"E":-0.35,"N":4.72,"Q":-0.17},
"11":{"-":0,"S":0.54,"T":0.08,"G":-1.54,"A":-0.79,"P":-0.6,"C":0.41,"I":-1.29,"L":-0.15,"V":0.51,"M":-1.14,"F":0.95,"H":0.14,"Y":0.52,"W":-4.38,"K":0.98,"R":-0.31,"D":0.52,"E":-0.52,"N":2.63,"Q":-0.39}};

var abba = {
"0":{"-":0,"S":0.19,"T":-0.66,"G":-1.11,"A":2.25,"P":3.55,"C":-2,"I":-2.44,"L":-2.36,"V":-1.56,"M":-1.39,"F":-3.21,"H":-1.18,"Y":-2.2,"W":-3.05,"K":2.03,"R":0.12,"D":-1.34,"E":0.14,"N":-1.1,"Q":2.27},
"1":{"-":0,"S":-2.5,"T":-2.43,"G":-3.58,"A":-2.6,"P":-3.88,"C":-3.24,"I":-1.09,"L":-0.43,"V":-1.68,"M":-0.7,"F":7.74,"H":-1.48,"Y":2.14,"W":0.01,"K":-1.33,"R":2.25,"D":-3.61,"E":-2.75,"N":-2.78,"Q":-2.11},
"2":{"-":0,"S":0,"T":0,"G":0,"A":0,"P":0,"C":0,"I":0,"L":0,"V":0,"M":0,"F":0,"H":0,"Y":0,"W":0,"K":0,"R":0,"D":0,"E":0,"N":0,"Q":0},
"3":{"-":0,"S":-2.71,"T":-1.01,"G":-4.2,"A":-1.49,"P":-3.23,"C":-1.55,"I":4.98,"L":2.43,"V":3.01,"M":1.07,"F":-0.45,"H":-3.69,"Y":-1.66,"W":-2.92,"K":-3.07,"R":-3.25,"D":-3.8,"E":-3.53,"N":-3.76,"Q":-3.04},
"4":{"-":0,"S":-2.29,"T":-2.35,"G":-3.49,"A":-2.45,"P":-3.65,"C":-3.1,"I":-1.52,"L":-1.03,"V":-1.82,"M":-1.08,"F":6.03,"H":4.33,"Y":6.77,"W":0.93,"K":-2.53,"R":-2.24,"D":-3.35,"E":-2.45,"N":-2.15,"Q":-1.95},
"5":{"-":0,"S":-0.35,"T":-0.72,"G":-1.89,"A":-1.26,"P":-1.87,"C":-3.02,"I":-1.2,"L":-2.24,"V":0.41,"M":-1.73,"F":-2.98,"H":-0.65,"Y":-2.37,"W":-3.7,"K":-0.27,"R":-1.01,"D":2.98,"E":3.25,"N":2.87,"Q":0.5},
"6":{"-":0,"S":-0.46,"T":-1.29,"G":-1.97,"A":-1.72,"P":-1.68,"C":-4.07,"I":-3.66,"L":-3.81,"V":-3.33,"M":-3.06,"F":-3.9,"H":-0.97,"Y":-3.07,"W":-4.14,"K":-0.26,"R":-1.23,"D":5.38,"E":3.73,"N":0.6,"Q":0.58}};

var ken = {
"0":{"-":0,"S":0.56,"T":-3.1,"G":2.12,"A":-0.84,"P":-1.71,"C":-5.15,"I":-5.01,"L":-1.63,"V":-1.67,"M":-4.3,"F":-5.25,"H":-3.26,"Y":0.14,"W":-5.58,"K":-1.69,"R":-3.48,"D":7.37,"E":7.37,"N":7.37,"Q":1.42},
"1":{"-":0,"S":-5.46,"T":-6,"G":-6.9,"A":-6.15,"P":-6.45,"C":-8.6,"I":-8.23,"L":-8,"V":-7.79,"M":-6.84,"F":-8.61,"H":-5.96,"Y":-7.3,"W":-8.54,"K":7.57,"R":-0.45,"D":-0.01,"E":-1.95,"N":-1.03,"Q":-3.95},
"2":{"-":0,"S":-5.71,"T":-6.5,"G":-7.86,"A":-6.5,"P":-6.77,"C":-9.5,"I":-9.04,"L":-8.66,"V":-8.22,"M":-7.73,"F":-9.04,"H":-5.68,"Y":-7.76,"W":-8.65,"K":-4.71,"R":-5.68,"D":-3.91,"E":7.35,"N":-5.85,"Q":-3.53},
"3":{"-":0,"S":-5.42,"T":-6.13,"G":-6.54,"A":-7.75,"P":-8.26,"C":-8.98,"I":-9.59,"L":-9.76,"V":-9.21,"M":-8.42,"F":-9.34,"H":-5.44,"Y":-8.35,"W":-10.1,"K":-6.27,"R":-6.56,"D":-4.69,"E":-6.37,"N":9.25,"Q":-6.07},
"4":{"-":0,"S":-0.42,"T":-1.07,"G":0.73,"A":0.05,"P":-1.91,"C":3.91,"I":5.13,"L":5.13,"V":5.13,"M":5.13,"F":-0.61,"H":-0.53,"Y":-0.51,"W":-3.91,"K":-0.99,"R":-0.69,"D":0.07,"E":0.08,"N":-0.1,"Q":0.29},
"5":{"-":0,"S":-0.31,"T":-0.1,"G":0.71,"A":-0.56,"P":3.03,"C":2,"I":-0.28,"L":-0.91,"V":-1.1,"M":0.17,"F":2.02,"H":1.42,"Y":1.7,"W":-3.5,"K":-0.6,"R":-1.63,"D":-0.11,"E":-0.75,"N":-0.14,"Q":-0.29},
"6":{"-":0,"S":-1.52,"T":-3.43,"G":-2.51,"A":-1.49,"P":3.83,"C":-4.86,"I":2.45,"L":1.58,"V":0.3,"M":-3.01,"F":3.82,"H":-4.05,"Y":1.24,"W":-4.73,"K":0.88,"R":-1.43,"D":-1.73,"E":-0.46,"N":-2.2,"Q":-0.65}};

var name_mapping = {"dbox":"D box","ken":"KEN box","abba":"ABBA motif"};
var pwms = {"dbox":dbox,"ken":ken,"abba":abba};
//var motif_colour = {"dbox":"#FAA0A0","ken":"#A0A0FA","abba":"#A0FAA0"};
var motif_colour = {"dbox":"green","ken":"blue","abba":"red"};

var ics = {
"dbox":[ 0.09305260073527055, 0.11922911353305943, 0.9098082712677107, 0.14556349203827856, 0.20770123221897696, 0.9455813755676629, 0.0914317563436513, 0.13281071913734532, 0.16931453535313978, 0.16075868579417862, 0.18400490660836621, 0.07386213371157835, 0.06242774163967224, 0.07244977259549223],
"ken":[  0.2798375194327043, 0.8579598031683351, 1, 1, 0.07476662073658835, 0.0797999420454708, 0.21341602999713583],
"abba":[ 0.555307711583422, 0.832961567375133, 0.4627564263195183, 0.6827919011247691, 0.6478589968473256, 0.555307711583422, 0.7753431863886727]
}

var pwm_max = {"ken":5,"abba":5,"dbox":5};
var similarity_cutoff = {
"abba":{0.00001: 3.2041313684228347, 0.0001: 3.038172011952547, 0.001: 2.322884180394267},
"ken":{0.00001: 4.549516412107698, 0.0001: 4.226631094897414, 0.001: 2.462549438584754},
"dbox":{0.00001: 3.516419844854331, 0.0001: 3.433179365999104, 0.001: 3.0477699122674333}
};

var background = {
"all":{'A': 0.086167, 'C': 0.012759, 'D': 0.052951, 'E': 0.061395, 'F': 0.040392, 'G': 0.071295, 'H': 0.021925, 'I': 0.060238, 'K': 0.052746, 'L': 0.098334, 'M': 0.024703, 'N': 0.041559, 'P': 0.047353, 'Q': 0.038562, 'R': 0.054707, 'S': 0.066961, 'T': 0.056144, 'V': 0.067440, 'W': 0.013183, 'Y': 0.030613},
"0.3":{"A":0.082767,"C":0.007309,"D":0.058283,"E":0.082903,"F":0.022992,"G":0.073166,"H":0.024454,"I":0.043799,"K":0.064038,"L":0.073925,"M":0.023502,"N":0.043715,"P":0.062209,"Q":0.048512,"R":0.064976,"S":0.075305,"T":0.062569,"V":0.059192,"W":0.007122,"Y":0.019261},
"0.5":{"A":0.079609,"C":0.005029,"D":0.057989,"E":0.08815,"F":0.015527,"G":0.075008,"H":0.024831,"I":0.028494,"K":0.06696,"L":0.055605,"M":0.021528,"N":0.047938,"P":0.085282,"Q":0.055837,"R":0.067414,"S":0.0964,"T":0.065369,"V":0.044676,"W":0.004848,"Y":0.013504}
}

var residues = ["S","T","G","A","P","C","I","L","V","M","F","Y","W","H","K","R","D","E","N","Q","-"];
var aas = ["S","T","G","A","P","C","I","L","V","M","F","Y","W","H","K","R","D","E","N","Q"];

var lamda_value = 0.2486767;
var substitution_matrix = {'*': {'*': 1.0, '-': -4, 'A': -4.0, 'C': -4.0, 'B': -4.0, 'E': -4.0, 'D': -4.0, 'G': -4.0, 'F': -4.0, 'I': -4.0, 'H': -4.0, 'K': -4.0, 'M': -4.0, 'L': -4.0, 'N': -4.0, 'Q': -4.0, 'P': -4.0, 'S': -4.0, 'R': -4.0, 'T': -4.0, 'W': -4.0, 'V': -4.0, 'Y': -4.0, 'X': -4.0, 'Z': -4.0}, '-': {'*': -4, '-': -1, 'A': -4, 'C': -4, 'B': -4, 'E': -4, 'D': -4, 'G': -4, 'F': -4, 'I': -4, 'H': -4, 'K': -4, 'M': -4, 'L': -4, 'N': -4, 'Q': -4, 'P': -4, 'S': -4, 'R': -4, 'T': -4, 'W': -4, 'V': -4, 'Y': -4, 'X': -4, 'Z': -4}, 'A': {'*': -4.0, '-': -4, 'A': 4.0, 'C': 0.0, 'B': -2.0, 'E': -1.0, 'D': -2.0, 'G': 0.0, 'F': -2.0, 'I': -1.0, 'H': -2.0, 'K': -1.0, 'M': -1.0, 'L': -1.0, 'N': -2.0, 'Q': -1.0, 'P': -1.0, 'S': 1.0, 'R': -1.0, 'T': 0.0, 'W': -3.0, 'V': 0.0, 'Y': -2.0, 'X': 0.0, 'Z': -1.0}, 'C': {'*': -4.0, '-': -4, 'A': 0.0, 'C': 9.0, 'B': -3.0, 'E': -4.0, 'D': -3.0, 'G': -3.0, 'F': -2.0, 'I': -1.0, 'H': -3.0, 'K': -3.0, 'M': -1.0, 'L': -1.0, 'N': -3.0, 'Q': -3.0, 'P': -3.0, 'S': -1.0, 'R': -3.0, 'T': -1.0, 'W': -2.0, 'V': -1.0, 'Y': -2.0, 'X': -2.0, 'Z': -3.0}, 'B': {'*': -4.0, '-': -4, 'A': -2.0, 'C': -3.0, 'B': 4.0, 'E': 1.0, 'D': 4.0, 'G': -1.0, 'F': -3.0, 'I': -3.0, 'H': 0.0, 'K': 0.0, 'M': -3.0, 'L': -4.0, 'N': 3.0, 'Q': 0.0, 'P': -2.0, 'S': 0.0, 'R': -1.0, 'T': -1.0, 'W': -4.0, 'V': -3.0, 'Y': -3.0, 'X': -1.0, 'Z': 1.0}, 'E': {'*': -4.0, '-': -4, 'A': -1.0, 'C': -4.0, 'B': 1.0, 'E': 5.0, 'D': 2.0, 'G': -2.0, 'F': -3.0, 'I': -3.0, 'H': 0.0, 'K': 1.0, 'M': -2.0, 'L': -3.0, 'N': 0.0, 'Q': 2.0, 'P': -1.0, 'S': 0.0, 'R': 0.0, 'T': -1.0, 'W': -3.0, 'V': -2.0, 'Y': -2.0, 'X': -1.0, 'Z': 4.0}, 'D': {'*': -4.0, '-': -4, 'A': -2.0, 'C': -3.0, 'B': 4.0, 'E': 2.0, 'D': 6.0, 'G': -1.0, 'F': -3.0, 'I': -3.0, 'H': -1.0, 'K': -1.0, 'M': -3.0, 'L': -4.0, 'N': 1.0, 'Q': 0.0, 'P': -1.0, 'S': 0.0, 'R': -2.0, 'T': -1.0, 'W': -4.0, 'V': -3.0, 'Y': -3.0, 'X': -1.0, 'Z': 1.0}, 'G': {'*': -4.0, '-': -4, 'A': 0.0, 'C': -3.0, 'B': -1.0, 'E': -2.0, 'D': -1.0, 'G': 6.0, 'F': -3.0, 'I': -4.0, 'H': -2.0, 'K': -2.0, 'M': -3.0, 'L': -4.0, 'N': 0.0, 'Q': -2.0, 'P': -2.0, 'S': 0.0, 'R': -2.0, 'T': -2.0, 'W': -2.0, 'V': -3.0, 'Y': -3.0, 'X': -1.0, 'Z': -2.0}, 'F': {'*': -4.0, '-': -4, 'A': -2.0, 'C': -2.0, 'B': -3.0, 'E': -3.0, 'D': -3.0, 'G': -3.0, 'F': 6.0, 'I': 0.0, 'H': -1.0, 'K': -3.0, 'M': 0.0, 'L': 0.0, 'N': -3.0, 'Q': -3.0, 'P': -4.0, 'S': -2.0, 'R': -3.0, 'T': -2.0, 'W': 1.0, 'V': -1.0, 'Y': 3.0, 'X': -1.0, 'Z': -3.0}, 'I': {'*': -4.0, '-': -4, 'A': -1.0, 'C': -1.0, 'B': -3.0, 'E': -3.0, 'D': -3.0, 'G': -4.0, 'F': 0.0, 'I': 4.0, 'H': -3.0, 'K': -3.0, 'M': 1.0, 'L': 2.0, 'N': -3.0, 'Q': -3.0, 'P': -3.0, 'S': -2.0, 'R': -3.0, 'T': -1.0, 'W': -3.0, 'V': 3.0, 'Y': -1.0, 'X': -1.0, 'Z': -3.0}, 'H': {'*': -4.0, '-': -4, 'A': -2.0, 'C': -3.0, 'B': 0.0, 'E': 0.0, 'D': -1.0, 'G': -2.0, 'F': -1.0, 'I': -3.0, 'H': 8.0, 'K': -1.0, 'M': -2.0, 'L': -3.0, 'N': 1.0, 'Q': 0.0, 'P': -2.0, 'S': -1.0, 'R': 0.0, 'T': -2.0, 'W': -2.0, 'V': -3.0, 'Y': 2.0, 'X': -1.0, 'Z': 0.0}, 'K': {'*': -4.0, '-': -4, 'A': -1.0, 'C': -3.0, 'B': 0.0, 'E': 1.0, 'D': -1.0, 'G': -2.0, 'F': -3.0, 'I': -3.0, 'H': -1.0, 'K': 5.0, 'M': -1.0, 'L': -2.0, 'N': 0.0, 'Q': 1.0, 'P': -1.0, 'S': 0.0, 'R': 2.0, 'T': -1.0, 'W': -3.0, 'V': -2.0, 'Y': -2.0, 'X': -1.0, 'Z': 1.0}, 'M': {'*': -4.0, '-': -4, 'A': -1.0, 'C': -1.0, 'B': -3.0, 'E': -2.0, 'D': -3.0, 'G': -3.0, 'F': 0.0, 'I': 1.0, 'H': -2.0, 'K': -1.0, 'M': 5.0, 'L': 2.0, 'N': -2.0, 'Q': 0.0, 'P': -2.0, 'S': -1.0, 'R': -1.0, 'T': -1.0, 'W': -1.0, 'V': 1.0, 'Y': -1.0, 'X': -1.0, 'Z': -1.0}, 'L': {'*': -4.0, '-': -4, 'A': -1.0, 'C': -1.0, 'B': -4.0, 'E': -3.0, 'D': -4.0, 'G': -4.0, 'F': 0.0, 'I': 2.0, 'H': -3.0, 'K': -2.0, 'M': 2.0, 'L': 4.0, 'N': -3.0, 'Q': -2.0, 'P': -3.0, 'S': -2.0, 'R': -2.0, 'T': -1.0, 'W': -2.0, 'V': 1.0, 'Y': -1.0, 'X': -1.0, 'Z': -3.0}, 'N': {'*': -4.0, '-': -4, 'A': -2.0, 'C': -3.0, 'B': 3.0, 'E': 0.0, 'D': 1.0, 'G': 0.0, 'F': -3.0, 'I': -3.0, 'H': 1.0, 'K': 0.0, 'M': -2.0, 'L': -3.0, 'N': 6.0, 'Q': 0.0, 'P': -2.0, 'S': 1.0, 'R': 0.0, 'T': 0.0, 'W': -4.0, 'V': -3.0, 'Y': -2.0, 'X': -1.0, 'Z': 0.0}, 'Q': {'*': -4.0, '-': -4, 'A': -1.0, 'C': -3.0, 'B': 0.0, 'E': 2.0, 'D': 0.0, 'G': -2.0, 'F': -3.0, 'I': -3.0, 'H': 0.0, 'K': 1.0, 'M': 0.0, 'L': -2.0, 'N': 0.0, 'Q': 5.0, 'P': -1.0, 'S': 0.0, 'R': 1.0, 'T': -1.0, 'W': -2.0, 'V': -2.0, 'Y': -1.0, 'X': -1.0, 'Z': 3.0}, 'P': {'*': -4.0, '-': -4, 'A': -1.0, 'C': -3.0, 'B': -2.0, 'E': -1.0, 'D': -1.0, 'G': -2.0, 'F': -4.0, 'I': -3.0, 'H': -2.0, 'K': -1.0, 'M': -2.0, 'L': -3.0, 'N': -2.0, 'Q': -1.0, 'P': 7.0, 'S': -1.0, 'R': -2.0, 'T': -1.0, 'W': -4.0, 'V': -2.0, 'Y': -3.0, 'X': -2.0, 'Z': -1.0}, 'S': {'*': -4.0, '-': -4, 'A': 1.0, 'C': -1.0, 'B': 0.0, 'E': 0.0, 'D': 0.0, 'G': 0.0, 'F': -2.0, 'I': -2.0, 'H': -1.0, 'K': 0.0, 'M': -1.0, 'L': -2.0, 'N': 1.0, 'Q': 0.0, 'P': -1.0, 'S': 4.0, 'R': -1.0, 'T': 1.0, 'W': -3.0, 'V': -2.0, 'Y': -2.0, 'X': 0.0, 'Z': 0.0}, 'R': {'*': -4.0, '-': -4, 'A': -1.0, 'C': -3.0, 'B': -1.0, 'E': 0.0, 'D': -2.0, 'G': -2.0, 'F': -3.0, 'I': -3.0, 'H': 0.0, 'K': 2.0, 'M': -1.0, 'L': -2.0, 'N': 0.0, 'Q': 1.0, 'P': -2.0, 'S': -1.0, 'R': 5.0, 'T': -1.0, 'W': -3.0, 'V': -3.0, 'Y': -2.0, 'X': -1.0, 'Z': 0.0}, 'T': {'*': -4.0, '-': -4, 'A': 0.0, 'C': -1.0, 'B': -1.0, 'E': -1.0, 'D': -1.0, 'G': -2.0, 'F': -2.0, 'I': -1.0, 'H': -2.0, 'K': -1.0, 'M': -1.0, 'L': -1.0, 'N': 0.0, 'Q': -1.0, 'P': -1.0, 'S': 1.0, 'R': -1.0, 'T': 5.0, 'W': -2.0, 'V': 0.0, 'Y': -2.0, 'X': 0.0, 'Z': -1.0}, 'W': {'*': -4.0, '-': -4, 'A': -3.0, 'C': -2.0, 'B': -4.0, 'E': -3.0, 'D': -4.0, 'G': -2.0, 'F': 1.0, 'I': -3.0, 'H': -2.0, 'K': -3.0, 'M': -1.0, 'L': -2.0, 'N': -4.0, 'Q': -2.0, 'P': -4.0, 'S': -3.0, 'R': -3.0, 'T': -2.0, 'W': 11.0, 'V': -3.0, 'Y': 2.0, 'X': -2.0, 'Z': -3.0}, 'V': {'*': -4.0, '-': -4, 'A': 0.0, 'C': -1.0, 'B': -3.0, 'E': -2.0, 'D': -3.0, 'G': -3.0, 'F': -1.0, 'I': 3.0, 'H': -3.0, 'K': -2.0, 'M': 1.0, 'L': 1.0, 'N': -3.0, 'Q': -2.0, 'P': -2.0, 'S': -2.0, 'R': -3.0, 'T': 0.0, 'W': -3.0, 'V': 4.0, 'Y': -1.0, 'X': -1.0, 'Z': -2.0}, 'Y': {'*': -4.0, '-': -4, 'A': -2.0, 'C': -2.0, 'B': -3.0, 'E': -2.0, 'D': -3.0, 'G': -3.0, 'F': 3.0, 'I': -1.0, 'H': 2.0, 'K': -2.0, 'M': -1.0, 'L': -1.0, 'N': -2.0, 'Q': -1.0, 'P': -3.0, 'S': -2.0, 'R': -2.0, 'T': -2.0, 'W': 2.0, 'V': -1.0, 'Y': 7.0, 'X': -1.0, 'Z': -2.0}, 'X': {'*': -4.0, '-': -4, 'A': 0.0, 'C': -2.0, 'B': -1.0, 'E': -1.0, 'D': -1.0, 'G': -1.0, 'F': -1.0, 'I': -1.0, 'H': -1.0, 'K': -1.0, 'M': -1.0, 'L': -1.0, 'N': -1.0, 'Q': -1.0, 'P': -2.0, 'S': 0.0, 'R': -1.0, 'T': 0.0, 'W': -2.0, 'V': -1.0, 'Y': -1.0, 'X': -1.0, 'Z': -1.0}, 'Z': {'*': -4.0, '-': -4, 'A': -1.0, 'C': -3.0, 'B': 1.0, 'E': 4.0, 'D': 1.0, 'G': -2.0, 'F': -3.0, 'I': -3.0, 'H': 0.0, 'K': 1.0, 'M': -1.0, 'L': -3.0, 'N': 0.0, 'Q': 3.0, 'P': -1.0, 'S': 0.0, 'R': 0.0, 'T': -1.0, 'W': -3.0, 'V': -2.0, 'Y': -2.0, 'X': -1.0, 'Z': 4.0}}
var beta = 10;

//#################
//#################
// DRAW
//#################
//#################

degradation = {
    /*
     VARIABLES
     */

    sequence: "",
    disorder_score: [],
    similarity_cutoff: {},
    pwm_length: 0,
    pwm: "dbox",

    /*
     FUNCTIONS
     */
    reset: function () {
        degradation.sequence = "";
        degradation.disorder_score = [];
        degradation.similarity_cutoff = {};
        degradation.pwm_length = 0;
        degradation.pwm = "dbox";
    },
    calculateCutoff: function () {
        var random_peptide_score_list = [];
        for (i = 0; i < 1000000; i++) {
            var random_peptide = degradation.randomString();
            var random_peptide_scores = degradation.scorePeptide(random_peptide);
            var random_peptide_score = random_peptide_scores.reduce(function (a, b) {
                return a + b;
            });
            random_peptide_score_list.push(random_peptide_score);
        }

        random_peptide_score_list.sort(function (a, b) {
            return a - b
        });
        random_peptide_score_list.reverse();

        degradation.similarity_cutoff["0.00001"] = random_peptide_score_list[50];
        degradation.similarity_cutoff["0.0001"] = random_peptide_score_list[100];
        degradation.similarity_cutoff["0.001"] = random_peptide_score_list[1000];

        console.log(degradation.similarity_cutoff);
    },
    calculateMax: function () {
        var similarity_max = 0;

        for (var p_i = 0; p_i < degradation.pwm_length; p_i++) {
            var column_scores = [];
            for (var c_i = 0; c_i < residues.length; c_i++) {
                column_scores.push(degradation.use_scores[p_i][residues[c_i]]);
            }

            var score = Math.max.apply(null, column_scores);

            if (!isNaN(parseFloat(score)) && isFinite(score)) {
                similarity_max += (score / pwm_max[degradation.pwm]) * ics[degradation.pwm][p_i];
            }
        }
        degradation.similarity_max = similarity_max;
    },
    scorePeptide: function (peptide) {
        var summer = [];

        for (p_i = 0; p_i < peptide.length; p_i++) {
            score = degradation.use_scores[p_i][peptide[p_i]];

            if (!isNaN(parseFloat(score)) && isFinite(score)) {
                summer.push((score / pwm_max[degradation.pwm]) * ics[degradation.pwm][p_i]);
            }
        }
        return summer;
    },
    randomString: function () {
        var text = "";
        var possible = "ACDEFGHIKLMNPQRSTVWY";
        for (var i = 0; i < degradation.pwm_length; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    },
    unique: function (nonunique_array) {
        var n = {}, r = [];

        for (var i = 0; i < nonunique_array.length; i++) {
            if (!n[nonunique_array[i]]) {
                n[nonunique_array[i]] = true;
                r.push(nonunique_array[i]);
            }
        }
        return r;

    },
    extractSequence: function (sequenceHTML) {
        var sequence_gapped = "";
        var sequence_ungapped = "";
        $(sequenceHTML).find('.aC').each(function (indexaa, aa) {
            aa = $(aa);
            sequence_gapped += aa.html();
            if (!aa.hasClass('gap') && !(aa.html().localeCompare('-') == 0)) {
                sequence_ungapped += aa.html();
            } else if (!aa.hasClass('gap') && (aa.html().localeCompare('-') == 0)) {
                sequence_ungapped += aa.html();
            }
        });
        return sequence_ungapped;
    },
    extractIupred: function (iupredHTML) {
        var disorderScore = [];

        $(iupredHTML).find('div').each(function (indexaa, aa) {
            if ($(aa).data("tooltip") != undefined) {
                disorderScore.push(parseFloat($(aa).data("tooltip").split(":")[1]));
            }
        });

        return disorderScore;
    },
    extractPfam: function (pfamHTML) {
        var domains = [];
        $(pfamHTML).find('.fC').each(function (indexdomain, domain) {
            if ($(domain).data("pos") != undefined) {
                var pos = $(domain).data("pos").split(":");
                var start = pos[0];
                var end = pos[1];
                var disorder = "?";
                if (degradation.disorder_score.length != 0) {
                    disorder = ((degradation.disorder_score.slice(start, end).reduce(function (a, b) {
                        return a + b;
                    })) / (end - start)).toFixed(2);
                }

                var domain_name = $(domain).data("tooltip");

                domains.push([start, end, domain_name, disorder]);
            }
        });
        return domains;
    },
    extractMods: function (modHTML) {
        var mods = [];

        $(modHTML).find('.fC').each(function (indexmod, mod) {
            if ($(mod).data("pos") != undefined) {
                var pos = $(mod).data("pos").split(":");
                var start = pos[0];
                var end = pos[1];
                var mod_name = $(mod).data("tooltip");

                mods.push([start, end, mod_name]);
            }
        });

        return mods;
    },
    extractSS: function (ssHTML) {
        var sss = [];

        $(ssHTML).find('.fC').each(function (indexss, ss) {
            if ($(ss).data("pos") != undefined) {

                var pos = $(ss).data("pos").split(":");
                var start = pos[0];
                var end = pos[1];
                var ss_name = $(ss).data("tooltip");

                sss.push([start, end, ss_name]);
            }
        });

        return sss;
    },
    extractHModel: function (hModelHTML) {
        var hModels = [];

        $(hModelHTML).find('.fC').each(function (indexhModel, hModel) {
            if ($(hModel).data("pos") != undefined) {

                var pos = $(hModel).data("pos").split(":");
                var start = pos[0];
                var end = pos[1];
                var hModel_name = $(hModel).html();

                hModels.push([start, end, hModel_name]);
            }
        });

        return hModels;
    },
    extractPdb: function (pdbHTML) {
        var pdbs = [];

        $(pdbHTML).find('.fC').each(function (indexpdb, pdb) {
            if ($(pdb).data("pos") != undefined) {
                var pos = $(pdb).data("pos").split(":");
                var start = pos[0];
                var end = pos[1];
                var pdb_name = $(pdb).data("tooltip").split("<br>")[0];

                pdbs.push(pdb_name);
            }
        });

        return pdbs;
    },

    isValidated: function (start, end, validated_degrons) {

        var output = "no";
        for (degron in validated_degrons) {
            degron_type = validated_degrons[degron]["type"];
            degron_start = validated_degrons[degron]["start"];
            degron_end = validated_degrons[degron]["end"];
            degron_pmid = validated_degrons[degron]["pmid"];

            if ((start <= parseInt(degron_start) && end >= parseInt(degron_end))) {
                output = 'yes (<small><a target="_blank" href="http://www.ncbi.nlm.nih.gov/pubmed/' + degron_pmid + '">paper</a></small>)';
            }
        }

        return output;
    },
    overlap: function (start, end, features) {
        var overlaps = [];
        for (feature in features) {
            if ((start >= parseInt(features[feature][0]) && start <= parseInt(features[feature][1])) || (end >= parseInt(features[feature][0]) && end <= parseInt(features[feature][1]))) {
                overlaps.push(features[feature]);
            }
            else {
                if ((start <= parseInt(features[feature][0]) && end >= parseInt(features[feature][1]))) {
                    overlaps.push(features[feature]);
                }
            }
        }
        return overlaps;
    },
    translate_score_to_color: function (start, end, steps, count) {
        var s = start,
            e = end,
            final = s + (((e - s) / steps) * count);
        return Math.floor(final);
    },
    translate_score_to_RGB: function (scoreToColor, color) {
        /*
         if (color == "blue")
         {max_color = [104, 189, 246];}

         if (color == "red")
         {max_color = [255, 148, 148];}

         if (color == "green")
         {max_color = [ 148, 255, 148];}
         */

        var max_color = [188, 188, 255];
        var min_color = [256, 256, 256];

        var red = 256;
        var green = 256;
        var blue = 256;

        scoreToColor = Math.abs(scoreToColor);

        var red = degradation.translate_score_to_color(min_color[0], max_color[0], 100, parseInt(scoreToColor * 100));
        var green = degradation.translate_score_to_color(min_color[1], max_color[1], 100, parseInt(scoreToColor * 100));
        var blue = degradation.translate_score_to_color(min_color[2], max_color[2], 100, parseInt(scoreToColor * 100));

        color_gradient = 'rgb(' + red + "," + green + "," + blue + ')';
        return color_gradient;
    },
    focus: function (min, max) {
        //console.log(min*10);
        //console.log($("#trackData").width(), $("#trackData").scrollLeft());

        $("#trackData").animate({scrollLeft: min * 10 - ($("#trackData").width() / 2) + 50}, 'fast');
    },
    annotate: function (data) {
        var tracks = [];

        var start_adjustment = parseInt(-1);
        if (data.ali_start != undefined) {
            start_adjustment = parseInt(data.ali_start) - 1;
        }

        var protein_acc = data.protein_acc;

        var validated_degrons = validated[protein_acc];

        var sequenceHTML = data.alignmentDataQuery;
        var iupredHTML = "";
        if (data.tracks.iupred) {
            iupredHTML = data.tracks.iupred[1];
        }
        var pfamHTML = "";
        if (data.tracks.pfam) {
            pfamHTML = data.tracks.pfam[1];
        }

        var modHTML = "";
        if (data.tracks['modified residue']) {
            modHTML = modHTML + data.tracks['modified residue'][1];
        }
        if (data.tracks['phospho.elm']) {
            modHTML = modHTML + data.tracks['phospho.elm'][1];
        }

        if (data.tracks['secondary structure']) {
            ssHTML = data.tracks['secondary structure'][1];
        }
        else {
            ssHTML = "";
        }

        if (data.tracks['homology model']) {
            hModelHTML = data.tracks['homology model'][1];
        }
        else {
            hModelHTML = "";
        }

        if (data.tracks['PDB']) {
            pdbHTML = data.tracks['PDB'][1];
        }
        else {
            pdbHTML = "";
        }

        //var degradation = {};
        degradation.sequence = degradation.extractSequence(sequenceHTML);
        degradation.sequence_length = degradation.sequence.length;
        degradation.disorder_score = degradation.extractIupred(iupredHTML);
        degradation.pfam = degradation.extractPfam(pfamHTML);
        degradation.modification = degradation.extractMods(modHTML);
        degradation.ss = degradation.extractSS(ssHTML);
        degradation.hModel = degradation.extractHModel(hModelHTML);
        degradation.pdb = degradation.extractPdb(pdbHTML);

        var table = "";

        table = table + '<b>Predicted APC/C degron motif</b><br><small><a href="http://slim.ucd.ie/apc/index.php?page=scan_protein">Check another protein for APC/C degrons</a></small>';
        table = table + '<tr class="table_head">';
        table = table + "<td>";
        table = table + "Motif<br>type";
        table = table + "</td>";
        table = table + "<td>";
        table = table + "Validated";
        table = table + "</td>";
        table = table + "<td>";
        table = table + "Motif sequence<br><i>(click to view)</i>";
        table = table + "</td>";
        table = table + "<td>";
        table = table + "Start";
        table = table + "</td>";
        table = table + "<td>";
        table = table + "Stop";
        table = table + "</td>";
        table = table + "<td>";
        table = table + "Similarity<br>score ";
        table = table + "</td>";
        table = table + "<td>";
        table = table + "Consensus<br>similarity";
        table = table + "</td>";

        table = table + "<td>";
        table = table + "Disorder<br>score";
        table = table + "</td>";

        table = table + "<td>";
        table = table + "Domain";
        table = table + "</td>";

        table = table + "<td>";
        table = table + "Structure";
        table = table + "</td>";

        table = table + "<td>";
        table = table + "Modification";
        table = table + "</td>";
        table = table + "</tr>";

        var tableConfident = "";
        var tableOther = "";

        //features["data"] = [];


        for (var key in pwms) {
            features = [];
            degradation.pwm = key;
            degradation.use_scores = pwms[key];
            degradation.pwm_length = Object.keys(pwms[key]).length;

            degradation.similarity_cutoff = similarity_cutoff[degradation.pwm];
            //console.log(key);
            //degradation.calculateCutoff();
            degradation.calculateMax();

            for (var seq_iter = -degradation.pwm_length + 1; seq_iter < (degradation.sequence_length); seq_iter++) {
                var padding = "";
                var pad = "-----------";
                var right_padding_length,left_padding_length, peptide;

                right_padding_length = 0;
                if (seq_iter < 0) {
                    left_padding_length = Math.abs(seq_iter);
                    padding = pad.substring(0, left_padding_length);
                    peptide = padding + degradation.sequence.substr(Math.max(0, seq_iter), degradation.pwm_length + seq_iter);
                } else if (seq_iter > degradation.sequence_length - degradation.pwm_length) {
                    right_padding_length = (seq_iter - (degradation.sequence_length - degradation.pwm_length));
                    padding = pad.substring(0, right_padding_length);
                    peptide = degradation.sequence.substr(seq_iter, degradation.pwm_length - (seq_iter - (degradation.sequence_length - degradation.pwm_length))) + padding;
                }
                else {
                    peptide = degradation.sequence.substr(seq_iter, degradation.pwm_length);
                }

                var peptide_scores = degradation.scorePeptide(peptide);
                var peptide_score = peptide_scores.reduce(function (a, b) {
                    return a + b;
                });

                var cutoff = 0.5;

                if (peptide_score / degradation.similarity_max > degradation.similarity_cutoff["0.001"] / degradation.similarity_max) {


                    featureTmp = {};
                    featureTmp.id = key + "_" + seq_iter;
                    featureTmp.sequence = peptide;
                    featureTmp.hover = name_mapping[key] + " - " + degradation.sequence.substr(seq_iter, degradation.pwm_length) + " - " + (peptide_score / degradation.similarity_max).toFixed(2); //" - " + (start_adjustment + seq_iter+2) + ":" + ((start_adjustment+ seq_iter + peptide.length)+1) +
                    featureTmp.start = (start_adjustment + seq_iter + 2);
                    featureTmp.end = (start_adjustment + seq_iter + peptide.length + 1 - right_padding_length);
                    featureTmp.class = "degradation_peptide";
                    featureTmp.len = peptide.length;
                    featureTmp.colour = degradation.translate_score_to_RGB(((peptide_score / degradation.similarity_max) - cutoff) / (1 - cutoff), motif_colour[key]);
                    featureTmp.text_colour = "#000";
                    featureTmp.scores = [];

                    for (var score in peptide_scores) {
                        featureTmp.scores.push((peptide_scores[score] - 0.5));
                    }

                    featureTmp.scores = peptide_scores;
                    features.push(featureTmp);

                    var dis_score = 1;

                    if (degradation.disorder_score.length != 0) {
                        peptide_discores = degradation.disorder_score.slice(Math.max(0, seq_iter), Math.min(seq_iter + degradation.pwm_length, degradation.sequence_length - 1));
                        dis_score = peptide_discores.reduce(function (a, b) {
                                return a + b;
                            }) / peptide_discores.length;
                    }


                    var degron_validated = degradation.isValidated(seq_iter, seq_iter + degradation.pwm_length, validated_degrons);

                    var tableStr = "";
                    var badRow = false;

                    if (degron_validated.localeCompare("no") == 1) {
                        tableStr = tableStr + '<tr bgcolor="#EFE" style="">';
                    }
                    else if (peptide_score / degradation.similarity_max < degradation.similarity_cutoff["0.0001"] / degradation.similarity_max || dis_score < 0.3) {
                        tableStr = tableStr + '<tr bgcolor="#FEE" style="font-style:italic; font-size:90%">';
                        badRow = true;
                    }
                    else {
                        tableStr = tableStr + '<tr>';
                    }

                    tableStr = tableStr + "<td>";
                    tableStr = tableStr + name_mapping[key];
                    tableStr = tableStr + "</td>";

                    tableStr = tableStr + "<td>";
                    tableStr = tableStr + degron_validated;
                    tableStr = tableStr + "</td>";

                    tableStr = tableStr + "<td>";
                    tableStr = tableStr + '<a class="degradation_link" id="' + key + "_" + (start_adjustment + seq_iter + 1) + '">' + peptide + "</a>";
                    tableStr = tableStr + "</td>";
                    tableStr = tableStr + "<td>";
                    tableStr = tableStr + (start_adjustment + seq_iter + 2);
                    tableStr = tableStr + "</td>";
                    tableStr = tableStr + "<td>";
                    tableStr = tableStr + (start_adjustment + seq_iter + peptide.length + 1);
                    tableStr = tableStr + "</td>";

                    tableStr = tableStr + "<td>";
                    tableStr = tableStr + (peptide_score / degradation.similarity_max).toFixed(2);

                    if (peptide_score / degradation.similarity_max > 0.85) {
                        tableStr = tableStr + ' <i class="fa fa-check"></i>';
                    }

                    tableStr = tableStr + "</td>";

                    if (peptide_score / degradation.similarity_max > degradation.similarity_cutoff["0.00001"] / degradation.similarity_max) {
                        tableStr = tableStr + "<td>";
                        tableStr = tableStr + 'High';
                        tableStr = tableStr + "</td>";
                    }
                    else if (peptide_score / degradation.similarity_max > degradation.similarity_cutoff["0.0001"] / degradation.similarity_max) {
                        tableStr = tableStr + "<td >";
                        tableStr = tableStr + 'Medium';
                        tableStr = tableStr + "</td>";
                    }
                    else {
                        tableStr = tableStr + "<td>";
                        tableStr = tableStr + 'Low';
                        tableStr = tableStr + "</td>";
                    }

                    tableStr = tableStr + "<td>";

                    if (degradation.disorder_score.length != 0) {
                        peptide_discores = degradation.disorder_score.slice(Math.max(0, seq_iter), Math.min(seq_iter + degradation.pwm_length, degradation.sequence_length - 1));
                        dis_score = peptide_discores.reduce(function (a, b) {
                                return a + b;
                            }) / peptide_discores.length;

                        tableStr = tableStr + (dis_score).toFixed(2);

                        if (dis_score > 0.4) {
                            tableStr = tableStr + ' <i class="fa fa-check"></i>';
                        }
                        else if (dis_score > 0.3) {
                            tableStr = tableStr + ' <i class="fa fa-exclamation"></i>';
                        }
                        else {
                            tableStr = tableStr + ' <i class="fa fa-times"></i>';
                        }

                    }
                    else {
                        tableStr = tableStr + "N/A";
                    }

                    tableStr = tableStr + "</td>";
                    tableStr = tableStr + "<td>";
                    var overlap = degradation.overlap(seq_iter, seq_iter + degradation.pwm_length, degradation.pfam);
                    if (overlap.length > 0) {
                        tableStr = tableStr + overlap[0][2] + " (" + overlap[0][3] + ")";
                        if (overlap[0][3] > 0.4) {
                            tableStr = tableStr + " <br><i>(disordered domain?)</i>";
                        }

                        tableStr = tableStr + "<br>";
                    }

                    tableStr = tableStr + "</td>";
                    tableStr = tableStr + "<td>";

                    overlap = degradation.overlap(seq_iter, seq_iter + degradation.pwm_length, degradation.ss);
                    if (overlap.length > 0) {
                        tableStr = tableStr + overlap[0][2];
                    }

                    overlap = degradation.overlap(seq_iter, seq_iter + degradation.pwm_length, degradation.pdb);
                    if (overlap.length > 0) {
                        tableStr = tableStr + " (<a href='http://www.rcsb.org/pdb/search/smart.do?smartSearchSubtype_0=StructureIdQuery&structureIdList_0=" + overlap.join(",") + "'>PDB</a>)";
                    }
                    else {
                        overlap = degradation.overlap(seq_iter, seq_iter + degradation.pwm_length, degradation.hModel);
                        if (overlap.length > 0) {
                            tableStr = tableStr + " (" + overlap[0][2] + ")";
                        }
                    }
                    tableStr = tableStr + "</td>";

                    tableStr = tableStr + "<td>";
                    overlap = degradation.overlap(seq_iter - 5, seq_iter + degradation.pwm_length + 5, degradation.modification);

                    if (overlap.length > 0) {
                        mods = [];
                        for (mod in overlap) {
                            mods.push("" + overlap[mod][2] + '@' + overlap[mod][0] + "");
                        }

                        tableStr = tableStr + mods.join("<br>");
                    }
                    tableStr = tableStr + "</td>";

                    tableStr = tableStr + "</tr>";

                    if (badRow) {
                        tableOther += tableStr;
                    }
                    else {
                        tableConfident += tableStr;
                    }

                }
            }

            var track = {};
            track.data = features;
            track.type = "peptides";
            track.position = -1;
            track.name = "Putative " + name_mapping[key];
            track.colour = "#FFF";
            track.opacity = "1";
            track.id = null;
            track.class = null;
            track.height = null;
            track.help = null;
            track.text_colour = null;
            tracks.push(track);
        }

        /*
         =======
         */
        var features = [];

        for (var degron in validated_degrons) {
            var degron_type = validated_degrons[degron]["type"];
            var degron_start = validated_degrons[degron]["start"];
            var degron_end = validated_degrons[degron]["end"];
            var degron_pmid = validated_degrons[degron]["pmid"];

            var featureTmp = {};
            featureTmp.id = "Degron_" + degron;
            featureTmp.sequence = degron_type;
            featureTmp.hover = "Validated " + degron_type;
            featureTmp.start = parseInt(degron_start) - 1;
            featureTmp.end = parseInt(degron_end) + 2;
            featureTmp.class = "degradation_peptide";
            featureTmp.len = degron_end - degron_start + 2;
            featureTmp.colour = "#FFC200";
            featureTmp.text_colour = "#000";
            featureTmp.link = 'http://www.ncbi.nlm.nih.gov/pubmed/' + degron_pmid;
            featureTmp.scores = [];

            features.push(featureTmp);
        }

        track = {};
        track.data = features;
        track.type = "feature";
        track.position = -1;
        track.name = "Validated degrons";
        track.colour = "#FFF";
        track.opacity = "1";
        track.id = null;
        track.class = null;
        track.height = null;
        track.help = null;
        track.text_colour = null;
        tracks.push(track);

        /*
         =======
         */
        $(".tableContainer").html('<table width="100%">' + table + tableConfident + tableOther + "</table><br>");
        $(".table_head").css("font-weight", "700").css("background", "#F0F0F0");


        $('.degradation_link').on('click', function () {
            degradation.focus($(this).attr("id").split("_")[1]);
            $(".highlight").removeClass("highlight");
            $(this).addClass("highlight");
            console.log("#" + $(this).attr("id") + ".degradation_peptide");
            $("#" + $(this).attr("id") + ".degradation_peptide").addClass("highlight");
        });
        PROVIZ.displayTracks(tracks);
    }
};

