// Copyright (C) 2016 Norman E. Davey <normandavey@gmail.com>, Jean Manguy <jean.manguy@ucdconnect.ie> and Peter Jehl <peter.jehl@ucdconnect.ie>
// This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
// Public License as published by the Free Software Foundation; either version 2.1 of the License, or (at your option)
// any later version.
// 
// This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
// 
// You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
// the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Author contact: Norman E. Davey <normandavey@gmail.com> / Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
// Author contact: Jean Manguy <jean.manguy@ucdconnect.ie> / Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
// Author contact: Peter Jehl <peter.jehl@ucdconnect.ie> / Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.

var validated = {
"Q9Y4K1":[{"type":"B56 docking motif","start":"732","end":"737","pmid":"12345678"}],
"Q14674":[{"type":"B56 docking motif","start":"1485","end":"1490","pmid":"12345678"}],
"Q69YH5":[{"type":"B56 docking motif","start":"590","end":"595","pmid":"12345678"}],
"P51959":[{"type":"B56 docking motif","start":"287","end":"292","pmid":"12345678"}],
"O43524":[{"type":"B56 docking motif","start":"448","end":"453","pmid":"12345678"}],
"O60566":[{"type":"B56 docking motif","start":"669","end":"674","pmid":"12345678"}],
"O15169":[{"type":"B56 docking motif","start":"238","end":"243","pmid":"12345678"}],
"O96017":[{"type":"B56 docking motif","start":"71","end":"76","pmid":"12345678"}],
"Q92974":[{"type":"B56 docking motif","start":"967","end":"972","pmid":"12345678"}],
"O95239":[{"type":"B56 docking motif","start":"1224","end":"1229","pmid":"12345678"}],
"Q9H0H5":[{"type":"B56 docking motif","start":"143","end":"148","pmid":"12345678"}],
"Q6T4R5":[{"type":"B56 docking motif","start":"1626","end":"1631","pmid":"12345678"}],
"Q96L42":[{"type":"B56 docking motif","start":"706","end":"711","pmid":"12345678"}],
"Q4G163":[{"type":"B56 docking motif","start":"384","end":"389","pmid":"12345678"}],
"P51587":[{"type":"B56 docking motif","start":"1114","end":"1119","pmid":"12345678"}],
"P47074":[{"type":"B56 docking motif","start":"477","end":"482","pmid":"12345678"}],
"P14078":[{"type":"B56 docking motif","start":"1378","end":"1383","pmid":"12345678"}],
"P18272":[{"type":"B56 docking motif","start":"562","end":"567","pmid":"12345678"}]
};

var pwms_aa = {};
//spwms_aa["B56"] = {"S":[-1000,2.54,1.06,-1000,0.07,-1000,0.05,1.26,1.16],"T":[-1000,1.1,3.59,-1000,-0.99,-1000,-0.26,0.4,0.55],"G":[-1000,-2.02,-2.22,-1000,-2.33,-1000,0.25,-0.09,-0.75],"A":[-1000,-0.6,0.39,-1000,-1.24,-1000,-1.42,0.9,-0.88],"P":[-1000,3.43,5.05,-1000,4.52,-1000,-2.14,0.44,1.95],"C":[6.13,-2.59,-2.36,-2.9,-2.9,-1000,-3.37,-2.35,-2.82],"I":[0.07,-3.1,-2.53,8.34,-0.54,-1000,-2.57,-2.31,-2.36],"L":[5.45,-3,-2.87,2.36,-0.48,-1000,-1.2,-2.51,-0.35],"V":[-1000,-2.48,-1.84,1.04,-0.8,-1000,-2.44,-1.06,-2.13],"M":[6.13,-1.95,-2.29,-1000,-1.38,-1000,1.36,-1.68,-1.56],"F":[-1.14,-2.67,-3.64,-1000,-2.74,-1000,-3.14,-3.01,-2.76],"H":[-1000,-1.14,-2.57,-1000,1.2,-1000,-1.25,1.27,2.12],"Y":[-2.52,1.59,-3.03,-1000,-2.15,-1000,-2.78,-1.88,-1.92],"W":[-3.05,-2.79,-3.94,-4.21,-3.55,-1000,-3.58,-3.16,-3.33],"K":[-1000,-0.42,-1.52,-1000,-0.64,-1000,-0.63,0.79,-0.46],"R":[-1000,0.25,-2.3,-1000,0.34,-1000,-1.34,1.01,-0.04],"D":[-1000,-1.42,0.2,-1000,0.42,-1000,3.49,-0.48,0.73],"E":[-1000,-0.35,-1.42,-1000,0.59,6.3,2.91,2.25,2.77],"N":[-1000,-0.81,-1.28,-1000,1.29,-1000,1.09,-0.58,-0.66],"Q":[-1000,3.01,-1.58,-1000,0.63,-1000,1.27,1.17,0.11],"-":[-5,-5,-5,-5,-5,-5,-5,-5,-5]};
pwms_aa["B56"] = {"S":[-1000,2.58,1.07,-1000,0.04,-1000,0.05,1.51,1.19],"T":[-1000,1.05,3.58,-1000,-1,-1000,-0.24,0.43,0.56],"G":[-1000,-1.98,-2.19,-1000,-2.4,-1000,0.25,-0.1,-0.76],"A":[-1000,-0.56,0.4,-1000,-1.19,-1000,-1.39,0.55,-0.89],"P":[-1000,3.45,5.04,-1000,4.53,-1000,-2.13,0.43,1.85],"C":[6.3,-2.56,-2.32,-2.84,-2.88,-1000,-3.33,-2.41,-2.89],"I":[0.09,-3.08,-2.49,8.25,-0.51,-1000,-2.5,-2.4,-2.47],"L":[5.27,-2.99,-2.84,2.41,-0.37,-1000,-1.16,-2.62,-0.39],"V":[-1000,-2.46,-1.8,1.09,-0.76,-1000,-2.38,-1.11,-2.19],"M":[6.31,-1.93,-2.26,-1000,-1.25,-1000,1.48,-1.75,-1.61],"F":[-1.13,-2.64,-3.61,-1000,-2.7,-1000,-3.08,-2.97,-2.82],"H":[-1000,-1.13,-2.54,-1000,0.98,-1000,-1.25,2.32,2.35],"Y":[-2.49,1.56,-3,-1000,-2.13,-1000,-2.75,-1.66,-1.89],"W":[-3.01,-2.77,-3.91,-4.15,-3.44,-1000,-3.54,-3.19,-3.33],"K":[-1000,-0.41,-1.49,-1000,-0.54,-1000,-0.62,0.62,-0.42],"R":[-1000,0.24,-2.27,-1000,0.38,-1000,-1.33,0.37,0.03],"D":[-1000,-1.39,0.17,-1000,0.38,-1000,3.48,-0.43,-0.35],"E":[-1000,-0.34,-1.4,-1000,0.76,6.28,2.88,2.31,3.09],"N":[-1000,-0.78,-1.25,-1000,0.47,-1000,0.99,-0.47,-0.8],"Q":[-1000,2.94,-1.55,-1000,1.16,-1000,1.27,1.17,0.2],"-":[-5,-5,-5,-5,-5,-5,-5,-5,-5]};
pwms = {};

console.log(pwms_aa["B56"]);
var name_mapping = {"B56":"B56 docking motif"};

//var motif_colour = {"dbox":"#FAA0A0","ken":"#A0A0FA","abba":"#A0FAA0"};
var motif_colour = {"B56":"green"};

var ics = {
"B56":[ 1,1,1,1,1,1,1,1,1]
}

var pwm_max = {"B56":1};

var similarity_cutoff = {
"B56":{0.00001: 26.130000000000003, 0.0001:19.919999999999998, 0.001: 9.989999999999998, 0.000001: 30.409999999999997}
};

var background = {
"all":{'A': 0.086167, 'C': 0.012759, 'D': 0.052951, 'E': 0.061395, 'F': 0.040392, 'G': 0.071295, 'H': 0.021925, 'I': 0.060238, 'K': 0.052746, 'L': 0.098334, 'M': 0.024703, 'N': 0.041559, 'P': 0.047353, 'Q': 0.038562, 'R': 0.054707, 'S': 0.066961, 'T': 0.056144, 'V': 0.067440, 'W': 0.013183, 'Y': 0.030613},
"0.3":{"A":0.082767,"C":0.007309,"D":0.058283,"E":0.082903,"F":0.022992,"G":0.073166,"H":0.024454,"I":0.043799,"K":0.064038,"L":0.073925,"M":0.023502,"N":0.043715,"P":0.062209,"Q":0.048512,"R":0.064976,"S":0.075305,"T":0.062569,"V":0.059192,"W":0.007122,"Y":0.019261},
"0.5":{"A":0.079609,"C":0.005029,"D":0.057989,"E":0.08815,"F":0.015527,"G":0.075008,"H":0.024831,"I":0.028494,"K":0.06696,"L":0.055605,"M":0.021528,"N":0.047938,"P":0.085282,"Q":0.055837,"R":0.067414,"S":0.0964,"T":0.065369,"V":0.044676,"W":0.004848,"Y":0.013504}
}

var residues = ["S","T","G","A","P","C","I","L","V","M","F","Y","W","H","K","R","D","E","N","Q","-"];
var aas = ["S","T","G","A","P","C","I","L","V","M","F","Y","W","H","K","R","D","E","N","Q"];

var lamda_value = 0.2486767;
var substitution_matrix = {'*': {'*': 1.0, '-': -4, 'A': -4.0, 'C': -4.0, 'B': -4.0, 'E': -4.0, 'D': -4.0, 'G': -4.0, 'F': -4.0, 'I': -4.0, 'H': -4.0, 'K': -4.0, 'M': -4.0, 'L': -4.0, 'N': -4.0, 'Q': -4.0, 'P': -4.0, 'S': -4.0, 'R': -4.0, 'T': -4.0, 'W': -4.0, 'V': -4.0, 'Y': -4.0, 'X': -4.0, 'Z': -4.0}, '-': {'*': -4, '-': -1, 'A': -4, 'C': -4, 'B': -4, 'E': -4, 'D': -4, 'G': -4, 'F': -4, 'I': -4, 'H': -4, 'K': -4, 'M': -4, 'L': -4, 'N': -4, 'Q': -4, 'P': -4, 'S': -4, 'R': -4, 'T': -4, 'W': -4, 'V': -4, 'Y': -4, 'X': -4, 'Z': -4}, 'A': {'*': -4.0, '-': -4, 'A': 4.0, 'C': 0.0, 'B': -2.0, 'E': -1.0, 'D': -2.0, 'G': 0.0, 'F': -2.0, 'I': -1.0, 'H': -2.0, 'K': -1.0, 'M': -1.0, 'L': -1.0, 'N': -2.0, 'Q': -1.0, 'P': -1.0, 'S': 1.0, 'R': -1.0, 'T': 0.0, 'W': -3.0, 'V': 0.0, 'Y': -2.0, 'X': 0.0, 'Z': -1.0}, 'C': {'*': -4.0, '-': -4, 'A': 0.0, 'C': 9.0, 'B': -3.0, 'E': -4.0, 'D': -3.0, 'G': -3.0, 'F': -2.0, 'I': -1.0, 'H': -3.0, 'K': -3.0, 'M': -1.0, 'L': -1.0, 'N': -3.0, 'Q': -3.0, 'P': -3.0, 'S': -1.0, 'R': -3.0, 'T': -1.0, 'W': -2.0, 'V': -1.0, 'Y': -2.0, 'X': -2.0, 'Z': -3.0}, 'B': {'*': -4.0, '-': -4, 'A': -2.0, 'C': -3.0, 'B': 4.0, 'E': 1.0, 'D': 4.0, 'G': -1.0, 'F': -3.0, 'I': -3.0, 'H': 0.0, 'K': 0.0, 'M': -3.0, 'L': -4.0, 'N': 3.0, 'Q': 0.0, 'P': -2.0, 'S': 0.0, 'R': -1.0, 'T': -1.0, 'W': -4.0, 'V': -3.0, 'Y': -3.0, 'X': -1.0, 'Z': 1.0}, 'E': {'*': -4.0, '-': -4, 'A': -1.0, 'C': -4.0, 'B': 1.0, 'E': 5.0, 'D': 2.0, 'G': -2.0, 'F': -3.0, 'I': -3.0, 'H': 0.0, 'K': 1.0, 'M': -2.0, 'L': -3.0, 'N': 0.0, 'Q': 2.0, 'P': -1.0, 'S': 0.0, 'R': 0.0, 'T': -1.0, 'W': -3.0, 'V': -2.0, 'Y': -2.0, 'X': -1.0, 'Z': 4.0}, 'D': {'*': -4.0, '-': -4, 'A': -2.0, 'C': -3.0, 'B': 4.0, 'E': 2.0, 'D': 6.0, 'G': -1.0, 'F': -3.0, 'I': -3.0, 'H': -1.0, 'K': -1.0, 'M': -3.0, 'L': -4.0, 'N': 1.0, 'Q': 0.0, 'P': -1.0, 'S': 0.0, 'R': -2.0, 'T': -1.0, 'W': -4.0, 'V': -3.0, 'Y': -3.0, 'X': -1.0, 'Z': 1.0}, 'G': {'*': -4.0, '-': -4, 'A': 0.0, 'C': -3.0, 'B': -1.0, 'E': -2.0, 'D': -1.0, 'G': 6.0, 'F': -3.0, 'I': -4.0, 'H': -2.0, 'K': -2.0, 'M': -3.0, 'L': -4.0, 'N': 0.0, 'Q': -2.0, 'P': -2.0, 'S': 0.0, 'R': -2.0, 'T': -2.0, 'W': -2.0, 'V': -3.0, 'Y': -3.0, 'X': -1.0, 'Z': -2.0}, 'F': {'*': -4.0, '-': -4, 'A': -2.0, 'C': -2.0, 'B': -3.0, 'E': -3.0, 'D': -3.0, 'G': -3.0, 'F': 6.0, 'I': 0.0, 'H': -1.0, 'K': -3.0, 'M': 0.0, 'L': 0.0, 'N': -3.0, 'Q': -3.0, 'P': -4.0, 'S': -2.0, 'R': -3.0, 'T': -2.0, 'W': 1.0, 'V': -1.0, 'Y': 3.0, 'X': -1.0, 'Z': -3.0}, 'I': {'*': -4.0, '-': -4, 'A': -1.0, 'C': -1.0, 'B': -3.0, 'E': -3.0, 'D': -3.0, 'G': -4.0, 'F': 0.0, 'I': 4.0, 'H': -3.0, 'K': -3.0, 'M': 1.0, 'L': 2.0, 'N': -3.0, 'Q': -3.0, 'P': -3.0, 'S': -2.0, 'R': -3.0, 'T': -1.0, 'W': -3.0, 'V': 3.0, 'Y': -1.0, 'X': -1.0, 'Z': -3.0}, 'H': {'*': -4.0, '-': -4, 'A': -2.0, 'C': -3.0, 'B': 0.0, 'E': 0.0, 'D': -1.0, 'G': -2.0, 'F': -1.0, 'I': -3.0, 'H': 8.0, 'K': -1.0, 'M': -2.0, 'L': -3.0, 'N': 1.0, 'Q': 0.0, 'P': -2.0, 'S': -1.0, 'R': 0.0, 'T': -2.0, 'W': -2.0, 'V': -3.0, 'Y': 2.0, 'X': -1.0, 'Z': 0.0}, 'K': {'*': -4.0, '-': -4, 'A': -1.0, 'C': -3.0, 'B': 0.0, 'E': 1.0, 'D': -1.0, 'G': -2.0, 'F': -3.0, 'I': -3.0, 'H': -1.0, 'K': 5.0, 'M': -1.0, 'L': -2.0, 'N': 0.0, 'Q': 1.0, 'P': -1.0, 'S': 0.0, 'R': 2.0, 'T': -1.0, 'W': -3.0, 'V': -2.0, 'Y': -2.0, 'X': -1.0, 'Z': 1.0}, 'M': {'*': -4.0, '-': -4, 'A': -1.0, 'C': -1.0, 'B': -3.0, 'E': -2.0, 'D': -3.0, 'G': -3.0, 'F': 0.0, 'I': 1.0, 'H': -2.0, 'K': -1.0, 'M': 5.0, 'L': 2.0, 'N': -2.0, 'Q': 0.0, 'P': -2.0, 'S': -1.0, 'R': -1.0, 'T': -1.0, 'W': -1.0, 'V': 1.0, 'Y': -1.0, 'X': -1.0, 'Z': -1.0}, 'L': {'*': -4.0, '-': -4, 'A': -1.0, 'C': -1.0, 'B': -4.0, 'E': -3.0, 'D': -4.0, 'G': -4.0, 'F': 0.0, 'I': 2.0, 'H': -3.0, 'K': -2.0, 'M': 2.0, 'L': 4.0, 'N': -3.0, 'Q': -2.0, 'P': -3.0, 'S': -2.0, 'R': -2.0, 'T': -1.0, 'W': -2.0, 'V': 1.0, 'Y': -1.0, 'X': -1.0, 'Z': -3.0}, 'N': {'*': -4.0, '-': -4, 'A': -2.0, 'C': -3.0, 'B': 3.0, 'E': 0.0, 'D': 1.0, 'G': 0.0, 'F': -3.0, 'I': -3.0, 'H': 1.0, 'K': 0.0, 'M': -2.0, 'L': -3.0, 'N': 6.0, 'Q': 0.0, 'P': -2.0, 'S': 1.0, 'R': 0.0, 'T': 0.0, 'W': -4.0, 'V': -3.0, 'Y': -2.0, 'X': -1.0, 'Z': 0.0}, 'Q': {'*': -4.0, '-': -4, 'A': -1.0, 'C': -3.0, 'B': 0.0, 'E': 2.0, 'D': 0.0, 'G': -2.0, 'F': -3.0, 'I': -3.0, 'H': 0.0, 'K': 1.0, 'M': 0.0, 'L': -2.0, 'N': 0.0, 'Q': 5.0, 'P': -1.0, 'S': 0.0, 'R': 1.0, 'T': -1.0, 'W': -2.0, 'V': -2.0, 'Y': -1.0, 'X': -1.0, 'Z': 3.0}, 'P': {'*': -4.0, '-': -4, 'A': -1.0, 'C': -3.0, 'B': -2.0, 'E': -1.0, 'D': -1.0, 'G': -2.0, 'F': -4.0, 'I': -3.0, 'H': -2.0, 'K': -1.0, 'M': -2.0, 'L': -3.0, 'N': -2.0, 'Q': -1.0, 'P': 7.0, 'S': -1.0, 'R': -2.0, 'T': -1.0, 'W': -4.0, 'V': -2.0, 'Y': -3.0, 'X': -2.0, 'Z': -1.0}, 'S': {'*': -4.0, '-': -4, 'A': 1.0, 'C': -1.0, 'B': 0.0, 'E': 0.0, 'D': 0.0, 'G': 0.0, 'F': -2.0, 'I': -2.0, 'H': -1.0, 'K': 0.0, 'M': -1.0, 'L': -2.0, 'N': 1.0, 'Q': 0.0, 'P': -1.0, 'S': 4.0, 'R': -1.0, 'T': 1.0, 'W': -3.0, 'V': -2.0, 'Y': -2.0, 'X': 0.0, 'Z': 0.0}, 'R': {'*': -4.0, '-': -4, 'A': -1.0, 'C': -3.0, 'B': -1.0, 'E': 0.0, 'D': -2.0, 'G': -2.0, 'F': -3.0, 'I': -3.0, 'H': 0.0, 'K': 2.0, 'M': -1.0, 'L': -2.0, 'N': 0.0, 'Q': 1.0, 'P': -2.0, 'S': -1.0, 'R': 5.0, 'T': -1.0, 'W': -3.0, 'V': -3.0, 'Y': -2.0, 'X': -1.0, 'Z': 0.0}, 'T': {'*': -4.0, '-': -4, 'A': 0.0, 'C': -1.0, 'B': -1.0, 'E': -1.0, 'D': -1.0, 'G': -2.0, 'F': -2.0, 'I': -1.0, 'H': -2.0, 'K': -1.0, 'M': -1.0, 'L': -1.0, 'N': 0.0, 'Q': -1.0, 'P': -1.0, 'S': 1.0, 'R': -1.0, 'T': 5.0, 'W': -2.0, 'V': 0.0, 'Y': -2.0, 'X': 0.0, 'Z': -1.0}, 'W': {'*': -4.0, '-': -4, 'A': -3.0, 'C': -2.0, 'B': -4.0, 'E': -3.0, 'D': -4.0, 'G': -2.0, 'F': 1.0, 'I': -3.0, 'H': -2.0, 'K': -3.0, 'M': -1.0, 'L': -2.0, 'N': -4.0, 'Q': -2.0, 'P': -4.0, 'S': -3.0, 'R': -3.0, 'T': -2.0, 'W': 11.0, 'V': -3.0, 'Y': 2.0, 'X': -2.0, 'Z': -3.0}, 'V': {'*': -4.0, '-': -4, 'A': 0.0, 'C': -1.0, 'B': -3.0, 'E': -2.0, 'D': -3.0, 'G': -3.0, 'F': -1.0, 'I': 3.0, 'H': -3.0, 'K': -2.0, 'M': 1.0, 'L': 1.0, 'N': -3.0, 'Q': -2.0, 'P': -2.0, 'S': -2.0, 'R': -3.0, 'T': 0.0, 'W': -3.0, 'V': 4.0, 'Y': -1.0, 'X': -1.0, 'Z': -2.0}, 'Y': {'*': -4.0, '-': -4, 'A': -2.0, 'C': -2.0, 'B': -3.0, 'E': -2.0, 'D': -3.0, 'G': -3.0, 'F': 3.0, 'I': -1.0, 'H': 2.0, 'K': -2.0, 'M': -1.0, 'L': -1.0, 'N': -2.0, 'Q': -1.0, 'P': -3.0, 'S': -2.0, 'R': -2.0, 'T': -2.0, 'W': 2.0, 'V': -1.0, 'Y': 7.0, 'X': -1.0, 'Z': -2.0}, 'X': {'*': -4.0, '-': -4, 'A': 0.0, 'C': -2.0, 'B': -1.0, 'E': -1.0, 'D': -1.0, 'G': -1.0, 'F': -1.0, 'I': -1.0, 'H': -1.0, 'K': -1.0, 'M': -1.0, 'L': -1.0, 'N': -1.0, 'Q': -1.0, 'P': -2.0, 'S': 0.0, 'R': -1.0, 'T': 0.0, 'W': -2.0, 'V': -1.0, 'Y': -1.0, 'X': -1.0, 'Z': -1.0}, 'Z': {'*': -4.0, '-': -4, 'A': -1.0, 'C': -3.0, 'B': 1.0, 'E': 4.0, 'D': 1.0, 'G': -2.0, 'F': -3.0, 'I': -3.0, 'H': 0.0, 'K': 1.0, 'M': -1.0, 'L': -3.0, 'N': 0.0, 'Q': 3.0, 'P': -1.0, 'S': 0.0, 'R': 0.0, 'T': -1.0, 'W': -3.0, 'V': -2.0, 'Y': -2.0, 'X': -1.0, 'Z': 4.0}}
var beta = 10;

//#################
//#################
// DRAW
//#################
//#################

pp2a = {
	/*
	 VARIABLES
	 */

	sequence: "",
	disorder_score: [],
	similarity_cutoff: {},
	pwm_length: 0,
	pwm: "dbox",

	/*
	 FUNCTIONS
	 */
	reset: function () {
		pp2a.sequence = "";
		pp2a.disorder_score = [];
		pp2a.similarity_cutoff = {};
		pp2a.pwm_length = 0;
		pp2a.pwm = "dbox";
	},
	calculateCutoff: function () {
		var random_peptide_score_list = [];
		for (i = 0; i < 10000000; i++) {
			var random_peptide = pp2a.randomString();
			var random_peptide_scores = pp2a.scorePeptide(random_peptide);
			var random_peptide_score = random_peptide_scores.reduce(function (a, b) {
				return a + b;
			});
			random_peptide_score_list.push(random_peptide_score);
		}

		random_peptide_score_list.sort(function (a, b) {
			return a - b
		});
		random_peptide_score_list.reverse();

		pp2a.similarity_cutoff["0.000001"] = random_peptide_score_list[10];
		pp2a.similarity_cutoff["0.00001"] = random_peptide_score_list[100];
		pp2a.similarity_cutoff["0.0001"] = random_peptide_score_list[1000];

		console.log(pp2a.similarity_cutoff);
	},
	calculateMax: function () {
		var similarity_max = 0;

		for (var p_i = 0; p_i < pp2a.pwm_length; p_i++) {
			var column_scores = [];
			for (var c_i = 0; c_i < residues.length; c_i++) {
				column_scores.push(pp2a.use_scores[p_i][residues[c_i]]);
			}

			var score = Math.max.apply(null, column_scores);

			if (!isNaN(parseFloat(score)) && isFinite(score)) {
				similarity_max += (score / pwm_max[pp2a.pwm]) * ics[pp2a.pwm][p_i];
			}
		}
		pp2a.similarity_max = similarity_max;
	},
	scorePeptide: function (peptide) {
		var summer = [];

		for (p_i = 0; p_i < peptide.length; p_i++) {
			score = pp2a.use_scores[p_i][peptide[p_i]];

			if (!isNaN(parseFloat(score)) && isFinite(score)) {
				summer.push((score / pwm_max[pp2a.pwm]) * ics[pp2a.pwm][p_i]);
			}
		}
		return summer;
	},
	randomString: function () {
		var text = "";
		var possible = "ACDEFGHIKLMNPQRSTVWY";
		for (var i = 0; i < pp2a.pwm_length; i++) {
			text += possible.charAt(Math.floor(Math.random() * possible.length));
		}
		return text;
	},
	unique: function (nonunique_array) {
		var n = {}, r = [];

		for (var i = 0; i < nonunique_array.length; i++) {
			if (!n[nonunique_array[i]]) {
				n[nonunique_array[i]] = true;
				r.push(nonunique_array[i]);
			}
		}
		return r;

	},
	extractSequence: function (sequenceHTML) {
		var sequence_gapped = "";
		var sequence_ungapped = "";
		$(sequenceHTML).find('.aC').each(function (indexaa, aa) {
			aa = $(aa);
			sequence_gapped += aa.html();
			if (!aa.hasClass('gap') && !(aa.html().localeCompare('-') == 0)) {
				sequence_ungapped += aa.html();
			} else if (!aa.hasClass('gap') && (aa.html().localeCompare('-') == 0)) {
				sequence_ungapped += aa.html();
			}
		});
		return sequence_ungapped;
	},
	extractIupred: function (iupredHTML) {
		var disorderScore = [];

		$(iupredHTML).find('div').each(function (indexaa, aa) {
			if ($(aa).data("tooltip") != undefined) {
				disorderScore.push(parseFloat($(aa).data("tooltip").split(":")[1]));
			}
		});

		return disorderScore;
	},
	extractPfam: function (pfamHTML) {
		var domains = [];
		$(pfamHTML).find('.fC').each(function (indexdomain, domain) {
			if ($(domain).data("pos") != undefined) {
				var pos = $(domain).data("pos").split(":");
				var start = pos[0];
				var end = pos[1];
				var disorder = "?";
			
				if (pp2a.data.ali_start == 0 && pp2a.data.ali_end == pp2a.data.protein_length)
				{
					if (pp2a.disorder_score.length != 0) {
						disorder = ((pp2a.disorder_score.slice(start, end).reduce(function (a, b) {
							return a + b;
						})) / (end - start)).toFixed(2);
					}
				}
			
				var domain_name = $(domain).data("tooltip");
				domains.push([start, end, domain_name, disorder]);
			}
		});
		return domains;
	},
	extractMods: function (modHTML) {
		var mods = [];

		$(modHTML).find('.fC').each(function (indexmod, mod) {
			if ($(mod).data("pos") != undefined) {
				var pos = $(mod).data("pos").split(":");
				var start = pos[0];
				var end = pos[1];
				var mod_name = $(mod).data("tooltip");

				mods.push([start, end, mod_name]);
			}
		});

		return mods;
	},
	extractSS: function (ssHTML) {
		var sss = [];

		$(ssHTML).find('.fC').each(function (indexss, ss) {
			if ($(ss).data("pos") != undefined) {

				var pos = $(ss).data("pos").split(":");
				var start = pos[0];
				var end = pos[1];
				var ss_name = $(ss).data("tooltip");

				sss.push([start, end, ss_name]);
			}
		});

		return sss;
	},
	extractHModel: function (hModelHTML) {
		var hModels = [];

		$(hModelHTML).find('.fC').each(function (indexhModel, hModel) {
			if ($(hModel).data("pos") != undefined) {

				var pos = $(hModel).data("pos").split(":");
				var start = pos[0];
				var end = pos[1];
				var hModel_name = $(hModel).html();

				hModels.push([start, end, hModel_name]);
			}
		});

		return hModels;
	},
	extractPdb: function (pdbHTML) {
		var pdbs = [];

		$(pdbHTML).find('.fC').each(function (indexpdb, pdb) {
			if ($(pdb).data("pos") != undefined) {
				var pos = $(pdb).data("pos").split(":");
				var start = pos[0];
				var end = pos[1];
				var pdb_name = $(pdb).data("tooltip").split("<br>")[0];

				pdbs.push(pdb_name);
			}
		});

		return pdbs;
	},

	isValidated: function (start, end, validated_motifs) {

		var output = "no";
		for (motif in validated_motifs) {
			motif_type = validated_motifs[motif]["type"];
			motif_start = validated_motifs[motif]["start"];
			motif_end = validated_motifs[motif]["end"];
			motif_pmid = validated_motifs[motif]["pmid"];
		
			if (((parseInt(pp2a.data.ali_start)+start + 1) >= parseInt(motif_start) && (parseInt(pp2a.data.ali_start) + start + 1) < parseInt(motif_end)) || ((parseInt(pp2a.data.ali_start) + end + 1) > parseInt(motif_start) && (parseInt(pp2a.data.ali_start) + end + 1) <= parseInt(motif_end))) {
				output = 'yes (<small><a target="_blank" href="http://www.ncbi.nlm.nih.gov/pubmed/' + motif_pmid + '">paper</a></small>)';
			}
		}

		return output;
	},
	overlap: function (start, end, features) {
		var overlaps = [];
		for (feature in features) {
			if (((parseInt(pp2a.data.ali_start)+start) >= parseInt(features[feature][0]) && (parseInt(pp2a.data.ali_start)+start) <= parseInt(features[feature][1])) || ((parseInt(pp2a.data.ali_start)+end) >= parseInt(features[feature][0]) && (parseInt(pp2a.data.ali_start)+end) <= parseInt(features[feature][1]))) {
				overlaps.push(features[feature]);
			}
			else {
				if ((start <= parseInt(features[feature][0]) && end >= parseInt(features[feature][1]))) {
					overlaps.push(features[feature]);
				}
			}
		}
		return overlaps;
	},
	translate_score_to_color: function (start, end, steps, count) {
		var s = start,
			e = end,
			final = s + (((e - s) / steps) * count);
		return Math.floor(final);
	},
	translate_score_to_RGB: function (scoreToColor, color) {
		/*
		 if (color == "blue")
		 {max_color = [104, 189, 246];}

		 if (color == "red")
		 {max_color = [255, 148, 148];}

		 if (color == "green")
		 {max_color = [ 148, 255, 148];}
		 */

		var max_color = [140, 140, 255];
		var min_color = [256,256, 256];

		var red = 256;
		var green = 256;
		var blue = 256;

		scoreToColor = Math.abs(scoreToColor);

		var red = pp2a.translate_score_to_color(min_color[0], max_color[0], 100, parseInt(scoreToColor * 100));
		var green = pp2a.translate_score_to_color(min_color[1], max_color[1], 100, parseInt(scoreToColor * 100));
		var blue = pp2a.translate_score_to_color(min_color[2], max_color[2], 100, parseInt(scoreToColor * 100));

		color_gradient = 'rgb(' + red + "," + green + "," + blue + ')';
		return color_gradient;
	},
	focus: function (min, max) {
		//console.log(min*10);
		//console.log($("#trackData").width(), $("#trackData").scrollLeft());

		$("#trackData").animate({scrollLeft: min * 10 - ($("#trackData").width() / 2) + 50}, 'fast');
	},
	convert_profiles: function (min, max) {
		for (var key in pwms_aa) {
		
			pwms[key] = {};
		
			for (var i = 0; i < pwms_aa[key]["A"].length; i++) {
				pwms[key][i] = {};
			}
				
			for (aa in pwms_aa[key]){
				for (var i = 0; i < pwms_aa[key][aa].length; i++) {
				pwms[key][i][aa] = pwms_aa[key][aa][i];
				}
			}
	   }
	},
	annotate: function (data) {
		var tracks = [];

		pp2a.data = data;
   
		var start_adjustment = parseInt(-1);
	
		pp2a.convert_profiles();
	
		if (pp2a.data.ali_start != undefined) {
			start_adjustment = parseInt(pp2a.data.ali_start) - 1;
		}

		var protein_acc = data.protein_acc;

		var validated_motifs = validated[protein_acc];

		var sequenceHTML = pp2a.data.alignmentDataQuery;
		var iupredHTML = "";
		if (data.tracks.iupred) {
			iupredHTML = pp2a.data.tracks.iupred[1];
		}
		var pfamHTML = "";
		if (pp2a.data.tracks.pfam) {
			pfamHTML = pp2a.data.tracks.pfam[1];
		}

		var modHTML = "";
		if (pp2a.data.tracks['modified residue']) {
			modHTML = modHTML + pp2a.data.tracks['modified residue'][1];
		}
		if (pp2a.data.tracks['phospho.elm']) {
			modHTML = modHTML + pp2a.data.tracks['phospho.elm'][1];
		}

		if (pp2a.data.tracks['secondary structure']) {
			ssHTML = pp2a.data.tracks['secondary structure'][1];
		}
		else {
			ssHTML = "";
		}

		if (pp2a.data.tracks['homology model']) {
			hModelHTML = pp2a.data.tracks['homology model'][1];
		}
		else {
			hModelHTML = "";
		}

		if (pp2a.data.tracks['PDB']) {
			pdbHTML = pp2a.data.tracks['PDB'][1];
		}
		else {
			pdbHTML = "";
		}

		//var pp2a = {};
		pp2a.sequence = pp2a.extractSequence(sequenceHTML);
		pp2a.sequence_length = pp2a.sequence.length;
	
   
		pp2a.disorder_score = pp2a.extractIupred(iupredHTML);
		pp2a.pfam = pp2a.extractPfam(pfamHTML);
   
		pp2a.modification = pp2a.extractMods(modHTML);
	 
		pp2a.ss = pp2a.extractSS(ssHTML);
		pp2a.hModel = pp2a.extractHModel(hModelHTML);
		pp2a.pdb = pp2a.extractPdb(pdbHTML);
	
			
		var table = "";

		table = table + '<br><b>Predicted PP2A docking motifs</b><br><small><a href="http://slim.ucd.ie/pp2a/index.php?page=scan_protein">Check another protein for PP2A docking motifs</a></small>';
		table = table + '<tr class="table_head">';
		table = table + "<td>";
		table = table + "Motif<br>type";
		table = table + "</td>";
		table = table + "<td>";
		table = table + "Validated";
		table = table + "</td>";
		table = table + "<td>";
		table = table + "Motif sequence<br><i>(click to view)</i>";
		table = table + "</td>";
		table = table + "<td>";
		table = table + "Start";
		table = table + "</td>";
		table = table + "<td>";
		table = table + "Stop";
		table = table + "</td>";
		table = table + "<td>";
		table = table + "S<sup>pep</sup>";
		table = table + "</td>";
		table = table + "<td>";
		table = table + "Normalised<br>S<sup>pep</sup>";
		table = table + "</td>";
	   table = table + "<td>";
		table = table + "Consensus<br>similarity";
		table = table + "</td>";

		table = table + "<td>";
		table = table + "Disorder<br>score";
		table = table + "</td>";

		table = table + "<td>";
		table = table + "Domain";
		table = table + "</td>";

		table = table + "<td>";
		table = table + "Structure";
		table = table + "</td>";

		table = table + "<td>";
		table = table + "Modification";
		table = table + "</td>";
		table = table + "</tr>";

		var tableConfident = "";
		var tableOther = "";

		//features["data"] = [];

		for (var key in pwms) {
			features = [];
			pp2a.pwm = key;
			pp2a.use_scores = pwms[key];
			pp2a.pwm_length = Object.keys(pwms[key]).length;

			pp2a.similarity_cutoff = similarity_cutoff[pp2a.pwm];
			//console.log(key);
			//pp2a.calculateCutoff();
			pp2a.calculateMax();
		
			var PSSM_hits = 0;
			
			for (var seq_iter = -pp2a.pwm_length + 1; seq_iter < (pp2a.sequence_length); seq_iter++) {
				var padding = "";
				var pad = "-----------";
				var padding_length, peptide;
				if (seq_iter < 0) {
					padding_length = Math.abs(seq_iter);
					padding = pad.substring(0, padding_length);
					peptide = padding + pp2a.sequence.substr(Math.max(0, seq_iter), pp2a.pwm_length + seq_iter);
				} else if (seq_iter > pp2a.sequence_length - pp2a.pwm_length) {
					padding_length = (seq_iter - (pp2a.sequence_length - pp2a.pwm_length));
					padding = pad.substring(0, padding_length);
					peptide = pp2a.sequence.substr(seq_iter, pp2a.pwm_length - (seq_iter - (pp2a.sequence_length - pp2a.pwm_length))) + padding;
				}
				else {
					peptide = pp2a.sequence.substr(seq_iter, pp2a.pwm_length);
				}

				var peptide_scores = pp2a.scorePeptide(peptide);
				var peptide_score = peptide_scores.reduce(function (a, b) {
					return a + b;
				});
	
				var cutoff = 0.5;
		   
			
				if (peptide_score >  0 && (peptide.substr(0,1) == "L" || peptide.substr(3,1) == "I")) {
					PSSM_hits += 1;
					console.log(peptide,PSSM_hits);
					featureTmp = {};
					featureTmp.id = key + "_" + seq_iter;
					featureTmp.sequence = peptide;
					featureTmp.hover = name_mapping[key] + " - " + pp2a.sequence.substr(seq_iter, pp2a.pwm_length) + " - " + (peptide_score / pp2a.similarity_max).toFixed(2); //" - " + (start_adjustment + seq_iter+2) + ":" + ((start_adjustment+ seq_iter + peptide.length)+1) +
					featureTmp.start = (start_adjustment + seq_iter + 2);
					featureTmp.end = (start_adjustment + seq_iter + peptide.length + 1);
					featureTmp.class = "pp2a_peptide";
					featureTmp.len = peptide.length;
					featureTmp.colour = pp2a.translate_score_to_RGB(((peptide_score / pp2a.similarity_max)), motif_colour[key]);
					featureTmp.text_colour = "#000";
					featureTmp.scores = [];

					for (var score in peptide_scores) {
						featureTmp.scores.push((peptide_scores[score] - 0.5));
					}

					featureTmp.scores = peptide_scores;
					features.push(featureTmp);

					var dis_score = 1;

					if (pp2a.disorder_score.length != 0) {
						peptide_discores = pp2a.disorder_score.slice(Math.max(0, seq_iter), Math.min(seq_iter + pp2a.pwm_length, pp2a.sequence_length - 1));
						dis_score = peptide_discores.reduce(function (a, b) {
								return a + b;
							}) / peptide_discores.length;
					}


					var motif_validated = pp2a.isValidated(seq_iter, seq_iter + pp2a.pwm_length, validated_motifs);

					var tableStr = "";
					var badRow = false;

					if (motif_validated.localeCompare("no") == 1) {
						tableStr = tableStr + '<tr bgcolor="#EFE" style="">';
					}
					else if (peptide_score / pp2a.similarity_max < pp2a.similarity_cutoff["0.0001"] / pp2a.similarity_max || dis_score < 0.3) {
						tableStr = tableStr + '<tr bgcolor="#FEE" style="font-style:italic; font-size:90%">';
						badRow = true;
					}
					else {
						tableStr = tableStr + '<tr>';
					}

					tableStr = tableStr + "<td>";
					tableStr = tableStr + name_mapping[key];
					tableStr = tableStr + "</td>";

					tableStr = tableStr + "<td>";
					tableStr = tableStr + motif_validated;
					tableStr = tableStr + "</td>";

					tableStr = tableStr + "<td>";
					tableStr = tableStr + '<a class="pp2a_link" id="' + key + "_" + (start_adjustment + seq_iter + 1) + '">' + peptide + "</a>";
					tableStr = tableStr + "</td>";
					tableStr = tableStr + "<td>";
					tableStr = tableStr + (start_adjustment + seq_iter + 2);
					tableStr = tableStr + "</td>";
					tableStr = tableStr + "<td>";
					tableStr = tableStr + (start_adjustment + seq_iter + peptide.length + 1);
					tableStr = tableStr + "</td>";

					tableStr = tableStr + "<td>";
					tableStr = tableStr + (peptide_score).toFixed(2);
					tableStr = tableStr + "</td>";
				
					tableStr = tableStr + "<td>";
					tableStr = tableStr + (peptide_score / pp2a.similarity_max).toFixed(2);

					if (peptide_score / pp2a.similarity_max > 0.85) {
						tableStr = tableStr + ' <i class="fa fa-check"></i>';
					}

					tableStr = tableStr + "</td>";
			   
				
					if (peptide_score / pp2a.similarity_max > pp2a.similarity_cutoff["0.000001"] / pp2a.similarity_max) {
						tableStr = tableStr + "<td>";
						tableStr = tableStr + '<b>***</b>';
						tableStr = tableStr + "</td>";
					}
					else if (peptide_score / pp2a.similarity_max > pp2a.similarity_cutoff["0.00001"] / pp2a.similarity_max) {
						tableStr = tableStr + "<td >";
						tableStr = tableStr + '<b>**</b>';
						tableStr = tableStr + "</td>";
					}
					else if (peptide_score / pp2a.similarity_max > pp2a.similarity_cutoff["0.0001"] / pp2a.similarity_max) {
						tableStr = tableStr + "<td>";
						tableStr = tableStr + '<b>*</b>';
						tableStr = tableStr + "</td>";
					}
				   else {
						tableStr = tableStr + "<td>";
						tableStr = tableStr + '';
						tableStr = tableStr + "</td>";
					}

					tableStr = tableStr + "<td>";

					if (pp2a.disorder_score.length != 0) {
						peptide_discores = pp2a.disorder_score.slice(Math.max(0, seq_iter), Math.min(seq_iter + pp2a.pwm_length, pp2a.sequence_length - 1));
						dis_score = peptide_discores.reduce(function (a, b) {
								return a + b;
							}) / peptide_discores.length;

						tableStr = tableStr + (dis_score).toFixed(2);

						if (dis_score > 0.4) {
							tableStr = tableStr + ' <i class="fa fa-check"></i>';
						}
						else if (dis_score > 0.3) {
							tableStr = tableStr + ' <i class="fa fa-exclamation"></i>';
						}
						else {
							tableStr = tableStr + ' <i class="fa fa-times"></i>';
						}

					}
					else {
						tableStr = tableStr + "N/A";
					}

					tableStr = tableStr + "</td>";
					tableStr = tableStr + "<td>";
				
					var overlap = pp2a.overlap(seq_iter, seq_iter + pp2a.pwm_length, pp2a.pfam);
					if (overlap.length > 0) {
						tableStr = tableStr + overlap[0][2] + " (" + overlap[0][3] + ")";
						if (overlap[0][3] > 0.4) {
							tableStr = tableStr + " <br><i>(disordered domain?)</i>";
						}

						tableStr = tableStr + "<br>";
					}

					tableStr = tableStr + "</td>";
					tableStr = tableStr + "<td>";

					overlap = pp2a.overlap(seq_iter, seq_iter + pp2a.pwm_length, pp2a.ss);
					if (overlap.length > 0) {
						tableStr = tableStr + overlap[0][2];
					}

					overlap = pp2a.overlap(seq_iter, seq_iter + pp2a.pwm_length, pp2a.pdb);
					if (overlap.length > 0) {
						tableStr = tableStr + " (<a href='http://www.rcsb.org/pdb/search/smart.do?smartSearchSubtype_0=StructureIdQuery&structureIdList_0=" + overlap.join(",") + "'>PDB</a>)";
					}
					else {
						overlap = pp2a.overlap(seq_iter, seq_iter + pp2a.pwm_length, pp2a.hModel);
						if (overlap.length > 0) {
							tableStr = tableStr + " (" + overlap[0][2] + ")";
						}
					}
					tableStr = tableStr + "</td>";

					tableStr = tableStr + "<td>";
					overlap = pp2a.overlap(seq_iter - 5, seq_iter + pp2a.pwm_length + 5, pp2a.modification);

					if (overlap.length > 0) {
						mods = [];
						for (mod in overlap) {
							mods.push("" + overlap[mod][2] + '@' + overlap[mod][0] + "");
						}

						tableStr = tableStr + mods.join("<br>");
					}
					tableStr = tableStr + "</td>";

					tableStr = tableStr + "</tr>";

					if (badRow) {
						tableOther += tableStr;
					}
					else {
						tableConfident += tableStr;
					}

				}
			}
		}
	
	 
		if (PSSM_hits > 0)
		{
			var track = {};
			track.data = features;
			track.type = "peptides";
			track.position = -1;
			track.name = "Putative " + name_mapping[key];
			track.colour = "#FFF";
			track.opacity = "1";
			track.id = null;
			track.class = null;
			track.height = null;
			track.help = null;
			track.text_colour = null;
			tracks.push(track);
   
			$(".tableContainer").html('<table width="100%">' + table + tableConfident + tableOther + "</table>");
			var footnotes = "";
			footnotes = footnotes + '<b>S<sup>pep</sup>:</b>PSSM score of the peptide; &nbsp&nbsp&nbsp&nbsp&nbsp ';
			footnotes = footnotes + ' <b>Normalised S<sup>pep</sup>:</b>S<sup>pep</sup>/(max possible peptide score for a PSSM); &nbsp&nbsp&nbsp&nbsp&nbsp ';
			footnotes = footnotes + ' <b>Consensus similarity p(S<sup>pep</sup>):</b> ***<0.000001 **<0.00001 *<0.0001; <br><br>';
	
			$(".tableContainer").append(footnotes)
			$(".table_head").css("font-weight", "700").css("background", "#F0F0F0");


			$('.pp2a_link').on('click', function () {
				pp2a.focus($(this).attr("id").split("_")[1]);
				$(".highlight").removeClass("highlight");
				$(this).addClass("highlight");
				console.log("#" + $(this).attr("id") + ".pp2a_peptide");
				$("#" + $(this).attr("id") + ".pp2a_peptide").addClass("highlight");
			});
		}
		else
		{
			var outStr = '<br><b>Predicted PP2A docking motifs</b><br>';
			outStr = outStr + '<br>No PSSM hits found<br>';
			outStr = outStr + '<small><a href="http://slim.ucd.ie/pp2a/index.php?page=scan_protein">Check another protein for PP2A docking motifs</a></small>';
		
			$(".tableContainer").html(outStr);
		
		}
   

		/*
		 =======
		 */
	 
	 
		var features = [];

		for (var motif in validated_motifs) {
			var motif_type = validated_motifs[motif]["type"];
			var motif_start = validated_motifs[motif]["start"];
			var motif_end = validated_motifs[motif]["end"];
			var motif_pmid = validated_motifs[motif]["pmid"];

			var featureTmp = {};
			featureTmp.id = "motif_" + motif;
			featureTmp.sequence = motif_type;
			featureTmp.hover = "Validated " + motif_type;
			featureTmp.start = parseInt(motif_start) ;
			featureTmp.end = parseInt(motif_end);
			featureTmp.class = "pp2a_peptide";
			
			featureTmp.text = "PP2A B56 site";
			featureTmp.len = motif_end - motif_start + 1;
			featureTmp.colour = "#FFC200";
			featureTmp.text_colour = "#000";
			featureTmp.link = 'http://www.ncbi.nlm.nih.gov/pubmed/' + motif_pmid;
			featureTmp.scores = [];

			features.push(featureTmp);
		}

		if (features.length > 0)
			{
			track = {};
			track.data = features;
			track.type = "feature";
			track.position = -1;
			track.name = "Validated motifs";
			track.colour = "#FFF";
			track.opacity = "1";
			track.id = null;
			track.class = null;
			track.height = null;
			track.help = null;
			track.text_colour = null;
			tracks.push(track);
			}

		/*
		 =======
		 */
	
	
		PROVIZ.displayTracks(tracks);
	
	}
};

