
// Copyright (C) 2016 Norman E. Davey <normandavey@gmail.com>, Jean Manguy <jean.manguy@gmail.com> and Peter Jehl <peter.jehl@ucdconnect.ie>
// This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
// Public License as published by the Free Software Foundation; either version 2.1 of the License, or (at your option)
// any later version.
// 
// This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
// 
// You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
// the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Author contact: Norman E. Davey <normandavey@gmail.com> / Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
// Author contact: Jean Manguy <jean.manguy@gmail.com> / Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
// Author contact: Peter Jehl <peter.jehl@ucdconnect.ie> / Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.

var background = {
"all":{'A': 0.086167, 'C': 0.012759, 'D': 0.052951, 'E': 0.061395, 'F': 0.040392, 'G': 0.071295, 'H': 0.021925, 'I': 0.060238, 'K': 0.052746, 'L': 0.098334, 'M': 0.024703, 'N': 0.041559, 'P': 0.047353, 'Q': 0.038562, 'R': 0.054707, 'S': 0.066961, 'T': 0.056144, 'V': 0.067440, 'W': 0.013183, 'Y': 0.030613},
"0.3":{"A":0.082767,"C":0.007309,"D":0.058283,"E":0.082903,"F":0.022992,"G":0.073166,"H":0.024454,"I":0.043799,"K":0.064038,"L":0.073925,"M":0.023502,"N":0.043715,"P":0.062209,"Q":0.048512,"R":0.064976,"S":0.075305,"T":0.062569,"V":0.059192,"W":0.007122,"Y":0.019261},
"0.5":{"A":0.079609,"C":0.005029,"D":0.057989,"E":0.08815,"F":0.015527,"G":0.075008,"H":0.024831,"I":0.028494,"K":0.06696,"L":0.055605,"M":0.021528,"N":0.047938,"P":0.085282,"Q":0.055837,"R":0.067414,"S":0.0964,"T":0.065369,"V":0.044676,"W":0.004848,"Y":0.013504}
}

var aas = ["S","T","G","A","P","C","I","L","V","M","F","Y","W","H","K","R","D","E","N","Q"];

var substitution_matrix = {'*': {'*': 1.0, '-': -4, 'A': -4.0, 'C': -4.0, 'B': -4.0, 'E': -4.0, 'D': -4.0, 'G': -4.0, 'F': -4.0, 'I': -4.0, 'H': -4.0, 'K': -4.0, 'M': -4.0, 'L': -4.0, 'N': -4.0, 'Q': -4.0, 'P': -4.0, 'S': -4.0, 'R': -4.0, 'T': -4.0, 'W': -4.0, 'V': -4.0, 'Y': -4.0, 'X': -4.0, 'Z': -4.0}, '-': {'*': -4, '-': -1, 'A': -4, 'C': -4, 'B': -4, 'E': -4, 'D': -4, 'G': -4, 'F': -4, 'I': -4, 'H': -4, 'K': -4, 'M': -4, 'L': -4, 'N': -4, 'Q': -4, 'P': -4, 'S': -4, 'R': -4, 'T': -4, 'W': -4, 'V': -4, 'Y': -4, 'X': -4, 'Z': -4}, 'A': {'*': -4.0, '-': -4, 'A': 4.0, 'C': 0.0, 'B': -2.0, 'E': -1.0, 'D': -2.0, 'G': 0.0, 'F': -2.0, 'I': -1.0, 'H': -2.0, 'K': -1.0, 'M': -1.0, 'L': -1.0, 'N': -2.0, 'Q': -1.0, 'P': -1.0, 'S': 1.0, 'R': -1.0, 'T': 0.0, 'W': -3.0, 'V': 0.0, 'Y': -2.0, 'X': 0.0, 'Z': -1.0}, 'C': {'*': -4.0, '-': -4, 'A': 0.0, 'C': 9.0, 'B': -3.0, 'E': -4.0, 'D': -3.0, 'G': -3.0, 'F': -2.0, 'I': -1.0, 'H': -3.0, 'K': -3.0, 'M': -1.0, 'L': -1.0, 'N': -3.0, 'Q': -3.0, 'P': -3.0, 'S': -1.0, 'R': -3.0, 'T': -1.0, 'W': -2.0, 'V': -1.0, 'Y': -2.0, 'X': -2.0, 'Z': -3.0}, 'B': {'*': -4.0, '-': -4, 'A': -2.0, 'C': -3.0, 'B': 4.0, 'E': 1.0, 'D': 4.0, 'G': -1.0, 'F': -3.0, 'I': -3.0, 'H': 0.0, 'K': 0.0, 'M': -3.0, 'L': -4.0, 'N': 3.0, 'Q': 0.0, 'P': -2.0, 'S': 0.0, 'R': -1.0, 'T': -1.0, 'W': -4.0, 'V': -3.0, 'Y': -3.0, 'X': -1.0, 'Z': 1.0}, 'E': {'*': -4.0, '-': -4, 'A': -1.0, 'C': -4.0, 'B': 1.0, 'E': 5.0, 'D': 2.0, 'G': -2.0, 'F': -3.0, 'I': -3.0, 'H': 0.0, 'K': 1.0, 'M': -2.0, 'L': -3.0, 'N': 0.0, 'Q': 2.0, 'P': -1.0, 'S': 0.0, 'R': 0.0, 'T': -1.0, 'W': -3.0, 'V': -2.0, 'Y': -2.0, 'X': -1.0, 'Z': 4.0}, 'D': {'*': -4.0, '-': -4, 'A': -2.0, 'C': -3.0, 'B': 4.0, 'E': 2.0, 'D': 6.0, 'G': -1.0, 'F': -3.0, 'I': -3.0, 'H': -1.0, 'K': -1.0, 'M': -3.0, 'L': -4.0, 'N': 1.0, 'Q': 0.0, 'P': -1.0, 'S': 0.0, 'R': -2.0, 'T': -1.0, 'W': -4.0, 'V': -3.0, 'Y': -3.0, 'X': -1.0, 'Z': 1.0}, 'G': {'*': -4.0, '-': -4, 'A': 0.0, 'C': -3.0, 'B': -1.0, 'E': -2.0, 'D': -1.0, 'G': 6.0, 'F': -3.0, 'I': -4.0, 'H': -2.0, 'K': -2.0, 'M': -3.0, 'L': -4.0, 'N': 0.0, 'Q': -2.0, 'P': -2.0, 'S': 0.0, 'R': -2.0, 'T': -2.0, 'W': -2.0, 'V': -3.0, 'Y': -3.0, 'X': -1.0, 'Z': -2.0}, 'F': {'*': -4.0, '-': -4, 'A': -2.0, 'C': -2.0, 'B': -3.0, 'E': -3.0, 'D': -3.0, 'G': -3.0, 'F': 6.0, 'I': 0.0, 'H': -1.0, 'K': -3.0, 'M': 0.0, 'L': 0.0, 'N': -3.0, 'Q': -3.0, 'P': -4.0, 'S': -2.0, 'R': -3.0, 'T': -2.0, 'W': 1.0, 'V': -1.0, 'Y': 3.0, 'X': -1.0, 'Z': -3.0}, 'I': {'*': -4.0, '-': -4, 'A': -1.0, 'C': -1.0, 'B': -3.0, 'E': -3.0, 'D': -3.0, 'G': -4.0, 'F': 0.0, 'I': 4.0, 'H': -3.0, 'K': -3.0, 'M': 1.0, 'L': 2.0, 'N': -3.0, 'Q': -3.0, 'P': -3.0, 'S': -2.0, 'R': -3.0, 'T': -1.0, 'W': -3.0, 'V': 3.0, 'Y': -1.0, 'X': -1.0, 'Z': -3.0}, 'H': {'*': -4.0, '-': -4, 'A': -2.0, 'C': -3.0, 'B': 0.0, 'E': 0.0, 'D': -1.0, 'G': -2.0, 'F': -1.0, 'I': -3.0, 'H': 8.0, 'K': -1.0, 'M': -2.0, 'L': -3.0, 'N': 1.0, 'Q': 0.0, 'P': -2.0, 'S': -1.0, 'R': 0.0, 'T': -2.0, 'W': -2.0, 'V': -3.0, 'Y': 2.0, 'X': -1.0, 'Z': 0.0}, 'K': {'*': -4.0, '-': -4, 'A': -1.0, 'C': -3.0, 'B': 0.0, 'E': 1.0, 'D': -1.0, 'G': -2.0, 'F': -3.0, 'I': -3.0, 'H': -1.0, 'K': 5.0, 'M': -1.0, 'L': -2.0, 'N': 0.0, 'Q': 1.0, 'P': -1.0, 'S': 0.0, 'R': 2.0, 'T': -1.0, 'W': -3.0, 'V': -2.0, 'Y': -2.0, 'X': -1.0, 'Z': 1.0}, 'M': {'*': -4.0, '-': -4, 'A': -1.0, 'C': -1.0, 'B': -3.0, 'E': -2.0, 'D': -3.0, 'G': -3.0, 'F': 0.0, 'I': 1.0, 'H': -2.0, 'K': -1.0, 'M': 5.0, 'L': 2.0, 'N': -2.0, 'Q': 0.0, 'P': -2.0, 'S': -1.0, 'R': -1.0, 'T': -1.0, 'W': -1.0, 'V': 1.0, 'Y': -1.0, 'X': -1.0, 'Z': -1.0}, 'L': {'*': -4.0, '-': -4, 'A': -1.0, 'C': -1.0, 'B': -4.0, 'E': -3.0, 'D': -4.0, 'G': -4.0, 'F': 0.0, 'I': 2.0, 'H': -3.0, 'K': -2.0, 'M': 2.0, 'L': 4.0, 'N': -3.0, 'Q': -2.0, 'P': -3.0, 'S': -2.0, 'R': -2.0, 'T': -1.0, 'W': -2.0, 'V': 1.0, 'Y': -1.0, 'X': -1.0, 'Z': -3.0}, 'N': {'*': -4.0, '-': -4, 'A': -2.0, 'C': -3.0, 'B': 3.0, 'E': 0.0, 'D': 1.0, 'G': 0.0, 'F': -3.0, 'I': -3.0, 'H': 1.0, 'K': 0.0, 'M': -2.0, 'L': -3.0, 'N': 6.0, 'Q': 0.0, 'P': -2.0, 'S': 1.0, 'R': 0.0, 'T': 0.0, 'W': -4.0, 'V': -3.0, 'Y': -2.0, 'X': -1.0, 'Z': 0.0}, 'Q': {'*': -4.0, '-': -4, 'A': -1.0, 'C': -3.0, 'B': 0.0, 'E': 2.0, 'D': 0.0, 'G': -2.0, 'F': -3.0, 'I': -3.0, 'H': 0.0, 'K': 1.0, 'M': 0.0, 'L': -2.0, 'N': 0.0, 'Q': 5.0, 'P': -1.0, 'S': 0.0, 'R': 1.0, 'T': -1.0, 'W': -2.0, 'V': -2.0, 'Y': -1.0, 'X': -1.0, 'Z': 3.0}, 'P': {'*': -4.0, '-': -4, 'A': -1.0, 'C': -3.0, 'B': -2.0, 'E': -1.0, 'D': -1.0, 'G': -2.0, 'F': -4.0, 'I': -3.0, 'H': -2.0, 'K': -1.0, 'M': -2.0, 'L': -3.0, 'N': -2.0, 'Q': -1.0, 'P': 7.0, 'S': -1.0, 'R': -2.0, 'T': -1.0, 'W': -4.0, 'V': -2.0, 'Y': -3.0, 'X': -2.0, 'Z': -1.0}, 'S': {'*': -4.0, '-': -4, 'A': 1.0, 'C': -1.0, 'B': 0.0, 'E': 0.0, 'D': 0.0, 'G': 0.0, 'F': -2.0, 'I': -2.0, 'H': -1.0, 'K': 0.0, 'M': -1.0, 'L': -2.0, 'N': 1.0, 'Q': 0.0, 'P': -1.0, 'S': 4.0, 'R': -1.0, 'T': 1.0, 'W': -3.0, 'V': -2.0, 'Y': -2.0, 'X': 0.0, 'Z': 0.0}, 'R': {'*': -4.0, '-': -4, 'A': -1.0, 'C': -3.0, 'B': -1.0, 'E': 0.0, 'D': -2.0, 'G': -2.0, 'F': -3.0, 'I': -3.0, 'H': 0.0, 'K': 2.0, 'M': -1.0, 'L': -2.0, 'N': 0.0, 'Q': 1.0, 'P': -2.0, 'S': -1.0, 'R': 5.0, 'T': -1.0, 'W': -3.0, 'V': -3.0, 'Y': -2.0, 'X': -1.0, 'Z': 0.0}, 'T': {'*': -4.0, '-': -4, 'A': 0.0, 'C': -1.0, 'B': -1.0, 'E': -1.0, 'D': -1.0, 'G': -2.0, 'F': -2.0, 'I': -1.0, 'H': -2.0, 'K': -1.0, 'M': -1.0, 'L': -1.0, 'N': 0.0, 'Q': -1.0, 'P': -1.0, 'S': 1.0, 'R': -1.0, 'T': 5.0, 'W': -2.0, 'V': 0.0, 'Y': -2.0, 'X': 0.0, 'Z': -1.0}, 'W': {'*': -4.0, '-': -4, 'A': -3.0, 'C': -2.0, 'B': -4.0, 'E': -3.0, 'D': -4.0, 'G': -2.0, 'F': 1.0, 'I': -3.0, 'H': -2.0, 'K': -3.0, 'M': -1.0, 'L': -2.0, 'N': -4.0, 'Q': -2.0, 'P': -4.0, 'S': -3.0, 'R': -3.0, 'T': -2.0, 'W': 11.0, 'V': -3.0, 'Y': 2.0, 'X': -2.0, 'Z': -3.0}, 'V': {'*': -4.0, '-': -4, 'A': 0.0, 'C': -1.0, 'B': -3.0, 'E': -2.0, 'D': -3.0, 'G': -3.0, 'F': -1.0, 'I': 3.0, 'H': -3.0, 'K': -2.0, 'M': 1.0, 'L': 1.0, 'N': -3.0, 'Q': -2.0, 'P': -2.0, 'S': -2.0, 'R': -3.0, 'T': 0.0, 'W': -3.0, 'V': 4.0, 'Y': -1.0, 'X': -1.0, 'Z': -2.0}, 'Y': {'*': -4.0, '-': -4, 'A': -2.0, 'C': -2.0, 'B': -3.0, 'E': -2.0, 'D': -3.0, 'G': -3.0, 'F': 3.0, 'I': -1.0, 'H': 2.0, 'K': -2.0, 'M': -1.0, 'L': -1.0, 'N': -2.0, 'Q': -1.0, 'P': -3.0, 'S': -2.0, 'R': -2.0, 'T': -2.0, 'W': 2.0, 'V': -1.0, 'Y': 7.0, 'X': -1.0, 'Z': -2.0}, 'X': {'*': -4.0, '-': -4, 'A': 0.0, 'C': -2.0, 'B': -1.0, 'E': -1.0, 'D': -1.0, 'G': -1.0, 'F': -1.0, 'I': -1.0, 'H': -1.0, 'K': -1.0, 'M': -1.0, 'L': -1.0, 'N': -1.0, 'Q': -1.0, 'P': -2.0, 'S': 0.0, 'R': -1.0, 'T': 0.0, 'W': -2.0, 'V': -1.0, 'Y': -1.0, 'X': -1.0, 'Z': -1.0}, 'Z': {'*': -4.0, '-': -4, 'A': -1.0, 'C': -3.0, 'B': 1.0, 'E': 4.0, 'D': 1.0, 'G': -2.0, 'F': -3.0, 'I': -3.0, 'H': 0.0, 'K': 1.0, 'M': -1.0, 'L': -3.0, 'N': 0.0, 'Q': 3.0, 'P': -1.0, 'S': 0.0, 'R': 0.0, 'T': -1.0, 'W': -3.0, 'V': -2.0, 'Y': -2.0, 'X': -1.0, 'Z': 4.0}}
var min_region_length = 10;

//#################
//#################
// DRAW
//#################
//#################

complexity = {
	/*
	 VARIABLES
	 */

	sequence: "",
	disorder_score: [],
	similarity_cutoff: {},
	pwm_length: 0,
	pwm: "dbox",

	/*
	 FUNCTIONS
	 */
	reset: function () {
		complexity.sequence = "";
		complexity.disorder_score = [];
		complexity.similarity_cutoff = {};
		complexity.pwm_length = 0;
		complexity.pwm = "dbox";
	},
	extractSequence: function (sequenceHTML) {
		var sequence_gapped = "";
		var sequence_ungapped = "";
		$(sequenceHTML).find('.aC').each(function (indexaa, aa) {
			aa = $(aa);
			sequence_gapped += aa.html();
			if (!aa.hasClass('gap') && !(aa.html().localeCompare('-') == 0)) {
				sequence_ungapped += aa.html();
			} else if (!aa.hasClass('gap') && (aa.html().localeCompare('-') == 0)) {
				sequence_ungapped += aa.html();
			}
		});
		return sequence_ungapped;
	},
	extractIupred: function (iupredHTML) {
		var disorderScore = [];

		$(iupredHTML).find('div').each(function (indexaa, aa) {
			if ($(aa).data("tooltip") != undefined) {
				disorderScore.push(parseFloat($(aa).data("tooltip").split(":")[1]));
			}
		});

		return disorderScore;
	},
	translate_score_to_color: function (start, end, steps, count) {
		var s = start,
			e = end,
			final = s + (((e - s) / steps) * count);
		return Math.floor(final);
	},
	translate_score_to_RGB: function (scoreToColor, color) {
		
		var max_color = [140, 140, 255];
		var min_color = [256,256, 256];

		var red = 256;
		var green = 256;
		var blue = 256;

		scoreToColor = Math.abs(scoreToColor);

		var red = complexity.translate_score_to_color(min_color[0], max_color[0], 100, parseInt(scoreToColor * 100));
		var green = complexity.translate_score_to_color(min_color[1], max_color[1], 100, parseInt(scoreToColor * 100));
		var blue = complexity.translate_score_to_color(min_color[2], max_color[2], 100, parseInt(scoreToColor * 100));

		color_gradient = 'rgb(' + red + "," + green + "," + blue + ')';
		return color_gradient;
	},
	focus: function (min, max) {
		$("#trackData").animate({scrollLeft: min * 10 - ($("#trackData").width() / 2) + 50}, 'fast');
	},
	annotate: function (data) {
		var tracks = [];
		var start_adjustment = parseInt(-1);
		
		complexity.data = data;
	
		if (complexity.data.ali_start != undefined) {
			start_adjustment = parseInt(complexity.data.ali_start) - 1;
		}

		var sequenceHTML = complexity.data.alignmentDataQuery;
		var iupredHTML = "";
		
		if (data.tracks.iupred) {
			iupredHTML = complexity.data.tracks.iupred[1];
		}
	
		complexity.sequence = complexity.extractSequence(sequenceHTML);
		complexity.sequence_length = complexity.sequence.length;
		complexity.disorder_score = complexity.extractIupred(iupredHTML);
			
		var table = "";

		table = table + '<br><b>Predicted low complexity regions</b><br><small><a href="http://slim.ucd.ie/complexity/index.php?page=scan_protein">Check another protein for low complexity region</a></small>';
		/*
		table = table + '<tr class="table_head">';
		table = table + "<td>";
		table = table + "Motif<br>type";
		table = table + "</td>";
		table = table + "<td>";
		table = table + "Validated";
		table = table + "</td>";
		table = table + "<td>";
		table = table + "Motif sequence<br><i>(click to view)</i>";
		table = table + "</td>";
		table = table + "<td>";
		table = table + "Start";
		table = table + "</td>";
		table = table + "<td>";
		table = table + "Stop";
		table = table + "</td>";
		table = table + "<td>";
		table = table + "S<sup>pep</sup>";
		table = table + "</td>";
		table = table + "<td>";
		table = table + "Normalised<br>S<sup>pep</sup>";
		table = table + "</td>";
	    table = table + "<td>";
		table = table + "Consensus<br>similarity";
		table = table + "</td>";

		table = table + "<td>";
		table = table + "Disorder<br>score";
		table = table + "</td>";

		table = table + "<td>";
		table = table + "Domain";
		table = table + "</td>";

		table = table + "<td>";
		table = table + "Structure";
		table = table + "</td>";

		table = table + "<td>";
		table = table + "Modification";
		table = table + "</td>";
		table = table + "</tr>";
		*/
		var tableContent = "";
		var complexity_hits = 0;
		var features = [];
		var aa_p = {};
		var aa_0 = {};
		
		for (var aa_iter = 0; aa_iter < 20; aa_iter++)
			{
			aa_p[aas[aa_iter]] = [];
			aa_use = [];
			aa_0[aas[aa_iter]] = binomial_statistics.BinomialPF(background["0.5"][aas[aa_iter]], min_region_length, 0);
			}
		
		
		for (var seq_iter = 0; seq_iter < complexity.sequence_length - min_region_length; seq_iter++) {
				
				start = seq_iter - (min_region_length/2);
				peptide_length = min_region_length;
				
				if (start < 0)
					{
					start = 0;
					peptide_length = seq_iter + min_region_length/2;
					}
					
				peptide = complexity.sequence.substr(Math.max(0,seq_iter - (min_region_length/2)), peptide_length);
				//console.log(peptide)
				peptide_score = 0;
				
				p = binomial_statistics.BinomialPF(background["0.5"][aas[aa_iter]], peptide_length, peptide_length - aa_count);
					
				for (var aa_iter = 0; aa_iter < 20; aa_iter++)
				{
					var aa_count = peptide.split(aas[aa_iter]).length - 1;
					p = binomial_statistics.BinomialPF(background["0.5"][aas[aa_iter]], peptide_length, aa_count);
					//aa_p[aas[aa_iter]].push(p);
					console.log(p - aa_0[aas[aa_iter]],(aa_count/peptide_length),background["0.5"][aas[aa_iter]],(aa_count/peptide_length) > background["0.5"][aas[aa_iter]])
					
					if (1 - p  < 0.0001 ) 
						{
						aa_p[aas[aa_iter]].push(p);
						}
					else
						{
						aa_p[aas[aa_iter]].push(0.001);
						}
						
				}
		}
		
		for (var aa_iter = 0; aa_iter < 20; aa_iter++)
		{
			hist = [];
			$.each(aa_p[aas[aa_iter]], function (ihisto, histo) {
				//console.log(ihisto,histo);
				
				histTmp = {};
				histTmp.id = seq_iter;
				//histTmp.sequence = peptide;
				histTmp.hover = histo;
				//histTmp.start = (start_adjustment + seq_iter + 2);
				histTmp.position = ihisto;
				histTmp.class = "complexity_peptide";
				//histTmp.len = peptide.length;
				histTmp.colour = complexity.translate_score_to_RGB(((peptide_score / complexity.similarity_max)), "#FF8888");
				histTmp.text_colour = "#0F0";
				histTmp.opacity = "0.5";
				histTmp.value =  histo;
				
				
			
				hist.push(histTmp);
				}
			);
			
			var track = {};
			track.data = hist;
			track.type = "histogram";
			track.position = 1;
			track.name = aas[aa_iter] + " rich region";
			track.colour = "#FFF";
			track.opacity = "1";
			track.id = null;
			track.class = null;
			track.height = null;
			track.help = null;
			track.text_colour = null;
			tracks.push(track);
		}

		for (var seq_iter = 0; seq_iter < complexity.sequence_length - min_region_length; seq_iter++) {
				/*
				start = seq_iter - (min_region_length/2);
				peptide_length = min_region_length;
				
				if (start < 0)
					{
					start = 0;
					peptide_length = seq_iter + min_region_length/2;
					}
					
				peptide = complexity.sequence.substr(Math.max(0,seq_iter - (min_region_length/2)), peptide_length);
				console.log(peptide)
				peptide_score = 0;
				
				for (var aa_iter = 0; aa_iter < 20; aa_iter++)
				{
					var aa_count = peptide.split(aas[aa_iter]).length - 1;
					p = binomial_statistics.BinomTerm(background["0.5"][aas[aa_iter]], peptide_length, aa_count);
					aa_p[aas[aa_iter]].push(1 - p);
					
				}
				*/
				/*
				for (var aa_iter = 0; aa_iter < 20; aa_iter++)
				{
				seq_iter - (min_region_length/2),seq_iter + (min_region_length/2)
				complexity.sequence.substr(seq_iter, last_aa_match);
				}
				
				for (var aa_iter = 0; aa_iter < 20; aa_iter++)
				{
					region_length = min_region_length;
					last_aa_match = 0;
					last_aa_match_p = 1;
					max_p = 1;
					var aa_count = peptide.split(aas[aa_iter]).length - 1;
					p = binomial_statistics.BinomTerm( background["0.5"][aas[aa_iter]], region_length, aa_count)
					last_aa_match
					while (aa_count > 3 && p < 0.001)
						{
						
						if (aas[aa_iter] == peptide.charAt(peptide.length-1))
						{
							last_aa_match  = region_length;
							last_aa_match_p = p
						}
						if (aas[aa_iter] == peptide.charAt(peptide.length-1))
						{
							last_aa_match  = region_length;
							last_aa_match_p  = p
						}
						//console.log(p,aas[aa_iter],peptide,(peptide.split(aas[aa_iter]).length - 1),aa_count/region_length,last_aa_match);
						
						
						region_length = region_length + 1;
						peptide = complexity.sequence.substr(seq_iter, region_length);
						aa_count = peptide.split(aas[aa_iter]).length - 1;
						p = binomial_statistics.BinomTerm( background["0.5"][aas[aa_iter]], region_length, aa_count)
						}
					
					if (last_aa_match > 0)
						{
						peptide_score = last_aa_match_p;
						peptide = complexity.sequence.substr(seq_iter, last_aa_match);
						console.log("#",peptide,last_aa_match_p);
						}
				}*/
				peptide_score = 0;
				if (peptide_score >  0) {
					complexity_hits += 1;
					console.log(peptide,complexity_hits);
					featureTmp = {};
					featureTmp.id = seq_iter;
					//featureTmp.sequence = peptide;
					featureTmp.hover = complexity.sequence.substr(seq_iter, min_region_length) 
					//featureTmp.start = (start_adjustment + seq_iter + 2);
					//featureTmp.end = (start_adjustment + seq_iter + peptide.length + 1);
					featureTmp.class = "complexity_peptide";
					featureTmp.len = peptide.length;
					featureTmp.colour = complexity.translate_score_to_RGB(((peptide_score / complexity.similarity_max)), "#FF8888");
					featureTmp.text_colour = "#000";
					featureTmp.scores = [];
					var peptide_scores = [];
					
					
					for (var score in peptide_scores) {
						featureTmp.scores.push((peptide_scores[score] - 0.5));
					}

					featureTmp.scores = peptide_scores;
					
					features.push(featureTmp);

					var dis_score = 1;
					/*
					if (complexity.disorder_score.length != 0) {
						peptide_discores = complexity.disorder_score.slice(Math.max(0, seq_iter), Math.min(seq_iter + complexity.pwm_length, complexity.sequence_length - 1));
						dis_score = peptide_discores.reduce(function (a, b) {
								return a + b;
							}) / peptide_discores.length;
					}
					*/

					var tableStr = "";
					var badRow = false;

					

					tableStr = tableStr + "<td>";
					tableStr = tableStr + "";
					tableStr = tableStr + "</td>";

					tableStr = tableStr + "<td>";
					tableStr = tableStr + '<a class="complexity_link" id="' + (start_adjustment + seq_iter + 1) + '">' + peptide + "</a>";
					tableStr = tableStr + "</td>";
					tableStr = tableStr + "<td>";
					tableStr = tableStr + (start_adjustment + seq_iter + 2);
					tableStr = tableStr + "</td>";
					tableStr = tableStr + "<td>";
					tableStr = tableStr + (start_adjustment + seq_iter + peptide.length + 1);
					tableStr = tableStr + "</td>";

					tableStr = tableStr + "<td>";
					tableStr = tableStr + (peptide_score).toFixed(2);
					tableStr = tableStr + "</td>";
				
					tableStr = tableStr + "<td>";
					tableStr = tableStr + (peptide_score / complexity.similarity_max).toFixed(2);

					if (peptide_score / complexity.similarity_max > 0.85) {
						tableStr = tableStr + ' <i class="fa fa-check"></i>';
					}

					tableStr = tableStr + "</td>";
			   
				
					if (peptide_score / complexity.similarity_max > complexity.similarity_cutoff["0.000001"] / complexity.similarity_max) {
						tableStr = tableStr + "<td>";
						tableStr = tableStr + '<b>***</b>';
						tableStr = tableStr + "</td>";
					}
					else if (peptide_score / complexity.similarity_max > complexity.similarity_cutoff["0.00001"] / complexity.similarity_max) {
						tableStr = tableStr + "<td >";
						tableStr = tableStr + '<b>**</b>';
						tableStr = tableStr + "</td>";
					}
					else if (peptide_score / complexity.similarity_max > complexity.similarity_cutoff["0.0001"] / complexity.similarity_max) {
						tableStr = tableStr + "<td>";
						tableStr = tableStr + '<b>*</b>';
						tableStr = tableStr + "</td>";
					}
				   else {
						tableStr = tableStr + "<td>";
						tableStr = tableStr + '';
						tableStr = tableStr + "</td>";
					}

					tableStr = tableStr + "<td>";
					
					/*
					if (complexity.disorder_score.length != 0) {
						peptide_discores = complexity.disorder_score.slice(Math.max(0, seq_iter), Math.min(seq_iter + complexity.pwm_length, complexity.sequence_length - 1));
						dis_score = peptide_discores.reduce(function (a, b) {
								return a + b;
							}) / peptide_discores.length;

						tableStr = tableStr + (dis_score).toFixed(2);

						if (dis_score > 0.4) {
							tableStr = tableStr + ' <i class="fa fa-check"></i>';
						}
						else if (dis_score > 0.3) {
							tableStr = tableStr + ' <i class="fa fa-exclamation"></i>';
						}
						else {
							tableStr = tableStr + ' <i class="fa fa-times"></i>';
						}

					}
					else {
						tableStr = tableStr + "N/A";
					}
					*/

					tableStr = tableStr + "</td>";
					tableStr = tableStr + "<td>";
					
					/*
					var overlap = complexity.overlap(seq_iter, seq_iter + complexity.pwm_length, complexity.pfam);
					if (overlap.length > 0) {
						tableStr = tableStr + overlap[0][2] + " (" + overlap[0][3] + ")";
						if (overlap[0][3] > 0.4) {
							tableStr = tableStr + " <br><i>(disordered domain?)</i>";
						}

						tableStr = tableStr + "<br>";
					}
					*/

					tableStr = tableStr + "</td>";
					tableStr = tableStr + "<td>";

					/*
					overlap = complexity.overlap(seq_iter, seq_iter + complexity.pwm_length, complexity.ss);
					if (overlap.length > 0) {
						tableStr = tableStr + overlap[0][2];
					}
					

					overlap = complexity.overlap(seq_iter, seq_iter + complexity.pwm_length, complexity.pdb);
					if (overlap.length > 0) {
						tableStr = tableStr + " (<a href='http://www.rcsb.org/pdb/search/smart.do?smartSearchSubtype_0=StructureIdQuery&structureIdList_0=" + overlap.join(",") + "'>PDB</a>)";
					}
					else {
						overlap = complexity.overlap(seq_iter, seq_iter + complexity.pwm_length, complexity.hModel);
						if (overlap.length > 0) {
							tableStr = tableStr + " (" + overlap[0][2] + ")";
						}
					}
					
					tableStr = tableStr + "</td>";

					tableStr = tableStr + "<td>";
					overlap = complexity.overlap(seq_iter - 5, seq_iter + complexity.pwm_length + 5, complexity.modification);

					if (overlap.length > 0) {
						mods = [];
						for (mod in overlap) {
							mods.push("" + overlap[mod][2] + '@' + overlap[mod][0] + "");
						}

						tableStr = tableStr + mods.join("<br>");
					}
					tableStr = tableStr + "</td>";
					*/
					
					tableStr = tableStr + "</tr>";

					if (badRow) {
						tableContent += tableStr;
					}
					else {
						tableContent += tableStr;
					}

				}
			}
		
	
		if (complexity_hits > 0)
		{
			var track = {};
			track.data = features;
			track.type = "peptides";
			track.position = -1;
			track.name = "Low complexity region";
			track.colour = "#FFF";
			track.opacity = "1";
			track.id = null;
			track.class = null;
			track.height = null;
			track.help = null;
			track.text_colour = null;
			tracks.push(track);
   
			$(".tableContainer").html('<table width="100%">' + table + tableContent + tableContent + "</table>");
			var footnotes = "";
			//footnotes = footnotes + '<b>S<sup>pep</sup>:</b>Low complexity score of the peptide; &nbsp&nbsp&nbsp&nbsp&nbsp ';
			//footnotes = footnotes + ' <b>Normalised S<sup>pep</sup>:</b>S<sup>pep</sup>/(max possible peptide score for a PSSM); &nbsp&nbsp&nbsp&nbsp&nbsp ';
			//footnotes = footnotes + ' <b>Consensus similarity p(S<sup>pep</sup>):</b> ***<0.000001 **<0.00001 *<0.0001; <br><br>';
	
			$(".tableContainer").append(footnotes)
			$(".table_head").css("font-weight", "700").css("background", "#F0F0F0");


			$('.complexity_link').on('click', function () {
				complexity.focus($(this).attr("id").split("_")[1]);
				$(".highlight").removeClass("highlight");
				$(this).addClass("highlight");
				console.log("#" + $(this).attr("id") + ".complexity_peptide");
				$("#" + $(this).attr("id") + ".complexity_peptide").addClass("highlight");
			});
		}
		else
		{
			var outStr = '<br><b>Predicted low complexity regions</b><br>';
			outStr = outStr + '<br>No low complexity regions found<br>';
			outStr = outStr + '<small><a href="http://slim.ucd.ie/complexity/index.php?page=scan_protein">Check another protein for low complexity regions</a></small>';
		
			$(".tableContainer").html(outStr);
		
		}
   
		/*
		 =======
		 */
	
		PROVIZ.displayTracks(tracks);
	
	}
};

