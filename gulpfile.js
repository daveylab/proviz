var gulp = require('gulp');
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var cssnano = require('gulp-cssnano');
var del = require('del');
var composer = require('gulp-composer');
var bower = require('gulp-bower');
var debug = require('gulp-debug');

// CONFIG
var dist_folder = 'dist';
var srcBuild = ['img/**/*','js_tools/**/*', 'php/**/*.php', 'js/call_phantom.js', 'js/js.cookie.js', 'vendor/**/*', 'help/custom_track/**/*', 'rest/iupred/**/*', 'rest/loader/**/*', 'rest/*.json', 'rest/*.php', 'rest/*.par', 'about_text.txt', 'config.php', 'proviz.php', 'createJob.php', 'favicon.ico', 'help/**/*','help_text.txt', 'index.php', '!php/javascripts.php', '!php/styles.php'];


gulp.task('mimify_JS_CSS', ['copytodist'], function() {
    "use strict";
    return gulp.src(['php/javascripts.php', 'php/styles.php'], {base: '.'})
        .pipe(useref({ searchPath: '.' }))
        .pipe(gulpIf('*.js', uglify()))
        .pipe(gulpIf('*.css', cssnano()))
        .pipe(gulp.dest(dist_folder));
});


gulp.task('clean:dist', function() {
    "use strict";
    return del.sync(dist_folder);
});


gulp.task('copytodist', function () {
    "use strict";
    return gulp.src(srcBuild,  {base: '.'}).pipe(gulp.dest(dist_folder));
});

gulp.task('icons', function() {
    "use strict";
    return gulp.src('bower_components/font-awesome/fonts/**.*')
        .pipe(gulp.dest(dist_folder + '/fonts'));
});

// create dist version
gulp.task('build', ['clean:dist', 'copytodist', 'icons', 'mimify_JS_CSS'], function (){
    "use strict";
});

// install php/composer dependencies
gulp.task('composer:install', function () {
    "use strict";
//    return composer();
});

//install bower components
gulp.task('bower', function() {
    return bower({ cmd: 'update'});
});

gulp.task('install', ['composer:install', 'bower']);

