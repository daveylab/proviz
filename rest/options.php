<?php

//     
//     ProViz - protein visualisation tool
//     Copyright (C) 2016  Norman E. Davey, Peter Jehl, Jean Manguy
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//     Author contact: Norman E. Davey <normandavey@gmail.com>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Peter Jehl <peter.jehl@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Jean Manguy <jean.manguy@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//
  
class options {
    
    private $mem_limit = "512M";
    private $time_limit = 100000;
    private $cache_path = "./data/";
    private $prefered_archive = "QFO";
    private $alignment_path = "./data/Orthologues/";
    private $alignment_info = "./data/Info";
    private $IUPred_path = "./iupred";
    private $self_path = "./proviz.php";
    private $protein_track_path = "./data/protein_track/";
    private $geneTree_path = "./data/geneTree/";

    public function set_options(){
        ini_set("memory_limit", $this->mem_limit);
        set_time_limit($this->time_limit);
        ini_set('display_errors', 1);
        ini_set('log_errors', 1);
        ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
    }
    
    public function get_cache_path(){
        return $this->cache_path;
    }
    
    public function get_prefered_archive(){
        return $this->prefered_archive;
    }
    
    public function get_alignment_path(){
        return $this->alignment_path;
    }
    
    public function get_iupred_path(){
        return $this->IUPred_path;
    }
    
    public function get_self_path(){
        return $this->self_path;
    }
    
    public function get_alignment_info(){
        return $this->alignment_info;
    }
    
    public function get_protein_track_path(){
        return $this->protein_track_path;
    }
    
    public function getGeneTreePath(){
        return $this->geneTree_path;
    }
}
?>