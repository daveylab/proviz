<?php

//     
//     ProViz - protein visualisation tool
//     Copyright (C) 2016  Norman E. Davey, Peter Jehl, Jean Manguy
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//     Author contact: Norman E. Davey <normandavey@gmail.com>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Peter Jehl <peter.jehl@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Jean Manguy <jean.manguy@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//
    
class treeNode{
    
    private $parentDist;
    private $ensemblId;
    private $type;
    private $name;
    private $geneName;
    private $seq;
    private $parent;
    private $taxon;
    private $paraNr;
    private $children = [];
    
    public function treeNode($parentDist = null, $children = [], $type = null, $name = null, $seq = null, $taxon = null, $ensembleId = null){
        $this->parentDist = $parentDist;
        $this->type = $type;
        $this->children = $children;
        $this->name = $name;
        $this->seq = $seq;
        $this->taxon = $taxon;
        $this->ensemblId = $ensembleId;
    }
    
    public function getParent(){
        return $this->parent;
    }
    
    public function getChildren(){
        return $this->children;
    }

    public function getParentDist(){
        return $this->parentDist;
    }
    
    public function getType(){
        return $this->type;
    }
    
    public function getName(){
        return $this->name;
    }
    
    public function getGeneName(){
        return $this->geneName;
    }
    
    public function getSeq(){
        return $this->seq;
    }
    
    public function getTaxon(){
        return $this->taxon;
    }
    
    public function getEnsembleId(){
        return $this->ensemblId;
    }
    
    public function getParaNr(){
        return $this->paraNr;
    }
    
    public function addChild($child){
        array_push($this->children, $child);
    }
    
    public function setName($name){
        $this->name = $name;
    }
    
    public function setGeneName($geneName){
        $this->geneName = $geneName;
    }
    
    public function setType($type){
        $this->type = $type;
    }
    
    public function setParentDist($dist){
        $this->parentDist = $dist;
    }
    
    public function setParent($parentNode) {
        $this->parent = $parentNode;
    }
    
    public function setParaNr($nr){
        $this->paraNr = $nr;
    }
}

?>
