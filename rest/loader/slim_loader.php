<?php

//     
//     ProViz - protein visualisation tool
//     Copyright (C) 2016  Norman E. Davey, Peter Jehl, Jean Manguy
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//     Author contact: Norman E. Davey <normandavey@gmail.com>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Peter Jehl <peter.jehl@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Jean Manguy <jean.manguy@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//
    
class slim_loader {
    
    private $conservation = [];
    private $psiPred = [];
    private $anchor = [];
    
    public function slim_loader($contents, $alignName){
        
        $dataArr = json_decode($contents);
        $this->psiPred["type"] = "feature";
        $this->psiPred["data"] = [];
        $this->conservation["type"] = "histogram";
        $this->conservation["data"] = [];
        $this->anchor["type"] = "histogram";
        $this->anchor["data"] = [];
        if(property_exists($dataArr, "Conservation")){
            foreach($dataArr->Conservation as $key => $item){
                if(property_exists($item, $alignName)){
                    foreach($item->{$alignName} as $key1 => $item1){
                        $tmp = [];
                        $tmp["value"] = 1 - $item1;
                        $tmp["start"] = $key1-1;
                        $tmp["end"] = $key1-1;
                        $tmp["length"] = 1;
                        $tmp["hover"] = "Score: " . (1 - $item1);
                        array_push($this->conservation["data"], $tmp);
                    }
                }
            }
        }
        if(property_exists($dataArr, "Anchor")){
            foreach($dataArr->Anchor as $key => $item){
                $tmp = [];
                $tmp["value"] = $item;
                $tmp["start"] = $key-1;
                $tmp["end"] = $key-1;
                $tmp["length"] = 1;
                $tmp["hover"] = "Score: " . $item;
                array_push($this->anchor["data"], $tmp);
            }
        }
        if(property_exists($dataArr, "PsiPred")){
            $psiPredData = get_object_vars($dataArr->PsiPred);
            for($i = 1; $i < count($psiPredData) ; $i++){
                if($psiPredData[$i] !== "C"){
                    if(($i === 1) || ($psiPredData[$i] !== $psiPredData[$i - 1])){
                        $tmp = [];
                        $tmp["start"] = $i;
                        if($psiPredData[$i] === "H"){
                            $tmp["text"] = "";
                            $tmp["hover"] = "helix";
                        } else if ($psiPredData[$i] === "E"){
                            $tmp["text"] = "";
                            $tmp["hover"] = "strand";
                        }
                    }
                    if(($i === count($psiPredData)-1) || ($psiPredData[$i] !== $psiPredData[$i+1])){
                        $tmp["end"] = $i;
                        $tmp["length"] = $tmp["end"] - $tmp["start"] + 1;
                        if($tmp["length"] !== 1){
                            array_push($this->psiPred["data"], $tmp);
                        }
                    }
                }
            }
        }
    }
    
    function getCon(){
        return $this->conservation;
    }
    
    function getPsiPred(){
        return $this->psiPred;
    }
    
    function getAnchor(){
        return $this->anchor;
    }
}

?>