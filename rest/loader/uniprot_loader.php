<?php

//     
//     ProViz - protein visualisation tool
//     Copyright (C) 2016  Norman E. Davey, Peter Jehl, Jean Manguy
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//     Author contact: Norman E. Davey <normandavey@gmail.com>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Peter Jehl <peter.jehl@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Jean Manguy <jean.manguy@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//
    
class uniprot_loader {

    private $features;
    private $uniprotName;
    private $seq;
    private $id;
    private $array;
    private $taxonomy;
    private $species;
    private $tool;
    private $geneTreeId;
    private $geneName;
    private $alignmentColor = [];
    private $metaprotein;
    private $functionProtein;
    private $keywordsProtein = array();

    public function uniprot_loader($contents, $uniprotid, $featuresToDisable) {
        
        $ptm_trans = json_decode(file_get_contents(dirname(__FILE__)."/PTM_translation.json"), true);
        
        $this->id = $uniprotid;
        $xmlUniprot = xml2array($contents);


        // meta information
        $this->metaprotein['dataset'] = $xmlUniprot["uniprot"]["entry"]["attr"]['dataset'];
        $this->metaprotein['created'] = $xmlUniprot["uniprot"]["entry"]["attr"]['created'];
        $this->metaprotein['modified'] = $xmlUniprot["uniprot"]["entry"]["attr"]['modified'];
        $this->metaprotein['version'] = $xmlUniprot["uniprot"]["entry"]["attr"]['version'];



        $entry = "";
        if(array_key_exists("protein", $xmlUniprot["uniprot"]["entry"])){
            $entry = $xmlUniprot["uniprot"]["entry"];
        } else if(array_key_exists (0, $xmlUniprot["uniprot"]["entry"])){
            $entry = $xmlUniprot["uniprot"]["entry"][0];
        }
        if(array_key_exists("accession", $entry)){
            if(array_key_exists(0, $entry["accession"])){
                $this->id = $entry["accession"][0]["value"];
            } else {
                $this->id = $entry["accession"]["value"];
            }
        }
        $this->geneName = "";
        if(array_key_exists("gene", $entry)){
            if(array_key_exists("name", $entry["gene"])){
                if(array_key_exists(0, $entry["gene"]["name"])){
                    $this->geneName = $entry["gene"]["name"][0]["value"];
                } else{
                    $this->geneName = $entry["gene"]["name"]["value"];
                }
            }
        }
        if(array_key_exists("recommendedName", $entry["protein"])){
            $this->uniprotName = $entry["protein"]["recommendedName"]["fullName"]["value"];
        } else if(array_key_exists("alternativeName", $entry["protein"])){
            if(array_key_exists(0, $entry["protein"]["alternativeName"])){
                $this->uniprotName = $entry["protein"]["alternativeName"][0]["fullName"]["value"];
            } else {
                $this->uniprotName = $entry["protein"]["alternativeName"]["fullName"]["value"];
            }
        } else if(array_key_exists("submittedName", $entry["protein"])){
            if(array_key_exists(0, $entry["protein"]["submittedName"])){
                $this->uniprotName = $entry["protein"]["submittedName"][0]["fullName"]["value"];
            } else {
                $this->uniprotName = $entry["protein"]["submittedName"]["fullName"]["value"];
            }
        } else if($this->geneName !== ""){
            $this->uniprotName = $this->geneName;
        } else {
            $this->uniprotName = $this->id;
        }
        $this->seq = str_split(str_replace("\n", "", trim($entry["sequence"]["value"], "'\n'")));
        $this->colSeq[0] = $this->seq;
        if(array_key_exists(0, $entry["organism"]["name"])){
            $this->species = $entry["organism"]["name"][0]["value"];
        } else {
            $this->species = $entry["organism"]["name"]["value"];
        }
        foreach($entry["dbReference"] as $refKey => $refItem){
            if(array_key_exists("attr",$refItem)){
                if(array_key_exists("type", $refItem["attr"])){
                    if($refItem["attr"]["type"] === "GeneTree"){
                        if($refItem["attr"]["id"] !== ""){
                            $this->geneTreeId = $refItem["attr"]["id"];
                        }
                    }
                }
            }
        }
        // Load evidence papers - Evidence without "SOURCE" omitted
        $evidenceDict = [];
        if (array_key_exists("evidence", $entry)) {
            foreach ($entry["evidence"] as $tmp => $reference) {
                if (array_key_exists("attr", $reference)) {
                    $key = $reference["attr"]["key"];
                    if (array_key_exists("source", $reference)) {
                        if (array_key_exists("dbReference", $reference ["source"])) {
                            $id = $reference["source"]["dbReference"]["attr"]["id"];
                            $evidenceDict[$key] = $id;
                        }
                    }
                }
            }
        }

        if(array_key_exists("organism", $entry)){
            if(array_key_exists("dbReference", $entry["organism"])){
                if (array_key_exists("attr", $entry["organism"]["dbReference"])) {
                    $this->taxonomy = $entry["organism"]["dbReference"]["attr"]["id"];
                }
            }
        }

        // load function and keywords
        if (array_key_exists("comment", $entry)) {
            foreach ($entry['comment'] as $comment) {
                if(array_key_exists("attr", $comment)){
                    if(array_key_exists("type", $comment["attr"])){
                        if ($comment['attr']['type'] == 'function') {
                            $this->functionProtein = $comment['text']['value'];
                            break;
                        }
                    }
                }
            }
        }
        /*foreach ($entry['keyword'] as $keyword) {
            $this->keywordsProtein[$keyword['attr']['id']] = $keyword['value'];
        }*/


        $this->tool = $this->id . " " . $this->uniprotName . " ";

        // Load color
        $parData = loadColors(dirname(__FILE__)."/clustal.par");
        
        $consensus = $parData[0];
        $colours = $parData[1];

        for($j = 0 ; $j < count($this->colSeq[0]) ; $j++){
            $tmp = [];
            for($i = 0 ; $i < count($this->colSeq); $i++){
                array_push($tmp, $this->colSeq[$i][$j]);
            }
            $tmpCount = array_count_values($tmp);
            for($i = 0 ; $i < count($this->colSeq); $i++){
                $aa = $this->colSeq[$i][$j];
                if (array_key_exists($aa,$colours)){
                    if (count($colours[$aa]["rules"]) == 1){
                        $this->alignmentColor[$i][$j] = $colours[$aa]["colour"];
                    } else {
						foreach($colours[$aa]["rules"] as $consensusKey){
                            $countTmp = 0;
                            foreach($consensus[$consensusKey]["residues"] as $residue){
                                if (array_key_exists($residue,$tmpCount)){
                                    $countTmp = $countTmp + $tmpCount[$residue];
                                }
                            }
                            $percentTmp = $countTmp / count($this->colSeq);

                            if ($percentTmp > $consensus[$consensusKey]["proportion"]){
                                if (array_key_exists($aa,$colours)){
                                    $this->alignmentColor[$i][$j] = $colours[$aa]["colour"];
                                    break;
                                }
                            } else {
                                $this->alignmentColor[$i][$j] = "NO";
                            }
                        }
                    }
                } else {
                    $this->alignmentColor[$i][$j] = "NO";
                }
            }
        }

        // Load features
        $this->features = [];
            $featureType = "homology model";
            if (array_key_exists("dbReference", $entry)) {
                foreach ($entry["dbReference"] as $dbReference) {
                    if (array_key_exists("attr", $dbReference)) {
                        if ($dbReference["attr"]["type"] == "SMR" && $featuresToDisable['homology'] == 0) {
                            $featureType = "homology model";
                            if (!array_key_exists($featureType, $this->features)) {
                                $this->features[$featureType] = [];
                                $this->features[$featureType]["type"] = "feature";
                                $this->features[$featureType]["data"] = [];
                            }


                            $smr_ranges = explode(",", $dbReference["property"]["attr"]["value"]);
                            for ($c = 0; $c < count($smr_ranges); ++$c) {
                                $homologyModelRange = $smr_ranges [$c];

                                $smr_range = explode("-", str_replace(",", "", $homologyModelRange));
                                $smr_length = ($smr_range [1] - $smr_range [0] + 1);

                                $tmp = [];
                                $tmp["text"] = "Homology-based structure model";
                                $tmp["hover"] = "Structural model based on homologous proteins";
                                $tmp["link"] = 'http://swissmodel.expasy.org/repository/smr.php?sptr_ac=' . $uniprotid;
                                $tmp["end"] = $smr_range [1];
                                $tmp["start"] = $smr_range [0];
                                $tmp["length"] = $smr_length;

                                array_push($this->features[$featureType]["data"], $tmp);
                            }
                        } else if ($dbReference["attr"]["type"] == "PDB" && $featuresToDisable['PDB'] == 0) {
                            $featureType = "PDB";
                            if (!array_key_exists($featureType, $this->features)) {
                                $this->features[$featureType] = [];
                                $this->features[$featureType]["type"] = "feature";
                                $this->features[$featureType]["data"] = [];
                            }
                            $chain = "";
                            foreach($dbReference["property"] as $key => $prop){
                                if ($prop["attr"]["type"] === "chains") {
                                    $chain = $prop["attr"]["value"];
                                }
                            }

                            $range = substr($chain, strpos($chain, "=")+1);

                            $tmp = [];
                            $tmp["text"] = $dbReference["attr"]["id"];
                            $tmp["link"] = $tmp["text"];
                            $tmp["end"] = explode("-", $range)[1];
                            $tmp["start"] = explode("-", $range)[0];
                            $tmp["length"] = $tmp["end"] - $tmp["start"] + 1;

                            $added = False;
                            for($i = 0; $i < count($this->features[$featureType]["data"]); $i++){
                                if($this->features[$featureType]["data"][$i]["start"] == $tmp["start"] && $this->features[$featureType]["data"][$i]["end"] == $tmp["end"]){
                                    $this->features[$featureType]["data"][$i]["link"] = $this->features[$featureType]["data"][$i]["link"] . "," . $tmp["link"];
                                    $this->features[$featureType]["data"][$i]["text"] = $this->features[$featureType]["data"][$i]["text"] . "," . $tmp["text"];
                                    $added = True;
                                    break;
                                }
                            }
                            if(!$added){
                                $tmp["link"] = "http://www.rcsb.org/pdb/search/smart.do?smartSearchSubtype_0=StructureIdQuery&structureIdList_0=" . $tmp["link"];
                                array_push($this->features[$featureType]["data"], $tmp);
                            }
                        }
                    }
                }
            }


        if (array_key_exists("feature", $entry)) {
            foreach ($entry["feature"] as $feature) {
                if (array_key_exists("attr", $feature)) {
                    $featureType = strtolower($feature["attr"]["type"]);
                    if (self::needToDisable($featureType, $featuresToDisable)) {
                        continue;
                    }
                    $featureTypeTmp = $featureType;
                    if($featureType == "turn" || $featureType == "helix" || $featureType == "strand"){

                        $featureType = "secondary structure";
                    }
                    if(($featureType == "peptide") || ($featureType === "propeptide") || ($featureType === "signal peptide")){

                        $featureType = "peptide";
                    }
                    if($featureType == "nucleotide phosphate-binding region"){
                        $featureType = "nucleotide phosphate-binding";
                    }
                    if (!array_key_exists($featureType, $this->features)) {
                        $this->features[$featureType] = [];
                        $this->features[$featureType]["data"] = [];
                        if($featureType == "mutagenesis site" || $featureType == "sequence variant"){
                            $this->features[$featureType]["type"] = "peptide";
                        } else {
                            $this->features[$featureType]["type"] = "feature";
                        }
                    }
                    if (array_key_exists("description", $feature ["attr"])) {
                        $featureDesc = $feature["attr"]["description"];
                    } else {
                        $featureDesc = $featureType;
                    }

                    if (array_key_exists("position", $feature ["location"])) {
                        $featureStart = $feature["location"]["position"]["attr"]["position"];
                        $featureStop = $featureStart;
                        $featureLength = 1;
                    } else if (array_key_exists("begin", $feature["location"])) {
                        $featureStart = $feature["location"]["begin"]["attr"]["position"];
                        if(array_key_exists("position", $feature["location"]["end"]["attr"])){
                            $featureStop = $feature["location"]["end"]["attr"]["position"];
                        } else {
                            $featureStop = $featureStart;
                        }
                        $featureLength = $featureStop - $featureStart + 1;
                    }

                    if ($featureType == "secondary structure") {

                        $featureDesc = $featureTypeTmp;

                        $tmp = [];
                        $tmp["text"] = "";
                        $tmp["hover"] = $featureDesc;
                        $tmp["link"] = 'http://www.uniprot.org/uniprot/' . $uniprotid;
                        $tmp["end"] = $featureStop;
                        $tmp["start"] = $featureStart;
                        $tmp["length"] = $featureLength;

                        array_push($this->features[$featureType]["data"], $tmp);

                    } else if ($featureType == "sequence variant") {
                        if (array_key_exists("variation", $feature)) {
                            if (array_key_exists("id", $feature ["attr"])) {
                                $featureId = $feature["attr"]["id"];
                            } else {
                                $featureId = "NA";
                            }
                            $featureTo = $feature["variation"]['value'];
                            $featureFrom = $feature["original"]['value'];
                            $featureDesc = $featureFrom . " -> " . $featureTo . ". " . $featureId . ". " . $featureDesc;
                        }

                        $tmp = [];
                        $tmp["text"] = $featureTo;
                        $tmp["hover"] = $featureDesc;
                        if($featureId !== ""){
                            $tmp["link"] = 'http://web.expasy.org/variant_pages/' . $featureId . '.html';
                        } else {
                            $tmp["link"] = "";
                        }
                        $tmp["end"] = $featureStop;
                        $tmp["start"] = $featureStart;
                        $tmp["length"] = $featureLength;

                        array_push($this->features [$featureType]["data"], $tmp);

                    } else if ($featureType == "modified residue" || $featureType == "glycosylation site" || ($featureType == "cross-link" && $featureDesc == "Glycyl lysine isopeptide (Lys-Gly) (interchain with G-Cter in ubiquitin)") || ($featureType == "cross-link" && $featureDesc == "Glycyl lysine isopeptide (Lys-Gly) (interchain with G-Cter in SUMO)")) {

                        
                        $featureType = "modified residue";
                        $tmp = [];
                        
                        if(array_key_exists(explode(";",$featureDesc)[0], $ptm_trans)){
                            $tmp["text"] = $ptm_trans[explode(";",$featureDesc)[0]];
                        } else {
                            $tmp["text"] = "";
                        }
                        $tmp["hover"] = $featureDesc;
                        $tmp["link"] = 'http://www.uniprot.org/uniprot/' . $uniprotid;
                        $tmp["end"] = $featureStop;
                        $tmp["start"] = $featureStart;
                        $tmp["length"] = $featureLength;
                        
                        if(!array_key_exists($featureType, $this->features)){
                            $this->features[$featureType] = [];
                            $this->features[$featureType]["data"] = [];
                            $this->features[$featureType]["type"] = "feature";
                        }

                        array_push($this->features[$featureType]["data"], $tmp);

                    } else if($featureType == "topological domain"){
                        $tmp = [];
                        $tmp["text"] = $featureDesc;
                        $tmp["hover"] = $featureDesc;
                        $tmp["link"] = 'http://www.uniprot.org/uniprot/' . $uniprotid;
                        $tmp["end"] = $featureStop;
                        $tmp["start"] = $featureStart;
                        $tmp["length"] = $featureLength;
                        array_push($this->features [$featureType]["data"], $tmp);
                    } else if($featureType == "transmembrane region"){
                        $tmp = [];
                        $tmp["text"] = "Transmembrane";
                        $tmp["hover"] = $featureDesc;
                        $tmp["link"] = 'http://www.uniprot.org/uniprot/' . $uniprotid;
                        $tmp["end"] = $featureStop;
                        $tmp["start"] = $featureStart;
                        $tmp["length"] = $featureLength;
                        array_push($this->features [$featureType]["data"], $tmp);
                    } else if ($featureType == "mutagenesis site") {
                        $pmid = [];
                        if(array_key_exists("evidence", $feature["attr"])){
                            foreach (explode(" ", $feature["attr"]["evidence"]) as $evidence) {
                                if(array_key_exists($evidence, $evidenceDict)){
                                    array_push($pmid, $evidenceDict[$evidence]);
                                }
                            }
                        }

                        $original = "";
                        $variation = "";
                        if(array_key_exists("variation", $feature)){
                            if(array_key_exists(0, $feature["variation"])){
                                for($i = 0 ; $i < count($feature["variation"]); $i++){
                                    $variation .= $feature["variation"][$i]["value"];
                                    if($i < (count($feature["variation"]) -1)){
                                        $variation .= "/";
                                    }
                                }
                            } else {
                                $variation = $feature["variation"]["value"];
                            }
                            $original = $feature["original"]["value"];
                        } else {
                            $variation = "-";
                        }

                        $tmp = [];
                        $tmp["text"] = $variation;
                        $tmp["hover"] = $original . " -> " . $variation . ".\n" . $featureDesc;
                        $tmp["end"] = $featureStop;
                        $tmp["start"] = $featureStart;
                        $tmp["length"] = $featureLength;

                        $featureLink = "";
                        if (!empty($pmid)) {
                            $featureLink = 'http://www.ncbi.nlm.nih.gov/pubmed/';
                            foreach($pmid as $key => $id){
                                $featureLink = $featureLink . $id . ',';
                            }
                        }
                        $tmp["link"] = $featureLink;

                        array_push($this->features [$featureType]["data"], $tmp);
                    } else if($featureType == "peptide"){
                        $tmp = [];
                        $tmp["text"] = ($feature['attr']['type'] == 'signal peptide' || $feature['attr']['type'] == 'propeptide' ) ? $feature['attr']['type'] :$featureDesc;

                        if (isset($feature['attr']['description']) && $feature['attr']['description'] != '') {
                            $tmp["hover"] = $featureDesc;
                        } else {
                            $tmp["hover"] = $feature['attr']['type'];

                        }
                        if (isset($feature['attr']['id']) && $feature['attr']['id'] != '') {
                            $tmp["link"] = 'http://www.uniprot.org/uniprot/?query=' . $feature['attr']['id'];
                        } else {
                            $tmp["link"] = 'http://www.uniprot.org/uniprot/' . $uniprotid;
                        }
                        $tmp["end"] = $featureStop;
                        $tmp["start"] = $featureStart;
                        $tmp["length"] = $featureLength;

                        array_push($this->features [$featureType]["data"], $tmp);
                    } else if($featureType == "splice variant"){

                        $original = "";
                        $variant = "";
                        if(array_key_exists("original", $feature)){
                            $original = $feature["original"]["value"];
                        }
                        if(array_key_exists("variation", $feature)){
                            $variant = $feature["variation"]["value"];
                        }
                        $tmp = [];
                        $tmp["text"] = $featureDesc;
                        if(($original !== "") && ($variant !== "")){
                            $hover = $original . " -> " . $variant . ". " . $featureDesc;
                        } else {
                            $hover ="Sequence missing " . strtolower($featureDesc);
                        }
                        $tmp["hover"] = $hover;
                        $tmp["link"] = 'http://www.uniprot.org/uniprot/' . $uniprotid;
                        $tmp["end"] = $featureStop;
                        $tmp["start"] = $featureStart;
                        $tmp["length"] = $featureLength;

                        array_push($this->features[$featureType]["data"], $tmp);
                    } else if($featureType == "active site" || $featureType == "binding site" || $featureType == "nucleotide phosphate-binding"){
                        if (!array_key_exists($featureType, $this->features)) {
                            $this->features [$featureType] = [];
                        }

                        $tmp = [];
                        $tmp["text"] = $featureDesc;
                        if($featureType == "active site"){
                            $tmp["hover"] = "active site; " . $featureDesc;
                        } else if($featureType == "binding site"){
                            $tmp["hover"] = "binding site; " . $featureDesc;
                        } else {
                            $tmp["hover"] = "nucleotide phosphate-binding; " . $featureDesc;
                        }
                        $tmp["link"] = 'http://www.uniprot.org/uniprot/' . $uniprotid;
                        $tmp["end"] = $featureStop;
                        $tmp["start"] = $featureStart;
                        $tmp["length"] = $featureLength;

                        array_push($this->features[$featureType]["data"], $tmp);
                    } else {
                        if (!array_key_exists($featureType, $this->features)) {
                            $this->features [$featureType] = [];
                        }

                        $tmp = [];
                        $tmp["text"] = $featureDesc;
                        $tmp["hover"] = $featureDesc;
                        $tmp["link"] = 'http://www.uniprot.org/uniprot/' . $uniprotid;
                        $tmp["end"] = $featureStop;
                        $tmp["start"] = $featureStart;
                        $tmp["length"] = $featureLength;

                        array_push($this->features[$featureType]["data"], $tmp);
                    }
                }
            }
        }
    }

    private function needToDisable($featureType, $featuresToDisable) {
        if(($featureType == "turn" || $featureType == "helix" || $featureType == "strand") && $featuresToDisable['structure'] == 1){
            return true;
        } elseif ((($featureType == "peptide") || ($featureType === "propeptide") || ($featureType === "signal peptide")) && $featuresToDisable['peptide'] == 1) {
            return true;
        } elseif (($featureType == "nucleotide phosphate-binding region") && $featuresToDisable['nucleophospho'] == 1) {
            return true;
        } elseif ($featureType == "sequence variant" && $featuresToDisable['SNP'] == 1) {
            return true;
        } elseif ($featureType == "modified residue" && $featuresToDisable['modification'] == 1) {
            return true;
        } elseif ($featureType == "topological domain" && $featuresToDisable['topological_domain'] == 1) {
            return true;
        } elseif ($featureType == "transmembrane region" && $featuresToDisable['transmembrane_region'] == 1) {
            return true;
        } elseif ($featureType == "mutagenesis site" && $featuresToDisable['mutagenesis'] == 1) {
            return true;
        } elseif ($featureType == "region of interest" && $featuresToDisable['region'] == 1) {
            return true;
        } elseif ($featureType == "cross-link" && $featuresToDisable['cross_link'] == 1) {
            return true;
        } elseif ($featureType == "active site" && $featuresToDisable['activesite'] == 1) {
            return true;
        } elseif ($featureType == "binding site" && $featuresToDisable['bindingsite'] == 1) {
            return true;
        } elseif ($featureType == "site" && $featuresToDisable['site'] == 1) {
            return true;
        } elseif ($featureType == "metal ion-binding site" && $featuresToDisable['metal_binding'] == 1) {
            return true;
        } elseif ($featureType == "chain" && $featuresToDisable['chain'] == 1) {
            return true;
        } elseif ($featureType == "splice variant" && $featuresToDisable['splice_variant'] == 1) {
            return true;
        } elseif ($featureType == "short sequence motif" && $featuresToDisable['motif'] == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function getFeatures(){
        return $this->features;
    }

    public function getUniprotName(){
        return $this->uniprotName;
    }

    public function getSeq(){
        return $this->seq;
    }

    public function getId(){
        return $this->id;
    }

    public function getArray(){
        return $this->array;
    }

    public function getTaxonomy(){
        return $this->taxonomy;
    }

    public function getSpecies(){
        return $this->species;
    }

    public function getTool(){
        return $this->tool;
    }

    public function getColor(){
        return $this->alignmentColor;
    }

    public function getGeneTreeId(){
        return $this->geneTreeId;
    }

    public function getGeneName(){
        return $this->geneName;
    }

    public function getMeta(){
        return $this->metaprotein;
    }

    public function getFunctionProtein(){
        return $this->functionProtein;
    }

    public function getKeywordsProtein(){
        return $this->keywordsProtein;
    }

}
?>