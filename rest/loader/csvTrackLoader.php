<?php

//     
//     ProViz - protein visualisation tool
//     Copyright (C) 2016  Norman E. Davey, Peter Jehl, Jean Manguy
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//     Author contact: Norman E. Davey <normandavey@gmail.com>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Peter Jehl <peter.jehl@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Jean Manguy <jean.manguy@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//
  
class csvTrackLoader {
    
    private $tracks;
    
    public function csvTrackLoader($contents){
        
        $trackArr = [];
        $csvArr = explode("\n",$contents);
        for($i = 1; $i < count($csvArr); $i++){
            $lineArr = explode(",", $csvArr[$i]);
            if(!array_key_exists($lineArr[0]-1, $trackArr)){
                $trackArr[$lineArr[0]-1] = [];
                if($lineArr[3] !== ""){
                    $trackArr[$lineArr[0]-1]["position"] = $lineArr[3];
                } else {
                    return null;
                }
                if($lineArr[2] !== ""){
                    $trackArr[$lineArr[0]-1]["type"] = $lineArr[2];
                } else {
                    return null;
                }
                if($lineArr[1] !== ""){
                    $trackArr[$lineArr[0]-1]["name"] = $lineArr[1];
                }
                if($lineArr[4] !== ""){
                    $trackArr[$lineArr[0]-1]["colour"] = $lineArr[4];
                }
                if($lineArr[5] !== ""){
                    $trackArr[$lineArr[0]-1]["opacity"] = $lineArr[5];
                }
            }
            if(array_key_exists($lineArr[0]-1, $trackArr)){
                if($trackArr[$lineArr[0]-1]["type"] === "feature"){
                    $position = $lineArr[6]-1;
                } else {
                    $position = $lineArr[13]-1;
                }
                $trackArr[$lineArr[0]-1]["data"][$position] = [];
                if (!(($lineArr[11] !== "") && ($lineArr[12] !== "") && ($lineArr[13] !== ""))) {
                    if (($lineArr[11] !== "") && ($lineArr[12] !== "")) {
                        $trackArr[$lineArr[0]-1]["data"][$position]["start"] = $lineArr[11];
                        $trackArr[$lineArr[0]-1]["data"][$position]["end"] = $lineArr[12];
                        $trackArr[$lineArr[0]-1]["data"][$position]["length"] = $lineArr[12] - $lineArr[11] +1;
                    } else if ($lineArr[13] !== "") {
                        $trackArr[$lineArr[0]-1]["data"][$position]["position"] = $lineArr[13];
                    } else {
                        return null;
                    }
                    if(($lineArr[7] !== "") xor ($lineArr[8] !== "")){
                        if($lineArr[7] !== ""){
                            $trackArr[$lineArr[0]-1]["data"][$position]["text"] = $lineArr[7];
                        } else if($lineArr[8] !== ""){
                            $trackArr[$lineArr[0]-1]["data"][$position]["value"] = $lineArr[8];
                        }
                    }
                    if($lineArr[9] !== ""){
                        $trackArr[$lineArr[0]-1]["data"][$position]["hover"] = $lineArr[9];
                    }
                    if($lineArr[10] !== ""){
                        $trackArr[$lineArr[0]-1]["data"][$position]["link"] = $lineArr[10];
                    }
                    if($lineArr[14] !== ""){
                        $trackArr[$lineArr[0]-1]["data"][$position]["colour"] = $lineArr[14];
                    }
                    if($lineArr[15] !== ""){
                        $trackArr[$lineArr[0]-1]["data"][$position]["opacity"] = $lineArr[15];
                    }
                }
            }
        }
        $this->tracks = $trackArr;
    }
    
    public function getTracks() {
        return $this->tracks;
    }
}
?>