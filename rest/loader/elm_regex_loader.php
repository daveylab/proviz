<?php

//     
//     ProViz - protein visualisation tool
//     Copyright (C) 2016  Norman E. Davey, Peter Jehl, Jean Manguy
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//     Author contact: Norman E. Davey <normandavey@gmail.com>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Peter Jehl <peter.jehl@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Jean Manguy <jean.manguy@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//
  
class elm_regex_loader {
    
    private $regex_hits = [];
    
    public function elm_regex_loader($contents, $seq){
        
        $this->regex_hits["type"] = "feature";
        $this->regex_hits["data"] = [];
        
        $elm_lines = explode("\n", $contents);
        $classes = [];
        for($i = 6; $i < count($elm_lines); $i++){
            if($elm_lines[$i] !== ""){
                $tmp1 = explode("\t", $elm_lines[$i]);
            
                $elm = [];
                $elm["name"] = str_replace('"', '', $tmp1[1]);
                $elm["regex"] = str_replace('"', '', $tmp1[3]);
                
                array_push($classes, $elm);
            
            }
        }

        foreach($classes as $key => $elm){
            preg_match_all('/' . $elm["regex"] . '/', $seq, $matches);
        
            $intOffset = 0;
            $intIndex = 0;
            $intTagPositions = [];

            foreach($matches[0] as $strFullTag) {
                $intTagPositions[$intIndex] = array('text' => $elm["name"], 'start' => (strpos($seq, $strFullTag, $intOffset) +1), 'end' => (strpos($seq, $strFullTag, $intOffset) + strlen($strFullTag)));
                $intOffset = strpos($seq, $strFullTag, $intOffset);
                $intIndex++;
            }
            
            for($i = 0 ; $i < count($matches[0]); $i++){
                $tmp = [];
                $tmp["text"] = $intTagPositions[$i]["text"];
                $tmp["start"] = $intTagPositions[$i]["start"];
                $tmp["end"] = $intTagPositions[$i]["end"];
                $tmp["length"] = $tmp["end"] - ($tmp["start"] - 1);
                $tmp["link"] = 'http://elm.eu.org/elms/elmPages/' . $intTagPositions[$i]["text"] . '.html';
                $tmp["hover"] = 'ELM: ' . $intTagPositions[$i]["text"] . "\nRegex: " . $elm["regex"] . "\nMatch: " . substr($seq, $tmp["start"] - 1, $tmp["end"] - ($tmp["start"] - 1));
                            
                array_push($this->regex_hits["data"], $tmp);
            }
        }
    }
    
    public function getRegex(){
        return $this->regex_hits;
    }
}

?>