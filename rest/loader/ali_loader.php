<?php

//     
//     ProViz - protein visualisation tool
//     Copyright (C) 2016  Norman E. Davey, Peter Jehl, Jean Manguy
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//     Author contact: Norman E. Davey <normandavey@gmail.com>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Peter Jehl <peter.jehl@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Jean Manguy <jean.manguy@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//
  
class ali_loader {

    private $seqs = [];
    private $names = [];
    private $ids = [];
    private $alignmentColor = [];
    private $masked = [];
    private $tooltip = [];
    private $gene_name = [];
    private $protein_name = [];
    private $model = [];
    private $gapRange = [];
    private $posArr = [];
    private $inDel = [];

    public function ali_loader($contents, $ali_start, $ali_end, $ensembleIds, $settingsArr, $split){
        
        $model_species = array("Homo sapiens","Physcomitrella patens subsp. patens","Arabidopsis thaliana","Caenorhabditis elegans","Drosophila melanogaster","Danio rerio","Xenopus tropicalis","Mus musculus","Dictyostelium discoideum","Escherichia coli","Schizosaccharomyces pombe","Saccharomyces cerevisiae","Monosiga brevicollis","Thalassiosira pseudonana","Gallus gallus","Ciona intestinalis");

        trim($contents);
        $aliArr = explode($split, $contents);
        $count = -2;
        $aliFixArr = [];
        for($i = 0 ; $i < count($aliArr); $i++){
            if($aliArr[$i] !== ""){
                if(startsWith($aliArr[$i],">")){
                    $count = $count + 2;
                    $aliFixArr[$count] = $aliArr[$i];
                    $aliFixArr[$count + 1] = "";
                } else {
                    $aliFixArr[$count + 1] = $aliFixArr[$count + 1] . trim($aliArr[$i]);
                }
            }
        }
        for ($i = 0 ; $i < count($aliFixArr) ; $i = $i + 2){
            if(startsWith($aliFixArr[$i], ">")){
                $this->model[$i/2] = 0;
                $this->seqs[$i/2] = str_split($aliFixArr[$i+1]);
                $tmpStr = $aliFixArr[$i];
                if(in_array(trim(substr(explode("-", $tmpStr)[0], 1)), $model_species)) {
                    $this->model[$i/2] = 1;
                }
                if(strpos($tmpStr, "OS=")){
                    $tmpQFO = substr($tmpStr, strpos($tmpStr, "OS=") + 3);
                    if(strpos($tmpQFO, "PE=")){
                        $tmpQFO = substr($tmpQFO, 0, strpos($tmpQFO, "PE="));
                    }
                    if(strpos($tmpQFO, "GN=")){
                        $tmpQFO = substr($tmpQFO, 0, strpos($tmpQFO, "GN="));
                    }
                    if(strpos($tmpQFO, "(")){
                        $tmpQFO = substr($tmpQFO, 0, strpos($tmpQFO, "("));
                    }
                    $tmpQFO = trim($tmpQFO);
                    if(in_array($tmpQFO, $model_species)){
                        $this->model[$i/2] = 1;
                    }
                    $this->species[$i/2] = $tmpQFO;
                } else {
                    $this->species[$i/2] = "unknown";
                }
                if(strpos($tmpStr, "OS=")){
                    $tmpStr = substr($tmpStr, strpos($tmpStr, "OS=") + 3);
                    if(strpos($tmpStr, "GN=")){
                        $tmpStr = substr($tmpStr, 0, strpos($tmpStr, "GN="));
                    } else if(strpos($tmpStr, "PE=")) {
                        $tmpStr = substr($tmpStr, 0, strpos($tmpStr, "PE="));
                    }
                    if(strpos($tmpStr, "(")){
                        $tmpStr = substr($tmpStr, 0, strpos($tmpStr, "("));
                    }
                } else {
                    $tmpStr = substr($tmpStr, 1);
                }
                $this->names[$i/2] = $tmpStr;

                $tmpTool = $aliFixArr[$i];
                $tmpPN = $tmpTool;
                if(strpos($tmpPN, "OS=")){
                    $this->protein_name[$i/2] = substr(substr($tmpPN, 0, strpos($tmpPN, "OS=")), strpos($tmpPN, " "));
                } else {
                    $this->protein_name[$i/2] = "";
                }
                if(strpos($tmpTool, "PE=")){
                    $tmpTool = substr($tmpTool, 0 , strpos($tmpTool, "PE="));
                }
                $tmpGN = "";
                if(strpos($tmpTool, "OS=")){
                    if(strpos($tmpTool, "GN=")){
                        $tmpGN = substr($tmpTool, strpos($tmpTool, "GN=") + 3);
                        if(strpos($tmpGN, '\\')){
                            $tmpGN = substr($tmpGN, 0, strpos($tmpGN, '\\'));
                        }
                        $tmpTool = substr($tmpTool, 0, strpos($tmpTool, "OS=")-1) . " " . substr($tmpTool , strpos($tmpTool, "GN="));
                    } else {
                        $tmpTool = substr($tmpTool, 0, strpos($tmpTool, "OS=")-1);
                        $tmpGN = "";
                    }
                }
                if($tmpGN != ""){
                    $this->gene_name[$i/2] = $tmpGN;
                } else {
                    $this->gene_name[$i/2] = "unknown";
                }
                
                if($this->gene_name[$i/2] !== "" && $this->names[$i/2] !== "" ){
                    $this->names[$i/2] = $this->gene_name[$i/2] . " - " . $this->names[$i/2];
                }
                
                //print $this->names[$i/2] . " | " . $this->gene_name[$i/2] . " | " . $this->protein_name[$i/2] . " | " . $this->species[$i/2] . "\n";
                
                if(strpos($tmpTool, "OS")){
                    $this->tooltip[$i/2] = substr($tmpTool, 1,strpos($tmpTool, "OS") + 2);
                } else {
                    $this->tooltip[$i/2] = substr($tmpTool, 1);
                }
                preg_match("/[| _-][OPQ][0-9][A-Z0-9]{3}[0-9]|[A-NR-Z][0-9]([A-Z][A-Z0-9]{2}[0-9]){1,2}[| _-]/", $aliFixArr[$i], $matches);
                if(count($matches) > 0){
                    preg_match("/[OPQ][0-9][A-Z0-9]{3}[0-9]|[A-NR-Z][0-9]([A-Z][A-Z0-9]{2}[0-9]){1,2}/", $matches[0], $finalMatch);
                    $this->ids[$i/2] = $finalMatch[0];
                } else {
                    $this->ids[$i/2] = '';
                }
            }
        }

        if($this->ids[0] !== "" || $ensembleIds[0] !== ""){
            for($i = 1; $i < count($this->seqs); $i++){
                if($this->ids[0] !== ""){
                    $tmpId = $this->ids[$i];
                } else {
                    $tmpId = $ensembleIds[$i];
                }
                if($settingsArr !== NULL && ((in_array($tmpId, $settingsArr["hideAln"])) || (!empty($settingsArr["showAln"]) && !in_array($tmpId, $settingsArr["showAln"])))){
                    array_splice($this->seqs, $i, 1);
                    array_splice($this->names, $i, 1);
                    array_splice($this->tooltip, $i, 1);
                    if($this->ids[0] !== ""){
                        array_splice($this->ids, $i, 1);
                    }
                    if($ensembleIds[0] !== ""){
                        array_splice($ensembleIds, $i, 1);
                    }
                    $i--;
                }
            }
        }

        //Hide only gap columns
        for($i = 0; $i < count($this->seqs[0]); $i++){
            $delete = TRUE;
            for($j = 0 ; $j < count($this->seqs); $j++){
                if($this->seqs[$j][$i] !== "-"){
                    $delete = FALSE;
                }
            }
            if($delete){
                for($j = 0 ; $j < count($this->seqs); $j++){
                    array_splice($this->seqs[$j], $i, 1);
                }
                $i--;
            }
        }

        //Hide gap columns compared to mains seq
        $this->masking($this->seqs, $ali_start, $ali_end);


        $parData = loadColors(dirname(__FILE__)."/clustal.par");
        $consensus = $parData[0];
        $colours = $parData[1];

        for($j = 0 ; $j < count($this->seqs[0]) ; $j++){
            $tmp = [];
            for($i = 0 ; $i < count($this->seqs); $i++){
                array_push($tmp, $this->seqs[$i][$j]);
            }
            $tmpCount = array_count_values($tmp);
            for($i = 0 ; $i < count($this->seqs); $i++){
                $aa = $this->seqs[$i][$j];
                if (array_key_exists($aa,$colours)){
                    if (count($colours[$aa]["rules"]) == 1){
                        $this->alignmentColor[$i][$j] = $colours[$aa]["colour"];
                    } else {
                        foreach($colours[$aa]["rules"] as $consensusKey){
                            $countTmp = 0;
                            foreach($consensus[$consensusKey]["residues"] as $residue){
                                if (array_key_exists($residue,$tmpCount)){
                                    $countTmp = $countTmp + $tmpCount[$residue];
                                }
                            }
                            $percentTmp = $countTmp / count($this->seqs);

                            if ($percentTmp > $consensus[$consensusKey]["proportion"]){
                                if (array_key_exists($aa,$colours)){
                                    $this->alignmentColor[$i][$j] = $colours[$aa]["colour"];
                                    break;
                                }
                            } else {
                                $this->alignmentColor[$i][$j] = "NO";
                            }
                        }
                    }
                } else {
                    $this->alignmentColor[$i][$j] = "NO";
                }
            }
        }
    }

    public function masking($seqs, $ali_start, $ali_end){

        $noGapCount = 0;
        for($i = 0 ; $i < count($seqs[0]) ; $i++){
            if($seqs[0][$i] != "-"){
                $noGapCount++;
                if($noGapCount > $ali_start && $noGapCount <= $ali_end){
                    array_push($this->gapRange, 0);
                } else {
                    array_push($this->gapRange, 1);
                }
            } else {
                array_push($this->gapRange, 1);
            }
        }
        for($i = 0 ; $i < count($seqs); $i++){
            $posCount = 0;
            $this->posArr[$i] = [];
            for($j = 0 ; $j < count($seqs[$i]); $j++){
                if($seqs[$i][$j] !== "-"){
                    $posCount++;
                }
                array_push($this->posArr[$i], $posCount);
            }
        }
        $window = FALSE;
        $startPos = 0;
        for($j = 0 ; $j < count($seqs) ; $j++){
            $this->inDel[$j] = [];
        }
        for($i = 0 ; $i < count($seqs[0]); $i++){
            if($window === FALSE){
                if($i === 0 && $seqs[0][0] === "-"){
                    $window = TRUE;
                    $startPos = $i;
                } else if($seqs[0][$i] === "-" && $seqs[0][$i-1] !== "-"){
                    $window = TRUE;
                    $startPos = $i-1;
                }
            }
            if($window === TRUE){
                if(($seqs[0][$i] !== "-") || ($i === count($seqs[0])-1)){
                    $window = FALSE;
                    for($j = 1 ; $j < count($seqs) ; $j++ ){
                        $tmp = $this->createInDel($j, $startPos, $i);
                        if($tmp !== ""){
                            $this->inDel[$j][$startPos] = $this->posArr[$j][$startPos] . strtolower($seqs[$j][$startPos]) . $this->createInDel($j, $startPos, $i) . strtolower($seqs[$j][$i]) . $this->posArr[$j][$i];
                            $this->inDel[$j][$i] = $this->posArr[$j][$startPos] . strtolower($seqs[$j][$startPos]) . $this->createInDel($j, $startPos, $i) . strtolower($seqs[$j][$i]) . $this->posArr[$j][$i];
                        }
                    }
                }
            }
        }
    }

    public function createInDel($seqIndex, $posStart, $posEnd){
        $indelStr = "";
        $arrayPart = array_slice($this->seqs[$seqIndex], $posStart+1, $posEnd-$posStart-1);
        $arrayPartCount = array_count_values($arrayPart);
        if(!key_exists("-", $arrayPartCount) || ($arrayPartCount["-"] !== $posEnd-$posStart-1)){
            for($i = 0 ; $i < count($arrayPart); $i++){
                if($arrayPart[$i] !== "-"){
                    $indelStr .= $arrayPart[$i];
                }
            }
        }
        return $indelStr;
    }

    public function getSeqs(){
        return $this->seqs;
    }

    public function getNames(){
        return $this->names;
    }

    public function getColor(){
        return $this->alignmentColor;
    }

    public function getMasked(){
        return $this->masked;
    }

    public function getIds(){
        return $this->ids;
    }

    public function getTool(){
        return $this->tooltip;
    }

    public function getGapRange(){
        return $this->gapRange;
    }

    public function getPosArr(){
        return $this->posArr;
    }

    public function getInDel(){
        return $this->inDel;
    }
    
    public function getGeneName(){
        return $this->gene_name;
    }
    
    public function getProteinName(){
        return $this->protein_name;
    }
    
    public function getSpecies(){
        return $this->species;
    }
    
    public function getQFO(){
        return $this->model;
    }
}

?>