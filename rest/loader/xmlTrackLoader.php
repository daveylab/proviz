<?php

//     
//     ProViz - protein visualisation tool
//     Copyright (C) 2016  Norman E. Davey, Peter Jehl, Jean Manguy
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//     Author contact: Norman E. Davey <normandavey@gmail.com>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Peter Jehl <peter.jehl@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Jean Manguy <jean.manguy@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//
  
class xmlTrackLoader {
    
    private $tracks;
    
    public function xmlTrackLoader($contents){
        
        $xmlArray = xml2array($contents);
        $trackArr = [];
        if(array_key_exists("tracks", $xmlArray)) {
            if (array_key_exists("track", $xmlArray["tracks"])) {
                if(array_key_exists(0, $xmlArray["tracks"]["track"])){
                    $tracks = $xmlArray["tracks"]["track"];
                } else {
                    $tracks[0] = $xmlArray["tracks"]["track"];
                }
                for ($i = 0; $i < count($tracks); $i++) {
                    $tmpTrackArr = [];
                    $tmpTrackArr["data"] = [];
                    if(array_key_exists("attr", $tracks[$i])){
                        if(array_key_exists("type", $tracks[$i]["attr"])){
                            $tmpTrackArr["type"] = $tracks[$i]["attr"]["type"];
                        } else {
                            return null;
                        }
                        if(array_key_exists("position", $tracks[$i]["attr"])){
                            $tmpTrackArr["position"] = $tracks[$i]["attr"]["position"];
                        } else {
                            return null;
                        }
                        if (array_key_exists("name", $tracks[$i]["attr"])) {
                            $tmpTrackArr["name"] = $tracks[$i]["attr"]["name"];
                        }
                        if (array_key_exists("colour", $tracks[$i]["attr"])){
                            $tmpTrackArr["colour"] = $tracks[$i]["attr"]["colour"];
                        }
                        if (array_key_exists("opacity", $tracks[$i]["attr"])) {
                            $tmpTrackArr["opacity"] = $tracks[$i]["attr"]["opacity"];
                        }
                    } else {
                        return null;
                    }
                    if(array_key_exists("entry", $tracks[$i])){
                        if(array_key_exists(0, $tracks[$i]["entry"])){
                            $entry = $tracks[$i]["entry"];
                        } else {
                            $entry[0] = $tracks[$i]["entry"];
                        }
                        for($j = 0 ; $j < count($entry); $j++){
                            $tmpEntryArr = [];
                            if(array_key_exists("attr", $entry[$j])){
                                if(array_key_exists("start", $entry[$j]["attr"]) && array_key_exists("end", $entry[$j]["attr"])){
                                    $tmpEntryArr["start"] = $entry[$j]["attr"]["start"];
                                    $tmpEntryArr["end"] = $entry[$j]["attr"]["end"];
                                    $tmpEntryArr["length"] = $tmpEntryArr["end"] - $tmpEntryArr["start"] + 1;
                                } else if(array_key_exists("position", $entry[$j]["attr"])) {
                                    $tmpEntryArr["position"] = $entry[$j]["attr"]["position"];
                                } else {
                                    return null;
                                }
                                if(array_key_exists("text", $entry[$j]["attr"]) xor array_key_exists("value", $entry[$j]["attr"])){
                                    if(array_key_exists("text", $entry[$j]["attr"])) {
                                        $tmpEntryArr["text"] = $entry[$j]["attr"]["text"];
                                    } else if(array_key_exists("value", $entry[$j]["attr"])){
                                        $tmpEntryArr["value"] = $entry[$j]["attr"]["value"];
                                    } else {
                                        return null;
                                    }
                                } else {
                                    return null;
                                }
                                if(array_key_exists("hover", $entry[$j]["attr"])){
                                    $tmpEntryArr["hover"] = $entry[$j]["attr"]["hover"];
                                }
                                if(array_key_exists("link", $entry[$j]["attr"])){
                                    $tmpEntryArr["link"] = $entry[$j]["attr"]["link"];
                                }
                                if(array_key_exists("colour", $entry[$j]["attr"])){
                                    $tmpEntryArr["colour"] = $entry[$j]["attr"]["colour"];
                                }
                                if(array_key_exists("opacity", $entry[$j]["attr"])){
                                    $tmpEntryArr["opacity"] = $entry[$j]["attr"]["opacity"];
                                }
                            } else {
                                return null;
                            }
                            if($tmpTrackArr["type"] === "feature"){
                                array_push($tmpTrackArr["data"], $tmpEntryArr);
                            } else {
                                $tmpTrackArr["data"][$tmpEntryArr["position"]] = $tmpEntryArr;
                            }
                        }
                    }
                    array_push($trackArr, $tmpTrackArr);
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
        $this->tracks = $trackArr;
    }
    
    public function getTracks() {
        return $this->tracks;
    }
}
?>