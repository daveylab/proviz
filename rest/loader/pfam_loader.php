<?php

//     
//     ProViz - protein visualisation tool
//     Copyright (C) 2016  Norman E. Davey, Peter Jehl, Jean Manguy
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//     Author contact: Norman E. Davey <normandavey@gmail.com>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Peter Jehl <peter.jehl@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Jean Manguy <jean.manguy@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//
  
class pfam_loader {

    private $pfams = [];

    public function pfam_loader($contents) {

        if(startsWith($contents, '<?xml version="1.0" encoding="UTF-8"?>')){
            $this->pfams["type"] = "feature";
            $this->pfams["data"] = [];
            $xmlPfam = xml2array($contents);
            if(!array_key_exists("error", $xmlPfam)){
                if (array_key_exists("matches", $xmlPfam["pfam"]["entry"])) {
                    if (array_key_exists("match", $xmlPfam["pfam"]["entry"]["matches"])) {
                        foreach ($xmlPfam["pfam"]["entry"]["matches"] as $tmp1 => $matches) {
                            if (array_key_exists("attr", $matches)) {
                                $name = $matches["attr"]["id"];
                                $evalue = "";
                                if (array_key_exists("location", $matches)) {
                                    $start = $matches["location"]["attr"]["start"];
                                    $end = $matches["location"]["attr"]["end"];
                                    if (array_key_exists("evalue", $matches["location"]["attr"])) {
                                        $evalue = $matches["location"]["attr"]["evalue"];
                                    }
                                }

                                $tmp = [];

                                if ($evalue !== "") {
                                    $tmp["text"] = $name . ' (' . $evalue . ')';
                                    $tmp["hover"] = $name . ' (' . $evalue . ')';
                                } else {
                                    $tmp["text"] = $name;
                                    $tmp["hover"] = $name;
                                }
                                $tmp["link"] = 'http://pfam.xfam.org/family/' . $name;
                                $tmp["end"] = $end;
                                $tmp["start"] = $start;
                                $tmp["length"] = $end - $start + 1;

                                array_push($this->pfams["data"], $tmp);
                            } else {
                                foreach ($matches as $tmp1 => $match) {
                                    $name = $match["attr"]["id"];
                                    $evalue = "";
                                    if (array_key_exists("location", $match)) {
                                        $start = $match["location"]["attr"]["start"];
                                        $end = $match["location"]["attr"]["end"];
                                        if (array_key_exists("evalue", $match["location"]["attr"])) {
                                            $evalue = $match["location"]["attr"]["evalue"];
                                        }
                                    }

                                    $tmp = [];
                                    if ($evalue !== "") {
                                        $tmp["text"] = $name . ' (' . $evalue . ')';
                                        $tmp["hover"] = $name . ' (' . $evalue . ')';
                                    } else {
                                        $tmp["text"] = $name;
                                        $tmp["hover"] = $name;
                                    }
                                    $tmp["link"] = 'http://pfam.xfam.org/family/' . $name;
                                    $tmp["end"] = $end;
                                    $tmp["start"] = $start;
                                    $tmp["length"] = $end - $start + 1;

                                    array_push($this->pfams["data"], $tmp);
                                }
                            }
                        }
                    }
                }
            }
        } else {
            $this->pfams["type"] = "feature";
            $this->pfams["data"] = [];
        }
    }

    public function getPfams() {
        return $this->pfams;
    }
}

?>