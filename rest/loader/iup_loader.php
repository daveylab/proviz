<?php

//     
//     ProViz - protein visualisation tool
//     Copyright (C) 2016  Norman E. Davey, Peter Jehl, Jean Manguy
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//     Author contact: Norman E. Davey <normandavey@gmail.com>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Peter Jehl <peter.jehl@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Jean Manguy <jean.manguy@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//
  
class iup_loader {
    
    private $iup_hist = [];
    
    public function iup_loader($contents) {
        
        $this->iup_hist["type"] = "histogram";
        $this->iup_hist["data"] = [];
        trim($contents);
        $iupArr = explode("\n", $contents);
        array_shift($iupArr);
        $counterIUP = 0;
        foreach($iupArr as $key => $item){
            if(!startsWith($item, "#") && ($item != "")){
                $tmpLine = explode(" ", preg_replace('/\s+/' ,' ',trim($item)));
                $tmp["value"] = $tmpLine[2];
                $tmp["hover"] = "Score: " . $tmpLine[2];
                $tmp["end"] = $counterIUP;
                $tmp["start"] = $counterIUP;
                $tmp["length"] = 1;
                
                array_push($this->iup_hist["data"], $tmp);
                $counterIUP++;
            }
        }
    }
    
    public function getIUPHist(){
        return $this->iup_hist;
    }
}

?>