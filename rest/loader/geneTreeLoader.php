<?php

//     
//     ProViz - protein visualisation tool
//     Copyright (C) 2016  Norman E. Davey, Peter Jehl, Jean Manguy
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//     Author contact: Norman E. Davey <normandavey@gmail.com>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Peter Jehl <peter.jehl@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Jean Manguy <jean.manguy@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//
  
class geneTreeLoader {
    
    private $taxonomy;
    private $species;
    private $mainName = "";
    private $mainSeq;
    private $mainNode;
    private $uniSeq;
    private $duplications = [];
    private $seqs = [];
    private $names = [];
    private $gene_names = [];
    private $protein_names = [];
    private $qfo = [];
    private $type = [];
    private $taxon = [];
    private $id = [];
    private $leafList = [];
    private $check = true;
    private $paraNr = 0;
    private $taxonList = [7165,224324,3702,224308,226186,9913,224911,7739,6239,237561,9615,272561,324602,7719,214684,7955,243230,515635,44689,7227,83333,190304,9031,243231,184922,251221,64091,9606,6945,374847,5664,189518,9544,243232,188937,13616,81824,10090,83332,45351,330879,367110,9258,9598,321614,3218,36329,208964,10116,243090,559292,6183,284812,665079,100226,273057,1111708,31033,35128,69014,289376,243274,5722,237631,8364,284591];

    public function geneTreeLoader($content, $taxonomy, $speciesName, $uniSeq, $geneTreeMode) {
        
        if($content === null){
            $this->check = false;
        } else {
            $this->species = $speciesName;
            $this->taxonomy = $taxonomy;
            $this->uniSeq = implode("",$uniSeq);
            $tree = json_decode($content);
            $tmp = $tree->tree;
            $root = $this->calcNode($tmp);
            $this->calcParaNr($root);
            if($this->mainName === ""){
                $this->check = false;
            }
            if($this->check){
                // Iterate tree from mainNode
                $tmp = $this->mainNode;
                if($geneTreeMode !== "paralog"){
                    $this->addSubtreeLeaves($tmp, 0, "ortholog");
                    $paraNr = 0;
                    $tmpChild = NULL;
                    while($tmp !== null){
                        if(!empty($tmp->getChildren())){
                            if($tmp->getType() !== "duplication"){
                                // Resolve subtree
                                if($geneTreeMode !== "paralog"){
                                    if($tmp->getChildren()[0] === $tmpChild){
                                        $this->addSubtreeLeaves($tmp->getChildren()[1], 0, "ortholog");
                                    } else {
                                        $this->addSubtreeLeaves($tmp->getChildren()[0], 0, "ortholog");
                                    }
                                }
                            } else {
                                // Resolve duplication subtree
                                if($geneTreeMode !== "ortholog"){
                                    if($tmp->getChildren()[0] === $tmpChild){
                                        if($tmp->getChildren()[1]->getParaNr() !== 1){
                                            $this->resolveDuplication($tmp->getChildren()[1]);
                                        } else if($tmp->getChildren()[1]->getParaNr() === 0){
                                            $this->addSubtreeLeaves($tmp->getChildren()[1], 0, "ortholog");
                                        } else {
                                            $this->paraNr++;
                                            $this->addSubtreeLeaves($tmp->getChildren()[1], $this->paraNr, "paralog");
                                        }
                                    } else {
                                        if($tmp->getChildren()[0]->getParaNr() !== 1){
                                            $this->resolveDuplication($tmp->getChildren()[0]);
                                        } else if($tmp->getChildren()[0]->getParaNr() === 0){
                                            $this->addSubtreeLeaves($tmp->getChildren()[0], 0, "ortholog");
                                        } else {
                                            $this->paraNr++;
                                            $this->addSubtreeLeaves($tmp->getChildren()[0], $this->paraNr, "paralog");
                                        }
                                    }
                                }
                            }
                        }
                        //Iterate up the tree
                        $tmpChild = $tmp;
                        $tmp = $tmp->getParent();
                    }
                    $this->duplicationNr = $paraNr;
                } else {
                    for($i = 0 ; $i < count($this->leafList) ; $i++){
                        if($this->uniSeq === str_replace("-", "", $this->leafList[$i]->getSeq())){
                            if($this->leafList[$i]->getGeneName() !== ""){
                                array_unshift($this->names, $this->leafList[$i]->getGeneName() . " OS=" . $this->leafList[$i]->getName());
                                array_push($this->gene_names, $this->leafList[$i]->getGeneName());
                            } else {
                                array_unshift($this->names, $this->leafList[$i]->getGeneName() . " OS=" . $this->leafList[$i]->getName());
                                array_push($this->gene_names, "");
                            }
                            array_unshift($this->id, $this->leafList[$i]->getEnsembleId());
                            array_unshift($this->taxon, $this->leafList[$i]->getTaxon());
                            array_unshift($this->type, "paralog" . $i);
                            array_unshift($this->seqs, $this->leafList[$i]->getSeq());
                        } else {
                            if($this->leafList[$i]->getGeneName() !== ""){
                                array_unshift($this->names, $this->leafList[$i]->getGeneName() . " OS=" . $this->leafList[$i]->getName());
                                array_push($this->gene_names, $this->leafList[$i]->getGeneName());
                            } else {
                                array_unshift($this->names, $this->leafList[$i]->getGeneName() . " OS=" . $this->leafList[$i]->getName());
                                array_push($this->gene_names, "");
                            }
                            array_push($this->id, $this->leafList[$i]->getEnsembleId());
                            array_push($this->taxon, $this->leafList[$i]->getTaxon());
                            array_push($this->type, "paralog" . $i);
                            array_push($this->seqs, $this->leafList[$i]->getSeq());
                        }
                    }
                }
            }
        }
        $flag1 = FALSE;
        $matches = 0;
        for($i = 0; $i < count($this->type); $i++){
            $tmpStr = $this->type[$i];
            preg_match('#(\d+)$#', $tmpStr, $matches);
            if(!$flag1 && $matches[0] == 1){
                $flag1 = TRUE;
            }
            if(($flag1 && ($matches[0] != 0)) || (!$flag1 && ($matches[0] == 0))){
                $tmpStr = str_replace($matches[0], ($matches[0]+1), $tmpStr);
            }
            $this->type[$i] = $tmpStr;
        }
    }
    
    private function calcParaNr($tmp){
        if($tmp->getType() === "leaf"){
            if($tmp->getTaxon() == $this->taxonomy){
                $tmp->setParaNr(1);
                return $tmp->getParaNr();
            } else {
                $tmp->setParaNr(0);
                return $tmp->getParaNr();
            }
        } else {
            $tmp->setParaNr($this->calcParaNr($tmp->getChildren()[0]) + $this->calcParaNr($tmp->getChildren()[1]));
            return $tmp->getParaNr();
        }
    }
    
    private function resolveDuplication($tmp){
        if($tmp->getChildren() !== NULL){
            if($tmp->getChildren()[1]->getParaNr() !== 1){
                $this->resolveDuplication($tmp->getChildren()[1]);
            } else if($tmp->getChildren()[1]->getParaNr() === 0){
                $this->addSubtreeLeaves($tmp->getChildren()[1], 0, "ortholog");
            } else {
                $this->paraNr++;
                $this->addSubtreeLeaves($tmp->getChildren()[1], $this->paraNr, "paralog");
            }
            if($tmp->getChildren()[0]->getParaNr() !== 1){
                $this->resolveDuplication($tmp->getChildren()[0]);
            } else if($tmp->getChildren()[0]->getParaNr() === 0){
                $this->addSubtreeLeaves($tmp->getChildren()[0], 0, "ortholog");
            }else {
                $this->paraNr++;
                $this->addSubtreeLeaves($tmp->getChildren()[0], $this->paraNr, "paralog");
            }
        } else {
            $this->addSubtreeLeaves($tmp, $this->paraNr, "paralog");
        }
    }

    private function addSubtreeLeaves($tmpChild, $paraNr, $homoType){
        
        $nodes = array($tmpChild);
        while(!empty($nodes)){
            $tmp = array_pop($nodes);
            if($tmp->getChildren() !== null){
                array_push($nodes, $tmp->getChildren()[0]);
                array_push($nodes, $tmp->getChildren()[1]);
            } else {
                if(in_array($tmp->getTaxon(), $this->taxonList)){
                    if($tmp->getGeneName() !== ""){
                        array_push($this->names, $tmp->getGeneName() . " OS=" . $tmp->getName());
                        array_push($this->gene_names, $tmp->getGeneName());
                        array_push($this->protein_names, $tmp->getGeneName());
                    } else {
                        array_push($this->names, $tmp->getName());
                        array_push($this->protein_names, $tmp->getGeneName());
                    }
                    array_push($this->id, $tmp->getEnsembleId());
                    array_push($this->taxon, $tmp->getTaxon());
                    array_push($this->type, $homoType . $paraNr);
                    array_push($this->seqs, $tmp->getSeq());
                }
            }
        }
    }
    
    private function calcNode($tmp){
        $tmpNode = new treeNode();
        if(isset($tmp->children)){
            $tmpNode->addChild($this->calcNode($tmp->children[0]));
            $tmpNode->addChild($this->calcNode($tmp->children[1]));
            $tmpNode->setType($tmp->events->type);
            $tmpNode->setName($tmp->taxonomy->scientific_name);
            $tmpNode->setParentDist($tmp->branch_length);
            $tmpNode->getChildren()[0]->setParent($tmpNode);
            $tmpNode->getChildren()[1]->setParent($tmpNode);
        } else {
            $tmpName = "";
            $tmpTaxonId = "";
            if(property_exists($tmp->taxonomy, "scientific_name")){
                $tmpName = $tmp->taxonomy->scientific_name;
                $tmpTaxonId = $tmp->taxonomy->id;
            } else {
                $taxonId = "";
            }
            if((property_exists($tmp, "id")) && (property_exists($tmp->id, "source")) && ($tmp->id->source === "EnsEMBL") && (property_exists($tmp->id, "accession"))){
                $ensemblId = $tmp->id->accession;
            } else {
                $ensemblId = "";
            }
            $tmpNode = new treeNode($tmp->branch_length, null, "leaf", $tmpName, $tmp->sequence->mol_seq->seq, $tmpTaxonId, $ensemblId);
            if(property_exists($tmp->sequence, "name")){
                $tmpNode->setGeneName($tmp->sequence->name);
            } else {
                $tmpNode->setGeneName("");
            }
            if($tmpNode->getTaxon() == $this->taxonomy){
                array_push($this->leafList, $tmpNode);
            }
            if(($tmpTaxonId == $this->taxonomy)&& (str_replace("-", "", $tmp->sequence->mol_seq->seq) == $this->uniSeq)){
                $this->mainName = $tmpName;
                $this->mainSeq = $tmp->sequence->mol_seq->seq;
                $this->mainNode = $tmpNode;
            }
        }
        return $tmpNode;
    }
    
    public function writeAlignment($path){
        $alignmentFasta = "";
        for($k = 0 ; $k < count($this->names); $k++){
            $alignmentFasta .= ">" . $this->names[$k];
            if($this->gene_names[$k] !== ""){
                $alignmentFasta .= " GN=" . $this->gene_names[$k] . "\n";
            }
            $alignmentFasta .= $this->seqs[$k] . "\n";
        }
        fileSaver($path, $alignmentFasta);
    }
    
    public function getCheck(){
        return $this->check;
    }
    
    public function getTypes(){
        return $this->type;
    }
    
    public function getDups(){
        return $this->duplications;
    }
    
    public function getTaxons(){
        return $this->taxon;
    }
    
    public function getEnsembleIds(){
        return $this->id;
    }
    
    public function getGeneName(){
        return $this->gene_names;
    }
    
    public function getProteinName(){
        return $this->protein_names;
    }
    
    public function getQFO(){
        return $this->qfo;
    }
}

?>
