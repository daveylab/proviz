<?php

//     
//     ProViz - protein visualisation tool
//     Copyright (C) 2016  Norman E. Davey, Peter Jehl, Jean Manguy
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//     Author contact: Norman E. Davey <normandavey@gmail.com>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Peter Jehl <peter.jehl@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Jean Manguy <jean.manguy@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//
  
class phosphoElm_loader {

    private $phospos = [];

    public function phosphoElm_loader($contents) {

        if(!startsWith($contents, "<h2>Sorry")){
            $this->phospos["type"] = "feature";
            $this->phospos["data"] = [];
            trim($contents);
            $phosElmArr = explode("\n", $contents);
            array_shift($phosElmArr);
            $tmp = [];
            $hover = [];
            $elm = [];
            foreach($phosElmArr as $key => $item){
                if($item != ""){
                    $tmpLine = explode(";", $item);
                    if(!empty($tmp) && $tmp["start"] !== trim($tmpLine[2])){
                        $tmp["hover"] = $hover;
                        $tmp["elm"] = $elm;
                        $tmp = $this->createHoverLink($tmp);
                        array_push($this->phospos["data"], $tmp);
                        $tmp = [];
                        $hover = [];
                        $elm = [];
                    }
                    $tmp["text"] = "P";
                    if(empty($tmp["link"])){
                        $tmp["link"] = [];
                    }
                    if(((empty($hover) || (!in_array(trim($tmpLine[4]), $hover)))) && trim($tmpLine[4]) !== "none"){
                        array_push($hover, trim($tmpLine[4]));
                        if((trim($tmpLine[8] !== "")) && (trim($tmpLine[8]) !== "none")){
                            array_push($elm, trim($tmpLine[8]));
                        } else {
                            array_push($elm, "");
                        }
                    }
                    if((empty($tmp["link"]) || (!in_array(trim($tmpLine[5]), $tmp)))){
                        array_push($tmp["link"], trim($tmpLine[5]));
                    }
                    $tmp["end"] = trim($tmpLine[2]);
                    $tmp["start"] = trim($tmpLine[2]);
                    $tmp["length"] = 1;
                }
            }
            $tmp["hover"] = $hover;
            $tmp["elm"] = $elm;
            $tmp = $this->createHoverLink($tmp);
            array_push($this->phospos["data"], $tmp);
        } else {
            $this->phospos = null;
        }
    }

    private function createHoverLink($tmp){
        $linkStr = "http://www.ncbi.nlm.nih.gov/pubmed/";
        for($i = 0 ; $i < count($tmp["link"]); $i++){
            if($tmp["link"][$i] !== ""){
                $linkStr = $linkStr . $tmp["link"][$i] . ",";
            }
        }
        $tmp["link"] = $linkStr;
        $hoverStr = "Phosphorylation<br>";
        for($i = 0 ; $i < count($tmp["hover"]); $i++){
            if($tmp["hover"][$i] !== ""){
                $hoverStr = $hoverStr . "Kinase: " . $tmp["hover"][$i];
                if($tmp["elm"][$i] !== ""){
                    $hoverStr = $hoverStr . " Elm: " . $tmp["elm"][$i];
                }
            }
            $hoverStr = $hoverStr . "<br>";
        }
        $tmp["hover"] = $hoverStr;
        return $tmp;
    }

    public function getPhosphos(){
        return $this->phospos;
    }
}

?>