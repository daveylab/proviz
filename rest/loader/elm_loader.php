<?php

//     
//     ProViz - protein visualisation tool
//     Copyright (C) 2016  Norman E. Davey, Peter Jehl, Jean Manguy
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//     Author contact: Norman E. Davey <normandavey@gmail.com>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Peter Jehl <peter.jehl@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Jean Manguy <jean.manguy@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//
  
class elm_loader {
    
    private $elms = [];
    
    public function elm_loader($contents, $uniprotid){
        
        $this->elms["type"] = "feature";
        $this->elms["data"] = [];
        $elmArr = explode("\n", $contents);
        foreach($elmArr as $key => $item){
            if(startsWith($item, $uniprotid)){
                $tmpLine = explode("\t", $item);
                $name = trim(explode("=", $tmpLine[8])[1]);

                $tmp["text"] = substr($name, 4);
                $tmp["hover"] = $name . " motif";
                $tmp["link"] = "http://elm.eu.org/elms/" . $name . ".html";
                $tmp["end"] = $tmpLine[4];
                $tmp["start"] = $tmpLine[3];
                $tmp["length"] = $tmpLine[4] - $tmpLine[3] + 1;
                
                array_push($this->elms["data"], $tmp);
            }
        }
        if(empty($this->elms["data"])){
            $this->elms = null;
        }
    }
    
    public function getElms(){
        return $this->elms;
    }
}
?>