<?php

//     
//     ProViz - protein visualisation tool
//     Copyright (C) 2016  Norman E. Davey, Peter Jehl, Jean Manguy
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//     Author contact: Norman E. Davey <normandavey@gmail.com>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Peter Jehl <peter.jehl@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Jean Manguy <jean.manguy@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//
  
class mobiDB_loader {
    
    private $disorder = [];
    
    public function mobiDB_loader($contents, $uniprot_acc) {
        
        if($contents != '{"error":"Error while fetching disorder data"}'){
            $this->disorder["type"] = "feature";
            $this->disorder["data"] = [];
            $contents = json_decode($contents);
            $tmp = $contents->consensus->predictors;
            for($i = 0 ; $i < count($tmp); $i++){
                $tmpMobi = [];
                if($tmp[$i]->ann == "d"){
                    $tmpMobi["text"] = "Disordered";
                    $tmpMobi["link"] = 'http://mobidb.bio.unipd.it/entries/' . $uniprot_acc;
                    $tmpMobi["end"] = $tmp[$i]->end;
                    $tmpMobi["start"] = $tmp[$i]->start;
                    $tmpMobi["length"] = $tmp[$i]->end - $tmp[$i]->start + 1;
                    array_push($this->disorder["data"], $tmpMobi);
                }
            }
        } else {
            $this->disorder = null;
        }
    }
    
    public function getMobi(){
        return $this->disorder;
    }
    
}

?>