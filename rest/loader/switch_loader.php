<?php

//     
//     ProViz - protein visualisation tool
//     Copyright (C) 2016  Norman E. Davey, Peter Jehl, Jean Manguy
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//     Author contact: Norman E. Davey <normandavey@gmail.com>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Peter Jehl <peter.jehl@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Jean Manguy <jean.manguy@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//
    
class switch_loader {
    
    private $switches = [];
    private $ptm = [];
    
    public function switch_loader($contents){
        
        $dataArr = json_decode($contents);
        $this->switches["type"] = "feature";
        $this->switches["data"] = [];
        $this->ptm["type"] = "feature";
        $this->ptm["data"] = [];

        for($i = 0; $i < count($dataArr->result); $i++){
            //print_r($dataArr->result[$i]);
            //echo "<br>";
            $tmp = [];
            $tmp["text"] = $dataArr->result[$i]->description->type;
            $tmp["start"] = $dataArr->result[$i]->start;
            $tmp["end"] = $dataArr->result[$i]->stop;
            $tmp["length"] = $tmp["end"] - $tmp["start"] + 1;
            $tmp["hover"] = $dataArr->result[$i]->description->mechanism;
            $tmp["link"] = $dataArr->result[$i]->url;
            array_push($this->switches["data"], $tmp);
        }
    }
    
    function getSwitch(){
        return $this->switches;
    }
}

?>