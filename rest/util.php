<?php

//     
//     ProViz - protein visualisation tool
//     Copyright (C) 2016  Norman E. Davey, Peter Jehl, Jean Manguy
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//     Author contact: Norman E. Davey <normandavey@gmail.com>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Peter Jehl <peter.jehl@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Jean Manguy <jean.manguy@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//
  
function checkMD5($seq){
    $md5 = md5($seq);
    $contents = "";
    $url = "http://www.uniprot.org/uniparc/?query=checksum:" . $md5 . "&sort=score&columns=kb&format=tab";
    if (get_headers($url)[0] !== "HTTP/1.1 404 Not Found") {
        $contents = fileReader($url);
    }
    $uniprot_acc = "";
    if ($contents !== "") {
        $contents_exp = explode(";", explode("\n", $contents)[1]);
        for ($i = 0; $i < count($contents_exp); $i++) {
            if (!endsWith($contents_exp[$i], "(obsolete)")) {
                $uniprot_acc = trim($contents_exp[$i]);
                break;
            }
        }
    }
    return $uniprot_acc;
}

function createOffsetInfo(){
    return '<div id="i_offset" class="featureLine" style="width:204px;"><div class="featureType">offset</div><div class="featureHelp" data-tooltip="Position relative to the start of the protein"><i class="fa fa-question-circle"></i></div></div>';
}

function createOffsetData($ali_start, $ali_end){
    $offsetData = "";
    $offsetData .= '<div id="d_offset" class="featureLine"><div class="offsetData" style="width:' . ($ali_end - $ali_start) * 10 . 'px;">';
    $offsetData .= '<div id="first" class="oC" name="' . ($ali_start + 1) . '" style="float:left;"></div>';
    for ($i = $ali_start + 1; $i < $ali_end; $i = $i + 50) {
        if ($ali_end - 5 > $i) {
            $offsetData .= '<div class="oC" style="left:' . ((($i - $ali_start - 1) * 10) + 5) . 'px"><i class="fa fa-long-arrow-down"></i>' . $i . '</div>';
        }
    }
    $offsetData .= '<div id="last" class="oC" name="' . $ali_end . '" style="float:right;margin-right:3px;right:1px">' . $ali_end . '<i class="fa fa-long-arrow-down"></i></div></div></div>';
    return $offsetData;
}

function createAlignmentData($seqs, $aliWindowStart, $aliWindowEnd, $aliLoad, $aliColor, $start, $end, $settingsArr, $ids = NULL, $ensembleIds = NULL){
    $aliData = "";
    for ($i = $start; $i < $end; $i++) {
        $aliData .= '<div class="aliLine" id="d_' . $i . '"><div class="alignmentData"><div class="speciesA">';
        for ($j = $aliWindowStart; $j < $aliWindowEnd; $j++) {
            if (isset($aliLoad) && $aliLoad->getGapRange()[$j] == 0) {
                $gapIndi = "";
            } else if (!isset($aliLoad)) {
                $gapIndi = "";
            } else {
                $gapIndi = "gap";
            }
            if (isset($aliLoad) && array_key_exists($j, $aliLoad->getInDel()[$i])) {
                $inDel = " data-indel=" . $aliLoad->getInDel()[$i][$j];
                $inDelClass = "inDel";
            } else {
                $inDel = "";
                $inDelClass = "";
            }
            if (isset($aliLoad)) {
                $pos = $aliLoad->getPosArr()[$i][$j];
            } else {
                $pos = $j + 1;
            }
            $aliData .= '<div class="aC ' . $aliColor[$i][$j] . " " . $gapIndi . " " . $inDelClass . '" data-pos="' . $pos . '" ' . $inDel . '>' . $seqs[$i][$j] . '</div>';
        }
        $aliData .= '</div></div></div>';
    }
    return $aliData;
}

function createAlignmentInfo($seqs, $aliName, $options, $seqType, $aliTool, $aliGeneName, $aliProteinName, $aliModel, $names, $start, $end, $settingsArr, $taxons = NULL, $ids = NULL, $ensembleIds = NULL){
    $aliInfo = "";
    for ($i = $start; $i < $end; $i++) {
        $idAliLine = 'i_' . $i;
        $seqModel = $aliModel[$i];
        if ((count($aliName) !== 0) && ($aliName[0] === "GeneTree" && isset($seqType[$i]))) {
            preg_match('#(\d+)$#', $seqType[$i], $matches);
            $dataGeneTreeTaxon = $taxons[$i];
            $dataHomologyType = substr($seqType[$i], 0, strlen($seqType[$i]) - 1);
            $dataCladeNr = $matches[0];
            $geneTreeInfos = sprintf('data-geneTreeTaxon="%s" data-homologyType="%s" data-cladeNr="%s"', $dataGeneTreeTaxon, $dataHomologyType, $dataCladeNr, $seqModel);
        } else {
            $geneTreeInfos = '';
        }


        //if ($ids[$i] !== "") {
            $uniprot_id = $ids[$i];
            $urlProviz = sprintf('%s?uniprot_acc=%s', $options->get_self_path(), $uniprot_id);
            $urlUniprot = sprintf('http://www.uniprot.org/uniprot/%s', $uniprot_id);
            $proteinName = ($aliProteinName[$i] != '') ? trim($aliProteinName[$i]) : 'Uncharacterized protein';
            $geneName = ($aliGeneName[$i] != '') ? trim($aliGeneName[$i]) : 'Uncharacterized gene';
            $linkProviz = sprintf('<a class="self_link" target="_blank" href="%1$s" title="Load %3$s (%4$s) in Proviz" data-tooltip="load %3$s(%4$s) in Proviz" ></a>', $urlProviz, $uniprot_id, $proteinName, $geneName);
            $dataProteinId = $ids[$i];
            $nameSequence = sprintf('<a class="ali_name" target="_blank" href="%1$s" data-tooltip="%5$s (%4$s)<br>Click to view at UniProt " data-gene_name="%4$s" data-protein_name="%5$s"  data-proteinId="%2$s">%3$s</a>', $urlUniprot, $dataProteinId, $names[$i], $geneName, $proteinName);
        /*} else {
            $linkProviz = '';
            $proteinName = ($aliProteinName[$i] != '') ? trim($aliProteinName[$i]) : 'Uncharacterized protein';
            $geneName = ($aliGeneName[$i] != '') ? trim($aliGeneName[$i]) : 'Uncharacterized gene';
            if ($aliName[0] === "GeneTree" && $ensembleIds[$i] != '') {
                $nameSequence = sprintf('<span class="ali_name" data-tooltip="%1$s" data-proteinId="%1$s">%3$s</span>', $aliTool[$i], $ensembleIds[$i], $names[$i]);
            } else {
                $nameSequence = sprintf('<span class="ali_name" data-tooltip="%s">%s</span>', $aliTool[$i], $names[$i]);
            }

        }*/
        $buttons = ($i !== 0) ? '<i class="fa fa-circle-o select_protein" data-tooltip="select to move"></i><i class="fa fa-times hide_protein" data-tooltip="hide sequence"></i>' : '';
        $featureHelp = sprintf('<div class="featureHelp">%s</div>', $buttons);


        $aliInfo .= sprintf("
			<div class='aliLine' id='%s' data-model=\"%s\">
				<div class='aliType' %s>
				%s %s
				</div>
				%s
			</div>", $idAliLine, $seqModel, $geneTreeInfos, $linkProviz, $nameSequence, $featureHelp);
    }
    return $aliInfo;
}

function printArchitecture($testArr, $uniLoad){
    $archiStr = "";
    if (array_key_exists("secondary structure", $testArr)) {
        $archiStr .= '<div class="architectureTrack" data-name="secondary structure" style="height: 10px">';
        $archiStr .= createArchitecture("secondary_structure", $testArr["secondary structure"], count($uniLoad->getSeq()), 10);
        $archiStr .= "</div>";
    }
    if (array_key_exists("pfam", $testArr)) {
        $archiStr .= '<div class="architectureTrack" data-name="pfam" style="height: 10px">';
        $archiStr .= createArchitecture("pfam", $testArr["pfam"], count($uniLoad->getSeq()), 10);
        $archiStr .= "</div>";
    }
    if (array_key_exists("elm", $testArr) || array_key_exists("short sequence motif", $testArr)) {
        $archiStr .= '<div class="architectureTrack" data-name="elm" style="height: 10px">';
        if(array_key_exists("elm", $testArr)){
            $archiStr .= createArchitecture("elm", $testArr["elm"], count($uniLoad->getSeq()), 10);
        }
        if(array_key_exists("short sequence motif", $testArr)){
            $archiStr .= createArchitecture("ssm", $testArr["short sequence motif"], count($uniLoad->getSeq()), 10);
        }
        $archiStr .= '</div>';
    }
    if (array_key_exists("modified residue", $testArr)) {
        $archiStr .= '<div class="architectureTrack" data-name="modified residues"  style="height: 10px">';
        $archiStr .= createArchitecture("modified_residue", $testArr["modified residue"], count($uniLoad->getSeq()), 10);
        $archiStr .= '</div>';
    }
    $tmpArchiArr = [];
    $tmpArchiArr["data"] = [];
    if (array_key_exists("transmembrane region", $testArr)) {
        $tmpArchiArr["data"] = array_merge($tmpArchiArr["data"], $testArr["transmembrane region"]["data"]);
    }
    if (array_key_exists("topological domain", $testArr)) {
        $tmpArchiArr["data"] = array_merge($tmpArchiArr["data"], $testArr["topological domain"]["data"]);
    }
    if (!empty($tmpArchiArr["data"])) {
        $archiStr .= '<div class="architectureTrack" data-name="topological domain"  style="height: 10px">';
        $archiStr .= createArchitecture("topological domain", $tmpArchiArr, count($uniLoad->getSeq()), 10);
        $archiStr .= '</div>';
    }
    return $archiStr;
}

function createArchitecture($name, $inputArr, $ali_end, $height){
    $archi = [];
    foreach ($inputArr["data"] as $key => $item) {
        $classes = "archiCell {$name}";
        $position = $item["start"] . ':' . $item["end"];
        $left = intval($item["start"]) / $ali_end * 100;
        $start = intval($item["start"]);
        $end = intval($item["end"]);
        if ($start == $end) {
            $width = 1 / intval($ali_end) * 100;
        } else {
            $width = ($end - $start + 1) / intval($ali_end) * 100;
        }
        $backgroundColorAndBorder = "";
        if (isset($item["hover"])) {
            if ($item["hover"] === "helix") {
                $backgroundColorAndBorder = 'background-color:#FFCCCC; border:none;';
            } else if ($item["hover"] === "strand") {
                $backgroundColorAndBorder = 'background-color:#CCCCFF; border:none;';
            } else if ($item["hover"] === "turn") {
                $backgroundColorAndBorder = 'background-color:#CCFFCC; border:none;';
            } else if (startsWith($item["hover"], "Helical")) {
                $backgroundColorAndBorder = 'background-color:#CEECF5; border:none;';
            } else if (startsWith($item["hover"], "Cytoplasmic")) {
                $backgroundColorAndBorder = 'background-color:#A9D0F5; border:none;';
            } else if (startsWith($item["hover"], "Extracellular")) {
                $backgroundColorAndBorder = 'background-color:#A9D0F5; border:none;';
            }
        }
        $style = "left: {$left}%; height: {$height}px; width: {$width}%; {$backgroundColorAndBorder}";
        if($name == "pfam"){
            $archi[] = "<div class='{$classes}' style='{$style}' data-pos='{$position}' data-tooltip='{$item["hover"]}'>{$item["text"]}</div>";
        } else {
            $archi[] = "<div class='{$classes}' style='{$style}' data-pos='{$position}' data-tooltip='{$item["hover"]}'></div>";
        }
    }
    return implode('', $archi);
}

function createTrack($source, $data, $ali_start, $ali_end, $hover, $featureId, $disableId, $toCollapse=0)
{

    $trackStr = "";
    $dataStr = "";
    $trackColour = "";
    $opacity = "1";
    $source = str_replace(" ", "_", $source);
    $width = ($ali_end - $ali_start) * 10;
    if (array_key_exists("colour", $data)) {
        $trackColour = $data["colour"];
    }
    if (array_key_exists("opacity", $data)) {
        $opacity = $data["opacity"];
    }
    if ($data["type"] == "feature") {
        $height = 12 * count($data["data"]);
        $display = (!$toCollapse && !($source === "elm_regex" || $source === "psiPred"));
        $trackStr = createTrackInfo($featureId, $source, $hover, $height, $disableId, $display);
        $featureDataContent = createFeatureTrack($data["data"], $ali_start, $ali_end, $trackColour, $opacity);
        $dataStr = createTrackData($featureId, $source, $height, $width, $featureDataContent, $display);
    } elseif ($data["type"] == "peptide") {
        $height = 12 * count($data["data"]);
        $display = !$toCollapse;
        $trackStr = createTrackInfo($featureId, $source, $hover, $height, $disableId, $display);
        $peptideDataContent = createPeptideTrack($data["data"], $ali_start, $ali_end, $trackColour, $opacity);
        $dataStr = createTrackData($featureId, $source, $height, $width, $peptideDataContent, $display);
     } elseif ($data["type"] == "histogram") {
         $maxValue = 0;
         $minValue = 0;
         $display = !$toCollapse;
         foreach ($data["data"] as $key => $item) {
            $maxValue = max($maxValue, $item['value']);
            $minValue = min($minValue, $item['value']);
        }
        if ($minValue >= 0) {
            $height = 20;
            $positionZero = 'bottom';
        } elseif ($maxValue <= 0) {
            $height = 20;
            $positionZero = 'top';
        } else {
            $height = 40;
            $positionZero = 'bottom';
        }
        $display = !$toCollapse;
        $trackStr = createTrackInfo($featureId, $source, $hover, $height, $disableId, $display);
        $histogramDataContent = createHistoTrack($data["data"], $height, $positionZero, $source, $trackColour, $opacity, $width, $ali_start);
        $dataStr = createTrackData($featureId, $source, $height, $width, $histogramDataContent, $display);
    }
    return [$trackStr, $dataStr];
}

function createTrackInfo($featureId, $source, $hover, $height = 12, $disableId = '', $display = true)
{
    $displayStyle = ($display) ? '' : 'style="display: none;"';
    $source = str_replace('_', ' ', $source);
    return "
<div class='featureLine' data-disable='{$disableId}' id='i_{$featureId}' {$displayStyle}>
	<div class='featureType' style='height:{$height}px'>
	{$source}
	</div>
	<div class='featureHelp'>
		<i class='fa fa-circle-o select_feature'></i><i class='fa fa-times hide_feature'></i><i class='fa fa-question-circle' data-tooltip='{$hover}'></i>
	</div>
</div>
		";
}

function createTrackData ($featureId, $source, $height, $width, $trackDataContent, $display = true) {
    $displayStyle = ($display) ? '' : 'display: none;';
    return "<div class='featureLine' id='d_{$featureId}' style='{$displayStyle}'>
	<div id='{$source}' class='featureData' style='height: {$height}px; width: {$width}px;'>
	{$trackDataContent}
	</div>
</div>";
}

function createFeatureTrack($data, $ali_start, $ali_end, $trackColour, $opacity) {
    $content = array();
    foreach ($data as $position => $line) {
        $top = $position * 12;
        foreach ($line as $j => $feat) {
            if ($feat['start'] < $ali_end || $feat['end'] > $ali_start) {
                $eleOpacity = (isset($histo['opacity'])) ? $histo['opacity'] : $opacity;
                $dataTooltip = (isset($feat['hover'])) ? $feat['hover'] : '';
                switch ($dataTooltip) {
                    case "helix":
                        $eleBGcolor = "#FFCCCC";
                        break;
                    case "strand":
                        $eleBGcolor = "#CCCCFF";
                        break;
                    case "turn":
                        $eleBGcolor = "#CCFFCC";
                        break;
                    default:
                        $eleBGcolor = (isset($histo['colour'])) ? $histo['colour'] : $trackColour;
                        break;
                }

                if ($ali_start > $feat['start'] && $ali_end < $feat["end"]) {
                    $left = 0;
                    $width = ($ali_end - $ali_start) * 10;
                    $posStart = $ali_start + 1;
                    $posEnd = $ali_end;
                } else if ($ali_start >= $feat["start"]) {
                    $left = 0;
                    $width = (($feat["length"] - ($ali_start - $feat["start"])) - 1) * 10;
                    $posStart = $ali_start + 1;
                    $posEnd = $feat["end"];
                } else if ($ali_end <= $feat["end"]) {
                    $left = ($feat["start"] - (1 + $ali_start)) * 10;
                    $width = ($feat["length"] - ($feat["end"] - $ali_end)) * 10;
                    $posStart = $feat["start"];
                    $posEnd = $ali_end;
                } else {
                    $left = ($feat["start"] - (1 + $ali_start)) * 10;
                    $width = $feat["length"] * 10;
                    $posStart = $feat["start"];
                    $posEnd = $feat["end"];
                }

                $dataPos = $posStart . ":" . $posEnd;

                $style = "width: {$width}px; top: {$top}px; left: {$left}px;";

                if ($eleBGcolor != "") {
                    $style .= " background-color: {$eleBGcolor};";
                }
                if ($eleOpacity != "") {
                    $style .= " opacity: {$eleOpacity};";
                }

                if (array_key_exists("link", $feat)) {
                    $contentDiv = "<a href='{$feat['link']}' target='_blank'>{$feat['text']}</a>";
                } else {
                    $contentDiv = $feat['text'];
                }
                $content[] = "<div data-tooltip='{$dataTooltip}' data-pos='{$dataPos}' style='{$style}' class='fC'>{$contentDiv}</div>";
            }
        }
    }
    return implode(' ', $content);
}

function createPeptideTrack($data, $ali_start, $ali_end, $trackColour, $opacity) {
    $content = array();
    foreach ($data as $position => $line) {
        $top = $position * 12;
        foreach ($line as $j => $peptide) {
            if ($peptide['start'] < $ali_end || $peptide['end'] > $ali_start) {
                $eleOpacity = (isset($histo['opacity'])) ? $histo['opacity'] : $opacity;
                $eleBGcolor = (isset($histo['colour'])) ? $histo['colour'] : $trackColour;

                if ($ali_start > $peptide['start'] && $ali_end < $peptide["end"]) {
                    $left = 0;
                    $width = ($ali_end - $ali_start) * 10;
                    $posStart = $ali_start + 1;
                    $posEnd = $ali_end;
                } else if ($ali_start >= $peptide["start"]) {
                    $left = 0;
                    $width = (($peptide["length"] - ($ali_start - $peptide["start"])) - 1) * 10;
                    $posStart = $ali_start + 1;
                    $posEnd = $peptide["end"];
                } else if ($ali_end <= $peptide["end"]) {
                    $left = ($peptide["start"] - (1 + $ali_start)) * 10;
                    $width = ($peptide["length"] - ($peptide["end"] - $ali_end)) * 10;
                    $posStart = $peptide["start"];
                    $posEnd = $ali_end;
                } else {
                    $left = ($peptide["start"] - (1 + $ali_start)) * 10;
                    $width = $peptide["length"] * 10;
                    $posStart = $peptide["start"];
                    $posEnd = $peptide["end"];
                }

                $dataPos = $posStart . ":" . $posEnd;
                $dataTooltip = (isset($peptide["hover"])) ? $peptide["hover"] : "";

                $style = "width: {$width}px; top: {$top}px; left: {$left}px;";

                if ($eleBGcolor != "") {
                    $style .= " background-color: {$eleBGcolor};";
                }
                if ($eleOpacity != "") {
                    $style .= " opacity: {$eleOpacity};";
                }

                $sequence = str_split($peptide['text']);
                $seqs = array();
                foreach ($sequence as $aa) {
                    array_push($seqs, "<div class='aC'>{$aa}</div>");
                }
                $seqDivs = implode('', $seqs);


                if (array_key_exists("link", $peptide)) {
                    $contentDiv = "<a href='{$peptide['link']}' target='_blank'>{$seqDivs}</a>";
                } else {
                    $contentDiv = $seqDivs;
                }
                $content[] = "<div data-tooltip='{$dataTooltip}' data-pos='{$dataPos}' style='{$style}' class='peptide'>{$contentDiv}</div>";
            }
        }
    }
    return implode(' ', $content);
}



function createHistoTrack($data, $height, $positionZero, $source, $trackColour, $opacity, $width, $ali_start) {
    $content = array();
    $topBottomOffset = ($height == 20) ? 0 : 50;
    foreach ($data as $position => $histo) {
        $eleColour = (isset($histo['colour'])) ? $histo['colour'] : $trackColour;
        $eleOpacity = (isset($histo['opacity'])) ? $histo['opacity'] : $opacity;
        $elePosition = (isset($histo['position'])) ? $histo['position'] : $position;
        $elePosition = ($elePosition - $ali_start) * 10;
        $eleTooltip = $histo['hover'];
        $tool = $histo['start'] + 1;
        $heightDiv = ($histo['value'] >= 0) ? $histo['value'] * 100 : $histo['value'] * -100;
        $topBottom = ($histo['value'] >= 0) ? 'bottom' : 'top';
        $style = "height: {$heightDiv}%; {$topBottom}:{$topBottomOffset}%; left: {$elePosition}px; background-color: {$eleColour}; opacity: {$eleOpacity}";
        $class = ($histo['value'] <= 0.4) ? 'hCL' : 'hC';
        $histoDiv = "<div class='{$class}' style='{$style}' data-pos='{$tool}:{$tool}' data-tooltip='{$eleTooltip}'></div>";
        $content[] = $histoDiv;
    }
    $content[] = "<div class='hC zeroLine' style='{$positionZero}:{$topBottomOffset}%; width: {$width}'></div>";
    return implode(' ', $content);
}

function fileReader($url) {
    $contents = file_get_contents($url);
    return $contents;
}

function xml2array($contents, $get_attributes = 1, $priority = 'attr') {

    if (!function_exists('xml_parser_create')) {
        return array();
    }
    $parser = xml_parser_create('');

    xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8");
    xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
    xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
    xml_parse_into_struct($parser, trim($contents), $xml_values);
    xml_parser_free($parser);

    if (!$xml_values)
        return; //Hmm...

    $xml_array = array();
    $parents = array();
    $opened_tags = array();
    $arr = array();
    $current = &$xml_array;
    $repeated_tag_index = array();

    foreach ($xml_values as $data) {

        unset($attributes, $value);
        extract($data);
        $result = array();
        $attributes_data = array();

        if (isset($value)) {
            if ($priority == 'tag')
                $result = $value; else
                $result['value'] = $value;
        }
        if (isset($attributes) and $get_attributes) {
            foreach ($attributes as $attr => $val) {
                if ($priority == 'tag')
                    $attributes_data[$attr] = $val; else
                    $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
            }
        }
        if ($type == "open") {
            $parent[$level - 1] = &$current;
            if (!is_array($current) or (!in_array($tag, array_keys($current)))) {
                $current[$tag] = $result;
                if ($attributes_data)
                    $current[$tag . '_attr'] = $attributes_data;
                $repeated_tag_index[$tag . '_' . $level] = 1;
                $current = &$current[$tag];
            } else {
                if (isset($current[$tag][0])) {
                    $current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
                    $repeated_tag_index[$tag . '_' . $level]++;
                } else {
                    $current[$tag] = array($current[$tag], $result);
                    $repeated_tag_index[$tag . '_' . $level] = 2;
                    if (isset($current[$tag . '_attr'])) {
                        $current[$tag]['0_attr'] = $current[$tag . '_attr'];
                        unset($current[$tag . '_attr']);
                    }
                }
                $last_item_index = $repeated_tag_index[$tag . '_' . $level] - 1;
                $current = &$current[$tag][$last_item_index];
            }
        } elseif ($type == "complete") {
            if (!isset($current[$tag])) {
                $current[$tag] = $result;
                $repeated_tag_index[$tag . '_' . $level] = 1;
                if ($priority == 'tag' and $attributes_data)
                    $current[$tag . '_attr'] = $attributes_data;
            } else {
                if (isset($current[$tag][0]) and is_array($current[$tag])) {
                    $current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
                    if ($priority == 'tag' and $get_attributes and $attributes_data) {
                        $current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
                    }
                    $repeated_tag_index[$tag . '_' . $level]++;
                } else {
                    $current[$tag] = array($current[$tag], $result);
                    $repeated_tag_index[$tag . '_' . $level] = 1;
                    if ($priority == 'tag' and $get_attributes) {
                        if (isset($current[$tag . '_attr'])) {
                            $current[$tag]['0_attr'] = $current[$tag . '_attr'];
                            unset($current[$tag . '_attr']);
                        }
                        if ($attributes_data) {
                            $current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
                        }
                    }
                    $repeated_tag_index[$tag . '_' . $level]++; //0 and 1 index is already taken
                }
            }
        } elseif ($type == 'close') {
            $current = &$parent[$level - 1];
        }
    }
    return ($xml_array);
}

function url_exists($url) {

    $file_headers = get_headers($url);
    if ($file_headers[0] == 'HTTP/1.0 404 Not Found') {
        return false;
    } else if ($file_headers[0] == 'HTTP/1.0 302 Found' && $file_headers[7] == 'HTTP/1.0 404 Not Found') {
        return false;
    } else {
        return true;
    }
}

function layering($array, $gap) {
    $container = [];
    $container[0] = [];
    usort($array, 'sortByStart');
    foreach ($array as $key => $item) {
        for ($i = 0; $i < count($container); $i++) {
            if (count($container[$i]) == 0 || (($container[$i][count($container[$i]) - 1]["end"] + $gap) < ($item["start"]))) {
                array_push($container[$i], $item);
                break;
            } else if ($i == (count($container) - 1)) {
                $tmp = [];
                array_push($tmp, $item);
                array_push($container, $tmp);
                break;
            }
        }
    }
    return $container;
}

function sortByStart($a, $b) {
    return $a['start'] - $b['start'];
}

function startsWith($haystack, $needle) {
    return $needle === "" || strpos($haystack, $needle) === 0;
}

function endsWith($haystack, $needle) {
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
}

function fileSaver($path, $contents) {
    $uniFile = fopen($path, "w");
    fwrite($uniFile, $contents);
    fclose($uniFile);
}

function masking($seqs) {
    $newSeqs = [];
    for ($i = 0; $i < count($seqs); $i++) {
        $newSeqs[$i] = [];
    }
    for ($i = 0; $i < count($seqs[0]); $i++) {
        if ($seqs[0][$i] != "-") {
            for ($j = 0; $j < count($seqs); $j++) {
                array_push($newSeqs[$j], $seqs[$j][$i]);
            }
        } else {
            //save hidden info
        }
    }
    return $newSeqs;
}

function array_key_index(&$arr, $key) {
    $i = 0;
    foreach ($arr as $k) {
        if ($k == $key) {
            return $i;
        }
        $i++;
    }
}

function loadColors($path) {
    $parFile = file_get_contents($path);
    $chunks = explode("@", $parFile);
    $consensus = [];
    $colours = [];

    foreach ($chunks as $chunk) {
        $bits = explode("\n", $chunk . trim(" "));
        $type = $bits[0] . trim(" ");

        if ($type == "consensus" || $type == "color") {

            for ($c = 1; $c < count($bits); ++$c) {
                $bit = $bits[$c] . trim(" ");

                if (count($bit) > 0) {
                    $bitsArr = explode(" = ", $bit);

                    if ($type == "consensus") {
                        if (count($bitsArr) > 1) {
                            $tmp = preg_split('/\s+/', $bitsArr[1] . trim(" "));
                            if (count($tmp) == 2) {
                                $proportion = str_replace("%", "", $tmp[0]) / 100;
                                $residues = explode(":", strtoupper($tmp[1]));
                                $consensus[$bitsArr[0] . trim(" ")] = array("residues" => $residues, "proportion" => $proportion);
                            }
                        }
                    }

                    if ($type == "color") {
                        $rule = "";
                        if (count($bitsArr) > 1) {
                            $option_1_exploded = explode(" if ", $bitsArr[1]);
                            $colour = str_replace(" ", "", $option_1_exploded[0]);
                            if (count($option_1_exploded) > 1) {
                                $rule = $option_1_exploded[1] . trim(" ");
                            }
                            $option_0_trimmed = $bitsArr[0] . trim(" ");
                            if (!in_array($option_0_trimmed, $colours)) {
                                $colours[strtoupper($option_0_trimmed)] = array();
                            }
                            $ruleBits = explode(":", $rule);
                            $colours[strtoupper($option_0_trimmed)] = array("colour" => $colour, "rules" => $ruleBits);
                        }
                    }
                }
            }
        }
    }
    return [$consensus, $colours];
}

function findAlignments($alignmentPath, $alignmentInfoPath, $taxonomy, $uniprotId, $preferedArchive, $geneTreePath, $geneTreeMode) {

    //add name to alingment links
    $dirs = array_filter(glob($alignmentPath . "*"), 'is_dir');
    $dbNames = [];
    $dbLink = [];
    for ($i = 0; $i < count($dirs); $i++) {
        $tmpName = substr($dirs[$i], strrpos($dirs[$i], "/") + 1);
        $dbLink[$i] = $tmpName;
        $tmpPath = $alignmentInfoPath . "/" . $tmpName . "/" . $tmpName . ".name";
        if (is_file($tmpPath)) {
            $dbNames[$i] = fileReader($tmpPath);
        } else {
            $dbNames[$i] = $dbLink[$i];
        }
        $dirs[$i] = $dirs[$i] . "/" . $taxonomy . "/ALN/" . $uniprotId . ".orthaln.fas";
    }
    if (file_exists($geneTreePath . $uniprotId . "_" . $geneTreeMode)) {
        array_unshift($dirs, $geneTreePath . $uniprotId . "_" . $geneTreeMode);
        array_unshift($dbNames, "GeneTree");
        array_unshift($dbLink, "GeneTree");
    }
    $preferedPath = $alignmentPath . $preferedArchive . "/" . $taxonomy . "/ALN/" . $uniprotId . ".orthaln.fas";
    $finalDirs = [];
    $finalNames = [];
    $finalLinks = [];
    if (file_exists($preferedPath)) {
        array_push($finalDirs, $preferedPath);
        $tmpPath = $alignmentInfoPath . "/" . $preferedArchive . "/" . $preferedArchive . ".name";
        if (is_file($tmpPath)) {
            $tmpAliName = fileReader($tmpPath);
        } else {
            $tmpAliName = $preferedArchive;
        }
        array_push($finalNames, $tmpAliName);
        array_push($finalLinks, $preferedArchive);
    }
    for ($i = 0; $i < count($dirs); $i++) {
        if (file_exists($dirs[$i]) && ($preferedPath != $dirs[$i])) {
            array_push($finalDirs, $dirs[$i]);
            array_push($finalNames, $dbNames[$i]);
            array_push($finalLinks, $dbLink[$i]);
        }
    }
    return [$finalDirs, $finalNames, $finalLinks];
}

function getTextFromNode($Node, $Text = "") {
    if ($Node->tagName == null) {
        return $Text . $Node->textContent;
    }
    $Node = $Node->firstChild;
    if ($Node != null) {
        $Text = getTextFromNode($Node, $Text);
    }
    while ($Node->nextSibling != null) {
        $Text = getTextFromNode($Node->nextSibling, $Text);
        $Node = $Node->nextSibling;
    }
    return $Text;
}

function getTextFromDocument($DOMDoc) {
    return getTextFromNode($DOMDoc->documentElement);
}

function trimToAliRange($testArr, $ali_start, $ali_end) {
    foreach ($testArr as $track_i => $track) {
//        print '<br>';
        if ($track["type"] == "feature" || $track["type"] == "peptide") {
            foreach ($track['data'] as $feature_i => $feature) {
                if (intval($feature['end']) < $ali_start || intval($feature['start']) > $ali_end) {
//                    var_dump($feature['text']);
//                    print '<br>';
                    unset($testArr[$track_i]['data'][$feature_i]);
                } else {
                    if (intval($feature['start']) < $ali_start) {
                        $testArr[$track_i]['data'][$feature_i]['start'] = strval($ali_start);
                    }
                    if (intval($feature['end']) > $ali_end) {
                        $testArr[$track_i]['data'][$feature_i]['end'] = strval($ali_end);
                    }
                    $testArr[$track_i]['data'][$feature_i]['length'] = intval($testArr[$track_i]['data'][$feature_i]['end']) - intval($testArr[$track_i]['data'][$feature_i]['start']) + 1;
                }

            }
        } elseif ($track["type"] == "histogram") {
            $tmpArr = array();
            foreach ($track['data'] as $histo_i => $histo) {
                if ((intval($histo['end']) >= $ali_start && intval($histo['end']) <= $ali_end - 1)) {
                    $tmpArr[$histo_i] = $histo;
                }
            }
            $testArr[$track_i]['data'] = $tmpArr;
        } else {
            unset($testArr[$track_i]);
        }
    }
    return $testArr;
}

?>
