<?php

//     
//     ProViz - protein visualisation tool
//     Copyright (C) 2016  Norman E. Davey, Peter Jehl, Jean Manguy
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//     Author contact: Norman E. Davey <normandavey@gmail.com>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Peter Jehl <peter.jehl@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Jean Manguy <jean.manguy@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//
  
    include "./util.php";
    include "./accSelector";

    $form_input = "";
    if (filter_has_var(INPUT_GET, "form_input")){
        $form_input = strip_tags(filter_input(INPUT_GET, "form_input", FILTER_SANITIZE_STRING));
    } else {
        die("form_input not found");
    }
    
    
    //check if valid uniprot_acc
    $content = fileReader("http://www.uniprot.org/uniprot/" . $form_input . ".xml");
    if($content != ""){
        header('Location: ./proviz.php?uniprot_acc=' . $form_input);
    } else {
        header('Location: ./accSelector.php?search_phrase=' . $form_input);
    }
?>
