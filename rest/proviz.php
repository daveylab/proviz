<?php

//
//     ProViz - protein visualisation tool
//     Copyright (C) 2016  Norman E. Davey, Peter Jehl, Jean Manguy
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//     Author contact: Norman E. Davey <normandavey@gmail.com>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Peter Jehl <peter.jehl@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Jean Manguy <jean.manguy@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//

header("Cache-Control: max-age=2592000"); // cache 30days
// includes
include dirname(__FILE__)."/options.php";
include dirname(__FILE__)."/treeNode.php";
include dirname(__FILE__)."/loader/ali_loader.php";
include dirname(__FILE__)."/loader/uniprot_loader.php";
include dirname(__FILE__)."/loader/elm_loader.php";
include dirname(__FILE__)."/loader/phosphoElm_loader.php";
include dirname(__FILE__)."/loader/pfam_loader.php";
include dirname(__FILE__)."/loader/iup_loader.php";
include dirname(__FILE__)."/loader/elm_regex_loader.php";
include dirname(__FILE__)."/loader/xmlTrackLoader.php";
include dirname(__FILE__)."/loader/csvTrackLoader.php";
include dirname(__FILE__)."/loader/geneTreeLoader.php";
include dirname(__FILE__)."/loader/slim_loader.php";
include dirname(__FILE__)."/loader/switch_loader.php";
include dirname(__FILE__)."/loader/mobiDB_loader.php";
include dirname(__FILE__)."/util.php";

$jsonArr = [];
$jsonArr["errors"] = [];
$errorArr = [];
error_reporting(0);
register_shutdown_function("error_handler");
set_error_handler("error_handler");

function error_handler($e, $errorStr, $errorFile, $errorLine)
{
    global $jsonArr;
    $errorCodes = array(1 => "ERROR", 2 => "WARNING", 8 => "NOTICE");
    $errorLine = $errorCodes[$e] . ": " . $errorFile . " in line " . $errorLine . " | " . $errorStr;
    array_push($jsonArr["errors"], $errorLine);
}


// Set global server options
$options = new options();
$options->set_options();

$tools_input = filter_input(INPUT_GET, "tools", FILTER_SANITIZE_STRING);

$hideArr = [];
$hideAln = filter_input(INPUT_GET, "hideAln", FILTER_SANITIZE_STRING);
if ($hideAln && $hideAln != "") {
    $hideArr = explode(",", urldecode(($hideAln)));
}

$showArr = [];

$showAln = filter_input(INPUT_GET, "showAln", FILTER_SANITIZE_STRING);
if ($showAln && $showAln != "") {
    $showArr = explode(",", urldecode(($showAln)));
}

$ali_start = 0;
$input_start = filter_input(INPUT_GET, "ali_start", FILTER_VALIDATE_INT);
if ($input_start && $input_start > 0) {
        $ali_start = $input_start - 1;
}

$focus_start = 0;
$input_focus_start = filter_input(INPUT_GET, "focus_start", FILTER_VALIDATE_INT);
if ($input_focus_start && $input_focus_start  > $ali_start) {
    $focus_start = $input_focus_start ;
}

$input_alignment = filter_input(INPUT_GET, "alignment", FILTER_SANITIZE_STRING);
if ($input_alignment && $input_alignment != "") {
    $alignment = strip_tags($input_alignment);
} else {
    $alignment = $options->get_prefered_archive();
}

$input_condensed = filter_input(INPUT_GET, "condensed", FILTER_VALIDATE_INT);
$input_gapped = filter_input(INPUT_GET, "gapped", FILTER_VALIDATE_INT);
$input_disable_features = filter_input(INPUT_GET, "disable_features", FILTER_VALIDATE_INT);
$input_search = filter_input(INPUT_GET, "regexp", FILTER_SANITIZE_STRING);
if ($input_gapped) {
    $input_disable_features = 1;
}
$input_enabled_features = filter_input(INPUT_GET, "enable", FILTER_SANITIZE_STRING);

// name => disabled keys
$arrayFeatures = array(
    'alignment' =>'alignment',
    'switches' => 'switch',
    'anchor' => 'anchor',
    'peptide' => 'peptide',
    'psiPred' => 'psiPred',
    'conservation' => 'conservation',
    'short sequence motif' => 'motif',
    'elm' => 'elm',
    'elm_regex' =>'elm_regex',
    'modified residue' =>'modification',
    'phospho.elm' => 'phospho',
    'mutagenesis site' => 'mutagenesis',
    'pfam' => 'pfam',
    'secondary structure' => 'structure',
    "PDB" => "PDB",
    'homology model' => 'homology',
    'splice variant' => 'splice_variant',
    'sequence variant' => 'SNP',
    'chain' => 'chain',
    'dna-binding region' =>'dna_binding',
    'region of interest' => 'region',
    'metal ion-binding site' => 'metal_binding',
    'active site' => 'activesite',
    'binding site' => 'bindingsite',
    'site' => 'site',
    'cross-link' => 'cross_link',
    'iupred' =>'iupred',
    'mobiDB' => 'mobiDB',
    "topological domain" => "topological_domain",
    "transmembrane region" => "transmembrane_region",
    "nucleotide phosphate-binding" => "nucleophospho"
);


//Collapse individual tracks
$collapseArr = array_fill_keys(array_values($arrayFeatures), 0);

$collapseArr["psiPred"] = 1;
$collapseArr["elm_regex"] = 1;

if ($input_disable_features) {
    $disableArr = array_fill_keys(array_values($arrayFeatures), 1);
    $disableArr['alignment'] = 0;
} else if ($input_enabled_features != ""){
    //Enable individual tracks
    $disableArr = array_fill_keys(array_values($arrayFeatures), 1);
    $enableInputArr = explode(",", urldecode($input_enabled_features));
    foreach ($enableInputArr as $key) {
        if (array_key_exists($key, $disableArr)) {
            $disableArr[$key] = 0;
        }
    }
} else {
    //Disable individual tracks
    $disableArr = array_fill_keys(array_values($arrayFeatures), 0);
    $input_disable = filter_input(INPUT_GET, "disable", FILTER_SANITIZE_STRING);
    if ($input_disable && $input_disable != "") {
        $disableInputArr = explode(",", urldecode($input_disable));
        foreach ($disableInputArr as $key) {
            if (array_key_exists($key, $disableArr)) {
                $disableArr[$key] = 1;
            }
        }
    }
}


$input_collapse = filter_input(INPUT_GET, "collapse", FILTER_SANITIZE_STRING);
if ($input_collapse && $input_collapse != "") {
    $collapseInputArr = explode(",", urldecode($input_collapse));
    foreach ($collapseInputArr as $key) {
        if (array_key_exists($key, $collapseArr)) {
            $collapseArr[$key] = 1;
        }
    }
}

// Set uniprot id
$input_uniprot_acc = filter_input(INPUT_GET, "uniprot_acc", FILTER_SANITIZE_STRING);
if ($input_uniprot_acc && $input_uniprot_acc != "") {
    $uniprot_acc = strip_tags($input_uniprot_acc);
}


//Get user alignment
$input_jobId = filter_input(INPUT_GET, "jobId", FILTER_SANITIZE_STRING);
if ($input_jobId && $input_jobId != "") {
    $job_id = strip_tags($input_jobId);
    $user_alignment = fileReader("./data/user_alignment/" . $job_id . ".aln");
    $aliLoader = new ali_loader($user_alignment, 0, 0, NULL, NULL, "\n");
    $mainSeq = str_replace("-", "", implode("", $aliLoader->getSeqs()[0]));
    $uniprot_acc = checkMD5($mainSeq);
}


if (!isset($uniprot_acc) && !isset($user_alignment)) {
    die("No Uniprot_acc or user defined alignment given");
}

//Check for json mode
$input_output = filter_input(INPUT_GET, "output", FILTER_SANITIZE_STRING);
if ($input_output && $input_output != "") {
    $jsonTag = strip_tags($input_output);
    if ($jsonTag === "json") {
        $jsonMode = TRUE;
    }
}


$geneTreeMode = "ortholog";
$input_genetree_mode = filter_input(INPUT_GET, "genetree_mode", FILTER_SANITIZE_STRING);
if ($input_genetree_mode && $input_genetree_mode != "") {
    $geneTreeMode = strip_tags($input_genetree_mode);
    if (!($geneTreeMode === "paralog" || $geneTreeMode === "all")) {
        $geneTreeMode = "ortholog";
    }
}


// Load data from external sources
$cachePath = $options->get_cache_path();

if (isset($uniprot_acc) and ($uniprot_acc !== "") and ($uniprot_acc !== null)) {
// UniProt
    try {
        if (!file_exists($cachePath . "uniprot")) {
            mkdir($cachePath . "uniprot");
        }
        $uniprotPath = $cachePath . "uniprot/" . $uniprot_acc . ".xml";
        if (!file_exists($uniprotPath) || (filesize($uniprotPath) == 0)) {
            $url = "http://www.uniprot.org/uniprot/" . $uniprot_acc . ".xml";
            if (get_headers($url)[0] !== "HTTP/1.1 404 Not Found") {
                $contents = fileReader($url);
                if($contents == ""){
                    array_push($jsonArr["errors"], "Uniprot id not found");
                    echo json_encode($jsonArr);
                    die();
                }
                $uniLoad = new uniprot_loader($contents, $uniprot_acc, $disableArr);
                $testArr = $uniLoad->getFeatures();
                $uniprot_acc = $uniLoad->getId();
                fileSaver($uniprotPath, $contents);
            } else {
                array_push($jsonArr["errors"], "Uniprot id not found");
                echo json_encode($jsonArr);
                die();
            }
        } else {
            $contents = fileReader($uniprotPath);
            $uniLoad = new uniprot_loader($contents, $uniprot_acc, $disableArr);
            $testArr = $uniLoad->getFeatures();
            $uniprot_acc = $uniLoad->getId();
        }
    } catch (Exception $e) {

    }
}

if ($uniLoad) {
    $ali_end = count($uniLoad->getSeq());
    $lengthSeq = $ali_end;
} else {
    $ali_end = strlen(str_replace("-", "", $mainSeq));
}


$input_ali_end = filter_input(INPUT_GET, "ali_end", FILTER_VALIDATE_INT);
if ($input_ali_end && ($input_ali_end > $ali_start) && ($input_ali_end < $ali_end)) {
    $ali_end = $input_ali_end;
}

$focus_end = $ali_end;
$input_focus_end = filter_input(INPUT_GET, "focus_end", FILTER_VALIDATE_INT);
if ($input_focus_end && ($input_focus_end > $input_focus_start) && ($input_focus_end < $ali_end)) {
    $focus_end = $input_focus_end;
}

$settingsArr = [];
$settingsArr["tools"] = $tools_input;
$settingsArr["disable"] = $disableArr;
$settingsArr["collapse"] = $collapseArr;
$settingsArr["uniprot_acc"] = $uniprot_acc;
if (isset($job_id)) {
    $settingsArr["jobId"] = $job_id;

}
$settingsArr["alignment"] = $alignment;
$settingsArr["geneTreeMode"] = $geneTreeMode;
$settingsArr["hideAln"] = $hideArr;
$settingsArr["showAln"] = $showArr;

$settingsArr["regexp"] = $input_search;
$settingsArr["condensed"] = ($input_condensed);
$settingsArr["gapped"] = ($input_gapped);
$settingsArr['lengthSeq'] = $lengthSeq;
$settingsArr["ali_start"] = $ali_start;
$settingsArr["ali_end"] = $ali_end;
if ($focus_start != $ali_start || $focus_end != $ali_end) {
    $settingsArr["focus_start"] = $focus_start;
    $settingsArr["focus_end"] = $focus_end;
}

if (isset($uniprot_acc) and ($uniprot_acc !== "") and ($uniprot_acc !== null)) {
    //Load genetree
    try {
        if ($disableArr['alignment'] == 0) {
            if (!file_exists($cachePath . "geneTreeRaw")) {
                mkdir($cachePath . "geneTreeRaw");
            }
            $geneTreePath = $cachePath . "geneTreeRaw/" . $uniLoad->getGeneTreeId() . ".csv";
            if ((!file_exists($geneTreePath)) || (filesize($geneTreePath) == 0)) {
                $url = 'http://rest.ensembl.org/genetree/id/' . $uniLoad->getGeneTreeId() . '?content-type=application/json;aligned=1';
                if ((get_headers($url)[0] !== "HTTP/1.0 500 Internal Server Error") && (get_headers($url)[0] !== "HTTP/1.0 404 Not Found") && (get_headers($url)[0] !== "HTTP/1.0 400 Bad Request")) {
                    $contents = fileReader('http://rest.ensembl.org/genetree/id/' . $uniLoad->getGeneTreeId() . '?content-type=application/json;aligned=1');
                    fileSaver($geneTreePath, $contents);
                    $geneTreeLoad = new geneTreeLoader($contents, $uniLoad->getTaxonomy(), $uniLoad->getSpecies(), $uniLoad->getSeq(), $geneTreeMode);
                    $taxons = $geneTreeLoad->getTaxons();
                    $ensembleIds = $geneTreeLoad->getEnsembleIds();
                    $seqType = $geneTreeLoad->getTypes();
                    $dups = $geneTreeLoad->getDups();
                } else {
                    $geneTreeLoad = new geneTreeLoader(null, null, null, null, null);
                    $seqType = $geneTreeLoad->getTypes();
                    $dups = $geneTreeLoad->getDups();
                    $taxons = $geneTreeLoad->getTaxons();
                    $ensembleIds = $geneTreeLoad->getEnsembleIds();
                }
            } else {
                $contents = fileReader($geneTreePath);
                $geneTreeLoad = new geneTreeLoader($contents, $uniLoad->getTaxonomy(), $uniLoad->getSpecies(), $uniLoad->getSeq(), $geneTreeMode);
                $seqType = $geneTreeLoad->getTypes();
                $dups = $geneTreeLoad->getDups();
                $taxons = $geneTreeLoad->getTaxons();
                $ensembleIds = $geneTreeLoad->getEnsembleIds();
            }
            if ($geneTreeLoad->getCheck()) {
                if (!file_exists($options->getGeneTreePath())) {
                    mkdir($options->getGeneTreePath());
                }
                if ((!file_exists($options->getGeneTreePath() . $uniLoad->getId() . "_" . $geneTreeMode)) || (filesize($options->getGeneTreePath() . $uniLoad->getId() . "_" . $geneTreeMode) === 0)) {
                    $geneTreeLoad->writeAlignment($options->getGeneTreePath() . $uniLoad->getId() . "_" . $geneTreeMode);
                }
            }
        } else {
            $geneTreeLoad = new geneTreeLoader(null, null, null, null, null);
            $seqType = $geneTreeLoad->getTypes();
            $dups = $geneTreeLoad->getDups();
            $taxons = $geneTreeLoad->getTaxons();
            $ensembleIds = $geneTreeLoad->getEnsembleIds();
        }
    } catch (Exception $e) {
    }

    //Load alignment
    try {
        if ($disableArr['alignment'] === 0) {
            $preferedArchive = $options->get_prefered_archive();
            if ($alignment != "") {
                $preferedArchive = $alignment;
            }
            $alignmentPath = $options->get_alignment_path();
            $alignmentInfoPath = $options->get_alignment_info();
            $aliList = findAlignments($alignmentPath, $alignmentInfoPath, $uniLoad->getTaxonomy(), $uniprot_acc, $preferedArchive, $options->getGeneTreePath(), $geneTreeMode);
            $aliPath = $aliList[0];
            $aliName = $aliList[1];
            $aliLink = $aliList[2];
        } else {
            $aliPath = [];
            $aliName = [];
            $aliLink = [];
        }

        if (count($aliPath) > 0) {
            if ($aliName[0] == "GeneTree") {
                $aliPath[0] = explode("_", $aliPath[0])[0] . "_" . $geneTreeMode;
            }
            $contents = fileReader($aliPath[0]);
            $aliLoad = new ali_loader($contents, $ali_start, $ali_end, $ensembleIds, $settingsArr, "\n");
            if(implode("",$uniLoad->getSeq()) == str_replace("-", "" , implode("",$aliLoad->getSeqs()[0]))){
                $seqs = $aliLoad->getSeqs();
                $names = $aliLoad->getNames();
                $aliColor = $aliLoad->getColor();
                $aliMask = $aliLoad->getMasked();
                $ids = $aliLoad->getIds();
                $aliTool = $aliLoad->getTool();
                $aliGeneName = $aliLoad->getGeneName();
                $aliProteinName = $aliLoad->getProteinName();
                $aliQFO = $aliLoad->getQFO();
            } else {
                $seqs[0] = $uniLoad->getSeq();
                $names[0] = $uniLoad->getSpecies();
                $ids[0] = $uniLoad->getId();
                $aliColor = $uniLoad->getColor();
                $aliTool[0] = $uniLoad->getTool();
                $aliGeneName[0] = $uniLoad->getGeneName();
                $aliProteinName[0] = $uniLoad->getUniprotName();
                $aliQFO[0] = 1;
                unset($aliLoad);
            }
        } else {
            $seqs[0] = $uniLoad->getSeq();
            $names[0] = $uniLoad->getSpecies();
            $ids[0] = $uniLoad->getId();
            $aliColor = $uniLoad->getColor();
            $aliTool[0] = $uniLoad->getTool();
            $aliGeneName[0] = $uniLoad->getGeneName();
            $aliProteinName[0] = $uniLoad->getUniprotName();
            $aliQFO[0] = 1;
        }
    } catch (Exception $e) {
    }
}


if (isset($user_alignment) and $user_alignment !== null) {
    $aliLoad = new ali_loader($user_alignment, $ali_start, $ali_end, NULL, $settingsArr, "\n");
    $seqs = $aliLoad->getSeqs();
    $names = $aliLoad->getNames();
    $aliColor = $aliLoad->getColor();
    $aliMask = $aliLoad->getMasked();
    $ids = $aliLoad->getIds();
    $aliTool = $aliLoad->getTool();
    $aliGeneName = $aliLoad->getGeneName();
    $aliProteinName = $aliLoad->getProteinName();
    $aliQFO = $aliLoad->getQFO();

    if (!empty($aliName)) {
        array_unshift($aliPath, "");
        array_unshift($aliName, "Custom");
        array_unshift($aliLink, "");
    }

}


//Load custom protein track data
if (isset($uniprot_acc) and ($uniprot_acc !== "") and ($uniprot_acc !== null)) {
    try {
        if (file_exists($options->get_protein_track_path() . $uniprot_acc)) {
            $dirs = array_filter(glob($options->get_protein_track_path() . $uniprot_acc . "/*"), 'is_file');
            for ($i = 0; $i < count($dirs); $i++) {
                // CSV and XML parsers
                if (endsWith($dirs[$i], ".xml")) {
                    $contents = fileReader($dirs[$i]);
                    $xmlTrack = new xmlTrackLoader($contents);
                    $tmpXmlTracks = $xmlTrack->getTracks();
                    for ($j = 0; $j < count($tmpXmlTracks); $j++) {
                        $xmlArr[$tmpXmlTracks[$j]["name"]] = $tmpXmlTracks[$j];
                    }
                } else if (endsWith($dirs[$i], ".csv")) {
                    $contents = fileReader($dirs[$i]);
                    $csvTrack = new csvTrackLoader($contents);
                    $tmpCsvTracks = $csvTrack->getTracks();
                    for ($j = 0; $j < count($tmpCsvTracks); $j++) {
                        $xmlArr[$tmpCsvTracks[$j]["name"]] = $tmpCsvTracks[$j];
                    }
                }
            }
        }
    } catch (Exception $e) {
    }
    

//Slim Rest Conservation, PsiPred and Anchor
    if ($disableArr['conservation'] == 0 || $disableArr['psiPred'] == 0 || $disableArr['anchor'] == 0) {
        try {
            if (isset($aliName[0])) {
                $slimName = $aliName[0];
                if (!file_exists($cachePath . "slim")) {
                    mkdir($cachePath . "slim");
                }
                $slimPath = $cachePath . "slim/" . $uniprot_acc . ".json";
                if (!file_exists($slimPath) || (filesize($slimPath) == 0)) {
                    $url = "http://slim.ucd.ie/rest/attributes/?uniprot_acc=" . $uniprot_acc . "&type=Conservation&type=Anchor&type=PsiPred";
                    if (get_headers($url)[0] !== "HTTP/1.1 400 Bad Request") {
                        $contents = fileReader($url);
                        $slimLoad = new slim_loader($contents, $slimName);
                        if ($slimLoad->getCon() !== null && $disableArr['conservation'] == 0) {
                            $testArr["conservation"] = $slimLoad->getCon();
                        }
                        if ($slimLoad->getPsiPred() !== null && $disableArr['psiPred'] == 0) {
                            $testArr["psiPred"] = $slimLoad->getPsiPred();
                        }
                        if ($slimLoad->getAnchor() !== null && $disableArr['anchor'] == 0) {
                            $testArr["anchor"] = $slimLoad->getAnchor();
                        }
                        fileSaver($slimPath, $contents);
                    }
                } else {
                    $contents = fileReader($slimPath);
                    $slimLoad = new slim_loader($contents, $slimName);
                    if ($slimLoad->getCon() !== null && $disableArr['conservation'] == 0) {
                        $testArr["conservation"] = $slimLoad->getCon();
                    }
                    if ($slimLoad->getPsiPred() !== null && $disableArr['psiPred'] == 0) {
                        $testArr["psiPred"] = $slimLoad->getPsiPred();
                    }
                    if ($slimLoad->getAnchor() !== null && $disableArr['anchor'] == 0) {
                        $testArr["anchor"] = $slimLoad->getAnchor();
                    }
                }
            }
        } catch (Exception $e) {
        }
    }


//Slim Rest Switches
    if ($disableArr['switch'] == 0) {
        try {
            if (!file_exists($cachePath . "switches")) {
                mkdir($cachePath . "switches");
            }
            $switchPath = $cachePath . "switches/" . $uniprot_acc . ".json";
            if (!file_exists($switchPath) || (filesize($switchPath) == 0)) {
                $url = "http://slim.ucd.ie/rest/features/?uniprot_acc=" . $uniprot_acc . "&source_id=switchesELM";
                if (get_headers($url)[0] !== "HTTP/1.1 400 Bad Request") {
                    $contents = fileReader($url);
                    $switchLoad = new switch_loader($contents);
                    if ($switchLoad->getSwitch() !== null) {
                        $testArr["switches"] = $switchLoad->getSwitch();
                    }
                    fileSaver($switchPath, $contents);
                }
            } else {
                $contents = fileReader($switchPath);
                $switchLoad = new switch_loader($contents);
                if ($switchLoad->getSwitch() !== null) {
                    $testArr["switches"] = $switchLoad->getSwitch();
                }
            }
        } catch (Exception $e) {
        }
    }


//ELM
    if ($disableArr['elm'] == 0) {
        try {
            if (!file_exists($cachePath . "elm")) {
                mkdir($cachePath . "elm");
            }
            $elmPath = $cachePath . "elm/" . $uniprot_acc . ".gff";
            if (!file_exists($elmPath) || (filesize($elmPath) == 0)) {
                $contents = fileReader("http://elm.eu.org/instances.gff?q=" . $uniprot_acc);
                $elmLoad = new elm_loader($contents, $uniprot_acc);
                if ($elmLoad->getElms() !== null) {
                    $testArr["elm"] = $elmLoad->getElms();
                }
                fileSaver($elmPath, $contents);
            } else {
                $contents = fileReader($elmPath);
                $elmLoad = new elm_loader($contents, $uniprot_acc);
                if ($elmLoad->getElms() !== null) {
                    $testArr["elm"] = $elmLoad->getElms();
                }
            }
        } catch (Exception $e) {
        }

    }

//Phospho ELM
    if ($disableArr['phospho'] == 0) {
        try {
            if (!file_exists($cachePath . "phospho")) {
                mkdir($cachePath . "phospho");
            }
            $phosphoPath = $cachePath . "phospho/" . $uniprot_acc . ".csv";
            if (!file_exists($phosphoPath) || (filesize($phosphoPath) == 0)) {
                $contents = fileReader('http://phospho.elm.eu.org/byAccession/' . $uniprot_acc . '.csv');
                $phosphoElmLoad = new phosphoElm_loader($contents);
                if ($phosphoElmLoad->getPhosphos() !== null) {
                    $testArr["phospho.elm"] = $phosphoElmLoad->getPhosphos();
                }
                fileSaver($phosphoPath, $contents);
            } else {
                $contents = fileReader($phosphoPath);
                $phosphoElmLoad = new phosphoElm_loader($contents);
                if ($phosphoElmLoad->getPhosphos() !== null) {
                    $testArr["phospho.elm"] = $phosphoElmLoad->getPhosphos();
                }
            }
        } catch (Exception $e) {
        }
    }

//PFAM
    if ($disableArr['pfam'] == 0) {
        try {
            if (!file_exists($cachePath . "pfam")) {
                mkdir($cachePath . "pfam");
            }
            $pfamPath = $cachePath . "pfam/" . $uniprot_acc . ".xml";
            if (!file_exists($pfamPath) || (filesize($pfamPath) == 0)) {
                $contents = fileReader('http://pfam.xfam.org/protein/' . $uniprot_acc . '?output=xml');
                $pfamLoad = new pfam_loader($contents);
                $testArr["pfam"] = $pfamLoad->getPfams();
                fileSaver($pfamPath, $contents);
            } else {
                $contents = fileReader($pfamPath);
                $pfamLoad = new pfam_loader($contents);
                $testArr["pfam"] = $pfamLoad->getPfams();
            }
        } catch (Exception $e) {
        }
    }
    if ($disableArr['mobiDB'] == 0) {
        try {
            if (!file_exists($cachePath . "mobiDB")) {
                mkdir($cachePath . "mobiDB");
            }
            $mobiDBPath = $cachePath . "mobiDB/" . $uniprot_acc . ".json";
            if (!file_exists($mobiDBPath) || (filesize($mobiDBPath) == 0)) {
                $contents = fileReader('http://mobidb.bio.unipd.it/ws/entries/' . $uniprot_acc . '/disorder');
                $mobiDBLoad = new mobiDB_loader($contents, $uniprot_acc);
                $testArr["mobiDB"] = $mobiDBLoad->getMobi();
                fileSaver($mobiDBPath, $contents);
            } else {
                $contents = fileReader($mobiDBPath);
                $mobiDBLoad = new mobiDB_loader($contents, $uniprot_acc);
                $testArr["mobiDB"] = $mobiDBLoad->getMobi();
            }
        } catch (Exception $e) {
        }
    }
}

// Calculate IUPred disorder
if ($disableArr['iupred'] == 0) {
    try {
        if (!file_exists($cachePath . "sequence")) {
            mkdir($cachePath . "sequence");
        }
        if (isset($uniprot_acc) and ($uniprot_acc !== "") and ($uniprot_acc !== null)) {
            $seqPath = $cachePath . "sequence/" . $uniprot_acc . ".fasta";
            if (!file_exists($seqPath) || (filesize($seqPath) == 0)) {
                fileSaver($seqPath, ">" . $uniLoad->getUniprotName() . "\n" . implode($uniLoad->getSeq()));
            }
        } else {
            $seqPath = $cachePath . "sequence/" . $job_id . ".fasta";
            if (!file_exists($seqPath) || (filesize($seqPath) == 0)) {
                fileSaver($seqPath, ">" . $job_id . "\n" . $mainSeq);
            }
        }
        if (!file_exists($cachePath . "iupred")) {
            mkdir($cachePath . "iupred");
        }
        $iupredPath = $cachePath . "iupred/" . $uniprot_acc . ".hist";
        if (!file_exists($iupredPath) || (filesize($iupredPath) == 0)) {
            $contents = shell_exec("cd " . $options->get_iupred_path() . "; " . "./iupred" . " ../" . $seqPath . " long");
            $iupLoad = new iup_loader($contents);
            $testArr["iupred"] = $iupLoad->getIUPHist();
            fileSaver($iupredPath, $contents);
        } else {
            $contents = fileReader($iupredPath);
            $iupLoad = new iup_loader($contents);
            $testArr["iupred"] = $iupLoad->getIUPHist();
        }
    } catch (Exception $e) {
    }
}

//Calculate elm_regex hits
if ($disableArr['elm_regex'] == 0) {
    $filePath = $cachePath . "elm_classes/elm_classes.tsv";
    $contents = fileReader($filePath);
    if (isset($uniprot_acc) and ($uniprot_acc !== "") and ($uniprot_acc !== null)) {
        $elm_regexLoad = new elm_regex_loader($contents, implode($uniLoad->getSeq()));
    } else {
        $elm_regexLoad = new elm_regex_loader($contents, $mainSeq);
    }
    $testArr["elm_regex"] = $elm_regexLoad->getRegex();
}

// Add xml derived data
if (isset($xmlArr)) {
    $newXmlArr = [];
    foreach ($xmlArr as $key => $item) {
        $newXmlArr[$key]["type"] = $xmlArr[$key]["type"];
        foreach ($xmlArr[$key] as $key1 => $item1) {
            $newXmlArr[$key][$key1] = $item1;
        }
    }
    if ($ali_start != 0 || $ali_end != count($uniLoad->getSeq())) {
        $newXmlArr = trimToAliRange($newXmlArr, $ali_start, $ali_end);
    }
    foreach ($newXmlArr as $key => $item) {
        if ($newXmlArr[$key]["type"] == "feature") {
            $newXmlArr[$key]["data"] = layering($item["data"]);
        } else {
            $newXmlArr[$key]["data"] = $item["data"];
        }
    }
}

//Triming to ali_start and ali_end
$trimArr = trimToAliRange($testArr, $ali_start, $ali_end);


// Delete empty tracks
foreach ($trimArr as $key => $item) {
    if (count($item["data"]) === 0) {
        unset($trimArr[$key]);
    }
}


$gap0Arr = ["secondary structure", "sequence variant", "psiPred"];
foreach ($trimArr as $key => $item) {
    if (in_array($key, $gap0Arr)) {
        $trimArr[$key]["gap"] = 0;
    } else {
        $trimArr[$key]["gap"] = 1;
    }
}

//Layering
$newArr = [];
foreach ($trimArr as $key => $item) {
    $newArr[$key]["type"] = $trimArr[$key]["type"];
    if ($trimArr[$key]["type"] == "feature" || $trimArr[$key]["type"] == 'peptide') {
        $newArr[$key]["data"] = layering($item["data"], $item["gap"]);
    } else {
        $newArr[$key]["data"] = $item["data"];
    }
}


$hoverArr = array("anchor" => "Predicted binding regions located in disordered regions of the query protein",
    "conservation" => 'Relative conservation probability of the residue compared to its surrounding residues. For details see - "SLiMPrints: conservation-based discovery of functional motif fingerprints in intrinsically disordered protein regions"',
    "peptide" => "Uniprot annotated peptides of the query protein",
    "psiPred" => "Predicted secondary structure of the query protein by PsiPred",
    "short sequence motif" => "Short linear motifs of the query protein as annotated by UniProt",
    "elm" => "Short linear motifs of the query protein as annotated by ELM",
    "elm_regex" => "Short linear motifs regular expression hit on the query sequence",
    "switches" => "Curated experimentally validated motif-based melocular switches for the query protein",
    "modified residue" => "Residues of the query protein that have been post-translationally modified (information from UniProt)",
    "phospho.elm" => "Residues of the query protein that have been post-translationally modified retrived from the phospho.ELM database",
    "mutagenesis site" => "Mutated residues of the query protein which alter function",
    "PDB" => "Regions of the query protein that have been structurally solved by NMR or X-ray crystallography. Click for more details",
    "chain" => "Chain data for the query protein retrieved from the UniProt database",
    "homology model" => "Structural model for the query protein based on homologous proteins",
    "pfam" => "Regions of the query protein defined by Pfam as a domain or family descriptor",
    "dna-binding region" => "DNA binding regions of the query protein",
    "region of interest" => "Regions with experimental evidence for function in the query protein",
    "metal ion-binding site" => "Metal contacting residues of the query protein",
    "site" => "Sites of the query protein with experimental evidence for function",
    "cross-link" => "Residues that have cross-linked modifications in the query protein",
    "splice variant" => "Regions of the query protein in non-constitutive exons",
    "sequence variant" => "Single nucleotide polymorphisms in the query protein",
    "secondary structure" => "Regions of the query protein that have been shown to form secondary structure by NMR or X-ray crystallography",
    "transmembrane region" => "Helical regions of the query protein crossing the cell membrane",
    "topological domain" => "Subcellular location of membrane-spanning query proteins",
    "mobiDB" => "Consensus of disorder predictions for the query protein from MobiDB including DisEMBL-465, DisEMBL-HL, ESpritz-DisProt, ESpritz-NMR, ESpritz-XRay, GlobPlot, IUPred-long, IUPred-short, JRONN and VSL2b",
    "iupred" => "IUPred intrinsic disorder score for the query sequence. High regions less likely to be globular",
    "active site" => "Residues with catalytic function in the query protein",
    "binding site" => "Single amino acids of the query protein binding to another chemical entity",
    "nucleotide phosphate-binding" => "Regions of the query protein known to bind nucleotides"
);





//Load all track data
$featureId = 0;
$htmlArr = [];

foreach ($newArr as $key => $item) {
//    echo $key.'/n';
    $tmp = "";
    if (array_key_exists($key, $hoverArr)) {
        $tmp = $hoverArr[$key];
    }
    $disableId = (isset($arrayFeatures[$key])) ? $arrayFeatures[$key] : '';
    $toCollapse = (array_key_exists($key, $arrayFeatures) && array_key_exists($arrayFeatures[$key], $collapseArr) && $collapseArr[$arrayFeatures[$key]] == 1);
    $htmlArr[$key] = createTrack($key, $newArr[$key], $ali_start, $ali_end, $tmp, $featureId, $disableId, $toCollapse);
    $featureId++;
}


if (isset($newXmlArr)) {
    $xmlp = [];
    $xmln = [];
    foreach ($newXmlArr as $key => $item) {
        if ($item["position"] === "-1") {
            $xmln[$key] = createTrack($key, $newXmlArr[$key], $ali_start, $ali_end, "", $featureId, '');
            $featureId++;
        }
        if ($item["position"] === "1") {
            $xmlp[$key] = createTrack($key, $newXmlArr[$key], $ali_start, $ali_end, "", $featureId, '');
            $featureId++;
        }
    }
}


$aliWindowCount = 0;
$aliWindowStart = -1;
$aliWindowEnd = -1;
for ($j = 0; $j < count($seqs[0]); $j++) {
    if (($aliWindowCount === $ali_start) && ($aliWindowStart === -1)) {
        $aliWindowStart = $j;
    }
    if ($seqs[0][$j] !== "-") {
        $aliWindowCount++;
    }
    if ($aliWindowCount === $ali_end) {
        $aliWindowEnd = $j + 1;
        break;
    }
}
if ($aliWindowStart === -1) {
    $aliWindowStart = 0;
}
if ($aliWindowEnd === -1) {
    $aliWindowEnd = count($seqs[0]);
}

if (isset($jsonMode) and $jsonMode) {
    if (isset($aliLoad)) {
        if (isset($geneTreeLoad)) {
            $jsonArr["debug"] = $ids;
            $jsonArr["alignmentInfoQuery"] = createAlignmentInfo($seqs, $aliName, $options, $seqType, $aliTool, $aliGeneName, $aliProteinName, $aliQFO, $names, 0, 1, $settingsArr, $taxons, $ids, $ensembleIds);
            $jsonArr["alignmentInfo"] = createAlignmentInfo($seqs, $aliName, $options, $seqType, $aliTool, $aliGeneName, $aliProteinName, $aliQFO, $names, 1, count($seqs), $settingsArr, $taxons, $ids, $ensembleIds);
        } else {
            $jsonArr["alignmentInfoQuery"] = createAlignmentInfo($seqs, $aliName, $options, $seqType, $aliTool, $aliGeneName, $aliProteinName, $aliQFO, $names, 0, 1, $settingsArr, $taxons, $ids, $ensembleIds);
            $jsonArr["alignmentInfo"] = createAlignmentInfo($seqs, $aliName, $options, $seqType, $aliTool, $aliGeneName, $aliProteinName, $aliQFO, $names, 1, count($seqs), $settingsArr, $taxons, $ids, $ensembleIds);
        }
        $jsonArr["alignmentDataQuery"] = createAlignmentData($seqs, $aliWindowStart, $aliWindowEnd, $aliLoad, $aliColor, 0, 1, $settingsArr, $ids, $ensembleIds);
        $jsonArr["alignmentData"] = createAlignmentData($seqs, $aliWindowStart, $aliWindowEnd, $aliLoad, $aliColor, 1, count($seqs), $settingsArr, $ids, $ensembleIds);
    } else {
        $jsonArr["alignmentInfoQuery"] = createAlignmentInfo($seqs, $aliName, $options, $seqType, $aliTool, $aliGeneName, $aliProteinName, $aliQFO,  $names, 0, 1, $settingsArr, $taxons, $ids, $ensembleIds);
        $jsonArr["alignmentDataQuery"] = createAlignmentData($seqs, $aliWindowStart, $aliWindowEnd, NULL, $aliColor, 0, 1, $settingsArr, $ids, $ensembleIds);
    }
    $jsonArr["offsetData"] = createOffsetData($ali_start, $ali_end);
    $jsonArr["offsetInfo"] = createOffsetInfo();
    $jsonArr["ali_start"] = $ali_start;
    $jsonArr["ali_end"] = $ali_end;
    $jsonArr["available_alignments"] = $aliName;
    $jsonArr["available_alignments_id"] = $aliLink;
    $jsonArr["settings"] = $settingsArr;
    $jsonArr["tracks"] = $htmlArr;
    if (isset($uniprot_acc) and ($uniprot_acc !== "") and ($uniprot_acc !== null)) {
        $jsonArr["protein_acc"] = $uniprot_acc;
        $jsonArr["uniprot_sequence"] = $uniLoad->getSeq();
        $jsonArr["architecture"] = printArchitecture($testArr, $uniLoad);
        $jsonArr["protein_length"] = $ali_end - $ali_start;
        $jsonArr["species"] = $uniLoad->getSpecies();
        $jsonArr["protein_name"] = $uniLoad->getUniprotName();
        $jsonArr["gene_name"] = $uniLoad->getGeneName();
        $jsonArr["meta"] = $uniLoad->getMeta();
        $jsonArr["function"] = $uniLoad->getFunctionProtein();
    } else {
        $jsonArr["protein_length"] = strlen(str_replace("-", "", implode("", $aliLoad->getSeqs()[0])));
    }
    header('Content-type: application/json');
    echo json_encode($jsonArr);
}
?>