<p style="font-size:16px">Quick introduction</p>
<p id="input_title" class="headline">Input options <i class="fa fa-angle-double-down" style="display:none"></i><i class="fa fa-angle-double-up"></i></p>
<p id="input_text" class="text">The "Search" field is linked to the UniProt protein search engine. 
Use the protein name (e.g SRC_HUMAN) or other available information about the protein. 
After the search results are retrieved, select the protein of interest from the result list.</p>
<p id="input_text" class="text">The "Alignment input" and "Alignment file input" fields use a user prepared alignment as plain text or file respectively. 
Please provide the data in fasta format. This option will check the first sequence in the alignment against the UniProt 
database to retrieve available data about the protein of interest.</p>
<p id="vis_title" class="headline">Main visualisation <i class="fa fa-angle-double-down" style="display:none"></i><i class="fa fa-angle-double-up"></i></p>
<p id="vis_text" class="text">This is the central panel and shows the graphical representation of the protein, the alignment and 
the information tracks of retrieved data. The column on the left shows the species in the alignment or 
the origin of the data shown on the right. The visualisation on the right hand side 
shows the proteins amino acids from left to right with the known information aligned with their position 
in the protein.</p>
<p id="menu_title" class="headline">Top menu <i class="fa fa-angle-double-down" style="display:none"></i><i class="fa fa-angle-double-up"></i></p>
<p id="menu_text" class="text">On the top left the name of the protein and gene together with the species is displayed. On the right hand side the user can 
choose between different alignments. The buttons right of the alignment selector activate panel for additional information 
like uncollapsing the alignment, searching for residues by regular expression, showing the compact view, highlighting areas of interest, 
protein architecture overview and recoloring of the alignment. There are reset and home buttons at the end.</p>
<p id="left_title" class="headline">Left panel <i class="fa fa-angle-double-down" style="display:none"></i><i class="fa fa-angle-double-up"></i></p>
<p id="left_text" class="text">The panel on the left contains the download options, hidden proteins, the feature tab to show and hide tracks, this help and an about section</p>
<p id="database_title" class="headline">Used databases and programs <i class="fa fa-angle-double-down" style="display:none"></i><i class="fa fa-angle-double-up"></i></p>
<p id="database_text" class="text">ProViz utilises many databases to give the user a wide range of information about the protein of interest.
Among these are UniProt, Ensembl, ELM, Pfam, phospho.ELM and switches.ELM. It also uses programs like IUPred, PsiPred
and Anchor.</p>
<p id="option_title" class="headline">Url options<i class="fa fa-angle-double-down" style="display:none"></i><i class="fa fa-angle-double-up"></i></p>
<p id="option_text" class="text">
<b>uniprot_acc:</b> UniProt accession of the protein to be visualised<br>
<b>jobId:</b> Access custom alignment by id<br>
<b>alignment:</b> Type of homology alignment to be displayed<br>
<b>ali_start:</b> Starting residue for the alignment<br>
<b>ali_end:</b> Ending residue for the alignment<br>
<b>disable:</b> motif, elm, modification, phospho, mutagenesis, pfam, structure, PDB, homology, splice_variant, SNP, chain, dna_binding, region, metal_binding, site, cross_link, iupred
<b>collapse:</b> alignment, switch, motif, modification, mutation, structure, PDB, isoform, snp, feature, disorder<br>
<b>hideAln:</b> Hide proteins by id<br>
<b>showAln:</b> Show proteins by id<br>
<b>genetree_mode:</b> If alignment is set to GeneTree select paralog, ortholog or all<br>
<b>output:</b> json to return data in json format<br>
</p>
<p id="custom_title" class="headline">Use of custom data <i class="fa fa-angle-double-down" style="display:none"></i><i class="fa fa-angle-double-up"></i></p>
<p id="custom_text" class="text">Custom tracks can be provided for visualisation by drag and drop or REST-service by url. The available formats are xml, csv and json and the file 
has to end with these extensions to be recognized. Example files can be downloaded here.
<a href="./help/custom_track/track.xml" class="btn-proviz" download="track.xml">XML</a>
<a href="./help/custom_track/track.csv" class="btn-proviz" download="track.csv">CSV</a>
<a href="./help/custom_track/track.json" class="btn-proviz" download="track.json">JSON</a></p>