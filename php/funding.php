
	<div class="services-page main grid-wrap">

		<aside class="grid col-one-quarter mq2-col-full">
			<h4>Our funding</h4>
			
			<p class="mbottom">
			This page lists the sources of funding for our research<br><br><br>
			
			<br>
			
			</p>
			
		</aside>
		
		<section class="grid col-three-quarters mq2-col-full">
		
			
			
			<div class="grid-wrap">
			
		
			<article id="navtogg" class="grid col-full">
				<br><br><br>
				<a target="_blank" href="http://www.sfi.ie">Science Foundation Ireland</a> Starting Investigator Research Grant (SIRG) grant<br>
				<b>Amount:</b> €463,797.00<br >
				<b>Duration:</b> 4 years (September 2014- September 2018)<br >
			
		
			
			</article>
			
			<article id="navtabs" class="grid col-full">
			</article>
			
			</div> <!-- 100%articles-->
		
		</section>	
		
		
		
	</div> <!--main-->
