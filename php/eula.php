

<ul style="list-style:none; margin-left: 40px;">
	<li><b>Use of the ProViz Web service</b><br></li>
	<li><b>1.</b>  The developers of ProViz requires attribution for its service.</li>
	<li><b>2.</b>  Any use of the ProViz is non-confidential.</li>
	<li><b>3.</b>  Users of ProViz Online agree not to attempt to use any part of the UCD computers, files or networks apart from through the service interfaces provided.</li>
	<li><b>4.</b>  The developers of ProViz does not guarantee the accuracy of any data contained herein, nor the suitability of the ProViz web service for any purpose.</li>
	<li><b>5.</b>  If any single user exploits the ProViz web service to a level that prevents, or looks set to prevent, the developers of ProViz providing services to others, the developers of ProViz may discontinue service to that user. The developers of ProViz will then contact the user to discuss their needs and how (and if) these can be met.</li>
	<li><b>6.</b>  For planning and scientific review purposes, the developers of ProViz will keep records of usage. Logs of usage may also be maintained for the purposes of monitoring and improving services, and measuring the impact a particular service has on our resources.</li>
	<li><b>7.</b>  The developers of ProViz will make reasonable effort to maintain continuity of service and provide adequate warning of any changes or discontinuities. However, the developers of ProViz accepts no responsibility for the consequences of any temporary or permanent discontinuity in service.</li>
	<li><b>8.</b>  The developers of ProViz itself places no restrictions on the use or redistribution of the data available via the ProViz service. However, the original data may be subject to patent, copyright, or other intellectual property rights claimed by third parties. It is the responsibility of users of the service to ensure that their exploitation of the data does not infringe the rights of such third parties. The developers of ProViz can neither comment on the validity of their claims nor grant unrestricted permission regarding the use of the data.</li>
	<li><b>9.</b>  Any feedback on the service is welcome.</li>
	<li><b>10.</b> The developers of ProViz is not liable to you or third parties claiming through you, for any loss or damage.</li>
	<li><b>11.</b> The developers of ProViz reserves the right to alter these terms at any time in order to ensure their appropriateness to the ProViz mission.</li>
</ul>
