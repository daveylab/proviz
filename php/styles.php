<!--build:css css/style_homepage.min.css-->
<link rel="stylesheet" href="./css/main.css"/>
<link rel="stylesheet" href="./css/common.css"/>
<!--endbuild-->
<!--build:css css/libraries.min.css-->
<link rel="stylesheet" href="./bower_components/font-awesome/css/font-awesome.css"/>
<link rel="stylesheet" href="./bower_components/jquery-ui/themes/base/jquery-ui.css"/>
<link rel="stylesheet" href="./bower_components/bootstrap-tour/build/css/bootstrap-tour-standalone.min.css"/>
<!--endbuild-->

<?php if ($page == 'proviz'): ?>
    <!--build:css css/style_proviz.min.css-->
    <link rel="stylesheet" href="./css/selector.css"/>
    <link rel="stylesheet" href="./css/proviz.css"/>
    <!--endbuild-->
    <?php if ($parameters['print']): ?>
    <!--build:css css/style_proviz_print.min.css-->
    <link rel="stylesheet" href="./css/proviz_print.css"/>
    <!--endbuild-->
    <?php endif; ?>

<?php else: ?>

<?php endif; ?>
