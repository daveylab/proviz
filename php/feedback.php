<div class="about-page main grid-wrap">


    <aside class="grid col-one-quarter mq2-col-full">
        <h4>Contact & feedback</h4>

        <p class="mbottom">
        </p>
        <menu>
            <ul>

                <li><a href="#contact" class="arrow"> Contact</a></li>
                <li><a href="#bugtracker" class="arrow"> Bug tracker</a></li>
               <li><a href="# suggestions" class="arrow"> Suggestions</a></li>
            </ul>
        </menu>
    </aside>

    <section class="grid col-three-quarters mq2-col-full">
        <article>
            <article id="contact">
                <p class="headline">Contact</p>
                <p class="text">
                    You can contact us at: norman.davey <b>at</b> ucd.ie 
                </p>
            </article>
            <br>
            <article id="suggestions">
                <p class="headline">Suggestions</p>
                <p class="text">
                   If you have any suggestions about additions/enhancements to the ProViz tool or sources of data please suggest them <a
                        href="https://bitbucket.org/daveylab/proviz/issues?status=new">here</a>. We will try to add them. 
                </p>
                <br>
                <p class="headline">Bug tracker</p>
                <p class="text">
                    If you found a bug, a glitch or a typo please contact us or use the bug tracker available <a
                        href="https://bitbucket.org/daveylab/proviz/issues?status=new">here</a>. Create a new issue
                    there and we will try to solve it as soon as possible.
                </p>
            </article>
            <br>

        </article>
    </section>

</div> <!--main-->
