<!--build:js js/libraries.min.js -->
<script type="text/javascript" src="./bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="./bower_components/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="./bower_components/bootstrap-tour/build/js/bootstrap-tour-standalone.min.js"></script>
<!-- endbuild -->
<script>
    var bsTooltip = $.fn.tooltip.noConflict();
</script>
<!--build:js js/proviz.min.js -->
<script type="text/javascript" src="./js/js.cookie.js"></script>
<script type="text/javascript" src="./js/proviz.js"></script>
<script type="text/javascript" src="./js/interactive.js"></script>
<script type="text/javascript" src="./js/index.js"></script>
<script type="text/javascript" src="./js/tutorial.js"></script>
<!-- endbuild -->
