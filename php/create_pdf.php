<?php
// Adapted from https://github.com/microweber/screen
include_once '../config.php';
$cache_life = 60 * 60 * 24; //caching time, in seconds: 1day
//$cache_life = 0; //caching time, in seconds: 1day

if (!(isset($_GET['uniprot_acc']) || isset($_GET['jobId'])) || !isset($_GET['w']) || !isset($_GET['h']) || !isset($_GET['ali_start']) || !isset($_GET['ali_end'])) {
    header('Content-Type: text/plain');
    header("Content-disposition: attachment; filename=error.txt");
    print("ERROR: not enough args\n");
    var_dump($_GET);
    exit();
}

// gather parameters
$uniprot = filter_input(INPUT_GET, 'uniprot_acc', FILTER_SANITIZE_STRING);
$input_jobId = filter_input(INPUT_GET, "jobId", FILTER_SANITIZE_STRING);
$w = filter_input(INPUT_GET, 'w', FILTER_SANITIZE_NUMBER_INT);
$h = filter_input(INPUT_GET, 'h', FILTER_SANITIZE_NUMBER_INT);
$ali_start = filter_input(INPUT_GET, 'ali_start', FILTER_SANITIZE_NUMBER_INT);
$ali_end = filter_input(INPUT_GET, 'ali_end', FILTER_SANITIZE_NUMBER_INT);
$focus_start = filter_input(INPUT_GET, 'focus_start', FILTER_SANITIZE_NUMBER_INT);
$focus_end = filter_input(INPUT_GET, 'focus_end', FILTER_SANITIZE_NUMBER_INT);
$hideAln = urldecode(filter_input(INPUT_GET, "hideAln", FILTER_SANITIZE_STRING));
$showAln = urldecode(filter_input(INPUT_GET, "showAln", FILTER_SANITIZE_STRING));
$input_alignment = filter_input(INPUT_GET, "alignment", FILTER_SANITIZE_STRING);
$input_disable = urldecode(filter_input(INPUT_GET, "disable", FILTER_SANITIZE_STRING));
$input_genetree_mode = filter_input(INPUT_GET, "genetree_mode", FILTER_SANITIZE_STRING);
$input_search_regexp = filter_input(INPUT_GET, "regexp", FILTER_SANITIZE_STRING);
$input_gapped = filter_input(INPUT_GET, 'gapped', FILTER_SANITIZE_NUMBER_INT);
$input_condensed = filter_input(INPUT_GET, 'condensed', FILTER_SANITIZE_NUMBER_INT);


// directories & scripts
$here = dirname(__FILE__) . DIRECTORY_SEPARATOR;
$site_url = $config['path']. DIRECTORY_SEPARATOR . '..';
$bin_files = '/usr/bin/';
$jobs = $here . 'jobs' . DIRECTORY_SEPARATOR;
$cache = '/tmp' . DIRECTORY_SEPARATOR;
$js = $here . '..' . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR;
$job_file = $js . 'call_phantom.js';

// craft url
$root_url = "http://{$_SERVER['SERVER_NAME']}{$site_url}/proviz.php?";
$url = "{$root_url}" . http_build_query(array(
        'print' => 1,
        'uniprot_acc' => $uniprot,
        'jobId' => $input_jobId,
        'ali_start' => $ali_start,
        'ali_end' => $ali_end,
        'hideAln' => $hideAln,
        'showAln' => $showAln,
        'alignment' => $input_alignment,
        'disable' => $input_disable,
        'collapse' => $input_collapse,
        'gapped' => $input_gapped,
        'condensed' => $input_condensed,
        'focus_start' => $focus_start,
        'focus_end' => $focus_end,
        'genetree_mode' => $input_genetree_mode,
        'regexp' => $input_search_regexp

    ));


// error if the cache folder is not setup
if (!is_dir($cache)) {
    header('Content-Type: text/plain');
    header("Content-disposition: attachment; filename=error.txt");
    print("ERROR: create {$cache} and give the ownership to www-data");
    exit();
}


// cache name
$hash_options = md5(implode('-', [
    $uniprot, $ali_start, $ali_end, $focus_start, $focus_end, $input_search_regexp, $input_gapped, $input_genetree_mode, $input_alignment, $input_condensed, $input_jobId, $showAln
]));
$screen_file = "{$hash_options}.pdf";
$cache_job = $cache . $screen_file;


// check if the pdf was not already generated previously
function needToRefesh($file, $duration) {
    $filemtime = @filemtime($$file); // returns FALSE if file does not exist
    if (!$filemtime or (time() - $filemtime >= $duration)) {
        return true;
    } else {
        return false;
    }
}

// recreate the pdf
if (!is_file($cache_job) or needToRefesh($cache_job, $cache_life)) {
    $size = "{$w}*{$h}";
    $args = "{$job_file} '{$url}' {$cache_job} '{$size}'";
    $exec = "{$bin_files}phantomjs {$args}";
    print($exec);
    $result = exec($exec);

    if (is_file($here . $screen_file)) {
        rename($here . $screen_file, $cache_job);
    }
} else {
    $result = 'PDF cached';
}

if (isset($uniprot) && $uniprot != '') {
    $prefix_name = $uniprot;
} elseif (isset($input_jobId) && $input_jobId != '') {
    $prefix_name = $input_jobId;
} else {
    $prefix_name = 'ProVis';
}
$file_name = implode('-', [
        $prefix_name, $ali_start, $ali_end
    ]) . '.pdf';


// send the pdf (new or cached)
if (is_file($cache_job)) {
    header('Content-Type: application/pdf');
    header("Content-disposition: attachment; filename={$file_name}");
    readfile($cache_job);
} else {
    header('Content-Type: text/plain');
    header("Content-disposition: attachment; filename=error.txt");
    print("ERROR: error during the PDF generation '{$cache_job}' :: ");
    print($result);
    exit();
}






