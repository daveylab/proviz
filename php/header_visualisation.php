<div class="headerNameContainer">
        <div id="protein_name_container">
            <div id="protein-name"></div><a id="link-to-uniprot" data-tooltip="go to the UniProt page for the displayed protein" target="_blank" href="#">UniProt <i class="fa fa-external-link"></i></a>
            <div id="protein-species"></div>
        </div>

    <menu type="toolbar" id="quick-actions-buttons">
        <div id="select-aligns">
            <div class="alignment_options_picker_tag alignment_options_picker">Alignments:</div>
            <div class="alignment_options_picker_count alignment_options_picker"></div>
            <div class="alignment_options_picker_dropdown alignment_options_picker">
                <select class="btn-proviz" id="alignment_options_select" title="Select an alignment of homologues"></select>
            </div>
            <div class="clades_picker_dropdown clades_options_picker">
                <select class="btn-proviz" id="clades_options_select" title="Select a subalignment from the GeneTree data"></select>
            </div>
        </div>
        <div class="topOptionsContainer_button show_clades" data-tooltip="Split the alignment into clades">
            <i class="fa fa-chain-broken"></i></div>
        <div class="topOptionsContainer_button show_gaps" data-tooltip="View full alignment by showing gaps">
            <i class="fa fa-indent"></i></div>
        <div class="topOptionsContainer_button" id="search" data-tooltip="Search for a sequence or a motif with a regular expression">
            <i class="fa fa-search"></i></div>
        <div class="topOptionsContainer_button compactView" data-tooltip="Reduce the size of the visualisation by enabling the condensed view">
            <i class="fa fa-star"></i></div>
        <div class="topOptionsContainer_button" id="focus" data-tooltip="Highlight a region, extract sequences (fasta), resize visualisation">
            <i class="fa fa-expand"></i>
        </div>
        <div class="topOptionsContainer_button" id="add_track" data-tooltip="Add a new custom track by providing a xml, csv or json file">
            <i class="fa fa-plus"></i>
        </div>
        <div class="topOptionsContainer_button show_architecture" data-tooltip="Show protein architecture overview">
            <i class="fa fa-info-circle"></i></div>
        <div class="topOptionsContainer_button recolour" data-tooltip="Recolour alignment according to shown sequences">
            <i class="fa fa-paint-brush"></i></div>
        <div class="topOptionsContainer_button reset_view" data-tooltip="Reset visualisation"><i class="fa fa-refresh"></i>
        </div>
        <a class="topOptionsContainer_button download_pdf" data-tooltip="Download a pdf of the shown visualisation" ><i class="fa fa-file-pdf-o"></i></a>
        <div class="topOptionsContainer_button share_social dropdown">
            <i class="fa  fa-share" data-tooltip="Share"></i>
            <ul class="dropdown-content">
                <li><a href="javascript:shareTwitter()"><i class="fa fa-twitter"></i> Twitter</a></li>
                <li><a href="javascript:shareFacebook()"><i class="fa fa-facebook"></i> Facebook</a></li>
                <li><a href="javascript:shareReddit()"><i class="fa fa-reddit"></i> Reddit</a></li>
                <li><a href="javascript:shareGooglePlus()"><i class="fa fa-google-plus"></i> Google+</a></li>
                <li><a href="javascript:shareLinkedIn()"><i class="fa fa-linkedin"></i> LinkedIn</a></li>
                <li><a href="javascript:shareEmail()"><i class="fa fa-envelope"></i> Email</a></li>
            </ul>
        </div>
        <a class="topOptionsContainer_button" data-tooltip="Go back to the protein selection page" href="./index.php"><i class="fa fa-times home_button"></i></a>
    </menu>
</div>