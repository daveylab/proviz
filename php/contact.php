

	<div class="services-page main grid-wrap">

		<aside class="grid col-one-quarter mq2-col-full">
			<h4>Contact us</h4>
			
			<p class="mbottom">
			Feel free to contact us whether you have found a bug, have a suggestion about additions to the ProViz tool or you are interested in starting novel and exciting new SLiM-centric collaborations.<br><br><br>
			
			<br>
			
			</p>
			
		</aside>
		
		<section class="grid col-three-quarters mq2-col-full">
		
			
			
			<div class="grid-wrap">
			
			<article id="navbutton" class="grid col-full">
			</article>
		
			<article id="navtogg" class="grid col-full">
				<b><i>Suggestions and Bugs</i></b><hr>
				If you have found a bug or have a suggestions about additions to the ProViz tool please add them to the <a target="_blank" href="https://bitbucket.org/daveylab/proviz/issues?status=new">issue tracker</a>.
				<br><br>
				- or - <br><br>
				<i>Contact us at:</i> 
				norman.davey <b><i>at</b></i> ucd.ie / <a href="https://twitter.com/DaveyLab" target="_blank">@daveylab on Twitter</a><br >
				
			
			</article>
			
			<article id="navtabs" class="grid col-full">
			</article>
		
			</div> <!-- 100%articles-->
		
		</section>	
		
		
		
	</div> <!--main-->

