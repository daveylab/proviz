<div class="headerDataContainer topOptionsContainerContent">
</div>

<div class="headerDataContainer drop_zone">
    Drop or choose a file here! (see Help > use of custom data) <input type="file">
    <span class="drop_help_text"></span>
</div>

<div class="headerDataContainer" id="searchContainer">
    <form method="post" id="searchRegexp" style="position: relative;">
        <label for="search-regexp"><b>Pattern Search</b></label>
        <i data-tooltip="Search for a pattern (with a regular expression) in the protein sequence and in the alignment" class="fa fa-question-circle"></i>
                <span>
                    <input id="search-regexp" name="input" placeholder="a regular expression" size="20" type="search" class="btn-proviz" results="10" autosave="regexpSearch">
                    <button class="btn-proviz big-btn" id="searchregexpbtn" type="submit"><i class="fa fa-search"></i> search
                    </button>
                    <button class="btn-proviz" id="reset-searchregexp" type="reset">reset</button>
                    <span id="help-text-search-regexp"></span>
                </span>
    </form>
</div>

<div class="headerDataContainer" id="focusContainer">
    <b>Highlight a region</b>
    <label for="min">start</label> <input type="number" name="ali_start" id="min" class="btn-proviz" required>
    <label for="max">end</label> <input type="number" name="ali_end" id="max" class="btn-proviz" required>
    <button type="button" data-tooltip="Highlight a region of interest in the visualisation (shortcut: ctrl or ⌘ + left click on the query sequence or on a feature, bar, peptide...)" class="btn-proviz big-btn trigger_focus">
        <i class="fa fa-eye"></i> focus
    </button>
    <button type="button" class="btn-proviz big-btn trigger_resize" data-tooltip="Redraw the visualisation showing only the selected residue range"><i class="fa fa-arrows-h"></i> resize
    </button>
    <button type="button" data-tooltip="Extract sequence/alignment of the selected region to FASTA" class="btn-proviz big-btn trigger_export">
        <i class="fa fa-file-text"></i> extract fasta
    </button>
    <button type="button" class="btn-proviz trigger_reset_slider" data-tooltip="reset everything">reset
    </button>
</div>

<div class="headerDataContainer" id="architectureContainer">
</div>

<div class="tableContainer">
</div>