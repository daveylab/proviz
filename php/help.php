<div class="about-page main grid-wrap">
    <aside class="grid col-one-quarter mq2-col-full">
        <div>
        <h4>Help</h4>
        <menu>
            <ul>
                <li><a href="#quick_start" class="arrow">Quick start</a></li>
                <li><a href="#input_options" class="arrow">Input options</a></li>
                    <li><a href="#protein_search" class="arrow subcategory">Protein search</a></li>
                    <li><a href="#custom_input" class="arrow subcategory">Custom input</a></li>
                    <li><a href="#url_input" class="arrow subcategory">URL input</a></li>
                <li><a href="#main_visualisation" class="arrow">Main visualisation</a></li>
                    <li><a href="#overview" class="arrow subcategory">Overview</a></li>
                    <li><a href="#alignment" class="arrow subcategory">Alignment</a></li>
                    <li><a href="#features" class="arrow subcategory">Features</a></li>
                    <li><a href="#tool_bar" class="arrow subcategory">Tool bar</a></li>
                    <li><a href="#sidebar" class="arrow subcategory">Sidebar</a></li>
                        <li><a href="#options_sidebar" class="arrow subsubcategory">Options sidebar</a></li>
                        <li><a href="#proteins_sidebar" class="arrow subsubcategory">Proteins sidebar</a></li>
                        <li><a href="#features_sidebar" class="arrow subsubcategory">Features sidebar</a></li>
                        <li><a href="#help_sidebar" class="arrow subsubcategory">Help sidebar</a></li>
                        <li><a href="#about_sidebar" class="arrow subsubcategory">About sidebar</a></li>
                <li><a href="#databases_programs" class="arrow">Databases and Programs</a></li>
                    <li><a href="#databases" class="arrow subcategory">Databases</a></li>
                    <li><a href="#predictions" class="arrow subcategory">Predictions</a></li>
                    <li><a href="#data_source" class="arrow subcategory">Data sources table</a></li>
                <li><a href="#pdf" class="arrow">PDF download</a></li>
                <li><a href="#url_options" class="arrow">URL options</a></li>
                    <li><a href="#url_table" class="arrow subcategory">URL options table</a></li>
                    <li><a href="#url_example" class="arrow subcategory">URL example</a></li>         
                <li><a href="#custom_data" class="arrow">Use of custom data</a></li>
                    <li><a href="#xml" class="arrow subcategory">XML</a></li>
                    <li><a href="#csv" class="arrow subcategory">CSV</a></li>
                    <li><a href="#csv_table" class="arrow subcategory">CSV options</a></li>
                    <li><a href="#json" class="arrow subcategory">JSON</a></li>
                    <li><a href="#json_table" class="arrow subcategory">JSON options</a></li>
                <li><a href="#legal" class="arrow">Legal</a></li>
            </ul>
        </menu>
        </div>
    </aside>

    <section class="grid col-three-quarters mq2-col-full">
        <article id="quick_start">
            <p style="font-size:20px; font-weight:700;">Quick introduction</p>
            <br>
            <p style="font-size:16px; font-weight:700;">Quick start</p><hr>
            <p id="quick_text" class="text">
                Enter a search term in the "Search for a protein" field and press search. Click the "view" button
                for the protein of interest.
            </p>
        </article>
        <article id="input_options">
        	<br>
            <p style="font-size:16px; font-weight:700;">Input options</p><hr style="border-top: 3px double #8c8b8b;">
            <p id="protein_search" class="headline">Protein search</p>
            <p class="text">
                The "Search" field is linked to the UniProt protein search engine (Fig.1). Use any term to search for the
                protein in the UniParc database (e.g. Cyclin-dependent kinase inhibitor 1B). To increase the relevance of
                retrieved UniParc entries additional terms, like species and gene name, can be added. If the retrieved list
                doesn't include the protein of interest, the UniProt accession (e.g. P46527) or UniProt ID (e.g. CDKN1B)
                can be used. After the search results are retrieved, select the protein of interest from the result list.
                Valid searches are for example:
				
				<ul style="list-style:circle inside; margin-left: 20px;">
					<li>CDKN1B</li>
					<li>Cyclin-dependent kinase inhibitor 1B</li>
					<li>Cyclin-dependent kinase inhibitor p27</li>
					<li>p27 human</li>
					<li>Kip1</li>
					<li>CDN1B_HUMAN</li>
					<li>P46527</li>
				</ul>
				
                <br><br>
                <img src="./img/input_search.png" alt="Input search" style="display:block;margin:auto;width: 320px">
                <span class="legend">
                    <br><b>Fig. 1.</b> ProViz protein search. (1) Protein search field to input search term. (2) Search button
                    to submit search request. (3) View buttons to create visualisation of shown protein.
                </span>
            </p>
            <p id="custom_input" class="headline">Custom input</p>
            <p class="text">
                The "Custom alignment" input option allows for entering a plain text or uploading a file. These have
                to be in FASTA format and can be either one single protein or a multiple sequence alignment. Here
                the first sequence is used as the query sequence.
                <br><br>
                <img src="./img/input_custom.png" alt="Custom alignment" style="display:block;margin:auto;width: 320px">
                <span class="legend">
                    <b>Fig. 2.</b> ProViz custom alignment input. (1) Text field to input protein sequence or multiple
                    sequence alignment in FASTA format. (2) Submit button to create the ProViz visualisation with the
                    alignment provided. (3) File uploader to provide a protein sequence or multiple sequence alignment
                    by uploading a FASTA file. 
                </span>
            </p>
            <p id="url_input" class="headline">URL input</p>
            <p class="text">
                To access the main visualisation directly a set of URL options can be used. An extensive list is
                available in the "URL options" section.
            </p>
        </article>
        <article id="main_visualisation" style="width: 100%">	
        	<br>
            <p style="font-size:16px; font-weight:700;">Main visualisation</p><hr style="border-top: 3px double #8c8b8b;">
            <p id="overview" class="headline">Overview</p>
            <br>
            <img src="./img/main_vis.png" alt="Main visualisation" style="width:100%">
            <span class="legend">
                <b>Fig. 3.</b> ProViz main visualisation. ProViz visualisation for Cyclin-dependent kinase inhibitor 1A
                (CDKN1A) showing selected features of CDKN1A and a GeneTree alignment of CDKN1A orthologues. Key aspects
                of the visualisation are numbered: (1) Protein name and species; (2) options sidebar; (3) data information
                sidebar; (4) data select, hide and help buttons; (5) information hover tooltip; (6) options toolbar; (7)
                protein architecture overview; (8) protein sequence data; (9) protein feature data. The visualisation in
                the example can be viewed here: <a href="./proviz/proviz.php?uniprot_acc=P38936">Example</a><br><br>
            </span>
            
            <p id="alignment" class="headline">Alignment</p>
            <p class="text">
                The alignment section of the main visualisation shows the query sequence and if available a
                homologue alignment from Quest for Orthologues or GeneTree. It shows the name of the species on
                the left panel with links to UniProt or Ensemble. The right side displays the alignment coloured
                in the ClustalX scheme.
            </p>
            <p id="features" class="headline">Features</p>
            <p class="text">
                The feature section shows tracks containing information associated with the query protein. These
                tracks are represented in rows which are grouped by type of data shown, with the left part
                describing the type of data shown and the right showing the data in one of three available formats.
                Features mapping to a continuous segment of the query protein (e.g. domains or transmembrane regions)
                are displayed as horizontal bars spanning the corresponding residues. Peptide tracks are similar to
                bar tracks, but display amino acids aligned with the corresponding residue in the query protein.
                Histogram tracks display quantitative data for the protein on a residue by residue basis. Data is
                displayed as vertical bars corresponding to the value given to the residue.
            </p>
            <p id="tool_bar" class="headline">Tool bar</p>
            <p class="text">On the top left the name of the protein and gene together with the species is displayed.
                On the right hand side the user can choose between different alignments. The buttons right of the
                alignment selector activate panel for additional information like uncollapsing the alignment,
                searching for residues by regular expression, showing the compact view, highlighting areas of
                interest, protein architecture overview and recoloring of the alignment. There are reset and home
                buttons at the end.
            </p>
            <p id="sidebar" class="headline">Sidebar</p>
            <span style="float:left; width: 100%"><hr>
                <div style="float:right;text-align:center;width:30%">
                    <img src="./img/options_sidebar.png" alt="Options sidebar" height="270px">
                    <span class="legend">
                        <b>Fig. 4.</b> Options sidebar. Six sections are available, (1) Session, (2) Sequence, (3) Architecture,
                        (4) Selection, (5) Add new tracks and (6) Download.
                    </span>
                    <br>
                </div>
                <p id="options_sidebar" class="text" style="width: 60%; font-style: italic;">Options sidebar</p>
                <p class="text" style="width: 60%">
                    The options panel containes options to modify different aspects of the main visualisation.
                    The session section allows the user to create or reset a session and download a PDF of the
                    visualisation. In the sequence section contains the comapt view option. The architecture
                    overview can be enabled in the architecture section. The selection area containes controls
                    for the slider, focus and resize options. The add new track section allows the user to add a
                    custom feature file by drag/drop or file upload. Download options for the sequence data is
                    available in the download section.
                </p>
            </span>
            <span style="float:left; width: 100%"><hr>
                <div style="float:right;text-align:center;width:30%">
                    <img src="./img/proteins_sidebar.png" alt="Proteins sidebar" height="153px">
                    <span class="legend">
                        <b>Fig. 5.</b> Proteins sidebar. (1) Show all proteins. (2) Hide all proteins. (3) List of hidden proteins
                        with button to restore them.
                    </span>
                    <br>
                </div>
                <p id="proteins_sidebar" class="text" style="width: 60%; font-style: italic;">Proteins sidebar</p>
                <p class="text" style="width: 60%">
                    The protein tab of the sidebar contains a list of all hidden proteins and allows the user
                    to restore each individually or hide and show all sequences at once.
                </p>
            </span>
            <span style="float:left; width: 100%"><hr>
                <div style="float:right;text-align:center;width:30%">
                    <img src="./img/features_sidebar.png" alt="Features sidebar" height="284px">
                    <span class="legend">
                        <b>Fig. 6.</b> Features sidebar. (1) Show all features. (2) Hide all features. (3) Show and hide individual
                        feature groups. (4) Show and hide individual features.
                    </span>
                    <br>
                </div>
                <p id="features_sidebar" class="text" style="width: 60%; font-style: italic;">Features sidebar</p>
                <p class="text" style="width: 60%">
                    The features tab of the sidebar contains all available features and feature groups. Each feature
                    or feature group can be hidden or shown by the toggle buttons next to it. The show and hide all
                    buttons will switch all features on or off.
                </p>
            </span>
            <span style="float:left; width: 100%"><hr>
                <div style="float:right;text-align:center;width:30%">
                    <img src="./img/help_sidebar.png" alt="Help sidebar" height="152px">
                    <span class="legend">
                        <b>Fig. 7.</b> Help sidebar. Shows the quick introduction.
                    </span>
                    <br>
                </div>
                <p id="help_sidebar" class="text" style="width: 60%; font-style: italic;">Help sidebar</p>
                <p class="text" style="width: 60%">
                    The help tab of the sidebar displays the help for the main visualisation.
                </p>
            </span>
            <span style="float:left; width: 100%"><hr>
                <div style="float:right;text-align:center;width:30%">
                    <img src="./img/about_sidebar.png" alt="About sidebar" height="97px">
                    <span class="legend">
                        <b>Fig. 8.</b> About sidebar. ProViz description
                    </span>
                    <br>
                </div>
                <p id="about_sidebar" class="text" style="width: 60%;font-style: italic;">About sidebar</p>
                <p class="text" style="width: 60%">
                    The about tab of the options sidebar contains the about section describing ProViz and key
                    features.
                </p>
            </span>
        </article>
        <article id="databases_programs" style="clear:both">
        	<br><br><br>
            <p id="database_title"  style="font-size:16px; font-weight:700;">Databases and programs</p><hr style="border-top: 3px double #8c8b8b;">
            <p id="databases" class="headline">Databases</p>
            <p class="text">ProViz utilises many databases to give the user a wide range of information about
                the protein of interest. Multiple sequence alignments are retrieved from Quest for Orthologues
                and GeneTree. Protein modularity data are provided by ELM, Pfam and Phospho.ELM. For structural
                information, PDB, DSSP and homology models from SWISS-MODEL are used. Genomics data are retrived 
                from DbSNP, 1000 genomes and isoforms from UniProt. Additional curated data are available from
                UniProt and Switches.ELM.
            </p>
            <p id="predictions" class="headline">Predictions</p>
            <p class="text">
                ProViz includes data from various predictive programs and databases, like conservation, ELM,
                MobiDB, IUPred, PsiPred and Anchor.
                <br><br><i><p id="data_source">Data sources table</p></i>
            <table style="font-size:90%;">
                <thead>
                    <tr>
                        <td><b>Name</b></td>
                        <td><b>Description</b></td>
                        <td><b>PMID</b></td>
                        <td><b>URL</b></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><i><u>Multiple sequence alignments</u></i></td>
                    </tr>
                    <tr>
                        <td>GeneTree</td>
                        <td>Homo/Para/orthologue alignments and gene duplication information</td>
                        <td><a href="http://www.ncbi.nlm.nih.gov/pubmed/19029536">19029536</a></td>
                        <td><a href="http://www.ensembl.org">www.ensembl.org</a></td>
                    </tr>
                    <tr>
                        <td>GOPHER</td>
                        <td>Orthologue alignments by reciprocal best hit</td>
                        <td><a href="http://www.ncbi.nlm.nih.gov/pubmed/17576682">17576682</a></td>
                        <td><a href="http://bioware.ucd.ie">bioware.ucd.ie</a></td>
                    </tr>
                    <tr>
                        <td>Quest for orthologues</td>
                        <td>Datasets of homologous genes</td>
                        <td><a href="http://www.ncbi.nlm.nih.gov/pubmed/18819722">18819722</a></td>
                        <td><a href="http://questfororthologs.org">questfororthologs.org</a></td>
                    </tr>
                    <tr>
                        <td><i><u>Protein modularity</u></i></td>
                    </tr>
                    <tr>
                        <td>ELM</td>
                        <td>Manually curated linear motifs</td>
                        <td><a href="http://www.ncbi.nlm.nih.gov/pubmed/26615199">26615199</a></td>
                        <td><a href="http://elm.eu.org">elm.eu.org</a></td>
                    </tr>
                    <tr>
                        <td>Pfam</td>
                        <td>Functional regions and binding domains</td>
                        <td><a href="http://www.ncbi.nlm.nih.gov/pubmed/24288371">24288371</a></td>
                        <td><a href="http://pfam.xfam.org">pfam.xfam.org</a></td>
                    </tr>
                    <tr>
                        <td>Phospho.ELM</td>
                        <td>Experimentally verified phosphorylation sites</td>
                        <td><a href="http://www.ncbi.nlm.nih.gov/pubmed/21062810">21062810</a></td>
                        <td><a href="http://phospho.elm.eu.org">phospho.elm.eu.org</a></td>
                    </tr>
                    <tr>
                        <td><i><u>Structural information</u></i></td>
                    </tr>
                    <tr>
                        <td>PDB</td>
                        <td>Experimentally resolved protein tertiary structures</td>
                        <td><a href="http://www.ncbi.nlm.nih.gov/pubmed/10592235">10592235</a></td>
                        <td><a href="http://www.rcsb.org/pdb/home/home.do">www.rcsb.org</a></td>
                    </tr>
                    <tr>
                        <td>DSSP</td>
                        <td>Secondary structure derived from PDB tertiary structures</td>
                        <td><a href="http://www.ncbi.nlm.nih.gov/pubmed/25352545">25352545</a></td>
                        <td><a href="http://swift.cmbi.ru.nl/gv/dssp/"</a>swift.cmbi.ru.nl/gv/dssp</td>
                    </tr>
                    <tr>
                        <td>Homology models/ SWISS-MODEL</td>
                        <td>Assigned tertiary structure by sequence similarity to resolved structure</td>
                        <td><a href="http://www.ncbi.nlm.nih.gov/pubmed/24782522">24782522</a></td>
                        <td><a href="http://swissmodel.expasy.org/">swissmodel.expasy.org</a></td>
                    </tr>
                    <tr>
                        <td><i><u>Genomic data</u></i></td>
                    </tr>
                    <tr>
                        <td>DbSNP</td>
                        <td>Single-nucleotide polymorphism with disease association and genotype information</td>
                        <td><a href="http://www.ncbi.nlm.nih.gov/pubmed/11125122">11125122</a></td>
                        <td><a href="http://www.ncbi.nlm.nih.gov/SNP/">www.ncbi.nlm.nih.gov/SNP</a></td>
                    </tr>
                    <tr>
                        <td>1000 genomes</td>
                        <td>Single-nucleotide polymorphism</td>
                        <td><a href="http://www.ncbi.nlm.nih.gov/pubmed/23128226">23128226</a></td>
                        <td><a href="http://www.1000genomes.org/">www.1000genomes.org</a></td>
                    </tr>
                    <tr>
                        <td>Isoforms</td>
                        <td>Alternative splicing</td>
                        <td><a href="http://www.ncbi.nlm.nih.gov/pubmed/25348405">25348405</a></td>
                        <td><a href="http://www.uniprot.org/">www.uniprot.org</a></td>
                    </tr>
                    <tr>
                        <td><i><u>Additional curated data</u></i></td>
                    </tr>
                    <tr>
                        <td>Mutagenesis</td>
                        <td>Experimentally validated point mutations and effect</td>
                        <td><a href="http://www.ncbi.nlm.nih.gov/pubmed/25348405">25348405</a></td>
                        <td><a href="http://www.uniprot.org/">www.uniprot.org</a></td>
                    </tr>
                    <tr>
                        <td>Regions of interest</td>
                        <td>Experimentally validated functional areas</td>
                        <td><a href="http://www.ncbi.nlm.nih.gov/pubmed/25348405">25348405</a></td>
                        <td><a href="http://www.uniprot.org/">www.uniprot.org</a></td>
                    </tr>
                    <tr>
                        <td>Switches.ELM</td>
                        <td>Experimentally validated motif-based molecular switches</td>
                        <td><a href="http://www.ncbi.nlm.nih.gov/pubmed/23550212">23550212</a></td>
                        <td><a href="http://switches.elm.eu.org/">switches.elm.eu.org</a></td>
                    </tr>
                    <tr>
                        <td><i><u>Prediction</u></i></td>
                    </tr>
                    <tr>
                        <td>MobiDB</td>
                        <td>Collection of various disorder prediction methods</td>
                        <td><a href="http://www.ncbi.nlm.nih.gov/pubmed/25361972">25361972</a></td>
                        <td><a href="http://mobidb.bio.unipd.it/">mobidb.bio.unipd.it</a></td>
                    </tr>
                    <tr>
                        <td>IUPred</td>
                        <td>Intrinsically disordered regions</td>
                        <td><a href="http://www.ncbi.nlm.nih.gov/pubmed/15769473">15769473</a></td>
                        <td><a href="http://iupred.enzim.hu/">iupred.enzim.hu</a></td>
                    </tr>
                    <tr>
                        <td>PsiPred</td>
                        <td>Secondary structure for human proteins</td>
                        <td><a href="http://www.ncbi.nlm.nih.gov/pubmed/23748958">23748958</a></td>
                        <td><a href="http://bioinf.cs.ucl.ac.uk/psipred/">bioinf.cs.ucl.ac.uk/psipred</a></td>
                    </tr>
                    <tr>
                        <td>Anchor</td>
                        <td>Binding sites in disordered regions</td>
                        <td><a href="http://www.ncbi.nlm.nih.gov/pubmed/19412530">19412530</a></td>
                        <td><a href="http://anchor.enzim.hu/">anchor.enzim.hu</a></td>
                    </tr>
                    <tr>
                        <td>ELM</td>
                        <td>Linear motifs by regular expression</td>
                        <td><a href="http://www.ncbi.nlm.nih.gov/pubmed/26615199">26615199</a></td>
                        <td><a href="http://elm.eu.org/">elm.eu.org</a></td>
                    </tr>
                    <tr>
                        <td>Conservation</td>
                        <td>Conservation of residues across the alignment</td>
                        <td><a href="http://www.ncbi.nlm.nih.gov/pubmed/22977176">22977176</a></td>
                        <td><a href="http://bioware.ucd.ie">bioware.ucd.ie</a></td>
                    </tr>
                </tbody>
            </table>
        </article>
        <article id="pdf">
        	<br>
            <p style="font-size:16px; font-weight:700;">PDF download</p><hr style="border-top: 3px double #8c8b8b;">
            <p class="text">
                The PDF download is available by buttons in the options sidebar or the toolbar on the top right. This creates a file
                download containing a PDF document of the ProViz visualisation. 
            </p>
        </article>
        <article id="url_options">
        	<br>
            <p style="font-size:16px; font-weight:700;">URL options</p><hr style="border-top: 3px double #8c8b8b;">
            <p class="text">
                Users can construct URLs to access and customise protein visualisations using the URL options below:
                <br><br><i><p id="url_table">URL options table</p></i>
            <table style="font-size:90%;">
            <thead>
                <tr>
                    <td><b>URL option</b></td>
                    <td><b>Description</b></td>
                    <td><b>Input type</b></td>
                    <td><b>Example</b></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>uniprot_acc</td>
                    <td>UniProt accession of the protein to be visualised</td>
                    <td>string: UniProt accession</td>
                    <td><a href="./proviz.php?uniprot_acc=P46527">uniprot_acc=P46527</a></td>
                </tr>
                <tr>
                    <td>alignment</td>
                    <td>Type of homology alignment to be displayed</td>
                    <td>string: QFO,[TaxonID]</td>
                    <td><a href="./proviz.php?uniprot_acc=P46527&alignment=Metazoa">alignment=Metazoa</a></td>
                </tr>
                <tr>
                    <td>ali_start</td>
                    <td>Starting residue for the scope of the alignment</td>
                    <td>integer > 0</td>
                    <td><a href="./proviz.php?uniprot_acc=P46527&ali_start=10">ali_start=10</a></td>
                </tr>
                <tr>
                    <td>ali_end</td>
                    <td>Ending residue for the scope of the alignment. Will be set to protein length, if greater
                        than protein length.</td>
                    <td>integer > 0</td>
                    <td><a href="./proviz.php?uniprot_acc=P46527&ali_end=20">ali_end=20</a></td>
                </tr>
                <tr>
                    <td>disable</td>
                    <td>Disables feature tracks by providing the names of features. This prevents loading of data
                        for mentioned features, they can’t be activated without reload of the page.</td>
                    <td>string: motif, elm, modification, phospho, mutagenesis, pfam, structure, PDB, homology,
                        splice_variant, SNP, chain, dna_binding, region, metal_binding, site, cross_link, iupred</td>
                    <td><a href="./proviz.php?uniprot_acc=P46527&disable=motif,modification,SNP">disable=motif,modification,SNP</a></td>
                </tr>
                <tr>
                    <td>collapse</td>
                    <td>Collapses feature groups by providing the names of feature groups. Features are loaded
                        and hidden, but can be activated in the options panel.</td>
                    <td>string: alignment, switch, motif, modification, mutation, structure, PDB, isoform, snp,
                        feature, disorder</td>
                    <td><a href="./proviz.php?uniprot_acc=P46527&collapse=alignment,PDB">collapse=alignment,PDB</a></td>
                </tr>
                <tr>
                    <td>hideAln</td>
                    <td>Hide proteins by providing accessions separated by commas</td>
                    <td>string: UniProt accession"</td>
                    <td><a href="./proviz.php?uniprot_acc=P46527&hideAln=H2Q5H2,F6Z4R0">hideAln=H2Q5H2,F6Z4RO</a></td>
                </tr>
                <tr>
                    <td>showAln</td>
                    <td>Show proteins by providing accessions separated by commas. All other sequences will be
                        hidden</td>
                    <td>string: UniProt accession</td>
                    <td><a href="./proviz.php?uniprot_acc=P46527&showAln=H2Q5H2,F6Z4R0">showAln=H2Q5H2,F6Z4RO</td>
                </tr>
                <tr>
                    <td>genetree_mode</td>
                    <td>If alignment is set to GeneTree select paralog, ortholog or all</td>
                    <td>string: paralog, ortholog, all</td>
                    <td><a href="./proviz.php?uniprot_acc=P12931&alignment=GeneTree&genetree_mode=paralog">genetree_mode=paralog</a></td>
                </tr>
                <tr>
                    <td>url_rest</td>
                    <td>Providing a URL pointing to a custom track file will load the visualisation for the custom
                        data automatically</td>
                    <td>string: file URL</td>
                    <td><a href="./proviz.php?uniprot_acc=P46527&rest_url=http://slim.ucd.ie/proviz/help/custom_track/track.xml">rest_url=http://slim.ucd.ie/proviz/help/custom_track/track.xml</a></td>
                </tr>
            </tbody>
            </table>
            
            <br>
            <p id="url_example" class="headline">URL example</p>
                <a href="./proviz.php?uniprot_acc=P46527&disable=PDB&ali_start=140&ali_send=198">
                    http://slim.ucd.ie/proviz/proviz.php?uniprot_acc=P46527&alignment=33208&disable=PDB&ali_start=140&ali_send=198</a><br>
                    Shows the region of p27 between residues 140 and 198, and turns off the PDB track.
            </p>
        </article>
        <article id="custom_data">
        	<br>
            <p style="font-size:16px; font-weight:700;">Use of custom data</p><hr style="border-top: 3px double #8c8b8b;">
            <p class="text">
                Users are able to add custom data to any existing ProViz visualisation. This can be achieved by
                providing a file in either XML, CSV or JSON format by drag and drop, file upload or link to a
                server providing the file via REST service.
            </p>
            <p id="xml" class="headline">XML</p>
            <p class="text">
                The XML file has to start and end with the "tracks" tag. Each track starts and ends with the
                "track" tag and has the mandatory option "type" (feature, peptide, histogram) and accepts the
                options "name", "type", "position", "colour" and "opacity". The "track" tag accepts multiple
                "entry" tags. The "entry" tag requires the "start" and "end" options for type feature or peptide
                or "position" option for histogram, but also accepts "text" for feature, "value" for histogram
                and "sequence for peptide, "hover", "link", "text_colour", "colour" and "opacity".
                 An example of the format is available at:
                 <a href="./help/custom_track/track.xml" class="btn-proviz" download="track.xml">XML</a>
            	<br><br><i>XML schema</i>
            <p class="text">
                An XML schema can be used to design and validate a ProViz readable XML file and is available for
                download here: 
                <a href="./help/custom_track/XML_schema.xsd" class="btn-proviz" download="XML_schame.xsd">XML schema</a>
            </p><hr>
            <p id="csv" class="headline">CSV</p>
            <p class="text">
                The CSV file consists of two parts, the header line and the data lines which both have to be
                comma separated. The header line specifies the available fields and is mandatory, but the fields
                can be in any order. The data lines contain the data to be visualised and each line represents
                one element called entry. Fields starting with "t_" are track fields and only have to be defined
                once and will automatically be applied to all other lines with the same track number. Each track
                requires the "track", "t_type" and "t_position" fields, all other track fields are optional. The
                remaining fields define the shown element (entry fields). Required entry fields are "entry",
                either "start" and "end" or "position" as well as one of "text", "value" and "sequence".
                An example of the format is available at:
                <a href="./help/custom_track/track.csv" class="btn-proviz" download="track.csv">CSV</a>
                <br><br><i><p id="csv_table">CSV options</p></i>
            <p class="text">
                <table  style="font-size:90%;">
                    <thead>
                        <tr>
                            <td><b>Field name</b></td>
                            <td><b>Description</b></td>
                            <td><b>Input type</b></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>track</td>
                            <td>Numbering of tracks</td>
                            <td>integer > 0</td>
                        </tr>
                        <tr>
                            <td>t_name</td>
                            <td>Track name</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>t_type</td>
                            <td>Track type</td>
                            <td>"string: feature, peptide, histogram"</td>
                        </tr>
                        <tr>
                            <td>t_position</td>
                            <td>Track position, -1 for track display above the main sequence and alignment, 1 for track display above the main sequence and alignment</td>
                            <td>"integer: -1 / 1"</td>
                        </tr>
                        <tr>
                            <td>t_colour</td>
                            <td>Default colour of all elements in the given track</td>
                            <td>"string: #000000 - #FFFFFF"</td>
                        </tr>
                        <tr>
                            <td>t_opacity</td>
                            <td>Default opacity of all elements in the given track</td>
                            <td>"double: 0 - 1"</td>
                        </tr>
                        <tr>
                            <td>t_text_colour</td>
                            <td>Default colour of the text of all elements in the given track</td>
                            <td>"string: #000000 - #FFFFFF"</td>
                        </tr>
                        <tr>
                            <td>t_help</td>
                            <td>Tracks help tooltip</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>entry</td>
                            <td>Numbering of entries</td>
                            <td>integer > 0</td>
                        </tr>
                        <tr>
                            <td>text</td>
                            <td>Entries displayed text. For feature tracks only.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>value</td>
                            <td>Value determining the height of the bars in a histogram. For histogram tracks only.</td>
                            <td>double</td>
                        </tr>
                        <tr>
                            <td>sequence</td>
                            <td>A peptides sequence. For peptide tracks only.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>hover</td>
                            <td>Tooltip for given entry</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>link</td>
                            <td>Link activated by clicking the given entry</td>
                            <td>"string: URL"</td>
                        </tr>
                        <tr>
                            <td>start</td>
                            <td>Start position of entry. Only for feature and peptide tracks. Needs end value</td>
                            <td>integer > 0</td>
                        </tr>
                        <tr>
                            <td>end</td>
                            <td>End position of entry. Only for feature and peptide tracks. Needs start value.</td>
                            <td>integer > 0</td>
                        </tr>
                        <tr>
                            <td>position</td>
                            <td>Position of entry. Only for histogram tracks.</td>
                            <td>integer > 0</td>
                        </tr>
                        <tr>
                            <td>colour</td>
                            <td>Color of given entry. Overrides “t_colour” property.</td>
                            <td>"string: #000000 - #FFFFFF"</td>
                        </tr>
                        <tr>
                            <td>opacity</td>
                            <td>Opacity of given entry. Overrides “t_opacity” property</td>
                            <td>"double: 0 - 1"</td>
                        </tr>
                        <tr>
                            <td>text_colour</td>
                            <td>Text colour of given entry. Overrides “t_text_colour” property</td>
                            <td>"string: #000000 - #FFFFFF"</td>
                        </tr>
                    </tbody>
                </table>
            </p><hr>
            <p id="json" class="headline">JSON</p>
            <p class="text">
                The JSON file contains a standard JSON object. The object is an array filled with one dictionary
                per track. These dictionaries contain the "type", "position", "name",  "colour", "opacity",
                "text_colour" variables which represent the default values for all entries in the track and the
                "data" array filled with one dictionary per entry. The entry dictionary provides the variables
                "start", "end", "text", "link", "colour", "opacity", "hover" and "text_colour" to customise the
                entry further.
                An example of the format is available at:
                <a href="./help/custom_track/track.json" class="btn-proviz" download="track.json">JSON</a>
                <br><br><i><p id="json_table">JSON options</p></i>
            <p class="text">
                <table  style="font-size:90%;">
                    <thead>
                        <tr>
                            <td><b>Field name</b></td>
                            <td><b>Description</b></td>
                            <td><b>Input type</b></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>name</td>
                            <td>Track name</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>type</td>
                            <td>Track type</td>
                            <td>"string: feature, peptide, histogram"</td>
                        </tr>
                        <tr>
                            <td>position</td>
                            <td>Track position, -1 for track display above the main sequence and alignment, 1 for track display above the main sequence and alignment</td>
                            <td>"integer: -1 / 1"</td>
                        </tr>
                        <tr>
                            <td>colour</td>
                            <td>Default colour of all elements in the given track</td>
                            <td>"string: #000000 - #FFFFFF"</td>
                        </tr>
                        <tr>
                            <td>opacity</td>
                            <td>Default opacity of all elements in the given track</td>
                            <td>"double: 0 - 1"</td>
                        </tr>
                        <tr>
                            <td>text_colour</td>
                            <td>Default colour of the text of all elements in the given track</td>
                            <td>"string: #000000 - #FFFFFF"</td>
                        </tr>
                        <tr>
                            <td>help</td>
                            <td>Tracks help tooltip</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>text</td>
                            <td>Entries displayed text. For feature tracks only.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>value</td>
                            <td>Value determining the height of the bars in a histogram. For histogram tracks only.</td>
                            <td>double</td>
                        </tr>
                        <tr>
                            <td>sequence</td>
                            <td>A peptides sequence. For peptide tracks only.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>hover</td>
                            <td>Tooltip for given entry</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>link</td>
                            <td>Link activated by clicking the given entry</td>
                            <td>"string: URL"</td>
                        </tr>
                        <tr>
                            <td>start</td>
                            <td>Start position of entry. Only for feature and peptide tracks. Needs end value</td>
                            <td>integer > 0</td>
                        </tr>
                        <tr>
                            <td>end</td>
                            <td>End position of entry. Only for feature and peptide tracks. Needs start value.</td>
                            <td>integer > 0</td>
                        </tr>
                        <tr>
                            <td>position</td>
                            <td>Position of entry. Only for histogram tracks.</td>
                            <td>integer > 0</td>
                        </tr>
                        <tr>
                            <td>colour</td>
                            <td>Color of given entry. Overrides “t_colour” property.</td>
                            <td>"string: #000000 - #FFFFFF"</td>
                        </tr>
                        <tr>
                            <td>opacity</td>
                            <td>Opacity of given entry. Overrides “t_opacity” property</td>
                            <td>"double: 0 - 1"</td>
                        </tr>
                        <tr>
                            <td>text_colour</td>
                            <td>Text colour of given entry. Overrides “t_text_colour” property</td>
                            <td>"string: #000000 - #FFFFFF"</td>
                        </tr>
                    </tbody>
                </table>
         </article>
         <article id="legal">
                <br>
                  <p style="font-size:16px; font-weight:700;">Legal</p><hr style="border-top: 3px double #8c8b8b;">
                  
                  The ProViz tool is available for use according to the <a href="./index.php?page=eula">ProViz end-user license agreement (EULA)</a>.<br><br>
        </article>
    </section>
</div> <!--main-->
