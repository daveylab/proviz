<div class="sidebarContainer">
    <div class="sidebarContent">
        <div class="sidebar_header">
            <div class="sidebar_title">
            </div>
            <div class="sidebarClose">
                <i class="fa fa-times"></i>
            </div>
        </div>
        <div class="sidebar_text">
        </div>
        <div class="sidebarProteinsContent" style="display: none"></div>
    </div>
    <div class="sidebarOverhang">
        <div class="sidebar_tab" id="Options">
            <div class="sidebar_tab_icon">Options</div>
        </div>

        <div class="sidebar_tab" id="Proteins">
            <div class="sidebar_tab_icon"><span class="protein_count"><b>0</b></span> Proteins</div>
        </div>

        <div class="sidebar_tab" id="Features">
            <div class="sidebar_tab_icon">Features</div>
        </div>

        <div class="sidebar_tab" id="Help">
            <div class="sidebar_tab_icon">Help</div>
        </div>

        <div class="sidebar_tab" id="About">
            <div class="sidebar_tab_icon">About</div>
        </div>
    </div>
</div>