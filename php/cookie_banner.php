<?php if (!isset($_COOKIE["proviz_comply_cookie"]) && !(isset($_SERVER['HTTP_DNT']) && $_SERVER['HTTP_DNT'] == 1)): ?>
    <style>
        #cookies {
            display: none;
        }

        #cookies a {
            font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;;
            color: rgb(255, 255, 250);
            text-decoration: underline;
        }

        #cookies a:hover {
            color: rgb(220, 220, 220);
        }

        #cookies .cookie-accept {
            cursor: pointer;
            display: inline;
            color: rgb(255, 255, 250);
            text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.35);
            font-weight: bold;
            text-decoration: none;
        }

        #cookies .cookie-accept:hover {
            text-decoration: underline;
        }

        #cookies .cookie-not-accept {
            /*padding-left: 10px;*/
            cursor: pointer;
            display: inline;
            color: rgb(155, 155, 152);
            text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.35);
            text-decoration: none;
        }

        #cookies .cookie-not-accept:hover {
            color: rgb(199, 199, 199);
            text-decoration: underline;
        }
    </style>
    <div id="cookies" class="warning_header">
        This website uses cookies. By continuing to browse this site, you are agreeing to the use of our site cookies. To find out more, see our <a href="./index.php?page=cookies">cookie policy</a>.
            <span class="cookie-accept">ok</span> <span class="cookie-not-accept">no</span>
    </div>
    <script>
        //compliance with the EU and Irish law about cookies
        function comply () {
            $('.cookie-not-accept').unbind('click', nocomply);
            $('html, body').unbind('click', comply);
            var days = 365;
            cookie_compliance = true;
            Cookies.set("proviz_comply_cookie", "comply_yes", {expires: days});
            if (typeof PROVIZ != "undefined" && PROVIZ.protein_acc != 'undefined' && typeof PROVIZ.data != 'undefined') {
                PROVIZ.createCookie('proviz_p_' + PROVIZ.protein_acc, PROVIZ.data); // Setup cookies for the protein
            }
            $('#cookies').slideUp("slow");
            loadGA();
        }

        function nocomply () {
            $('.cookie-not-accept').unbind('click', nocomply);
            $('html, body').unbind('click', comply).unbind('scroll', comply);
            cookie_compliance = false;
            $('#cookies').slideUp("slow");

        }
            </script>
<?php elseif($_COOKIE["proviz_comply_cookie"] == 'comply_yes'): ?>
    <script>
        var cookie_compliance = true;
    </script>
<?php else: ?>
    <script>
        var cookie_compliance = false;
    </script>
<?php endif ?>