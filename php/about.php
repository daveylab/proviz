
	<div class="about-page main grid-wrap">


		<aside class="grid col-one-quarter mq2-col-full">
			<h4>About</h4>

			<p class="mbottom">
			</p>
			<menu>
				<ul>

					<li><a href="#proviz" class="arrow"> Proviz</a></li>
					<li><a href="#funding" class="arrow"> Funding</a></li>
					<li><a href="#reference" class="arrow"> Reference</a></li>
					<li><a href="#git" class="arrow"> Git repository</a></li>
					<li><a href="#license" class="arrow"> License</a></li>
					<li><a href="#eula" class="arrow"> EULA</a></li>
					<li><a href="#cookies" class="arrow"> Cookie policy</a></li>

				</ul>
			</menu>
		</aside>

		<section class="grid col-three-quarters mq2-col-full">
			<p style="font-size:16px; font-weight:700;">ProViz</p>
			<article>
					<article id="proviz">
					<p id="input_text" class="text">
					ProViz is an interactive protein exploration tool, which searches several databases
					for information about a given query protein. Data relevant to the protein like an
					alignment of homologues, linear motifs, post translational modifications, domains,
					secondary structure, sequence variations and others are graphically represented
					relative to their position in the protein. The tool is developed by the <a href="http://slim.ucd.ie">Davey Lab</a> in
					University College Dublin by Peter Jehl, Jean Manguy and Norman Davey.
					</p>
					</article>
				<br>
				<article id="funding">
					<p id="input_title" class="headline">Funding</p>
					<p id="input_text" class="text">
						The project is funded by a <a target="_blank" href="http://www.sfi.ie">Science Foundation Ireland</a> Starting Investigator Research Grant (SIRG) grant.

					</article>

				<br>
				<article id="reference">
					<p id="input_title" class="headline">Reference</p>
                    Peter Jehl, Jean Manguy, Denis C. Shields, Desmond G. Higgins, and Norman E. Davey  <b>"ProViz&nbsp;&#151;&nbsp;a web-based visualization tool to investigate the functional and evolutionary features of protein sequences."</b> Nucl. Acids Res. <i>April 16, 2016</i> <a target="_blank" href="https://dx.doi.org/10.1093/nar/gkw265" title="ProViz">doi:10.1093/nar/gkw265</a>
					</article>
				<br>
				<article id="git">
					<p class="headline">Download</p>
					<p>
						The ProViz tool has been uploaded to the Bitbucket Git repository and is available for download <a href="https://bitbucket.org/daveylab/proviz/" target="_blank">here<a>. If you have any additions please feel free to contact us about the possibility of incorporating it into the codebase. Please look into the <a href="./index.php?page=help#url_options">URL-based custom track functionality</a> as this offers many of the required functions without any need to alter the codebase. We are also very happy to add any interesting functionality required please make any suggestions <a href="./index.php?page=feedback">here</a>.
					</p>
				</article>
				<br>

				<article id="license">
					<p class="headline">License</p>
					<p>
						The ProViz tool is available for download and use under a GNU General Public License. It allows the users (individuals, organizations, companies) to run, study, share (copy), and modify the software. However, it requires that when distributing derived works, the source code of the work must be made available under the same license. For the full license please see the <a target="_blank" href="http://www.gnu.org/licenses/gpl.txt"> GNU GPL Version 3</a>.
						<pre><small>
ProViz - protein visualisation tool
Copyright (C) 2016  Norman E. Davey, Peter Jehl, Jean Manguy

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.

Author contact: Norman E. Davey < normandavey@gmail.com >  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
Author contact: Peter Jehl < peter.jehl@ucdconnect.ie >  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
Author contact: Jean Manguy < jean.manguy@ucdconnect.ie >  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
					</small></pre>
					</p>
				</article>

				<article id="eula">
					<p class="headline">EULA</p>
					<p>
						The ProViz tool is available for use according to the <a href="./index.php?page=eula">ProViz
							end-user license agreement (EULA)</a>.<br><br>
					</p>
				</article>

				<article id="cookies">
					<p class="headline">Cookie policy</p>
					<p>
						The ProViz tool cookie policy is available <a href="./index.php?page=cookies">here</a>.<br><br>
					</p>
				</article>
				<br>
			</article>
		</section>

	</div> <!--main-->
