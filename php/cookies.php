<?php
header("Cache-Control: max-age=" . (6 * 30 * 24 * 60 * 60 * 1000)); // cache 6 months
?>
<div class="about-page main grid-wrap">
    <aside class="grid col-one-quarter mq2-col-full">
        <h4>Cookie policy</h4>
    </aside>
    <section class="grid col-three-quarters mq2-col-full">
        <h3>Cookies</h3>
        <p>Cookies are small files utilised by website to store information on the user's computer. More information is available in the corresponding
            <a href="http://en.wikipedia.org/wiki/HTTP_cookie">Wikipedia page</a>.</p>

        <h4>Why we use cookies ?</h4>
        <p>Two different kind of cookies are utilised by ProViz. Fisrt, we use cookies to remember which proteins you already viewed so we can propose you on the main page to view it again. We also gather statistics about the uses of ProViz with the help of Google Analytics which uses cookies.</p>
        <p>We do not store or gather personal information about our users.</p>
        <h3>Cookies stored</h3>
        <h4>ProViz cookies</h4>
        <dl>
            <dt><dfn>proviz_p_*</dfn></dt>
            <dd>Cookies starting with this prefix are utilised to stored information about your recent visited proteins.<br>Expiration: 1 year.</dd>
            <dt><dfn>proviz_comply_cookie</dfn></dt>
            <dd>The role of this cookie is to remember that you choose to accept cookies.<br>Expiration: 1 year.</dd>
        </dl>
        <h4>Third party cookies</h4>
        <p>We are using Google Analytics to gather statistics about the uses of ProViz.
        <dl>
            <dt><dfn>_ga</dfn></dt>
            <dd>The role of this cookie is to count the users and to distinguish the new users from the returning users.<br>Expiration: 2 years</dd>
            <dt><dfn>_gat</dfn></dt>
            <dd>The role of this cookie is to survey the frequency of requests.<br>Expiration: 10 minutes.</dd>
        </dl>
        More information about Google Analytics cookies can be found <a href="https://developers.google.com/analytics/devguides/collection/gajs/cookie-usage" title="Google analytics cookies">here</a>.</p>
        <h3>Opt-out</h3>
        <p>
            No cookie will be stored on your computer by us if you don't use the website, you can choose to refuse cookies by clicking on the "no" button of the banner, but as we are not using cookies to remember your choice the banner will still appear on every page.
        </p>
        <h4>Do not track</h4>
        <p>You can configurate your browser to send the <a title="Do Not Track - wikipedia page" href="https://en.wikipedia.org/wiki/Do_Not_Track">Do Not Track </a> header. We will respect your decision and we won't ask you to store any cookies.</p>
        <h4>Disable cookies</h4>
        <p>If you don't want any cookie at all you can disable the use of cookies in the settings of your web browsers. This may break websites.
        </p>
        <h4>Disable Google Analytics cookies</h4>
        <p>
            You can use this <a href="http://tools.google.com/dlpage/gaoptout?hl=None">extension</a> to opt-out from Google Analytics on every website.
        </p>
    </section>
</div>


