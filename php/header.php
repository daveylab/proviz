<div class="header-div">
    <header id="navtop">
        <a href="index.php" class="logo fleft" title="back to the homepage">
            <img src="./img/logo.png" alt="ProViz logo" class="logo"> </a>
        <nav class="fright">
            <ul>
                <li><a id="help" class="page_chooser" href="./index.php?page=help">Help</a></i></li>
            </ul>
            <ul>
                <li><a id="about" class="page_chooser" href="./index.php?page=about">About</a></i></li>
            </ul>
            <ul>
                <li>
                    <a id="start-tutorial" class="page_chooser" href="./index.php?page=home&tutorial=1">Start
                        tutorial</a>
                </li>
            </ul>
        </nav>
    </header>
    <hr>
</div>