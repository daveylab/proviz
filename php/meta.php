<?php

//
//     ProViz - protein visualisation tool
//     Copyright (C) 2016  Norman E. Davey, Peter Jehl, Jean Manguy
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//     Author contact: Norman E. Davey <normandavey@gmail.com>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Peter Jehl <peter.jehl@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Jean Manguy <jean.manguy@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//

# from name 'firstname N. familyname' to 'familyname, firstname N.'
function formatDCname($name) {
    $names = explode(' ', $name, 3);
    if (count($names) == 2) {
        return implode(', ', array_reverse($names));
    } elseif (count($names) == 3) {
        return $names[2] . ', ' . $names[0] . ' ' . $names[1];
    } else {
        return $name;
    }
}
?>
<title><?php echo (isset($title)) ? $title : ''; ?></title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="HandheldFriendly" content="true"/>
    <meta name=viewport width=device-width>
    <meta name="MobileOptimized" content="320"/>
    <link rel="icon" type="image/x-icon" href="./favicon.ico" />
    <meta name="description" content="<?php echo (isset($config['description'])) ? $config['description'] : ''; ?>"/>
    <meta name="keywords" content="<?php echo (isset($config['keywords'])) ? implode(', ', $config['keywords']) : ''; ?>"/>
    <meta name="language" content="<?php echo (isset($config['language'])) ? $config['language'] : ''; ?>"/>
    <meta name="robots" content="index,follow"/>
    <meta name="author" content="<?php echo (isset($config['authors'])) ? implode(', ', $config['authors']) : ''; ?>"/>
    <meta name='subject' content='<?php echo (isset($config['subject'])) ? $config['subject'] : ''; ?>'>
    <meta name='Classification' content='<?php echo (isset($config['classification'])) ? $config['classification'] : ''; ?>'>
    <meta name='revised' content='<?php echo (isset($config['last update'])) ? date('c', $config['last update']) : ''; ?>'>
    <link rel="canonical" href="<?php echo (isset($canon_url)) ? $canon_url : ''; ?>" />

    <!-- Dublin core meta tags (zotero) -->
    <link rel="schema.DC" href="http://purl.org/DC/elements/1.1/"/>
    <meta name="DC.Title" content="<?php echo (isset($title)) ? $title : ''; ?>">
    <?php foreach($config['authors'] as $author): ?>
    <meta name="DC.Creator" content="<?php echo formatDCname($author) ?>">
    <?php endforeach; ?>
    <meta name="DC.Subject" content="<?php echo (isset($config['subject'])) ? $config['subject'] : ''; ?>">
    <meta name="DC.Description" content="<?php echo (isset($config['description'])) ? $config['description'] : ''; ?>">
    <meta name="DC.Publisher" content="<?php echo (isset($config['publisher'])) ? $config['publisher'] : ''; ?>">
    <meta name="DC.Date" content="<?php echo (isset($config['last update'])) ? date('c', $config['last update']) : ''; ?>">
    <meta name="DC.Type" content="Service">
    <meta name="DC.Language" content="<?php echo (isset($config['language'])) ? $config['language'] : ''; ?>">
    <meta name="DC.Coverage" content="world"/>
    <meta name="DC.Format" content="text/HTML"/>

    <!-- Opengraph (FB) tags -->
    <meta property="og:type" content="website">
    <meta property="og:image" content="<?php echo (isset($config['link image'])) ? $config['link image'] : ''; ?>">
    <meta property="og:locale" content="<?php echo (isset($config['language'])) ? str_replace('-', '_', $config['language']) : ''; ?>">
    <meta property="og:title" content="<?php echo (isset($title)) ? $title : ''; ?>">
    <meta property="og:description" content="<?php echo (isset($config['description'])) ? $config['description'] : ''; ?>">
    <meta property="og:url" content="<?php echo (isset($canon_url)) ? $canon_url : ''; ?>">
    <meta property="og:site_name" content="<?php echo (isset($config['name'])) ? $config['name'] : ''; ?>">
    <meta property="og:og:see_also" content="<?php echo (isset($config['link doi paper'])) ? $config['link doi paper'] : ''; ?>">
    <meta property="og:updated_time" content="<?php echo (isset($config['last update'])) ? date('c', $config['last update']) : ''; ?>">

    <!-- Twitter tags-->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="<?php echo (isset($title)) ? $title : ''; ?>">
    <meta name="twitter:image" content="<?php echo (isset($config['link image'])) ? $config['link image'] : ''; ?>">
    <meta name="twitter:description" content="<?php echo (isset($config['description'])) ? $config['description'] : ''; ?>">
    <meta name="twitter:site" content="<?php echo (isset($config['handle twitter'])) ? $config['handle twitter'] : ''; ?>">
