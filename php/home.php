<div class="home-page main grid-wrap">
    <section class="grid col-two-thirds mq2-col-full">
        <div class="grid-wrap">

            <article id="navtogg" class="grid col-full">
                <div class="tabs">
                    <nav>
                        <a href="#tab-search" class="tab-menu tab-menu-active">Search</a><a href="#tab-custom-align" class="tab-menu tab-menu-small">Custom
                            alignment</a>
                    </nav>
                    <div class="tab tab-active" id="tab-search">
                        <label for="searchTerm" style="float: left">Search for a protein</label>
                        <div class="content">
                            <span class="input_examples">
                                <a id="protein_load">Load example</a>
                                |
                                <a id="protein_clear">Clear</a>
                                 |
                                <a id="protein_example_load"><i class="fa fa-question-circle"></i> help</a>
                            </span>
                            <hr style="clear: both">

                            <form action="javascript:INDEX.searchTerm()" id="form-search">
                                <input id="searchTerm" placeholder="a protein name, a gene name or a UniProt accession number"  results="10" autosave="searchUniProt" name="searchUniProt" type="search" autocorrect="off" autocapitalize="off" spellcheck="false">
                                <!--                        <a class="button" id="search">search</a> -->
                                <input id="launch-search" type="submit" class="button" value="search"><br><br>
                            </form>
                            <span id="spinner" style="display: none">Searching <i class="fa fa-cog fa-3 fa-spin"></i></span>
                            <div id="searchResults">
                            </div>
                            <div class="input_help" id="search_input_help" style="display: none;">
                                <h4>Help</h4>
                                <p>Add a search term such as a protein name, gene name or accession. If the
                                    protein/species of interest is not present in the list return please add species of
                                    interest or a different synonym for the protein. Alternatively, search directly on
                                    <a target="_blank" href="http://www.uniprot.org/">UniProt</a> and use the UniProt
                                    accession.
                                </p>
                                <p>
                                    <i>For example:</i>
                                <ul style="list-style: circle" id="examples_search">
                                    <li>CDKN1B</li>
                                    <li>Cyclin-dependent kinase inhibitor 1B</li>
                                    <li>Cyclin-dependent kinase inhibitor p27</li>
                                    <li>p27 human</li>
                                    <li>Kip1</li>
                                    <li>CDN1B_HUMAN</li>
                                    <li>P46527</li>
                                </ul>
                                </p>
                            </div>
                            <div class="input_error" id="search_input_error"></div>
                        </div>
                    </div>
                    <div class="tab" id="tab-custom-align">
                        <label for="alignment_input" style="float: left">Paste a protein/alignment</label>
                        <div class="content">
                        <span class="input_examples">
                            <a id="alignment_load">Load example</a>
                            |
                            <a id="alignment_clear">Clear</a>
                            |
                            <a id="alignment_example_load"><i class="fa fa-question-circle"></i> help</a>
                            </span>
                            <hr style="clear: both">
                            <form action="javascript:INDEX.submitAlignmentTextarea()">
                                <textarea id="alignment_input" rows="6" autocorrect="off" autocapitalize="off" spellcheck="false" placeholder="a protein sequence or a alignment, in fasta format"></textarea>
                                <input type="submit" class="button"  id="alignment_input_button" value="submit">
                            </form>

<!--                            <a class="button" id="alignment_input_button">submit</a>-->
                            <div class="input_help" id="alignment_input_help" style="display: none">
                                <h4>Help</h4>
                                Paste one protein sequence or an alignment of proteins, in fasta format.
                            </div>
                            <div class="input_error" id="alignment_input_error">
                            </div>
                            <br>
                            <label for="alignment_file_input_button" style="float: left">Upload protein/alignment
                                file</label>
                            <span class="input_examples"><a id="file_example_load"><i class="fa fa-question-circle"></i>
                                    help</a></span>
                            <hr style="clear: both">
                            <input id="alignment_file_input_button" type="file">
                            <div class="input_error" id="alignment_file_input_error">
                            </div>

                            <div class="input_help" id="file_input_help" style="display: none">
                                <h4>Help</h4>
                                Choose a fasta file containing one protein sequence or an alignment of proteins.
                            </div>
                            <div class="input_error" id="jobError"></div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
    <aside class="grid col-one-third mq2-col-full">
        <div id="about_text">
            <h4>About</h4>
            <p>
                <medium>
                    ProViz (<b>Pro</b>tein <b>Viz</b>ualisation) is an interactive exploration tool for investigating
                    the functional and evolutionary features of proteins. The tool is designed to be an intuitive and
                    accessible resource to allow users with limited bioinformatic skills to rapidly access and visualise
                    data pertinent to their research.
                </medium>
            </p>

            <p>
                <medium style="text-align: left;">
                    <i> Reference:</i> <br>
                    <a target="_blank" href="https://dx.doi.org/10.1093/nar/gkw265" title="ProViz" style="font-family: Helvetica; color: #4179B5; text-decoration:none; font:Helvetica Neue;">"ProViz&nbsp;&#151;&nbsp;a web-based visualization tool to investigate the functional and evolutionary features of protein sequences."</a><br>
                    Peter Jehl, Jean Manguy, Denis C. Shields, Desmond G. Higgins, and Norman E. Davey<br>
                    <small>Nucl. Acids Res. <i>April 16, 2016</i> doi:10.1093/nar/gkw265</small>
                <medium>
                <br><br>
            </p>
        </div>
        <div id="visited">
        </div>
    </aside>
    <script>

    </script>
</div>