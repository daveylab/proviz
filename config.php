<?php
    date_default_timezone_set('GMT');
    $config = array(
        'name' => 'ProViz',
        'description' => 'ProViz is an interactive protein exploration tool, which searches several databases
    for information about a given query protein. Data relevant to the protein like an
    alignment of homologues, linear motifs, post translational modifications, domains,
    secondary structure, sequence variations and others are graphically represented
    relative to their position in the protein.',
        'keywords' => array('protein visualisation', 'functional and evolutionary features', 'exploration tool for proteins', 'uniprot', 'bioinformatics', 'computational biology', 'biology', 'evolution'),
        'language' =>'en-IE', 'authors' => array('Peter Jehl', 'Jean Manguy', 'Denis C. Shields', 'Desmond G. Higgins', 'Norman E. Davey'),
        'publisher' => 'Daveylab',
        'subject' => 'ProViz—a web-based visualization tool to investigate the functional and evolutionary features of protein sequences',
        'classification' => '25',
        'last update' => mktime(20, 20, 00, 04, 20, 2016),
        'link doi paper' => 'https://dx.doi.org/10.1093/nar/gkw265',
        'handle twitter' => 'ProViz_UCD',
        'link image' => 'http://proviz.ucd.ie/img/screenshot_p53_P04637.gif',
        'url project' => 'http://proviz.ucd.ie/',
        'path' => dirname($_SERVER['SCRIPT_NAME'])
    );

