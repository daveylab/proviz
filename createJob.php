<?php

//     
//     ProViz - protein visualisation tool
//     Copyright (C) 2016  Norman E. Davey, Peter Jehl, Jean Manguy
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//     Author contact: Norman E. Davey <normandavey@gmail.com>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Peter Jehl <peter.jehl@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//     Author contact: Jean Manguy <jean.manguy@ucdconnect.ie>  Conway Institute, University College Dublin, Belfield, Dublin 4, Ireland.
//
  
include "./rest/util.php";

$user_path = "./rest/data/user_alignment/";
$user_alignment = "";
if(filter_has_var(INPUT_POST, "user_alignment")){
    $user_alignment = strip_tags(filter_input(INPUT_POST, "user_alignment", FILTER_SANITIZE_STRING));
    $job_id = md5($user_alignment);
    if(!file_exists($user_path)){
        mkdir($user_path);
    }
    if(!file_exists($user_path . $job_id . ".aln")){
        fileSaver($user_path . $job_id . ".aln", $user_alignment);
    }
    echo json_encode($job_id);
}
?>